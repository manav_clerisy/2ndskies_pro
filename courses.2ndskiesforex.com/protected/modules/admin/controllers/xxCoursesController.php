<?php

class CoursesController extends AdminController
{
    /**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Courses;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Courses']))
		{
			$model->attributes=$_POST['Courses'];
                        $model->name = strip_tags($model->name);
                        
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Courses']))
		{
			$model->attributes=$_POST['Courses'];
                        $model->name = strip_tags($model->name);
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model = $this->loadModel($id);

            // course that has subscribed user couldn't bew deleted
            if ($model->couldBeDeleted())
                $model->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
    {
		$model=new Courses('search');
                
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Courses']))
			$model->attributes=$_GET['Courses'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

    /**
     * Renders page for sorting announce topics.
     * @param integer $id course id
     */
    public function actionSortTopics($id)
    {
        $course = $this->loadModel($id);

        $this->render('sort-topics',array(
            'course'=>$course,
        ));
    }

    /**
     * Ajax request handler for saving topics order.
     */
    public function actionSaveTopicsOrder()
    {
        $request = Yii::app()->request;
        $ids = $request->getPost('ids');
        
        foreach ($ids as $order => $id) {
            Topic::model()->updateByPk($id, array('order' => min($order, 255)));
        }
    }
    
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
     * @return Courses
	 */
	public function loadModel($id)
	{
		$model=Courses::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}
    
	/**
	 * @param integer the ID of the model to be loaded
     * @return Sections
	 */
	public function loadSection($id)
	{
		$model = Sections::model()->findByPk($id);
		if ($model===null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='courses-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}



    public function actionGetSections()
    {
        $id = Yii::app()->request->getParam('id', null);
        if (empty($id))
            throw new CHttpException(404,'The requested page does not exist.');

        $course = Courses::model()->findByPk($id);
        if (empty($course))
            throw new CHttpException(404,'The requested page does not exist.');

        if (isset($_POST['Courses']['section_tree'])) {
            $tree = CJSON::decode($_POST['Courses']['section_tree']);
            $course->saveSectionTree($tree);
            // delete unnecessary sections
            $course->deleteSections();

            Yii::app()->user->setFlash('success', 'Changes were saved!');
        }

        $this->render('sections/list', array(
            'course' => $course,
        ));
    }

}


