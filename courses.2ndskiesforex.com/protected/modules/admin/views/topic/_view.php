<div class="well well-small">
	<div class="topic-heading">
		<h2 class="topic-title"><?php echo CHtml::encode($data->subject); ?></h2>
	</div>
	<ul class="unstyled">
		<li>
			<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
			<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
		</li>
		<li>
			<b><?php echo CHtml::encode($data->getAttributeLabel('section_id')); ?>:</b>
			<?php echo CHtml::encode($data->section_id); ?>
		</li>
		<li>
			<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
			<?php echo CHtml::encode($data->user_id); ?>
		</li>
		<li>
			<b><?php echo CHtml::encode($data->getAttributeLabel('date_created')); ?>:</b>
			<?php echo CHtml::encode($data->date_created); ?>
		</li>
		<li>
			<b><?php echo CHtml::encode($data->getAttributeLabel('date_changed')); ?>:</b>
			<?php echo CHtml::encode($data->date_changed); ?>
		</li>
		<li>
			<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
			<?php echo CHtml::encode($data->status); ?>
		</li>
	</ul>

</div>