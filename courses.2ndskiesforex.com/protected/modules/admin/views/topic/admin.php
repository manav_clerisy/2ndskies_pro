<?php
$this->breadcrumbs=array(
	'Topics'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Topic','url'=>array('index')),
	array('label'=>'Create Topic','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggleClass('hidden');
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('topic-grid', {
		data: $(this).serialize()
	});
});
");
?>

<h1 class="content-title"><?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn-success btn-small btn pull-right', 'data-toggle'=>'button')); ?>Manage Topics</h1>

<p>You may optionally enter a comparison operator <code><b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> or <b>=</b></code> at the beginning of each of your search values to specify how the comparison should be done.</p>


<div class="search-form hidden">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'topic-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'section_id',
		'user_id',
		'subject',
		'date_created',
		'date_changed',
        array(
            'name' => 'Image',
            'value' => '(!empty($data->imageUrl)) ? CHtml::image($data->imageUrl,"",array("width"=>"200", "height"=>"170")) : $data->image',
            'type' => 'html',
            'sortable' => false,
            'filter' => false,

        ),
		/*
		'status',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
