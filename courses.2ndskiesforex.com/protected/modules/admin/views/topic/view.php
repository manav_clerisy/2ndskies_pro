<?php
$this->breadcrumbs=array(
	'Topics'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Topic','url'=>array('index')),
	array('label'=>'Create Topic','url'=>array('create')),
	array('label'=>'Update Topic','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Topic','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Topic','url'=>array('admin')),
);
?>

<h1 class="content-title">View Topic #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'section_id',
		'user_id',
		'subject',
		'date_created',
		'date_changed',
		'status',
        array(
            'name' => 'thumb',
            'type' => 'html',
            'value' => CHtml::image($model->imageUrl),
        ),
	),
)); ?>
