<?php
$this->breadcrumbs=array(
	'Topics',
);

$this->menu=array(
	array('label'=>'Create Topic','url'=>array('create')),
	array('label'=>'Manage Topic','url'=>array('admin')),
);
?>

<h1 class="content-title">Topics</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
