<?php
$this->breadcrumbs=array(
	'Sections'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Sections','url'=>array('index')),
	array('label'=>'Create Sections','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('sections-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Sections</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'sections-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,

	'columns'=>array(
		'id',
		'name',
		'parent_id',
		'order',
		'course_id',
		array(
            'template'=>'{view} {update} {delete} {addTopic}',
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'buttons'=>array(
                'addTopic' => array(
                    'label'=>'',
                    'imageUrl'=>false,
                    'url'=>'Yii::app()->createUrl("/topic/create", array("section_id" => $data->id))',
                    'options' => array('class' => 'icon-folder-close', 'title' => 'Add Topic'),
                ),
            )
		),
	),
)); ?>
