<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('section_id')); ?>:</b>
	<?php echo CHtml::encode($data->section_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('question')); ?>:</b>
	<?php echo CHtml::encode($data->question); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('answers')); ?>:</b>
	<?php echo CHtml::encode($data->answers); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('correct_answers')); ?>:</b>
	<?php echo CHtml::encode($data->correct_answers); ?>
	<br />


</div>