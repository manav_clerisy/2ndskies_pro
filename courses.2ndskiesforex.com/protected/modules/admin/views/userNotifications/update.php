<?php
$this->breadcrumbs=array(
	'User Notifications'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UserNotifications','url'=>array('index')),
	array('label'=>'Create UserNotifications','url'=>array('create')),
	array('label'=>'View UserNotifications','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage UserNotifications','url'=>array('admin')),
);
?>

<h1>Update UserNotifications <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>