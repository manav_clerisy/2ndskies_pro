<?php
$this->breadcrumbs=array(
	'User Notifications'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UserNotifications','url'=>array('index')),
	array('label'=>'Manage UserNotifications','url'=>array('admin')),
);
?>

<h1>Create UserNotifications</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>