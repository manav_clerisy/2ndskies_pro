<?php
$this->breadcrumbs=array(
	'User Notifications'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List UserNotifications','url'=>array('index')),
	array('label'=>'Create UserNotifications','url'=>array('create')),
	array('label'=>'Update UserNotifications','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete UserNotifications','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UserNotifications','url'=>array('admin')),
);
?>

<h1>View UserNotifications #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'notify',
	),
)); ?>
