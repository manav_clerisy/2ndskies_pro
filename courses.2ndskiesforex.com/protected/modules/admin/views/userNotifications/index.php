<?php
$this->breadcrumbs=array(
	'User Notifications',
);

$this->menu=array(
	array('label'=>'Create UserNotifications','url'=>array('create')),
	array('label'=>'Manage UserNotifications','url'=>array('admin')),
);
?>

<h1>User Notifications</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
