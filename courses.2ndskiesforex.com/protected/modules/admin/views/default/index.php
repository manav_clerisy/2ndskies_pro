<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	$this->module->id,
);




?>

<p>To view the users table follow this link: <br/>
    <a href="<?php echo $this->createUrl('/admin/user/manage')?>">Manage Users</a>
</p>

<p>To view the courses table follow this link: <br/>
    <a href="<?php echo Yii::app()->createUrl('/admin/courses')?>">Manage Courses</a>
</p>

<p>To view the subscriptions table follow this link: <br/>
    <a href="<?php echo Yii::app()->createUrl('/admin/userCourse/manage')?>">Manage Subscriptions to Courses</a>
</p>

<p>To view the topics table follow this link: <br/>
    <a href="<?php echo Yii::app()->createUrl('/admin/topic/admin')?>">Manage Topics</a>
</p>

<p>To view the comments table follow this link: <br/>
    <a href="<?php echo Yii::app()->createUrl('/admin/comment/admin')?>">Manage Comments</a>
</p>

<p>To view the badges table follow this link: <br/>
    <a href="<?php echo Yii::app()->createUrl('/admin/badges/manage')?>">Manage Badges</a>
</p>

<p>To view the economic events table follow this link: <br/>
    <a href="<?php echo Yii::app()->createUrl('/admin/economicEvents/manage')?>">Manage Economic Events</a>
</p>

<p>To view the discount codes table follow this link: <br/>
    <a href="<?php echo Yii::app()->createUrl('/admin/discountCode/admin')?>">Manage Discount Codes</a>
</p>

<p>To view the sections table follow this link: <br/>
    <a href="<?php echo Yii::app()->createUrl('/admin/sections/admin')?>">Manage Sections</a>
</p>

<p>To view the quiz table follow this link: <br/>
    <a href="<?php echo Yii::app()->createUrl('/admin/quiz/admin')?>">Manage Quiz</a>
</p>

<p>To view the subscriptions table follow this link: <br/>
    <a href="<?php echo Yii::app()->createUrl('/admin/subscriptions/admin')?>">Manage Subscriptions to Topics</a>
</p>
<p>To view the users log table follow this link: <br/>
    <a href="<?php echo Yii::app()->createUrl('/admin/user/userlog')?>">Manage User Logs</a>
</p>