<?php
$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    'courses' => array('/admin/courses'),
    'create',
);

$this->menu=array(
	array('label'=>'Manage Courses','url'=>array('/admin/courses')),
);
?>

<h1 class="content-title">Create Course</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>