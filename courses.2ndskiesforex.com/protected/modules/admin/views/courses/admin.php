<?php
$this->layout = '//layouts/column1';

$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    'courses'
);

Yii::app()->clientScript->registerScriptFile('/js/admin/courses/courses.js');

?>
<h1 class="content-title"><?php echo CHtml::link('Create Course',array('create'),array('class'=>'btn btn-success btn-small pull-right', 'data-toggle'=>'button')); ?>Manage Courses</h1>
<?php

$this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'courses-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model, // There are 3 courses, no need to filter them
	'columns'=>array(
		'id',
		'slug',
		'name',
		'price',
                'ontraportPid',
        array(
            'name' => 'is_calendar_public',
            'header'=>'Show calendar',
            'type'=>'raw',
            'value'=>'CHtml::link(
                ($data->is_calendar_public ? "Hide" : "Show"),
                "javascript:void(0);",
                 array(
                    "onclick" => "changeCalendarStatusForCourse(" . $data->id . "); return false;"
                )
            )'
        ),
        array(
            'name' => 'visible',
            'header'=>'Show course',
            'type'=>'raw',
            'value'=>'CHtml::link(
                ($data->visible ? "Hide" : "Show"),
                "javascript:void(0);",
                 array(
                    "onclick" => "changeVisibleStatusForCourse(" . $data->id . "); return false;"
                )
            )'
        ),
        array(
            'name'=>'amountOfSubscription',
            'header'=>'Subscriptions',
            'filter'=>false,
        ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view}&nbsp;{update}&nbsp;{delete}&nbsp;{sections}&nbsp;{topics}',
            'buttons'=>array(
                'delete' => array(
                    'visible' => '$data->couldBeDeleted()'
                ),
                'sections' => array(
                    'label'=>'',
                    'imageUrl'=>false,
                    'url'=>'Yii::app()->createUrl("/admin/courses/getsections", array("id" => $data->id))',
                    'options' => array('class' => 'icon-list', 'title' => 'Manage sections'),
                ),
                'topics' => array(
                    'label'=>'<i class="icon-sort-by-attributes"></i>',
                    'imageUrl'=>false,
                    'url' => 'Yii::app()->createUrl("/admin/courses/sortTopics", array("id" => $data->id))',
                    'options' => array('title' => 'Sort topics'),
                ),
            ),
            'htmlOptions' => array(
                'style' => 'min-width: 70px',
            ),
		),
	),
));
