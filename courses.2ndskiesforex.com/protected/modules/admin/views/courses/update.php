<?php
$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    'courses' => array('/admin/courses'),
    'update',
);

$this->menu=array(
	array('label'=>'Create Course','url'=>array('create')),
	array('label'=>'View Course','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Courses','url'=>array('index')),
);
?>

<h1>Update <?php echo $model->name; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>