<?php

/* @var $course Courses */

Yii::app()->clientScript
    ->registerPackage('jquery.ui')
    ->registerLocalScript('admin/courses/sort-topics.js', CClientScript::POS_END);

$this->breadcrumbs = array(
    'admin' => array('/admin/default/index'),
    'courses' => array('/admin/courses'),
    'sort topics',
);

$this->menu = array(
    array('label' => 'Create Course', 'url' => array('create')),
    array('label' => 'View Course', 'url' => array('view', 'id' => $course->id)),
    array('label' => 'Manage Courses', 'url' => array('index')),
); ?>

    <h1 class="content-title">Sort Topics</h1>

<?php
$sections = $course->findSectionsBy();

foreach ($sections as $section) {
    /* @var $section Sections */
    ?>
    <div class="section" data-id="<?php echo $section->id; ?>">

        <h2><?php echo $section->name; ?></h2>
        <?php if (($section->name == 'Course Lessons') || ($section->name == 'ATM v1') || ($section->name == 'ATM v2')) {

            $this->widget('bootstrap.widgets.TbGridView', array(
                'id' => 'lessons-grid',
                'summaryText'=>'',
                'dataProvider' => $section->getAnnouncesProvider(),
                //'filter'=>$model, // There are 3 courses, no need to filter them
                'columns' => array(
                    'subject',
                    array(
                        'name' => 'time_release',
                        'value' => function ($data) {
                            echo CHtml::textField("Topic[$data->id][time_release]", $data->time_release, array('class' => 'span1', 'maxlength' => 3));
                        }
                    ),
                    array(
                        'name' => 'discussion',
                        'value' => function ($data) {
                            echo CHtml::checkBox("Topic[$data->id][discussion]", $data->discussion);
                        }
                    ),
                ),
            ));
            } else { ?>
            <ul class="sortable">
                <?php $topics = $section->getAnnounces();
                foreach ($topics as $topic) { ?>
                    <li class="alert alert-info" data-id="<?php echo $topic->id;?>"><?php echo $topic->subject; ?></li>
                <?php } ?>
            </ul>

        <?php } ?>
        <button class="btn btn-primary disabled save" type="button">Save</button>
        <hr/>

    </div>
<?php
}
?>
