<?php
$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    'courses' => array('/admin/courses'),
    'view',
);

$this->menu=array(
	array('label'=>'Create Course','url'=>array('create')),
	array('label'=>'Update Course','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Course','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'), 'visible'=>$model->couldBeDeleted()),
	array('label'=>'Manage Courses','url'=>array('index')),
);
?>

<h1>View - <?php echo $model->name; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'slug',
		'name',
		'price',
                'ontraportPid',
	),
)); ?>
