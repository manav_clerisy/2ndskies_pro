<?php
/**
 *
 * @var CoursesController $this
 * @var Courses $course
 *
 */

$this->menu=array(
    array('label'=>'Manage Courses','url'=>array('index')),
);

$this->breadcrumbs=array(
    'Courses'=>array('index'),
    'Manage sections',
);

    Yii::app()->clientScript->registerPackage('jquery.dynatree');
    Yii::app()->clientScript->registerPackage('jquery.bootbox');
    Yii::app()->clientScript->registerScriptFile('/js/admin/sections/sections.js', CClientScript::POS_END);

    Yii::app()->clientScript->registerScript('sectionTree', "
       sectionTree = '".$course->sectionTree."';
    ", CClientScript::POS_HEAD);

?>
<h1><?=$course->name?></h1>

<div id="tree" >
    <ul class="dynatree-container1"></ul>
</div>

<p style='margin-top:10px;'>
    <?php $this->widget('bootstrap.widgets.TbLabel', array(
        'label'=>'Rename',
    )); ?>
    <input type="text" value="" id="activeNodeName" style="margin:0" placeholder="Name" disabled='true' maxlength="200"/>

    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Add new section',
        'type'=>'success', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        'htmlOptions' => array('id' => 'btnAddNode', 'style'=>'width:100px'),
    )); ?>

    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Remove',
        'type'=>'danger',
        'size'=>'small',
        'htmlOptions' => array('id' => 'btnRemoveNode', 'disabled' => 'true', 'style'=>'width:100px'),
    )); ?>

    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Save',
        'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'size'=>'small', // null, 'large', 'small' or 'mini'
        'htmlOptions' => array('id' => 'btnSaveTree', 'style'=>'width:100px')
    )); ?>

</p>