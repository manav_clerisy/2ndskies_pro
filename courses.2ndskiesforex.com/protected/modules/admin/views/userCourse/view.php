<?php
$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    'subscriptions' => array('/admin/userCourse/manage'),
    'view',
);

$this->menu=array(
	array('label'=>'Create Subscription','url'=>array('create')),
	array('label'=>'Update Subscription','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Subscription','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Subscriptions','url'=>array('manage')),
);
?>

<h1>View Subscription #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
        array(
            'name' => 'User',
            'value' => $model->userName,
        ),
        array(
            'name' => 'Course',
            'value' => $model->courseName,
        ),
        'paypal_id',
		'created',
        array(
            'name' => 'Payment Type',
            'value' => $model->paymentType,
        ),
	),
)); ?>
