<?php
$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    'subsctiptions' => array('/admin/userCourse/manage'),
    'update',
);

$this->menu=array(
	array('label'=>'Create Subscription','url'=>array('create')),
	array('label'=>'View Subscription','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Subscriptions','url'=>array('manage')),
);
?>

<h1>Update Subscription #<?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>