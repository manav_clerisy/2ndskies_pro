<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'type'=>'horizontal',
	'method'=>'get',

)); ?>

	<?php echo $form->textFieldRow($model,'id',array('maxlength'=>10)); ?>
	
	<div class="control-group">
	    <label class="control-label" for="userName">User</label>
	    <div class="controls">
	    	<?=CHtml::textField('userName', isset($_GET['userName']) ? $_GET['userName'] : '')?>
	    </div>
	</div>
	
	<div class="control-group">
	    <label class="control-label" for="courseName">Course</label>
	    <div class="controls">
		    <?=CHtml::textField('courseName', isset($_GET['courseName']) ? $_GET['courseName'] : '')?>
		</div>
	</div>

    <?=$form->textFieldRow($model,'price'); ?>

    <?=$form->textFieldRow($model,'paypal_id'); ?>

    <?=$form->dropDownList($model, 'payment_type', $model->getPaymentTypesArr()); ?>

	<div class="controls">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'icon'=>'search',
			'type'=>'success',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
