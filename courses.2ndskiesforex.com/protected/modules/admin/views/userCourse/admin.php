<?php
$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    'subscriptions'
);

$this->menu=array(
	array('label'=>'Create Subscription','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggleClass('hidden');
});	
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('user-grid', {
        data: $(this).serialize()
    });
});
");
?>

<h1 class="content-title"><?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn btn-success btn-small pull-right', 'data-toggle'=>'button')); ?>Manage Subscriptions</h1>

<div class="search-form hidden">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'user-course-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
        array(
            'name' => 'userName',
            'value' => '$data->userName',
            'filter'=>CHtml::textField('userName', isset($_GET['userName']) ? $_GET['userName'] : ''),
        ),
        array(
            'name' => 'courseName',
            'value' => '$data->courseName',
            'filter'=>CHtml::textField('courseName', isset($_GET['courseName']) ? $_GET['courseName'] : ''),
        ),
        'price',
        'paypal_id',
        array(
            'name' => 'Subscribed',
            'value' => '$data->created',
            'filter'=>false,
        ),
        array(
            'name' => 'Payment Type',
            'value' => '$data->paymentType',
            'filter'=>false,
        ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
