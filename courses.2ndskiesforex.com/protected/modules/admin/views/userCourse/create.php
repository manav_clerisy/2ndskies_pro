<?php
$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    'subscriptions' => array('/admin/userCourse/manage'),
    'create',
);

$this->menu=array(
	array('label'=>'Manage Subscriptions','url'=>array('manage')),
);
?>

<h1 class="content-title">Create Subscription</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>