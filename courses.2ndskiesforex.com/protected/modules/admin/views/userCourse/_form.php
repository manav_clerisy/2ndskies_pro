<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-course-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="control-group">
	    <label class="control-label required <?=$model->hasErrors('user_id')?'error':''?>" for="users">User <span class="required">*</span></label>
		<div class="controls">
		    <?php $this->widget('bootstrap.widgets.TbTypeahead', array(
		        'name'=>'users',
		        'options'=>array(
		            'source'=>'js:function(query, process){
		                getItems(query, process, "'.$this->createUrl('/user/user/getUsers').'")
		            }',
		            'items'=>10,
		            'highlighter' => 'js:function(item) {
		                var n=item.split("|");
		                return n[1];
		            }',
		            'matcher'=>"js:function(item) {
		                return ~item.toLowerCase().indexOf(this.query.toLowerCase());
		            }",
		            'updater'=>"js:function(item) {
		                var n=item.split('|');
		                $('#user_id').val(n[0]);
		                return n[1];
		            }"
		        ),
		        'htmlOptions' => array('class' => $model->hasErrors('user_id')?'error':'', 'autocomplete' => "off"),
		        'value' => $model->userName
		    )); ?>
		    <?php echo $form->hiddenField($model,'user_id',array('id'=>'user_id')); ?>
		</div>
	</div>
	


	<div class="control-group">
	    <label class="control-label required <?=$model->hasErrors('course_id')?'error':''?>" for="courses">Course <span class="required">*</span></label>
		<div class="controls">
		    <?php $this->widget('bootstrap.widgets.TbTypeahead', array(
		        'name'=>'courses',
		        'options'=>array(
		            'source'=>'js:function(query, process){
		                getItems(query, process, "'.$this->createUrl('/courses/getCourses').'")
		            }',
		            'items'=>10,
		            'highlighter' => 'js:function(item) {
		                    var n=item.split("|");
		                    return n[1];
		            }',
		            'matcher'=>"js:function(item) {
		                    return ~item.toLowerCase().indexOf(this.query.toLowerCase());
		            }",
		            'updater'=>"js:function(item) {
		                    var n=item.split('|');
		                    $('#course_id').val(n[0]);
		                    return n[1];
		            }"
		        ),
		        'htmlOptions' => array('class' => $model->hasErrors('course_id')?'error':'', 'autocomplete' => "off"),
		        'value' => $model->courseName
		    )); ?>
		    <?php echo $form->hiddenField($model,'course_id',array('id'=>'course_id')); ?>
		</div>
	</div>

    <div class="control-group">
        <label class="control-label" for="courses">Payment Type</label>
        <div class="controls">
            <?php echo $form->dropDownList($model, 'payment_type', $model->getPaymentTypesArr()) ?>
        </div>
    </div>

	<div class="controls">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
