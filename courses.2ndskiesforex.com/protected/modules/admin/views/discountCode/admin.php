<?php
$this->breadcrumbs=array(
	'Discount Codes'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List DiscountCode','url'=>array('index')),
	array('label'=>'Create DiscountCode','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('discount-code-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1 class="content-title"><?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn pull-right btn-success btn-small')); ?> Manage Discount Codes</h1>

<p>
You may optionally enter a comparison operator <code><b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b></code> at the beginning of each of your search values to specify how the comparison should be done.
</p>
<br><br>

<form action="<?php echo Yii::app()->createUrl('/admin/discountCode/changePercentage');?>" method="post">
    To change discount percentage put a new discount amount in this field (in %):<br><br>
    <input type="text" name="new_percentage">
    <input style="vertical-align: top;" type="submit" class="btn btn-primary"  value="Change Percentage">
</form>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'discount-code-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'afterAjaxUpdate' => TDatePicker::gridAfterAjax(array('date_start', 'date_end'), array('format' => 'yyyy-mm-dd')),
    'columns'=>array(
		'id',
		'code',
        array(
            'name' => 'date_start',
            'value' => function ($data) {
                echo trim(str_replace('00:00:00', '', $data->date_start));
            },
            'filter' => $this->widget('application.widgets.TDatePicker.TDatePicker', array(
                'model' => $model,
                'options' => array('format' => 'yyyy-mm-dd'),
                'attribute' => 'date_start',
                'htmlOptions' => array('id' => 'date_start'),
            ), true),

        ),
        array(
            'name' => 'date_end',
            'value' => function ($data) {
                echo trim(str_replace('00:00:00', '', $data->date_end));
            },
            'filter' => $this->widget('application.widgets.TDatePicker.TDatePicker', array(
                'model' => $model,
                'options' => array('format' => 'yyyy-mm-dd'),
                'attribute' => 'date_end',
                'htmlOptions' => array('id' => 'date_end'),
            ), true),
        ),
        array(
            'name' => 'discount',
            'value' => function ($data) {
                $slug = ((bool)$data->percentage) ? '%' : '$';
                    echo $data->discount . $slug;
            },
        ),
        'note',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
