<?php

$cs = Yii::app()->clientScript;
$cs->registerScriptFile('/js/admin/discountCode/discountCode.js', CClientScript::POS_BEGIN);
$cs->registerScriptFile('/js/datepicker.js', CClientScript::POS_BEGIN);
$cs->registerCssFile('/css/datepicker.css');

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'discount-code-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'code',array('class'=>'span5','maxlength'=>250)); ?>

	<?php echo $form->textFieldRow($model,'date_start',array(
        'class'=>'span5',
        'id' => 'dpd1',
        'value' => trim(str_replace('00:00:00', '', $model->date_start))
    )); ?>

    <?php echo $form->textFieldRow($model,'date_end', array(
        'class'=>'span5',
        'id' => 'dpd2',
        'value' => trim(str_replace('00:00:00', '', $model->date_end))
    )); ?>

	<?php echo $form->textFieldRow($model,'discount',array('class'=>'span5')); ?>

	<?php echo $form->radioButtonListRow($model,'percentage',array('1'=>'Percentage of full price','0'=>'Fixed dollar value'), array('separator'=>' ')); ?>

    <?php echo $form->textFieldRow($model,'note',array('class'=>'span5')); ?>

	<div class="controls">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
