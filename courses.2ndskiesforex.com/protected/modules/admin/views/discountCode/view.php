<?php
$this->breadcrumbs=array(
	'Discount Codes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List DiscountCode','url'=>array('index')),
	array('label'=>'Create DiscountCode','url'=>array('create')),
	array('label'=>'Update DiscountCode','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete DiscountCode','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DiscountCode','url'=>array('admin')),
);
?>

<h1 class="content-title">View DiscountCode #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'code',
        array(
            'name' => 'date_start',
            'value' => function ($data) {
                return trim(str_replace('00:00:00', '',$data->date_start));
            },
        ),
        array(
            'name' => 'date_end',
            'value' => function ($data) {
                return trim(str_replace('00:00:00', '',$data->date_end));
            },
        ),
		array(
			'name' => 'discount',
			'value' => function ($data) {
				$slug = ((bool)$data->percentage) ? '%' : '$';
				return $data->discount . $slug;
			},
		),
        'note',
	),
)); ?>
