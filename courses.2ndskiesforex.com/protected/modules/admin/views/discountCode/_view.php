<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('code')); ?>:</b>
	<?php echo CHtml::encode($data->code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_start')); ?>:</b>
	<?php echo CHtml::encode(trim(str_replace('00:00:00', '',$data->date_start))); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('date_end')); ?>:</b>
    <?php echo CHtml::encode(trim(str_replace('00:00:00', '',$data->date_end))); ?>
    <br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('discount')); ?>:</b>
	<?php echo CHtml::encode($data->discount . (((bool)$data->percentage) ? '%' : '$')); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('note')); ?>:</b>
    <?php echo CHtml::encode($data->note); ?>
    <br />


</div>