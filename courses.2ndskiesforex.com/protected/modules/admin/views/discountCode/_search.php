<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'type'=>'horizontal',
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'code',array('class'=>'span5','maxlength'=>250)); ?>

    <?php echo $form->textFieldRow($model,'date_start',array(
        'class'=>'span5',
        'id' => 'dpd1',
    )); ?>

    <?php echo $form->textFieldRow($model,'date_end',array(
        'class'=>'span5',
        'id' => 'dpd2',
    )); ?>

    <?php echo $form->textFieldRow($model,'note',array('class'=>'span5','maxlength'=>500)); ?>

	<div class="controls">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'success',
			'label'=>'Search',
			'icon'=>'search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
