<?php
$this->breadcrumbs=array(
	'Discount Codes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DiscountCode','url'=>array('index')),
	array('label'=>'Create DiscountCode','url'=>array('create')),
	array('label'=>'View DiscountCode','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage DiscountCode','url'=>array('admin')),
);
?>

<h1 class="content-title">Update DiscountCode <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>