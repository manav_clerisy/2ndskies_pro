<?php
$this->breadcrumbs=array(
	'Discount Codes',
);

$this->menu=array(
	array('label'=>'Create DiscountCode','url'=>array('create')),
	array('label'=>'Manage DiscountCode','url'=>array('admin')),
);
?>

<h1 class="content-title">Discount Codes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
