<?php
$this->breadcrumbs=array(
	'Discount Codes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DiscountCode','url'=>array('index')),
	array('label'=>'Manage DiscountCode','url'=>array('admin')),
);
?>

<h1 class="content-title">Create DiscountCode</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>