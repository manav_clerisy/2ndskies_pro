<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'comment-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
	'htmlOptions'=>array('class'=>'wysiwyg-form'),
)); ?>

	<p class="hint">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->textFieldRow($model,'topic_id',array('class'=>'span5')); ?>

    <?php echo $form->errorSummary($model); ?>
	<div class="control-group">
	    <?php $this->widget('ext.redactorWidget.ImperaviRedactorWidget',array(
	        'model'=>$model,
	        'attribute'=>'text',
	        'name'=>'redactor',
            'options'=>array(
                'minHeight'=>200,
                'convertVideoLinks'=> true,
                'convertImageLinks'=> true,
                'fileUpload'=>Yii::app()->createUrl('topic/upload'),
                'convertLinks'=>true,
                'dragUpload'=>true,
                'fileUploadErrorCallback'=>new CJavaScriptExpression(
                    'function(obj,json) { alert(json.error); }'
                ),
                'imageUpload'=>Yii::app()->createUrl('topic/upload'),
                'imageUploadErrorCallback'=>new CJavaScriptExpression(
                    'function(obj,json) { alert(json.error); }'
                ),
            ),
	    ));
	    ?>
	</div>


	<div class="clearfix">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'inverse',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
			'htmlOptions'=>array('class'=>'pull-right')
		)); ?>
	</div>

<?php $this->endWidget(); ?>
