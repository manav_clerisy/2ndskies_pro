<div class="well well-small">
	<ul class="unstyled">
		<li>
			<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
			<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
		</li>
		<li>
			<b><?php echo CHtml::encode($data->getAttributeLabel('topic_id')); ?>:</b>
			<?php echo CHtml::encode($data->topic_id); ?>
		</li>
		<li>
			<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
			<?php echo CHtml::encode($data->user_id); ?>
		</li>
		<li>
			<b><?php echo CHtml::encode($data->getAttributeLabel('date_created')); ?>:</b>
			<?php echo CHtml::encode($data->date_created); ?>
		</li>
		<li>
			<b><?php echo CHtml::encode($data->getAttributeLabel('date_changed')); ?>:</b>
			<?php echo CHtml::encode($data->date_changed); ?>
		</li>
        <li>
            <b><?php echo CHtml::encode($data->getAttributeLabel('is_first')); ?>:</b>
            <?php echo CHtml::encode($data->is_first); ?>
        </li>
		<li>
			<b><?php echo CHtml::encode($data->getAttributeLabel('text')); ?>:</b>
			<?php echo CHtml::encode($data->text); ?>
		</li>
	</ul>
</div>