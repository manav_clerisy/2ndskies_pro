<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'type'=>'horizontal',
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id'); ?>

	<?php echo $form->textFieldRow($model,'topic_id'); ?>

	<?php echo $form->textFieldRow($model,'user_id'); ?>

	<?php echo $form->textAreaRow($model,'text',array('rows'=>6, 'cols'=>50)); ?>

	<?php echo $form->textFieldRow($model,'date_created'); ?>

	<?php echo $form->textFieldRow($model,'date_changed'); ?>

    <?php echo $form->textFieldRow($model,'is_first'); ?>

	<div class="controls">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
