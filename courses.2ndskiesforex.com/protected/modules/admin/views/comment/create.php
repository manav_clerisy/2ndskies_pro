<?php
$this->breadcrumbs=array(
	'Topic'=>array('topic/list'),
	'Create',
);

?>

<h1 class="content-title">Create Comment</h1>

<?php echo $this->renderPartial('/comment/_form', array('model'=>$model)); ?>