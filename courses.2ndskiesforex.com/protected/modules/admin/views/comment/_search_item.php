<?php
// for global searching
if (is_array($data)) {
    $obj = new Comment();
    $obj->id = $data['id'];
    $obj->attributes = $data;
    $data = $obj;
}
?>
<div class="alert alert-block alert-info" xmlns="//www.w3.org/1999/html">
    <div class="media">
        <h4><?=CHtml::link($data->topic->subject, Yii::app()->createUrl('topic/view',array('id'=>$data->topic->id)))?></h4>
        <div class="span9">
            <div>
                <small><span >Posted by <b><a href="#"><?=$data->user->username ?></a></b><i class="muted"> >> </i><?php if(isset($data->date_created)) echo $data->getDate($data->date_created) ?></span></small>
            </div>
            <p><?=$data->text ?></p>
        </div>
        <div class="media-body">
            <img class="img-rounded" alt="" src="<?=$data->userImage ?>">
            <p>
                <b><?=$data->user->username ?></b><br>
                <br><small>Posts: <i class="muted"><?=$data->posts ?></i></small><br>
                <small>Joined: <i class="muted"><?=$data->user->create_at ?></i></small>
            </p>

        </div>
    </div>
</div>



