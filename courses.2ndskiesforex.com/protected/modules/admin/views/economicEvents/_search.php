<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'type'=>'horizontal',
	'method'=>'get',
));

Yii::app()->clientScript->registerScript('manage', "
    $('#EconomicEvents_event_date').datetimepicker({
        stepMinute: 5,
        dateFormat: 'yy-mm-dd',
        beforeShow: function (input, inst) {
             var offset = $(input).offset();
             var height = $(input).height();
             window.setTimeout(function () {
                 inst.dpDiv.css({ top: (offset.top + height + 10) + 'px', left: offset.left + 'px' })
             }, 1);
         }
    });
");
?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'event_date',array('class'=>'span5')); ?>

    <?php
        $currencies = array_combine(
            EconomicEvents::$currencies, // keys
            EconomicEvents::$currencies  // values
        );
        echo $form->dropDownListRow($model,'currency',$currencies,array('class'=>'span5', 'empty' => ''));
    ?>

	<?php echo $form->textFieldRow($model,'event',array('class'=>'span5','maxlength'=>250)); ?>

    <?php
        $importance = array_combine(
            EconomicEvents::$importance, // keys
            EconomicEvents::$importance  // values
        );
        echo $form->dropDownListRow($model,'importance',$importance,array('class'=>'span5', 'empty' => ''));
    ?>

	<?php echo $form->textFieldRow($model,'actual',array('class'=>'span5','maxlength'=>250)); ?>

	<?php echo $form->textFieldRow($model,'forecast',array('class'=>'span5','maxlength'=>250)); ?>

	<?php echo $form->textFieldRow($model,'previous',array('class'=>'span5','maxlength'=>250)); ?>

	<?php echo $form->textAreaRow($model,'notes',array('rows'=>6, 'class'=>'span5')); ?>

	<div class="controls">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'success',
			'label'=>'Search',
			'icon'=>'search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
