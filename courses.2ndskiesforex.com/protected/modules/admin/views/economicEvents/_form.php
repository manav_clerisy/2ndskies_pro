<?php

Yii::app()->clientScript->registerPackage('timepicker');

Yii::app()->clientScript->registerScript('create', "
    $('#EconomicEvents_event_date').datetimepicker({
        stepMinute: 5,
        beforeShow: function (input, inst) {
             var offset = $(input).offset();
             var height = $(input).height();
             window.setTimeout(function () {
                 inst.dpDiv.css({ top: (offset.top + height + 10) + 'px', left: offset.left + 'px' })
             }, 1);
         }
    });
");

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'economic-events-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'event_date',array('class'=>'span5')); ?>

    <?php
        $currencies = array_combine(
            EconomicEvents::$currencies, // keys
            EconomicEvents::$currencies  // values
        );
	    echo $form->dropDownListRow($model,'currency',$currencies,array('class'=>'span5', 'empty' => ''));
    ?>

    <?php echo $form->textAreaRow($model,'event',array('rows'=>5, 'cols'=>50, 'class'=>'span5', 'maxlength'=>250)); ?>

    <?php
        $importance = array_combine(
            EconomicEvents::$importance, // keys
            EconomicEvents::$importance  // values
        );
        echo $form->dropDownListRow($model,'importance',$importance,array('class'=>'span5', 'empty' => ''));
    ?>

	<?php echo $form->textFieldRow($model,'actual',array('class'=>'span5','maxlength'=>250)); ?>

	<?php echo $form->textFieldRow($model,'forecast',array('class'=>'span5','maxlength'=>250)); ?>

	<?php echo $form->textFieldRow($model,'previous',array('class'=>'span5','maxlength'=>250)); ?>

	<?php echo $form->textAreaRow($model,'notes',array('rows'=>6, 'cols'=>50, 'class'=>'span5')); ?>

	<div class="controls">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
