<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('currency')); ?>:</b>
	<?php echo CHtml::encode($data->currency); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('event')); ?>:</b>
	<?php echo CHtml::encode($data->event); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('importance')); ?>:</b>
	<?php echo CHtml::encode($data->importance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actual')); ?>:</b>
	<?php echo CHtml::encode($data->actual); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('forecast')); ?>:</b>
	<?php echo CHtml::encode($data->forecast); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('previous')); ?>:</b>
	<?php echo CHtml::encode($data->previous); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('notes')); ?>:</b>
	<?php echo CHtml::encode($data->notes); ?>
	<br />

	*/ ?>

</div>