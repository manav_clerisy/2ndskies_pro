<?php
$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    $model->event,
);

$this->menu=array(
	array('label'=>'Create Economic Events','url'=>array('create')),
	array('label'=>'Update Economic Events','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Economic Events','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Economic Events','url'=>array('manage')),
);
?>

<h1 class="content-title">View Economic Events #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'event_date',
		'currency',
		'event',
		'importance',
		'actual',
		'forecast',
		'previous',
		'notes',
	),
)); ?>
