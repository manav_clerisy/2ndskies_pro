<?php
$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    $model->event=>array('view','id'=>$model->id),
    'Update',
);

$this->menu=array(
	array('label'=>'Create Economic Events','url'=>array('create')),
	array('label'=>'View Economic Events','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Economic Events','url'=>array('manage')),
);
?>

<h1 class="content-title">Update Economic Events <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>