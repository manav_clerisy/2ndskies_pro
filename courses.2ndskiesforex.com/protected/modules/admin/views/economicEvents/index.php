<?php
$this->breadcrumbs=array(
	'Economic Events',
);

$this->menu=array(
	array('label'=>'Create Economic Events','url'=>array('create')),
	array('label'=>'Manage Economic Events','url'=>array('manage')),
);
?>

<h1 class="content-title">Economic Events</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
