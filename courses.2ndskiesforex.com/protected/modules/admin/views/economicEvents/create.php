<?php
$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    'Economic Events' => array('/admin/economicEvents/manage'),
    'Create',
);

$this->menu=array(
	array('label'=>'Manage Economic Events','url'=>array('manage')),
);



?>

<h1 class="content-title">Create Economic Events</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>