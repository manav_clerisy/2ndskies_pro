<?php
$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    'Economic Events'
);


$this->menu=array(
	array('label'=>'Create Economic Events','url'=>array('create')),
);

Yii::app()->clientScript->registerScriptFile('/js/admin/economicEvents/economicEvents.js', CClientScript::POS_END);
Yii::app()->clientScript->registerPackage('timepicker');
?>

<h1 class="content-title"><?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn pull-right btn-success btn-small')); ?>Manage Economic Events</h1>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php
$provider = $model->search();
$this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'economic-events-grid',
	'htmlOptions'=>array('class'=>'table-events grid-view'),
	'dataProvider'=>$provider,
	'filter'=>$model,
    'afterAjaxUpdate' => 'js:function(){
        hideDuplicatedDates();
        addIdForGridTable();
    }',
    'ajaxUrl'=>'/admin/economicEvents/manage',
	'columns'=>array(
        array(
            'name' => 'event_date',
            'type' => 'html',
            'value' => '$data->dateOnly',
            'filter' => false
        ),
        array(
            'header' => 'Time',
            'type'=>'html',
            'value' => '$data->timeOnly',
        ),
        array(
            'name' => 'currency',
            'type' => 'html',
            'value' => '$data->currencyWithFlag',
            'filter'=>  $currencies = array_combine(
                            EconomicEvents::$currencies, // keys
                            EconomicEvents::$currencies  // values
                        )
        ),
		array(
			'name' => 'event',
			'htmlOptions'=>array('class'=>'col-event'),
		),
        array(
            'name' => 'importance',
            'type' => 'html',
            'value' => '$data->coloredImportance',
            'filter'=>  $importance = array_combine(
                    EconomicEvents::$importance, // keys
                    EconomicEvents::$importance  // values
                )
        ),
		'actual',
		'forecast',
		'previous',
        array(
            'name' => 'notes',
            'type' => 'html',
            'value' => '$data->hiddenNotes',
            'filter' => false
        ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
