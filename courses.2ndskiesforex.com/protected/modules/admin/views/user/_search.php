<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
	'type'=>'horizontal',
    'method'=>'get',
)); ?>
	
	
	<?php echo $form->textFieldRow($model,'id'); ?>
	
	<?php echo $form->textFieldRow($model,'username'); ?>
	
	<?php echo $form->textFieldRow($model,'email'); ?>
	
	<?php echo $form->textFieldRow($model,'activkey'); ?>
	
	<?php echo $form->textFieldRow($model,'create_at'); ?>
	
	<?php echo $form->textFieldRow($model,'lastvisit_at'); ?>
	
	<?php echo $form->dropDownListRow($model,'role',$model->itemAlias('AdminStatus')); ?>
	
	<?php echo $form->dropDownListRow($model,'status',$model->itemAlias('UserStatus')); ?>
	
	<div class="controls">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'icon'=>'search',
			'type'=>'success',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
<!-- search-form -->