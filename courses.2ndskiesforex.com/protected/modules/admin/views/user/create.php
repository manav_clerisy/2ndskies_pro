<?php
$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    'users' => array('/admin/user/manage'),
	'create',
);

$this->menu=array(
    array('label'=>UserModule::t('Manage Users'), 'url'=>array('manage')),
);
?>
<h1 class="content-title"><?php echo UserModule::t("Create User"); ?></h1>

<?php
	echo $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile));
?>