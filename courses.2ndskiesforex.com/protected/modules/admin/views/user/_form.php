<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'user-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>true,
	'htmlOptions' => array('enctype'=>'multipart/form-data'),
));
?>

	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary(array($model,$profile)); ?>
	
	<?php echo $form->textFieldRow($model,'username'); ?>
	
	<?php echo $form->textFieldRow($model,'newPassword'); ?>
	
	<?php echo $form->textFieldRow($model,'email'); ?>
	
	<?php echo $form->dropDownListRow($model,'role',User::itemAlias('AdminStatus')); ?>
	
	<?php echo $form->dropDownListRow($model,'status',User::itemAlias('UserStatus')); ?>

        <?php // echo $form->textFieldRow($model,'ontraportId'); ?>
    <div class="control-group">
        <label class="control-label">Badge</label>
        <div class="controls">
            <?php
                $html = '';
                foreach ($model->userBadges as $userBadge) {
                    $html .= CHtml::dropDownList('Badges[]', $userBadge->badge_id, Badges::model()->getAllInArray(), array('empty' => '', 'options'=>array('15'=>array('disabled'=>'disabled')),'id' => false, 'disabled' => ($userBadge->set_manually == '0') ? 'disable' : ''));
                }

                $url = (Yii::app()->controller->action->id == 'create') ? Yii::app()->createUrl('/admin/badges/addNewBadge', array('action' => 'create')) : Yii::app()->createUrl('/admin/badges/addNewBadge');

                $html .= CHtml::ajaxLink('add', $url,
                            array('success' => <<<JS
                                function(data){
                                    $('#addNewBadge').before(data);
                                }
JS
                            ),
                            array(
                                'id' => 'addNewBadge'
                            )
                        );

                echo $html;
            ?>

        </div>
    </div>

	<?php
		$profileFields=Profile::getFields();
		if ($profileFields) {
			foreach($profileFields as $field) {
				?>
					<?php echo $form->labelEx($profile,$field->varname, array('class'=>'control-label'));
					echo '<div class="control-group">';
					if ($widgetEdit = $field->widgetEdit($profile)) {
						echo '<div class="controls">';
						echo $widgetEdit;
						echo $form->error($profile,$field->varname);
						echo '</div>';
					} elseif ($field->range) {
						echo '<div class="controls">';
						echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
						echo $form->error($profile,$field->varname); 
						echo '</div>';
					} elseif ($field->field_type=="TEXT") {
						echo '<div class="controls">';
						echo $form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
						echo $form->error($profile,$field->varname); 
						echo '</div>';
					} else {
						echo '<div class="controls">';
						echo $form->textField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
						echo $form->error($profile,$field->varname); 
						echo '</div>';
					}
					echo '</div>';
					?>
					
			<?php
			}
		}
	?>
        
	<div class=controls>
		<?php echo CHtml::submitButton($model->isNewRecord ? UserModule::t('Create') : UserModule::t('Save'), array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>
<!-- form -->