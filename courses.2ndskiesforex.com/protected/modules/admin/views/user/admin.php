<?php
$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    'users'
);

$this->menu=array(
    array('label'=>UserModule::t('Create User'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(e){
	e.preventDefault();
    $('.search-form').toggleClass('hidden');
});	
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('user-grid', {
        data: $(this).serialize()
    });
});
");


?>
<h1 class="content-title">
	<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn btn-success btn-small pull-right', 'data-toggle'=>'button')); ?>

	<?php echo CHtml::link('Export to CSV',Yii::app()->createUrl('admin/user/export'),array('class'=>'btn btn-small pull-right', 'data-toggle'=>'button')); ?>
	<?php echo UserModule::t("Manage Users"); ?>

</h1>
<div class="search-form hidden">
<?php $this->renderPartial('_search',array(
    'model'=>$model,
));


//die(var_dump($model->search()));?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'columns'=>array(
		array(
			'name' => 'id',
			'type'=>'raw',
            'htmlOptions' => array('width' => '70px'),
			'value' => 'CHtml::link(CHtml::encode($data->id),array("admin/update","id"=>$data->id))',
		),
		array(
			'name' => 'username',
			'type'=>'raw',
			'value' => 'CHtml::link(UHtml::markSearch($data,"username"),array("view","id"=>$data->id))',
		),
		array(
			'name'=>'email',
			'type'=>'raw',
			'value'=>'CHtml::link(UHtml::markSearch($data,"email"), "mailto:".$data->email)',
		),
		'create_at',
		'lastvisit_at',
		array(
			'name'=>'role',
			'value'=>'User::itemAlias("AdminStatus",$data->role)',
			'filter'=>User::itemAlias("AdminStatus"),
		),
		array(
			'name'=>'status',
			'value'=>'User::itemAlias("UserStatus",$data->status)',
			'filter' => User::itemAlias("UserStatus"),
		),
                'ontraportId',  
        /*array(
            'name'=>'subscription.course_id',
            //'value'=>'$data->subscription->course_id',
        ),*/
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view} {update} {delete}',
            'buttons'=>array(
                'delete'=> array(
                    'visible'=>'Yii::app()->user->id != $data->id'
                )
            )
        ),
	),
)); ?>
