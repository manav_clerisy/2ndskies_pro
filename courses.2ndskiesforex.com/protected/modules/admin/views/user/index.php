<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	$this->module->id,
);
?>

<p>To view the users table follow this link: <br/>
    <a href="<?php echo $this->createUrl('admin/manage')?>">View Users</a>
</p>

<p>To view the courses table follow this link: <br/>
    <a href="<?php echo Yii::app()->createUrl('courses/admin')?>">View Courses</a>
</p>

<p>To view the subcriptions table follow this link: <br/>
    <a href="<?php echo Yii::app()->createUrl('userCourse/admin')?>">View Subscriptions</a>
</p>

<p>To view the topics table follow this link: <br/>
    <a href="<?php echo $this->createUrl('/topic/admin')?>">View Topics</a>
</p>

<p>To view the comments table follow this link: <br/>
    <a href="<?php echo $this->createUrl('/comment/admin')?>">View Comments</a>
</p>