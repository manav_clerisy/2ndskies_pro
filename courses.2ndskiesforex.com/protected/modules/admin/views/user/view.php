<?php
$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    'users' => array('/admin/user/manage'),
    'view',
);
/* @var $model User */

$this->menu=array(
    array('label'=>UserModule::t('Create User'), 'url'=>array('create')),
    array('label'=>UserModule::t('Update User'), 'url'=>array('update','id'=>$model->id)),
    #array('label'=>UserModule::t('Delete User'), 'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>UserModule::t('Are you sure to delete this item?'))),
    array('label'=>UserModule::t('Manage Users'), 'url'=>array('manage')),
);
?>
<h1><?php echo UserModule::t('View User').' '.$model->username; ?></h1>

<?php

	$attributes = array(
		'id',
		'username',
	);

	$profileFields=ProfileField::model()->forOwner()->sort()->findAll();
    if(isset($model->userBadges->badge_id))
    $badge = Badges::model()->findByPk($model->userBadges->badge_id);
	if ($profileFields) {
		foreach($profileFields as $field) {

            $options = array(
                'label' => UserModule::t($field->title),
                'name' => $field->varname,
                'type'=>'raw',
            );



                $options['value'] = (($field->widgetView($model->profile))
                                        ? $field->widgetView($model->profile)
                                        :(($field->range)
                                            ? Profile::range($field->range,$model->profile->getAttribute($field->varname))
                                            : $model->profile->getAttribute($field->varname)));

			array_push($attributes,$options);
		}
	}

	array_push($attributes,
		'email',
		'activkey',
		'create_at',
		'lastvisit_at',
		array(
			'name' => 'role',
			'value' => User::itemAlias("AdminStatus",$model->role),
		),
		array(
			'name' => 'status',
			'value' => User::itemAlias("UserStatus", $model->status),
		),
		array(
			'name' => 'image',
            'type' => 'html',
            'value' => CHtml::image($model->profile->imageUrl),
		),
        array(
            'name'  => 'badge',
            'type'  => 'html',
            'value' => $model->badgeTitles,
        )
	);

	$this->widget('bootstrap.widgets.TbDetailView', array(
		'data'=>$model,
		'attributes'=>$attributes,
	));


?>
