<h1 class="content-title">
		Manage Users Log
</h1>
<div class="search-form hidden">
  <form class="form-horizontal" id="yw0" action="/admin/user/manage" method="get">
    <div class="control-group ">
      <label class="control-label" for="User_id">Id</label>
      
    </div>
    <div class="control-group ">
      <label class="control-label required" for="User_username">Username <span class="required">*</span></label>
      
    </div>
    <div class="control-group ">
      <label class="control-label required" for="User_email">E-mail <span class="required">*</span></label>
      
    </div>
    <div class="control-group ">
      <label class="control-label" for="User_activkey">Visited Date</label>
      
    </div>
    <div class="control-group ">
      <label class="control-label" for="User_create_at">Log</label>
      
    </div>
    
  </form>
  <!-- search-form -->
</div>
<!-- search-form -->
<div id="user-grid" class="grid-view">
  <table class="items table">
    <thead>
      <tr>
        <th id="user-grid_c0"><a class="sort-link" href="javascript:void(0);">Id<span class="caret"></span></a></th>
        <th id="user-grid_c1"><a class="sort-link" href="javascript:void(0);">Username<span class="caret"></span></a></th>
        <th id="user-grid_c2"><a class="sort-link" href="javascript:void(0);">E-mail<span class="caret"></span></a></th>
        <th id="user-grid_c3"><a class="sort-link" href="javascript:void(0);">Visited Date<span class="caret"></span></a></th>
		
		<?php
		if(isset($usersdetail) && $usersdetail==true)
		{
		?>
		<th id="user-grid_c4">
			<a class="sort-link" href="javascript:void(0);">Log<span class="caret"></span></a>
		</th>
		<th id="user-grid_c5">
			<a class="sort-link" href="javascript:void(0);">Log Details<span class="caret"></span></a>
		</th>
		
		<?php
		}
		else
		{
			?>
		<th id="user-grid_c4">
			<a class="sort-link" href="javascript:void(0);">Last Activity<span class="caret"></span></a>
		</th>
		<th id="user-grid_c5">
			<a class="sort-link" href="javascript:void(0);">Log<span class="caret"></span></a>
		</th>
		
		<?php
		}
	?>
      </tr>
      
    </thead>
    <tbody>
	<?php
//print_r($model);

	if(isset($usersdetail) && $usersdetail==true)
	{
		$i=1;
		foreach($model as $logData)
		{
			
			$id=$logData["id"];
			$username=$logData["username"];
			$email=$logData["email"];
			$userID=$logData["userID"];
			$create_at=$logData["create_at"];
			$description=$logData["description"];
			$visitingHours=$logData["visitingHours"];
			
			$sectionID=$logData["sectionID"];
			
			$getBaseUrl='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			$getBaseUrl=dirname(dirname(dirname($getBaseUrl)));
				if($i%2==0)
				{
			?>
			 <tr class="odd">
				<td width="70px"><a href="javascript:void(0);"><?php echo $id; ?></a></td>
				<td><a href="javascript:void(0);"><?php echo $username; ?></a></td>
				<td><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></td>
				<td><?php echo $create_at; ?></td>
				<td><?php echo $description; ?></td>
				<td><?php echo $visitingHours; ?></td>
			  </tr>
			<?php
			}
			else
			{
				
			?>
			  <tr class="even">
				<td width="70px"><a href="javascript:void(0);"><?php echo $id; ?></a></td>
				<td><a href="javascript:void(0);"><?php echo $username; ?></a></td>
				<td><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></td>
				<td><?php echo $create_at; ?></td>
				<td><?php echo $description; ?>
				<?php
				if($sectionID!=0)
				{
					?>
				<a target="_blank" href="<?php echo $getBaseUrl; ?>/courses/videoLessons/1?sectionId=<?php echo $sectionID; ?>">
				Goto View
				</a>	
				<?php
				}
				?>
				
				
				
				</td>
				<td><?php echo $visitingHours; ?></td>
			  </tr>
			  
			<?php
			
			}
			$i++;
		}
	}
	else
	{
	$i=1;
	foreach($model as $logData)
	{
		$id=$logData["id"];
		$username=$logData["username"];
		$email=$logData["email"];
		$create_at=$logData["create_at"];
		$lastvisit_at=$logData["lastvisit_at"];
		
		if($i%2==0)
		{
	?>
	 <tr class="odd">
        <td width="70px"><a href="javascript:void(0);"><?php echo $id; ?></a></td>
        <td><a href="javascript:void(0);"><?php echo $username; ?></a></td>
        <td><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></td>
        <td><?php echo $create_at; ?></td>
        <td><?php echo $lastvisit_at; ?></td>
		
		<td><a href="<?php echo Yii::app()->getBaseUrl(); ?>?UserID=<?php echo $id; ?>">View Log</a></td>
		</tr>
	<?php
	}
	else
	{
	?>
	  <tr class="even">
        <td width="70px"><a href="javascript:void(0);"><?php echo $id; ?></a></td>
        <td><a href="javascript:void(0);"><?php echo $username; ?></a></td>
        <td><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></td>
        <td><?php echo $create_at; ?></td>
        <td><?php echo $lastvisit_at; ?></td>
		<td><a href="<?php echo Yii::app()->getBaseUrl(); ?>?UserID=<?php echo $id; ?>">View Log</a></td>
      </tr>
	  
	<?php
	
	}
	$i++;
	}
	}
	?>
      
	  
	  
		<div class="pagination">
		<ul id="yw1" class="yiiPager">
		<?php
		for($pagin=1;$pagin<=$paginate;$pagin++)
		{
		?>
		<li class=""><a href="/admin/user/userlog?User_page=<?php echo $pagin; ?>"><?php echo $pagin; ?></a></li>
		<?php 
		}
		?>
	<?php /*?>	<li class=""><a href="/admin/user/userlog?User_page=2">2</a></li>
		<li class=""><a href="/admin/user/userlog?User_page=3">3</a></li>
		<li class=""><a href="/admin/user/userlog?User_page=4">4</a></li>
		<li class=""><a href="/admin/user/userlog?User_page=5">5</a></li>
		<li class=""><a href="/admin/user/userlog?User_page=6">6</a></li>
		<li class=""><a href="/admin/user/userlog?User_page=7">7</a></li>
		<li class=""><a href="/admin/user/userlog?User_page=8">8</a></li>
		<li class=""><a href="/admin/user/userlog?User_page=9">9</a></li>
		<li class=""><a href="/admin/user/userlog?User_page=10">10</a></li>		<?php */?>
		</ul>
		</div>
		

     
    </tbody>
  </table>
  
  

  
</div>
