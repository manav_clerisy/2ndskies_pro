<?php
$this->breadcrumbs=array(
	'Subscriptions',
);

$this->menu=array(
	array('label'=>'Create Subscriptions','url'=>array('create')),
	array('label'=>'Manage Subscriptions','url'=>array('admin')),
);
?>

<h1>Subscriptions</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
