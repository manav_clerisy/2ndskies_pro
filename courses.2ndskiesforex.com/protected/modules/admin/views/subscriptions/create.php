<?php
$this->breadcrumbs=array(
	'Subscriptions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Subscriptions','url'=>array('index')),
	array('label'=>'Manage Subscriptions','url'=>array('admin')),
);
?>

<h1>Create Subscriptions</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>