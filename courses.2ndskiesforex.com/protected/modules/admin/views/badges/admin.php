<?php
$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    'badges'
);

$this->menu=array(
	array('label'=>'Create Badges','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('badges-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1 class="content-title"><?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn pull-right btn-success btn-small')); ?> Manage Badges</h1>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'badges-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
        array(
            'name' => 'image',
            'type' => 'html',
            'value' => 'CHtml::image($data->imageUrl,"",array("width"=>50,"height"=>50))',
            'filter' => false
        ),
		'title',
		'minimum_posts',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'buttons'=>array
            (
                'delete' => array(
                    'visible'=>'$data->minimum_posts > 0 || is_null($data->minimum_posts)',
                ),
            ),
		),
	),
)); ?>
