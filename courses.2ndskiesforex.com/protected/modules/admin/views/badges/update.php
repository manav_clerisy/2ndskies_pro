<?php
$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    $model->title=>array('view','id'=>$model->id),
    'Update',
);

$this->menu=array(
	array('label'=>'Create Badges','url'=>array('create')),
	array('label'=>'View Badges','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Badges','url'=>array('manage')),
);
?>

<h1 class="content-title">Update Badges <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>