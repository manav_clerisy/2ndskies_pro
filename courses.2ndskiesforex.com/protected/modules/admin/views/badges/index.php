<?php
$this->breadcrumbs=array(
	'Badges',
);

$this->menu=array(
	array('label'=>'Create Badges','url'=>array('create')),
	array('label'=>'Manage Badges','url'=>array('manage')),
);
?>

<h1 class="content-title">Badges</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    /*'columns' => array(
        array(
            'name' => 'image',
            'type' => 'html',
            //'value' => CHtml::image($data->imageUrl),
        ),
        'title',
        'minimum_posts',
    )*/
)); ?>
