<?php
$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    $model->title,
);

$this->menu=array(
	array('label'=>'Create Badges','url'=>array('create')),
	array('label'=>'Update Badges','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Badges','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Badges','url'=>array('manage')),
);
?>

<h1 class="content-title">View Badge "<?php echo $model->title; ?>"</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
        'id',
        array(
            'name' => 'image',
            'type' => 'html',
            'value' => CHtml::image($model->imageUrl,'',array('width'=>50,'height'=>50)),
        ),
		'title',
		'minimum_posts',
	),
)); ?>
