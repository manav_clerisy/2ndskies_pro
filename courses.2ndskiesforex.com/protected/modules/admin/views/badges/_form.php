<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'badges-form',
	'enableAjaxValidation'=>false,
	'type'=>'horizontal',
    'htmlOptions' => array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <?php echo $form->fileFieldRow($model,'image'); ?>

	<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>255)); ?>


	<?php
        if ($model->minimum_posts > 0 || is_null($model->minimum_posts))
            echo $form->textFieldRow($model,'minimum_posts',array('class'=>'span5'));
    ?>

	<div class="controls">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
