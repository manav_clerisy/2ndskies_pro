<?php
$this->breadcrumbs=array(
    'admin' => array('/admin/default/index'),
    'badges' => array('/admin/badges/manage'),
    'Create',
);

$this->menu=array(
	array('label'=>'Manage Badges','url'=>array('manage')),
);
?>

<h1 class="content-title">Create Badges</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>