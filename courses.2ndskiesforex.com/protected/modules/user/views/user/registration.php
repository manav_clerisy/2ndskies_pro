<?php

$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Registration");


Yii::app()->clientScript->registerCssFile('/css/form.css');
?>

<div class="buy-box">
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'registration-form',
		'type'=>'horizontal',
		'enableAjaxValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
            'afterValidate' => 'js:function(data, errors){if(errors > 0) $("#error-msg").toggleClass("hidden", !errors); return true;}',
		),
		'htmlOptions' => array('enctype'=>'multipart/form-data'),
	));

//	$form=$this->beginWidget('CActiveForm', array(
//		'id'=>'registration-form',
//		'enableAjaxValidation'=>true,
//		//'enableClientValidation'=>true,
//		'clientOptions'=>array('validateOnSubmit'=>true,'afterValidateAttribute'=>new CJavaScriptExpression("function(form, attribute, data, hasError){
//                var id='RegistrationForm_verifyPassword';
//                var eid='RegistrationForm_verifyPassword_em_';
//
//                if(data[id] === undefined )
//                {
//        				$('#RegistrationForm_verifyPassword_em_').css('display','none');
//        		  		$('#success-image.flash-success').css('display','block');
//                        $('#'+id).parent().children('label.required').css('color','green');
//
//                }
//
//                else
//                {
//                        $('#success-image.flash-success').css('display','none');
//                        $('#'+id).parent().children('.errorMessage').css('color','red');
//                        $('#'+id).css('background','').addClass('error');
//                }
//                }")),
//
//	));

	?>
	<ul class="steps-list">
		<li class="active"><a href="#"><i class="counter"><b>1</b></i><span class="text">Registration</span></a></li>
		<li><a href="#"><i class="counter"><b>2</b></i><span class="text">Payment info</span></a></li>
		<li><a href="#"><i class="counter"><b>3</b></i><span class="text">Finish</span></a></li>
	</ul>
	<div class="buy-box-holder">

		<?php if(Yii::app()->user->hasFlash('registration')): ?>
			<div class="alert alert-success">
				<?php echo Yii::app()->user->hasFlash('registration'); ?>
			</div>
		<?php else: ?>
			<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

			<div id="error-msg" class="alert alert-block alert-error hidden">Please fix the following input errors.</div>

			<span id="are-you-real" class="hidden"></span>
			<div class="half-box-holder">
				<div class="half-box">
					<?php
					$profileFields=Profile::getFields();
					if ($profileFields) {
						foreach($profileFields as $field) {
							$field['title']=ucfirst(strtolower($field['title']));
							?>
							<?php
							echo '<div class="control-group"><div class="heading-box">';
							echo $form->labelEx($profile,$field->varname, array('class'=>'block-label filed_name'));

							if ($widgetEdit = $field->widgetEdit($profile)) {
								?>	<div class= "help-inline error">	<?php	echo $form->error($profile,$field->varname);?></div><?php
								echo '</div>';
								echo $widgetEdit;
							} elseif ($field->range) {
								?>	<div class= "help-inline error">	<?php	echo $form->error($profile,$field->varname);?></div><?php
								echo '</div>';
								$disabledValues = array('----------'=> Array ( 'disabled' => 'disabled' ));
								echo $form->dropDownList($profile,$field->varname,Profile::range($field->range),array(
									'options' => $disabledValues,
									'class' => 'chosen chosen-container chosen-container-single	',
									'style' => 'width: 321px;font-size: 15.5px !important;padding: 3px 1px 1px 1px; ',
								));
							} elseif ($field->field_type=="TEXT") {
								?>	<div class= "help-inline error">	<?php echo $form->error($profile,$field->varname);?></div><?php
								echo '</div>';
								echo $form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
							} else {
								?>	<div class= "help-inline error">	<?php echo $form->error($profile,$field->varname);?></div><?php
								echo '</div>';
								echo $form->textField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
							}
							echo '</div>';
							?>

						<?php
						}
					}
					?>
					<div class="control-group">
						<div class="heading-box">
							<div class= "help-inline error"><?php echo $form->error($model,'email'); ?></div>
							<?php echo $form->labelEx($model,'email',array('class'=> 'block-label')); ?>
						</div>
						<?php echo $form->textField($model,'email'); ?>
					</div>
				</div>
				<div class="half-box">
					<div class="control-group">
						<div class="heading-box">
							<div id="success-image-username" class="flash-success" style="display:none;">
								<img class="" src="/images/check-mark.png" alt="" height="16px" width="16px"  style="float: right;"/>
							</div>
							<div class= "help-inline error"><?php echo $form->error($model,'username'); ?></div>
							<?php echo $form->labelEx($model,'username',array('class'=> 'block-label')); ?>
						</div>
						<?php echo $form->textField($model,'username'); ?>
					</div>
					<div class="control-group">
						<div class="heading-box">
							<div id="success-image-pass" class="flash-success" style="display:none;">
								<img class="" src="/images/check-mark.png" alt="" height="16px" width="16px"  style="float: right;"/>
							</div>
							<div class= "help-inline error"><?php echo $form->error($model,'password'); ?></div>
							<span class="help-text"><?php echo "Minimum password length 4 characters"; ?></span>
							<?php echo $form->labelEx($model,'password',array('class'=> 'block-label')); ?>
						</div>
						<?php echo $form->passwordField($model,'password'); ?>
					</div>
					<div class="control-group">
						<div class="heading-box">
							<div id="success-image" class="flash-success" style="display:none;">
								<img class="" src="/images/check-mark.png" alt="" height="16px" width="16px"  style="float: right;"/>
								<?php // echo $form->error($ghost,'verifyPassword',array('style'=>"color:darkblue;",'class'=>'flash-success'));?>
							</div>
							<div class= "help-inline error"><?php echo $form->error($model,'verifyPassword'); ?></div>
							<?php echo $form->labelEx($model,'verifyPassword',array('label'=>'Retype password','class'=> 'block-label')); ?>
						</div>
						<?php echo $form->passwordField($model,'verifyPassword'); ?>
					</div>
				</div>

			</div>
			<div class="mb20 text-right">
				<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'submit',
					'type'=>'primary',
					'label'=>'Next',
					'htmlOptions'=>array('class'=>'btn-gold'),
				)); ?>
			</div>
			<div class="buy-box-bottom">
				<img src="/images/comodo.png" alt=""/>
				<img class="payment-img" src="/images/payment.png" alt=""/>
			</div>

		<?php endif; ?>
		<?php $this->endWidget(); ?>
	</div>
</div>
<ul class="list-testimonials">
	<li class="testimonial">
		<i class="icon-quote"></i>
		<p class="testimonial--quote">I made 3% in 3 days since obtaining the courses, they are excellent...I appreciate what you have done for me, even in this little time, I feel like with hard work, I have a light at the end of the tunnel :)</p>
		<h3 class="testimonial--title">Ken T</h3>
	</li>
</ul>

<script>
	$(document).ready(function(){
		$('#are-you-real').html('<input class="hidden" value=1 name="areYouReal">');
	})

	$("#RegistrationForm_password").keyup(function(){
		if($("#RegistrationForm_password").val().length >= 4)
		{
			$("#RegistrationForm_password_em_").css("display","none");
			$(".help-text").css("display","none");
			$("#success-image-pass").css("display","block");
		}
		else{
			$("#RegistrationForm_password_em_").css("display","none");
		}
	});

	/*  $("#RegistrationForm_username").keyup(function(){
	 if($("#RegistrationForm_username").val().length >= 4)
	 {
	 $("#RegistrationForm_username_em_").css("display","none");
	 $("#success-image-username").css("display","block");
	 }
	 else{
	 $("#RegistrationForm_username_em_").css("display","none");
	 }
	 }); */

</script>
