<?php
$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");
// Add custom stylesheet - login.css
Yii::app()->clientScript->registerCssFile('/2ndskiesdev-project/live/courses.2ndskiesforex.com/css/login.css');
?>

<div class="login-form-wrap">

    <img src='http://2ndskiesforex.com/wp-content/themes/2ndSkies/library/images/login-logo.png' class='login-logo' />
    
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'login-form',
        'enableClientValidation'=>true,
        'htmlOptions'=>array('class'=>'form-login'),
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); /* @var $form TbActiveForm */ ?>
    
        <h1><?php echo UserModule::t("Login"); ?></h1>
    
        <?php if(Yii::app()->user->hasFlash('loginMessage') || Yii::app()->user->hasFlash('recoveryMessage')): ?>
        
            <div class="success">
                <?php echo Yii::app()->user->getFlash('loginMessage'); ?>
                <?php echo Yii::app()->user->getFlash('recoveryMessage'); ?>
            </div>
        
        <?php endif; ?>
    
        <?php echo $form->errorSummary($model, 'Following errors occurred'); ?>
        
        <div class="controls-row">
            <?php echo $form->textField($model,'username', array('class'=>'input-block-level', 'placeholder'=>'Username or email')); ?>
            <?php echo $form->error($model,'username'); ?>
        </div>
        
        <div class="controls-row">
            <?php echo $form->passwordField($model,'password', array('class'=>'input-block-level', 'placeholder'=>'Password')); ?>
            <?php echo $form->error($model,'password'); ?>
        </div>
        
        <div class="clearfix button-wrap">
        
            <?php echo $form->checkBoxRow($model,'rememberMe'); ?>
            
            <?php 
                $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'primary',
                    'size'=>'large',
                    'label'=>'Login',
                    'htmlOptions'=>array('class'=>'pull-right'),
                )); 
            ?>
            
        </div><!-- .button-wrap -->
        
        <ul class="action-links">
            <?php echo '<li>'.CHtml::link(UserModule::t("Register"),Yii::app()->createUrl('/courses/list')).'</li>'; ?>
            <?php echo '<li>'.CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl).'</li>'; ?>
        </ul>
            
        <div class="clearfix"></div>
    
    <?php $this->endWidget(); ?>

</div><!-- .login-form-wrap -->
