<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login"); ?>

<h1><?php echo $title; ?></h1>

<div class="form">
    <?php echo $content; ?>

</div><!-- yiiForm -->
<script>
    setTimeout(function () {
        window.location.href = "/user/login";
    }, 4000);
</script>