<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Change password");
$this->breadcrumbs=array(
	UserModule::t("Profile") => array('/user/profile'),
	UserModule::t("Change password"),
);
$this->menu=array(
	((UserModule::isAdmin())
		?array('label'=>UserModule::t('Manage Users'), 'url'=>array('/admin/user/manage'))
		:array()),
   // array('label'=>UserModule::t('List User'), 'url'=>array('/user')),
    array('label'=>UserModule::t('Profile'), 'url'=>array('/user/profile')),
    array('label'=>UserModule::t('Edit'), 'url'=>array('edit')),
    array('label'=>UserModule::t('Manage Subscriptions'), 'url'=>array('subscriptions')),
    array('label'=>UserModule::t('Logout'), 'url'=>array('/user/logout')),
);
?>



<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'changepassword-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); /* @var $form TbActiveForm */ ?>
	
	<h1 class="content-title"><?php echo UserModule::t("Change password"); ?></h1>
	<p class="hint">
		<?php echo UserModule::t("Minimal password length 4 symbols."); ?>
	</p>
	
	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
	
	<?php echo $form->errorSummary($model); ?>
	
	<?php echo $form->passwordFieldRow($model,'oldPassword'); ?>
	
	<?php echo $form->passwordFieldRow($model,'password'); ?>
	
	<?php echo $form->passwordFieldRow($model,'verifyPassword'); ?>
	
	<div class="control-group">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Save',
        )); ?>
	</div>

<?php $this->endWidget(); ?>
<!-- form -->