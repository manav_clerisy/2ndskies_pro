<?php
/* @var $subscriptions Subscriptions*/
Yii::app()->clientScript->registerScriptFile('/js/admin/topics/topic.js', CClientScript::POS_END);

$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Subscriptions");
$this->breadcrumbs=array(
    UserModule::t("Subscriptions"),
);

$this->menu  = array(
        array('label'=>UserModule::t('Profile'), 'url'=>array('/user/profile')),
        array('label'=>UserModule::t('Edit'), 'url'=>array('edit')),
        array('label'=>UserModule::t('Change password'), 'url'=>array('changepassword')),
        array('label'=>UserModule::t('Logout'), 'url'=>array('/user/logout')),
    );

$userNotify = UserNotifications::model()->findByAttributes(array('user_id'=>Yii::app()->user->id));

if(!empty($userNotify))
    $notify = $userNotify->notify;
else
    $notify = 0;

?>

<h1 class="content-title"><?php echo UserModule::t('Subscriptions'); ?></h1>

<div class="clearfix">
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'subscriptions-notify-form',
        'type'=>'horizontal',
    ));
    ?>
    <p>Notify me about new posts via email</p>
        <input type='radio' name='notify' value="0" <?php echo ($notify == 0) ? 'checked' : '' ?>> No <br>
        <input type='radio' name='notify' value="1" <?php echo ($notify == 1) ? 'checked' : '' ?>> As soon as it happens <br>
        <input type='radio' name='notify' value="2" <?php echo ($notify == 2) ? 'checked' : '' ?>> Daily digest <br>
    <br>
    <p><a href="#" id="notify" class="btn btn-success">Save</a></p>
    <?php $this->endWidget(); ?>


    <h5>Subscribed Topics</h5>

<?php
    if(!empty($subscriptions))
    { ?>

        <table id="e-cal-table" class="items table">
            <thead>
            <tr>
                <th>Topic</th><th></th>
            </tr>
            </thead>
            <tbody>
        <?php

            foreach($subscriptions as $subscription)
            {
                echo   '
                <tr>
                    <td>'.$subscription->getSubject().'</td>
                    <td><a href="#" name='.$subscription->id.' id="unsubscribe" class="btn btn-success btn-mini">Unsubscribe</a></td>
                </tr>
                        ';
            }

        ?>
            </tbody>
        </table>
    <?php }
    else echo "No results found";?>
</div>