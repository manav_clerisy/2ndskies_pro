<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Profile");
$this->breadcrumbs=array(
	UserModule::t("Profile")=>array('profile'),
	UserModule::t("Edit"),
);
$this->menu=array(
	((UserModule::isAdmin())
		?array('label'=>UserModule::t('Manage Users'), 'url'=>array('/admin/user/manage'))
		:array()),
   // array('label'=>UserModule::t('Users'), 'url'=>array('/user')),
    array('label'=>UserModule::t('Profile'), 'url'=>array('/user/profile')),
    array('label'=>UserModule::t('Change password'), 'url'=>array('changepassword')),
    array('label'=>UserModule::t('Manage Subscriptions'), 'url'=>array('subscriptions')),
    array('label'=>UserModule::t('Logout'), 'url'=>array('/user/logout')),
);
?>

<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
</div>
<?php endif; ?>

<?php
    $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'profile-form',
    	'type'=>'horizontal',
        'enableAjaxValidation'=>true,
        'htmlOptions' => array('enctype'=>'multipart/form-data'),
    ));
    /* @var $form TbActiveForm */
?>
	<h1 class="content-title"><?php echo UserModule::t('Edit profile'); ?></h1>
	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary(array($model,$profile)); ?>
	
	<?php echo $form->textFieldRow($model,'username'); ?>

	<?php echo $form->textFieldRow($model,'email'); ?>
	
	<?php echo $form->fileFieldRow($profile,'image'); ?>
        <div class="control-group controls">
            <?=CHtml::image($profile->getImageUrl(true), '')?>
        </div>
	<?php
		$profileFields=Profile::getFields();
		if ($profileFields) {
			foreach($profileFields as $field) {
				?>
					<?php echo $form->labelEx($profile,$field->varname, array('class'=>'control-label'));
					echo '<div class="control-group">';
					if ($widgetEdit = $field->widgetEdit($profile)) {
						echo '<div class="controls">';
						echo $widgetEdit;
						echo $form->error($profile,$field->varname); 
						echo '</div>';
					} elseif ($field->range) {
						echo '<div class="controls">';
						echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
						echo $form->error($profile,$field->varname); 
						echo '</div>';
					} elseif ($field->field_type=="TEXT") {
						echo '<div class="controls">';
						echo $form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
						echo $form->error($profile,$field->varname); 
						echo '</div>';
					} else {
						echo '<div class="controls">';
						echo $form->textField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
						echo $form->error($profile,$field->varname); 
						echo '</div>';
					}
					echo '</div>';
					?>
					
			<?php
			}
		}

	?>

	<div class="control-group">
		<?php echo $form->labelEx($profile,'signature', array('class'=>'control-label')); ?>
		<div class="controls">
	        <?php $this->widget('ext.redactorWidget.ImperaviRedactorWidget',array(
	            'model'=>$profile,
	            'attribute'=>'signature',
	            'name'=>'redactor',
	            'options'=>array(
	                'minHeight'=>100,
	                'convertVideoLinks'=> true,
	                'convertImageLinks'=> true,
	                //'fileUpload'=>Yii::app()->createUrl('topic/upload'),
	                'convertLinks'=>true
	            ),
	        ));?>
			<?php echo $form->error($profile,'signature'); ?>
		</div>
	</div>

<?php if (UserModule::isAdmin()) { ?>
<div class="control-group">
    <label for="badges" class="control-label">Badge</label>
    <div class="controls">
        <?php
            $badgesArr = CHtml::listData(Badges::model()->findAll(), 'id', 'title');
            foreach ($model->badges as $badge) {
                echo CHtml::dropDownList('badges['.$badge->id.']', $badge->id, $badgesArr, array('empty' => ''));
            }
        ?>
    </div>
</div>
<?php } ?>


	<div class="controls">
		<?php echo CHtml::submitButton($model->isNewRecord ? UserModule::t('Create') : UserModule::t('Save'), array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>
<!-- form -->
