<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Restore");
$this->breadcrumbs=array(
	UserModule::t("Login") => array('/user/login'),
	UserModule::t("Restore"),
);
?>





<?php echo CHtml::beginForm('', 'post', array('class' => 'form-login')); ?>
	<h1><?php echo UserModule::t("Restore"); ?></h1>
	<?php if(Yii::app()->user->hasFlash('recoveryMessage')): ?>
		<div class="alert alert-success">
			<?php echo Yii::app()->user->getFlash('recoveryMessage'); ?>
		</div>
	<?php else: ?>
	
	<p class="hint"><?php echo UserModule::t("Please enter your email addres."); ?></p>
	<div class="controls-row">
		<?php echo CHtml::activeTextField($form,'login_or_email',array('placeholder'=>'Username or Email', 'class'=>'input-block-level')) ?>
		<?php echo CHtml::errorSummary($form); ?>
	</div>

	<!-- <?php echo CHtml::submitButton(UserModule::t("Restore")); ?> -->
	<div class="clearfix">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
			'size'=>'large',
            'label'=>'Restore',
			'htmlOptions'=>array('class'=>'pull-left'),
        )); ?>
	</div>

<?php echo CHtml::endForm(); ?>
<!-- form -->
<?php endif; ?>