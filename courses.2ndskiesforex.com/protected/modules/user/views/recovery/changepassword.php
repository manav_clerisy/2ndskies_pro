<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Change password");
$this->breadcrumbs=array(
	UserModule::t("Login") => array('/user/login'),
	UserModule::t("Change password"),
);
?>

<h1><?php echo UserModule::t("Change password"); ?></h1>


<div class="form">
<?php $aform=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'registration-form',
	'type'=>'horizontal',
    'action'=>Yii::app()->createUrl('/user/recovery'),
	/*'enableAjaxValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	)*/
)); ?>

<?php if (Yii::app()->user->hasFlash('recoveryMessage')) echo Yii::app()->user->getFlash('recoveryMessage'); ?>

	<?php echo $aform->errorSummary($form, 'Following errors occurred'); ?>
	
	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
	
	<?php echo $aform->passwordFieldRow($form,'password', array('hint'=>'Minimal password length 4 symbols.')); ?>
	
	<?php echo $aform->passwordFieldRow($form,'verifyPassword'); ?>

    <?php echo CHtml::hiddenField('userId',$userId);?>

	<div class="control-group cleatfix">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'success',
            'label'=>'Save',
            'url' => Yii::app() -> createUrl('userTime/changePassword'),
            'ajaxOptions' => array('success' => 'function(data){
                window.location = "/user/login";
                }'),
        )); ?>
	</div>
	
<?php $this->endWidget(); ?>
</div><!-- form -->