<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
	'type'=>'horizontal',
	'enableAjaxValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary($model); ?>
	
	<div class="varname">
		<?php echo $form->textFieldRow($model,'varname',array('readonly'=>true, 'hint'=>UserModule::t("Allowed lowercase letters and digits."))); ?>
	</div>
	<div class="title">
		<?php echo $form->textFieldRow($model,'title',array('hint'=>UserModule::t('Field name on the language of "sourceLanguage".'))); ?>
	</div>
	<div class="field_type">
		<?php echo $form->textFieldRow($model,'field_type',array('readonly'=>true,'id'=>'field_type', 'hint'=>UserModule::t('Field type column in the database.'))); ?>
	</div>
	<div class="field_size">
		<?php echo $form->textFieldRow($model,'field_size',array('readonly'=>true, 'hint'=>UserModule::t('Field size column in the database.'))); ?>
	</div>
	<div class="field_size_min">
		<?php echo $form->textFieldRow($model,'field_size_min',array('hint'=>UserModule::t('The minimum value of the field (form validator).'))); ?>
	</div>
	<div class="required">
		<?php echo $form->DropDownListRow($model,'required',ProfileField::itemAlias('required'),array('hint'=>UserModule::t('Required field (form validator).'))); ?>
	</div>
	<div class="match">
		<?php echo $form->textFieldRow($model,'match',array('hint'=>UserModule::t("Regular expression (example: '/^[A-Za-z0-9\s,]+$/u')."))); ?>
	</div>
	<div class="range">
		<?php echo $form->textFieldRow($model,'range',array('hint'=>UserModule::t('Predefined values (example: 1;2;3;4;5 or 1==One;2==Two;3==Three;4==Four;5==Five).'))); ?>
	</div>
	<div class="error_message">
		<?php echo $form->textFieldRow($model,'error_message',array('hint'=>UserModule::t('Error message when you validate the form.'))); ?>
	</div>
	<div class="other_validator">
		<?php echo $form->textFieldRow($model,'other_validator',array('hint'=>UserModule::t('JSON string (example: {example}).',array('{example}'=>CJavaScript::jsonEncode(array('file'=>array('types'=>'jpg, gif, png'))))))); ?>
	</div>
	<div class="default">
		<?php echo $form->textFieldRow($model,'default',array('readonly'=>true, 'hint'=>UserModule::t('The value of the default field (database).'))); ?>
	</div>
	<div class="widget">
		<?php 
		list($widgetsList) = ProfileFieldController::getWidgets($model->field_type);
		echo $form->DropDownListRow($model,'widget',$widgetsList, array('id'=>'widgetlist','hint'=>UserModule::t('Widget name.'))); ?>
	</div>
	<div class="widgetparams">
		<?php echo $form->textFieldRow($model,'widgetparams',array('id'=>'widgetparams','hint'=>UserModule::t('JSON string (example: {example}).',array('{example}'=>CJavaScript::jsonEncode(array('param1'=>array('val1','val2'),'param2'=>array('k1'=>'v1','k2'=>'v2'))))))); ?>
	</div>
	<div class="position">
		<?php echo $form->textFieldRow($model,'position',array('hint'=>UserModule::t('Display order of fields.'))); ?>
	</div>
	<div class="visible">
		<?php echo $form->DropDownListRow($model,'visible',ProfileField::itemAlias('visible')); ?>
	</div>
	<div class="controls">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Save',
        )); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<div id="dialog-form" title="<?php echo UserModule::t('Widget parametrs'); ?>">
	<form>
	<fieldset>
		<label for="name">Name</label>
		<input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all" />
		<label for="value">Value</label>
		<input type="text" name="value" id="value" value="" class="text ui-widget-content ui-corner-all" />
	</fieldset>
	</form>
</div>
