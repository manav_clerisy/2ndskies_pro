<div class="wide form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
	'type'=>'horizontal',
)); ?>
	
	<?php echo $form->textFieldRow($model,'id'); ?>
	
	<?php echo $form->textFieldRow($model,'varname'); ?>
	
	<?php echo $form->textFieldRow($model,'title'); ?>
	
	<?php echo $form->dropDownListRow($model,'field_type',ProfileField::itemAlias('field_type')); ?>
	
	<?php echo $form->textFieldRow($model,'field_size'); ?>
	
	<?php echo $form->textFieldRow($model,'field_size_min'); ?>
	
	<?php echo $form->dropDownListRow($model,'required',ProfileField::itemAlias('required')); ?>
	
	<?php echo $form->textFieldRow($model,'match'); ?>
	
	<?php echo $form->textFieldRow($model,'range'); ?>
	
	<?php echo $form->textFieldRow($model,'error_message'); ?>
	
	<?php echo $form->textFieldRow($model,'other_validator'); ?>
	
	<?php echo $form->textFieldRow($model,'default'); ?>
	
	<?php echo $form->textFieldRow($model,'widget'); ?>
	
	<?php echo $form->textFieldRow($model,'widgetparams'); ?>
	
	<?php echo $form->textFieldRow($model,'position',ProfileField::itemAlias('visible')); ?>
	
	<?php echo $form->dropDownListRow($model,'visible'); ?>
	
	<div class="controls">
		<?php echo CHtml::submitButton(UserModule::t('Search'), array('class'=>'btn btn-success')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form --> 