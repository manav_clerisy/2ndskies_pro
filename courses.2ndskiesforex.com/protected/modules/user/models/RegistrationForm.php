<?php
/**
 * RegistrationForm class.
 * RegistrationForm is the data structure for keeping
 * user registration form data. It is used by the 'registration' action of 'UserController'.
 */
class RegistrationForm extends User {
	public $verifyPassword;
	public $verifyCode;

	public function rules() {
		$rules = array(
			array('username, password, verifyPassword, email', 'required'),
			array('username', 'length', 'max'=>20, 'min' => 3,'message' => UserModule::t("Incorrect username (length between 3 and 20 characters).")),
			array('password', 'length', 'max'=>128, 'min' => 4,'message' => "Minimal password length 4 characters."),
			array('email', 'email','message' =>'Please enter a valid email address.'),
			array('username', 'unique', 'message' => 'This username already exists. If you have registered before, please <a href="/user/login">login</a>. If you are having problems registering, please <a href="mailto:chris@2ndskiesforex.com">contact support</a>'),
			array('email', 'unique', 'message' => UserModule::t("This user's email address already exists. If you have already registered in the system - please check yor email and activate your account or login using 'Login' button.")),
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_. ]+$/u','message' => UserModule::t("Incorrect symbols (A-z0-9. _).")),
			//array('password,verifyPassword','valid_number','compareAttribute'=>'verifyPassword'),	
		);
        array_push($rules,array('verifyPassword', 'compare', 'compareAttribute'=>'password', 'message' => UserModule::t("Retype Password is incorrect.")));
		return $rules;
	}
}
