<?php
use urmaul\gravacage\Gravacage;
/**
 * @property integer $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $image
 * @property string $website
 * @property string $location
 * @property string $occupation
 * @property string $interests
 * @property string $birthday
 * @property string $signature
 * @property string $billing_address
 * 
 * @property-read string $imageUrl Avatar url
 * 
 * @property AttachmentBehavior $attachmentBehavior
 */
class Profile extends UActiveRecord
{
	/**
	 * The followings are the available columns in table 'profiles':
	 * @var integer $user_id
	 * @var boolean $regMode
	 */
	public static $regMode = false;
	
	private static $_model;
	private static $_modelReg;
	private static $_rules = array();


    public function __get($name)
    {
        if ($name == 'birthday')
            return $this->getBirthday();
        else
            return parent::__get($name);
    }
    public function beforeSave()
    {
        if ($this->birthday == '')
            $this->birthday = new CDbExpression('null');

        return parent::beforeSave();
    }





    /**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
            return Yii::app()->getModule('user')->tableProfiles;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
        if (!self::$_rules) {
			$required = array();
			$numerical = array();
			$float = array();		
			$decimal = array();
			$rules = array();
			
			$model=self::getFields();
			
			foreach ($model as $field) {
				$field_rule = array();
				if ($field->required==ProfileField::REQUIRED_YES_NOT_SHOW_REG||$field->required==ProfileField::REQUIRED_YES_SHOW_REG)
					array_push($required,$field->varname);
				if ($field->field_type=='FLOAT')
					array_push($float,$field->varname);
				if ($field->field_type=='DECIMAL')
					array_push($decimal,$field->varname);
				if ($field->field_type=='INTEGER' || $field->field_type=='TINYINT')
					array_push($numerical,$field->varname);
				if ($field->field_type=='VARCHAR'||$field->field_type=='TEXT') {
					$field_rule = array($field->varname, 'length', 'max'=>$field->field_size, 'min' => $field->field_size_min);
					if ($field->error_message) $field_rule['message'] = UserModule::t($field->error_message);
					array_push($rules,$field_rule);
				}
				if ($field->other_validator) {
					if (strpos($field->other_validator,'{')===0) {
						$validator = (array)CJavaScript::jsonDecode($field->other_validator);
						foreach ($validator as $name=>$val) {
							$field_rule = array($field->varname, $name);
							$field_rule = array_merge($field_rule,(array)$validator[$name]);
							if ($field->error_message) $field_rule['message'] = UserModule::t($field->error_message);
							array_push($rules,$field_rule);
						}
					} else {
						$field_rule = array($field->varname, $field->other_validator);
						if ($field->error_message) $field_rule['message'] = UserModule::t($field->error_message);
						array_push($rules,$field_rule);
					}
				} elseif ($field->field_type=='DATE') {
                    if ($field->required)
                        $field_rule = array($field->varname, 'date', 'format' => array('yyyy-mm-dd'));
                    else
                        $field_rule = array($field->varname, 'date', 'format' => array('yyyy-mm-dd','0000-00-00'), 'allowEmpty'=>true);

					if ($field->error_message) $field_rule['message'] = UserModule::t($field->error_message);
					array_push($rules,$field_rule);
				}
				if ($field->match) {
					$field_rule = array($field->varname, 'match', 'pattern' => $field->match);
					if ($field->error_message) $field_rule['message'] = UserModule::t($field->error_message);
					array_push($rules,$field_rule);
				}
				if ($field->range) {
					$field_rule = array($field->varname, 'in', 'range' => self::rangeRules($field->range));
					if ($field->error_message) $field_rule['message'] = UserModule::t($field->error_message);
					array_push($rules,$field_rule);
				}
			}
			
			array_push($rules,array(implode(',',$required), 'required'));
			array_push($rules,array(implode(',',$numerical), 'numerical', 'integerOnly'=>true));
			array_push($rules,array(implode(',',$float), 'type', 'type'=>'float'));
			array_push($rules,array(implode(',',$decimal), 'match', 'pattern' => '/^\s*[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?\s*$/'));
            
            // Custom rules
			array_push($rules, array('image', 'file', 'types'=>'jpg, gif, png', 'maxSize' => 512 * 1024, 'allowEmpty' => true, 'safe' => true));
			array_push($rules, array('signature', 'filter', 'filter' => array(new CHtmlPurifier(), 'purify')));

			self::$_rules = $rules;
		}
		return self::$_rules;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		$relations = array(
			'user'=>array(self::HAS_ONE, 'User', 'id'),
		);
		if (isset(Yii::app()->getModule('user')->profileRelations)) $relations = array_merge($relations,Yii::app()->getModule('user')->profileRelations);
		return $relations;
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		$labels = array(
			'user_id' => UserModule::t('User ID'),
		);
		$model=self::getFields();
		
		foreach ($model as $field)
			$labels[$field->varname] = ((Yii::app()->getModule('user')->fieldsMessage)?UserModule::t($field->title,array(),Yii::app()->getModule('user')->fieldsMessage):UserModule::t($field->title));
		
		return array(
            'image' => 'Image',
        ) + $labels;

	}

	private function rangeRules($str) {
		$rules = explode(';',$str);
		for ($i=0;$i<count($rules);$i++)
			$rules[$i] = current(explode("==",$rules[$i]));
		return $rules;
	}
	
	static public function range($str,$fieldValue=NULL) {
		$rules = explode(';',$str);
		$array = array();
		for ($i=0;$i<count($rules);$i++) {
			$item = explode("==",$rules[$i]);
			if (isset($item[0])) $array[$item[0]] = ((isset($item[1]))?$item[1]:$item[0]);
		}
		if (isset($fieldValue)) 
			if (isset($array[$fieldValue])) return $array[$fieldValue]; else return '';
		else
			return $array;
	}
	
	public function widgetAttributes() {
		$data = array();
		$model=self::getFields();
		
		foreach ($model as $field) {
			if ($field->widget) $data[$field->varname]=$field->widget;
		}
		return $data;
	}
	
	public function widgetParams($fieldName) {
		$data = array();
		$model=self::getFields();
		
		foreach ($model as $field) {
			if ($field->widget) $data[$field->varname]=$field->widgetparams;
		}
		return $data[$fieldName];
	}
	
	public static function getFields() {
		if (self::$regMode) {
			if (!self::$_modelReg)
				self::$_modelReg=ProfileField::model()->forRegistration()->findAll();
			return self::$_modelReg;
		} else {
			if (!self::$_model)
				self::$_model=ProfileField::model()->forOwner()->findAll();
			return self::$_model;
		}
	}

    public function afterSave() {
        if (get_class(Yii::app())=='CWebApplication'&&Profile::$regMode==false) {
            Yii::app()->user->updateSession();
        }
        
        if ($this->image instanceof EUploadedImage) {
            $image = $this->image;
            /* @var $image EUploadedImage */
            
            $this->setImageName($image->getName());
            if (!$image->saveAs($this->getImagePath(true)))
                //throw new CException('Could not save user image as ' . $this->getImagePath(true));
                throw new CException('Could not save the image');
            $this->save(false, array('image'));
        }
        
        return parent::afterSave();
    }

    /**
     * Returns user image url.
     * @param boolean $fullSize
     * @return string
     */
    public function getImageUrl($fullSize = false)
    {
        return isset($this->image)
            ? '/' . $this->getImageRelativePath($fullSize)
            : Gravacage::forEmail($this->user->email)->imageUrl();
            //: 'http://gravacage.devk.itechcraft.com/' . ($fullSize ? 600 : 100) . '/' . ($this->user_id % 952) . '.jpg';

    }
    
    /**
     * @return string
     */
    public function getImagePath($fullSize = false)
    {
        return Yii::getPathOfAlias('web'). '/' . $this->getImageRelativePath($fullSize);
    }
    
    /**
     * @return string
     */
    protected function getImageRelativePath($fullSize = false)
    {
        $dir = $fullSize ? 'user-full' : 'user-thumb';
        
        return 'uploads/' . $dir . '/' . $this->image;
    }
    
    /**
     * Extends setAttributes to handle active date fields
     *
     * @param $values array
     * @param $safeOnly boolean
     */
    public function setAttributes($values, $safeOnly = true)
    {
        if (isset($values['image'])) {
            // Process avatar uploading
            $image = $this->uploadedImage();
            if ($image !== null)
                $values['image'] = $image;
            else
                unset($values['image']);
        }

        parent::setAttributes($values, $safeOnly);
    }
    
    /**
     * Returns uploaded image handler instance
     * @return EUploadedImage
     */
    protected function uploadedImage()
    {
        Yii::import('ext.EUploadedImage');

        if(! $this->validate())return null;

        $image = EUploadedImage::getInstance($this, 'image');

        if ($image !== null) {
            $image->maxWidth = 500;
            $image->maxHeight = 500;

            $image->thumb = array(
                'maxWidth' => 100,
                'maxHeight' =>100,
                'dir' => '../user-thumb',
                'prefix' => '',
            );
        }
        
        return $image;
    }


    /**
     * Sets proper image name for this file.
     */
    public function setImageName($originalName)
    {
        if (($pos=strrpos($originalName,'.')) !== false)
			$ext = (string) substr($originalName, $pos+1);
		else
			$ext = 'jpg';
        
        $this->image = $this->user_id . '.' . $ext;
    }

    public function getAttribute($name)
    {
        /*if ($name == 'birthday') {
            if ($this->$name == '0000-00-00')
              return '';
        }*/
        return parent::getAttribute($name);
    }

    /**
     * Welcome to the KOSTYL' world
     *
     * @return mixed|string
     */
    public function getBirthday()
    {
        if ($this->getAttribute('birthday') == '0000-00-00')
            return '';

        return $this->getAttribute('birthday');
    }
}