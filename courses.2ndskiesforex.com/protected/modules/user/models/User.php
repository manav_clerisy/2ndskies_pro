<?php

/**
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $activkey
 * @property integer $createtime
 * @property integer $lastvisit
 * @property integer $role
 * @property integer $status
 * @property timestamp $create_at
 * @property timestamp $lastvisit_at
 * @property string $uniqid
 * @property integer $ontraportId
 * @property MakeUniqueBehavior $makeUniqueBehavior
 * @method void makeUnique($attribute, $postfix = '') adds number to the end of attribute login till it is unique.
 * @method void makeUniqueFile($attribute) adds number to the end of attribute login till it is unique.
 */
class User extends CActiveRecord
{
	const STATUS_NOACTIVE=0;
	const STATUS_ACTIVE=1;
	const STATUS_BANNED=-1;

    public $newPassword = '';

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return Yii::app()->getModule('user')->tableUsers;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.CConsoleApplication
		return ((get_class(Yii::app())=='CConsoleApplication' || (get_class(Yii::app())!='CConsoleApplication' && Yii::app()->getModule('user')->isAdmin()))?array(
			array('username', 'length', 'max'=>20, 'min' => 3,'message' => UserModule::t("Incorrect username (length between 3 and 20 characters).")),
			array('password', 'length', 'max'=>128, 'min' => 4,'message' => "Minimal password length 4 characters."),
			array('email', 'email'),
            array('newPassword', 'safe'),
			array('username', 'unique', 'message' => UserModule::t("This user's name already exists.")),
			array('email', 'unique', 'message' => UserModule::t("This user's email address already exists.")),
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_ .]+$/u','message' => UserModule::t("Incorrect symbols (A-z0-9_ .).")),
			array('status', 'in', 'range'=>array(self::STATUS_NOACTIVE,self::STATUS_ACTIVE,self::STATUS_BANNED)),
			array('role', 'in', 'range'=>array(0,1,2)),
            array('create_at', 'default', 'value' => date('Y-m-d H:i:s'), 'setOnEmpty' => true, 'on' => 'insert'),
            array('lastvisit_at', 'default', 'value' => '0000-00-00 00:00:00', 'setOnEmpty' => true, 'on' => 'insert'),
			array('username, email, role, status', 'required'),
			array('role, status', 'numerical', 'integerOnly'=>true),
			array('id, username, password, email, activkey, create_at, lastvisit_at, role, status, ontraportId', 'safe', 'on'=>'search'),
		):((Yii::app()->user->id==$this->id)?array(
			array('username, email', 'required'),
			array('username', 'length', 'max'=>20, 'min' => 3,'message' => UserModule::t("Incorrect username (length between 3 and 20 characters).")),
			array('email', 'email'),
			array('username', 'unique', 'message' => UserModule::t("This user's name already exists.")),
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_ .]+$/u','message' => UserModule::t("Incorrect symbols (A-z0-9_ .).")),
			array('email', 'unique', 'message' => UserModule::t("This user's email address already exists.")),

		):array()));
	}

	public function behaviors()
	{
        return array(
            'makeUniqueBehavior' => array(
                'class' => 'ext.behaviors.MakeUniqueBehavior',
            ),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
        return Yii::app()->getModule('user')->relations + array(
            'profile' => array(self::HAS_ONE, 'Profile', 'user_id'),
            'userCourse' => array(self::HAS_MANY, 'UserCourse', 'user_id', 'index' => 'course_id'),
            'subscription' => array(self::BELONGS_TO, 'UserCourse', 'id'),
            'badges' => array(self::MANY_MANY, 'Badges', 'user_badge(user_id, badge_id)'),
            'roles'=>array(self::HAS_ONE,'User','id'),
            'userBadges' => array(self::HAS_MANY, 'UserBadge', 'user_id'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => UserModule::t("Id"),
			'username'=>UserModule::t("Username"),
			'password'=>UserModule::t("Password"),
			'verifyPassword'=>UserModule::t("Retype Password"),
			'email'=>UserModule::t("E-mail"),
			'verifyCode'=>UserModule::t("Verification Code"),
			'activkey' => UserModule::t("Activation key"),
			'createtime' => UserModule::t("Registration date"),
			'create_at' => UserModule::t("Registration date"),
			
			'lastvisit_at' => UserModule::t("Last visit"),
			'role' => UserModule::t("Role"),
			'status' => UserModule::t("Status"),
			'newPassword' => UserModule::t("Set Password"),
                        'ontraportId' => 'Ontraport Id'
		);
	}
	
	public function scopes()
    {
        return array(
            'active'=>array(
                'condition'=>'status='.self::STATUS_ACTIVE,
            ),
            'notactive'=>array(
                'condition'=>'status='.self::STATUS_NOACTIVE,
            ),
            'banned'=>array(
                'condition'=>'status='.self::STATUS_BANNED,
            ),
            'role'=>array(
                'condition'=>'role=1',
            ),
            'notsafe'=>array(
            	'select' => 'id, username, password, email, activkey, create_at, lastvisit_at, role, status',
            ),
        );
    }
	
	public function defaultScope()
    {
        return CMap::mergeArray(Yii::app()->getModule('user')->defaultScope,array(
            'alias'=>'user',
            //'select' => 'user.id, user.username, user.email, user.create_at, user.lastvisit_at, user.role, user.status',
        ));
    }
	
	public static function itemAlias($type,$code=NULL) {
		$_items = array(
			'UserStatus' => array(
				self::STATUS_NOACTIVE => UserModule::t('Not active'),
				self::STATUS_ACTIVE => UserModule::t('Active'),
				self::STATUS_BANNED => UserModule::t('Banned'),
			),
			'AdminStatus' => array(
				'0' => UserModule::t('User'),
				'1' => UserModule::t('Admin'),
                '2' => UserModule::t('EconomicEventsEditor'),
			),
		);
		if (isset($code))
			return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
		else
			return isset($_items[$type]) ? $_items[$type] : false;
	}
	
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('subscription');
        
        $criteria->compare('user.id',$this->id);
        $criteria->compare('username',$this->username,true);
        $criteria->compare('password',$this->password);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('activkey',$this->activkey);
        $criteria->compare('create_at',$this->create_at);
        $criteria->compare('lastvisit_at',$this->lastvisit_at);
        $criteria->compare('role',$this->role);
        $criteria->compare('status',$this->status);
        $criteria->compare('ontraportId',$this->ontraportId);
        
        return new CActiveDataProvider(get_class($this), array(
            'criteria'=>$criteria,
        	'pagination'=>array(
				'pageSize'=>Yii::app()->getModule('user')->user_page_size,
			),
        ));
    }

    public function getCreatetime() {
        return strtotime($this->create_at);
    }

    public function setCreatetime($value) {
        $this->create_at=date('Y-m-d H:i:s',$value);
    }

    public function getLastvisit() {
        return strtotime($this->lastvisit_at);
    }

    public function setLastvisit($value) {
        $this->lastvisit_at=date('Y-m-d H:i:s',$value);
    }

    public function afterSave() {
        if (get_class(Yii::app())=='CWebApplication'&&Profile::$regMode==false) {
            Yii::app()->user->updateSession();
        }
        return parent::afterSave();
    }

    /**
     * @param $courseId course id
     * @return bool if user subscribed on course
     */
    public function isSubscribedOnCourse($courseId)
    {
        Yii::trace('Checking user #' . $this->id . ' is subscribed on course #' . $courseId, 'application.assertions');
        
        if (Yii::app()->user->isAdmin())
            return true;

        return
            isset($this->userCourse[$courseId]);
    }

    /**
     * Returns amount of unread messages for current authenticated user
     * if $fromUserId > 0 get messages only from certain user
     *
     * @param int $fromUserId user id
     * @return string
     */
    public function getAmountOfUnreadMessages($fromUserId = 0)
    {
        $arr = array(
            'user_to' => Yii::app()->user->id,
            'is_read' => 0,
        );

        if ($fromUserId > 0)
            $arr['user_from'] = $fromUserId;

        $count = Messages::model()->countByAttributes($arr);

        if ($count == 0)
            return '';

        return $count;
    }


    /**
     * Checks if user is subscribed on course and this course has calendar opened by admin
     * @param null $courseId
     * @return bool
     */
    public function getHasAccessToCalendarOfCourse($courseId = null)
    {
        if ($courseId != null)
            $courseIds[] = $courseId;
        else
            $courseIds = Yii::app()->db->createCommand()
                        ->select('id')
                        ->from( Courses::model()->tableName() )
                        ->queryColumn();


        foreach ($courseIds as $id) {
            $calendarIsPublic = Yii::app()->user->model()->isSubscribedOnCourse($id) &&
                                Courses::model()->findByPk($id)->isCalendarPublic;
            if ($calendarIsPublic)
                return true;
        }

        return false;
    }

    public function getBadgeTitles()
    {
        $res = array();

        foreach ($this->badges as $badge) {
            $res[] = $badge->title;
        }

        return implode('<br>', $res);
    }

    public function sendActivationEmailForNewUser()
    {
        $activation_url = Yii::app()->createAbsoluteUrl(
            '/user/activation/activation',
            array(
                "activkey" => $this->activkey,
                "email" => $this->email
            )
        );

        Apostle::setup(Yii::app()->params->apostleKey);

        $mail = new Apostle\Mail('registration');

        $mail->from = Yii::app()->params->adminEmail;
        $mail->email = $this->email;
        $mail->__set('activation_url', $activation_url);
        $mail->replyTo = Yii::app()->params->adminEmail;

        $mail->deliver();
    }

/*
    public function deleteUserIfRegistrationFails()
    {
        $newUserId = Yii::app()->session['newUserId'];
        $profile = Profile::model()->findByPk($newUserId);
        $profile->delete();

        $user = User::model()->findByPk($newUserId);
        $user->delete();

        $badge = UserBadge::model()->findByAttributes(array('user_id'=>$newUserId));
        $badge->delete();
    }*/


}