<?php

class RecoveryController extends Controller
{
    public $defaultAction = 'recovery';

    /**
     * Recovery password
     */
    public function actionRecovery()
    {
        //die('<pre>'.var_export($_REQUEST).'</pre>');
        $form = new UserRecoveryForm;
        if (Yii::app()->user->id) {
            $this->redirect(Yii::app()->controller->module->returnUrl);
        } else {
            $email = ((isset($_GET['email'])) ? $_GET['email'] : '');
            $activkey = ((isset($_GET['activkey'])) ? $_GET['activkey'] : '');
            if ($email && $activkey) {
                $form2 = new UserChangePassword;
                $find = User::model()->notsafe()->findByAttributes(array('email' => $email));
                if (isset($find) && $find->activkey == $activkey) {
                    $this->render('changepassword', array('form' => $form2,'userId' =>$find->id));
                } else {
                    Yii::app()->user->setFlash('recoveryMessage', UserModule::t("Incorrect recovery link."));
                    $this->redirect(Yii::app()->controller->module->recoveryUrl);
                }
            } elseif (isset($_POST['UserChangePassword'])) {
                $form2 = new UserChangePassword;
                $form2->attributes = $_POST['UserChangePassword'];
                $find = User::model()->findByPk($_POST['userId']);
                if ($form2->validate()) {
                    if (isset($_POST['userId'])) {

                        $find->password = HashHelper::phpbbHash($form2->password);
                        $find->activkey = HashHelper::phpbbHash(microtime() . $form2->password);
                        if ($find->status == 0) {
                            $find->status = 1;
                        }
                        $find->save();
                        Yii::app()->user->setFlash('recoveryMessage', UserModule::t("New password is saved."));
                        $this->redirect(Yii::app()->createUrl('/user/login'));
                    }
                }
                $this->render('changepassword', array('form' => $form2,'userId' =>$find->id));
            } else {
                if (isset($_POST['UserRecoveryForm'])) {
                    $form->attributes = $_POST['UserRecoveryForm'];
                    if ($form->validate()) {
                        $user = User::model()->notsafe()->findbyPk($form->user_id);
                        if(!isset($user->activkey) || empty($user->activkey))
                        {
                            $user->activkey = md5(microtime());
                            $user->save();
                        }

                        $activation_url = $_SERVER['HTTP_HOST'] . $this->createUrl(implode(Yii::app()->controller->module->recoveryUrl), array("activkey" => $user->activkey, "email" => $user->email));

                        Apostle::setup(Yii::app()->params->apostleKey);

                        $mail = new Apostle\Mail('recovery');

                        $mail->from = Yii::app()->params->adminEmail;
                        $mail->email = $user->email;
                        $mail->__set('activation_url', $activation_url);
                        $mail->replyTo = Yii::app()->params->adminEmail;

                        $mail->deliver();

                        Yii::app()->user->setFlash('recoveryMessage', UserModule::t("Please check your email. Instructions were sent to your email. If you don't receive this email within 30 minutes, be sure to check your spam inbox. If you still have not received the message, please contact support at chris@2ndskiesforex.com with your username and email address you signed up with."));
                        $this->refresh();
                    }
                }
                $this->render('recovery', array('form' => $form));
            }
        }
    }

}