<?php

class ProfileController extends Controller
{
	public $defaultAction = 'profile';
	public $layout='//layouts/column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;
    
    
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    
	/**
	 * Shows a particular model.
	 */
	public function actionProfile()
	{
		$model = $this->loadUser();
	    $this->render('profile',array(
	    	'model'=>$model,
			'profile'=>$model->profile,
	    ));
	}

    public function actionUserProfile($id)
    {
        $model = User::model()->findbyPk($id);
        if($model===null)
        throw new CHttpException(404,'The requested user does not exist.');

        $this->render('profile',array(
            'model'=>$model,
            'profile'=>$model->profile,
        ));
    }


	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionEdit()
	{
		$model = $this->loadUser();
		$profile=$model->profile;

		// ajax validator
		if(isset($_POST['ajax']) && $_POST['ajax']==='profile-form')
		{
			echo UActiveForm::validate(array($model,$profile));
			Yii::app()->end();
		}

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
                        if(isset($_FILES['Profile']['name']['image']) && $_FILES['Profile']['name']['image'] == ''){
                            $img = $profile->image;
                        }
                        $profile->attributes=$_POST['Profile'];
                        if(($profile->birthday == "0000-00-00") || (!$profile->birthday))
                            $profile->birthday = null;
                        if($model->validate()&&$profile->validate()) {
                            $model->save();
                            $profile->save();
                            if(isset($img) && $img != '')
                                $update = Yii::app()->db->createCommand()
                                ->update('profiles', array('image'=>$img),
                                'user_id=:id',array(':id'=>$model->id));
                            if (isset($_POST['badges'])) {
                                foreach ($_POST['badges'] as $id => $val) {
                                    $ub = UserBadge::model()->findByAttributes(array('user_id' => $model->id, 'badge_id' => $id));
                                    $ub->badge_id = $val;
                                    $ub->set_manually = 1;
                                    $ub->save(false);
                                }
                            }
                            $fields['firstname'] = $profile->first_name;
                            $fields['lastname'] = $profile->last_name;
                            //$fields['state'] = $userProfile->billing_state; this is not compatible with ontraport states
                            $fields['zip'] = $profile->billing_zip;
                            $fields['city'] = $profile->billing_city;
                            $fields['country'] = $this->countriesListOntraport($profile->location);
                            $fields['address'] = $profile->billing_address;
                            $fields['id'] = $model->ontraportId;
                            $fields['objectID'] = 0;
                            $update = $this->curlPut('https://api.ontraport.com/1/objects', $fields, array(0 => 'Api-Appid: '.Yii::app()->params->ontraportAppId,1 => 'Api-Key: '.Yii::app()->params->ontraportAppKey));
                            Yii::app()->user->setFlash('profileMessage',UserModule::t("Changes is saved."));
                            $this->redirect(array('/user/profile'));
			} else $profile->validate();
		}

		$this->render('edit',array(
			'model'=>$model,
			'profile'=>$profile,
		));
	}
	
	/**
	 * Change password
	 */
	public function actionChangepassword() {
		$model = new UserChangePassword;
		if (Yii::app()->user->id) {
			
			// ajax validator
			if(isset($_POST['ajax']) && $_POST['ajax']==='changepassword-form')
			{
				echo UActiveForm::validate($model);
				Yii::app()->end();
			}
			
			if(isset($_POST['UserChangePassword'])) {
                $model->attributes=$_POST['UserChangePassword'];
                if($model->validate()) {
                    $new_password = User::model()->notsafe()->findbyPk(Yii::app()->user->id);
                    $new_password->password = HashHelper::phpbbHash($model->password);
                    $new_password->activkey = HashHelper::phpbbHash(microtime().$model->password);
                    $new_password->save();
                    Yii::app()->user->setFlash('profileMessage',UserModule::t("New password is saved."));
                    $this->redirect(array("profile"));
                }
			}
			$this->render('changepassword',array('model'=>$model));
	    }
	}

    public function actionSubscriptions()
    {
        $subscriptions = Subscriptions::model()->getSubscriptions();

        $this->render('subscriptions', array(
            'subscriptions'=>$subscriptions
        ));

    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the primary key value. Defaults to null, meaning using the 'id' GET variable
	 */
	public function loadUser()
	{
		if($this->_model===null)
		{
			if(Yii::app()->user->id)
				$this->_model=Yii::app()->controller->module->user();
			if($this->_model===null)
				$this->redirect(Yii::app()->controller->module->loginUrl);
		}
		return $this->_model;
	}
}