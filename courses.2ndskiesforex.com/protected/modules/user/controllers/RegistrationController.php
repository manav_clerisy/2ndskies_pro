<?php

class RegistrationController extends Controller
{
    public $defaultAction = 'registration';

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
                'testLimit'=>1,
            ),
        );
    }

    /**
     * Registration user
     */
    public function actionRegistration() {
        Profile::$regMode = true;
        $model = new RegistrationForm;
        $profile= new Profile;
        $code = new DiscountCode;
        // ajax validator
        
        if(isset($_POST['ajax']) && $_POST['ajax']==='registration-form')
        {
            $code->setScenario('registration');
            echo UActiveForm::validate(array($model,$profile,$code));
            
            Yii::app()->end();
        }

        if (Yii::app()->user->id) {
            $this->redirect(Yii::app()->controller->module->profileUrl);
        } else {
            if(isset($_POST['RegistrationForm'])) {
                $this->writeLog();

                if(!isset($_POST['areYouReal']) || $_POST['areYouReal']!=1)
                {
                    echo 'The system think that you are not real person. I am sorry.';
                    Yii::app()->end();
                }

                $model->attributes=$_POST['RegistrationForm'];
                $profile->attributes=((isset($_POST['Profile'])?$_POST['Profile']:array()));
                if($model->validate()&&$profile->validate())
                {
                    $soucePassword = $model->password;
                    $model->activkey = md5(microtime());
                    $model->password = HashHelper::phpbbHash($model->password);
                    $model->role=0;
                    $model->status=((Yii::app()->controller->module->activeAfterRegister)?User::STATUS_ACTIVE:User::STATUS_NOACTIVE);
                    $fields['firstname'] = $profile->first_name;
                    $fields['lastname'] = $profile->last_name;
                    $fields['email'] = $model->email;
                    $fields['country'] = $profile->location;
                    $fields['objectID'] = 0;
                    $contact = $this->curlPost('https://api.ontraport.com/1/objects', $fields,array(0 => 'Api-Appid: '.Yii::app()->params->ontraportAppId,1 => 'Api-Key: '.Yii::app()->params->ontraportAppKey));

                    if(isset($contact->data->id) && $contact->data->id != ''){
                        $model->ontraportId = $contact->data->id;
                    }
                    if ($model->save(false)) {
                        $profile->user_id=$model->id;

                        if(($profile->birthday == "0000-00-00") || (!$profile->birthday))
                            $profile->birthday = null;

                        $profile->save();

                        // auto assign to new user Initiate badge
//                        $ub = new UserBadge();
//                        $ub->user_id  = $model->id;
//                        $ub->badge_id = Badges::model()->findByAttributes(array('minimum_posts' => 0))->getAttribute('id');
//                        $ub->set_manually = 0;
//                        $ub->save(false);

                        if ((Yii::app()->controller->module->loginNotActiv||(Yii::app()->controller->module->activeAfterRegister&&Yii::app()->controller->module->sendActivationMail==false))&&Yii::app()->controller->module->autoLogin) {
                            $identity=new UserIdentity($model->username,$soucePassword);
                            $identity->authenticate();
                            Yii::app()->user->login($identity,0);
                            $this->redirect(Yii::app()->controller->module->returnUrl);
                        } else {
                            if (!Yii::app()->controller->module->activeAfterRegister&&!Yii::app()->controller->module->sendActivationMail) {
                                Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration. Contact Admin to activate your account."));
                            } elseif(Yii::app()->controller->module->activeAfterRegister&&Yii::app()->controller->module->sendActivationMail==false) {
                                Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration. Please {{login}}.",array('{{login}}'=>CHtml::link(UserModule::t('Login'),Yii::app()->controller->module->loginUrl))));
                            } elseif(Yii::app()->controller->module->loginNotActiv) {
                                Yii::app()->user->setFlash('registration',UserModule::t("Thank you for your registration. Please check your email or login."));
                            } else {
                                $url = Yii::app()->createUrl('/courses/startPayment', array('id'=> Yii::app()->session['courseId']));
                                Yii::app()->session['newUserId'] = $model->id;
                                $model->sendActivationEmailForNewUser();
                                $this->redirect($url);
                            }
                            $this->refresh();
                        }
                    }
                } else $profile->validate();
            }
            $this->render('/user/registration',array('model'=>$model,'profile'=>$profile,'code'=>$code));
        }
    }

    public function writeLog()
    {
        $ip = Yii::app()->request->userHostAddress;
        $uri = Yii::app()->request->requestUri;
        $ref = Yii::app()->request->urlReferrer;
        $headers = '';
        foreach ($_SERVER as $name => $value)
        {
            if (substr($name, 0, 5) == 'HTTP_')
            {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }

        file_put_contents(
            Yii::getPathOfAlias('runtime').'/registrationLog.txt',
            date('Y-m-d H:i:s',time())."\t".$ip."\t".$uri."\t".$ref."\t".implode(" ",$headers)."\n",
            FILE_APPEND
        );
    }
}