<?php

class ActivationController extends Controller
{
    public $defaultAction = 'activation';

    /**
     * Activation user account
     */
    public function actionActivation()
    {
        $email = $_GET['email'];
        $activkey = $_GET['activkey'];
        if ($email && $activkey) {
            $find = User::model()->notsafe()->findByAttributes(array('email' => $email));
            if (isset($find) && $find->status) {
                $this->render('/user/message', array('title' => UserModule::t("User activation"), 'content' => UserModule::t("You account is active.")));
            } elseif (isset($find->activkey) && ($find->activkey == $activkey)) {
                $find->activkey = HashHelper::phpbbHash(microtime());
                $find->status = 1;
                /* Pending Mail Sending Start*/
                $mail = MailNotification::model()->findAllByAttributes(array('user_id' => $find->id));

                if (!empty($mail)) {
                    foreach ($mail as $mails) {
                        $to = $mails->to;
                        $from = $mails->from;
                        $subject = $mails->subject;
                        $message = $mails->message;
                        //$attachments=$mails->attachments;

                        $mainPath = Yii::getPathOfAlias('data') . '/' . $mails->attachments;


                        echo $attPath = $mainPath . '/att/';
                        $attachmentsList = scandir($attPath);
                        $attachments = array();

                        // Define a list of FILES to send along with the e-mail. Key = File to be sent. Value = Name of file as seen in the e-mail.
                        foreach ($attachmentsList as $att)
                            $attachments[$attPath . $att] = $att;


                        $headers = array(
                            'Reply-to' => $mails->from,
                            'Some-Other-Header-Name' => 'Header Value');
                        UserModule::mailAttachments($to, $from, $subject, $message, $attachments, $headers);
                    }
                    MailNotification::model()->deleteAll('user_id' == $find->id);
                }
                /* Pending Mail Sending Start*/

                $find->save(array('user_id' => $find->id));

                Yii::app()->session['newUserId'] = null;

                $this->render('/user/afterActivation', array(
                    'title' => UserModule::t("User activation"),
                    'content' => UserModule::t("Your account has been successfully activated! You will now be directed to the login screen.")
                ));

            } else {
                $this->render('/user/message', array('title' => UserModule::t("User activation"), 'content' => UserModule::t("Incorrect activation URL.")));
            }
        } else {
            $this->render('/user/message', array('title' => UserModule::t("User activation"), 'content' => UserModule::t("Incorrect activation URL.")));
        }
    }
}
