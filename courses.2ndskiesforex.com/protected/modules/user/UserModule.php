<?php
/**
 * Yii-User module
 * 
 * @author Mikhail Mangushev <mishamx@gmail.com> 
 * @link http://yii-user.2mx.org/
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @version $Id: UserModule.php 132 2011-10-30 10:45:01Z mishamx $
 */

class UserModule extends CWebModule
{
	/**
	 * @var int
	 * @desc items on page
	 */
	public $user_page_size = 10;
	
	/**
	 * @var int
	 * @desc items on page
	 */
	public $fields_page_size = 10;
	
	/**
	 * @var string
	 * @desc hash method (md5,sha1 or algo hash function http://www.php.net/manual/en/function.hash.php)
	 */
	public $hash='md5';
	
	/**
	 * @var boolean
	 * @desc use email for activation user account
	 */
	public $sendActivationMail=true;
	
	/**
	 * @var boolean
	 * @desc allow auth for is not active user
	 */
	public $loginNotActiv=false;
	
	/**
	 * @var boolean
	 * @desc activate user on registration (only $sendActivationMail = false)
	 */
	public $activeAfterRegister=false;
	
	/**
	 * @var boolean
	 * @desc login after registration (need loginNotActiv or activeAfterRegister = true)
	 */
	public $autoLogin=true;
	
	public $registrationUrl = array("/user/registration");
	public $recoveryUrl = array("/user/recovery/recovery");
	public $loginUrl = array("/user/login");
	public $logoutUrl = array("/user/logout");
	public $profileUrl = array("/user/profile");
	public $returnUrl = array("/user/profile");
	public $returnLogoutUrl = array("/user/login");
    public $adminUrl = array("/user/admin/index");
	
	
	/**
	 * @var int
	 * @desc Remember Me Time (seconds), defalt = 2592000 (30 days)
	 */
	public $rememberMeTime = 2592000; // 30 days
	
	public $fieldsMessage = '';
	
	/**
	 * @var array
	 * @desc User model relation from other models
	 * @see http://www.yiiframework.com/doc/guide/database.arr
	 */
	public $relations = array();
	
	/**
	 * @var array
	 * @desc Profile model relation from other models
	 */
	public $profileRelations = array();
	
	/**
	 * @var boolean
	 */
	public $captcha = array('registration'=>true);
	
	/**
	 * @var boolean
	 */
	//public $cacheEnable = false;
	
	public $tableUsers = '{{users}}';
	public $tableProfiles = '{{profiles}}';
	public $tableProfileFields = '{{profiles_fields}}';

    public $defaultScope = array(
            //'with'=>array('profile'),
    );
	
	static private $_user;
	static private $_users=array();
	static private $_userByName=array();
	static private $_admin;
	static private $_admins;
	
	/**
	 * @var array
	 * @desc Behaviors for models
	 */
	public $componentBehaviors=array();
	
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'user.models.*',
			'user.components.*',
            'user.helpers.*',
		));
	}
	
	public function getBehaviorsFor($componentName){
        if (isset($this->componentBehaviors[$componentName])) {
            return $this->componentBehaviors[$componentName];
        } else {
            return array();
        }
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
	
	/**
	 * @param $str
	 * @param $params
	 * @param $dic
	 * @return string
	 */
	public static function t($str='',$params=array(),$dic='user') {
		if (Yii::t("UserModule", $str)==$str)
		    return Yii::t("UserModule.".$dic, $str, $params);
        else
            return Yii::t("UserModule", $str, $params);
	}
	
	/**
	 * @return hash string.
	 */
	/*public static function encrypting($string="")
    {
	/*	$hash = Yii::app()->getModule('user')->hash;

		if ($hash=="md5")//{echo '1';die(md5($string));}
        return md5($string);
		if ($hash=="sha1")//{echo '2';die(sha1($string));}
        return sha1($string);
		else //die(hash($hash,$string));
        return hash($hash,$string);
        return HashHelper::phpbbHash($string);
	}*/
	
	/**
	 * @param $place
	 * @return boolean 
	 */
	public static function doCaptcha($place = '') {
		if(!extension_loaded('gd'))
			return false;
		if (in_array($place, Yii::app()->getModule('user')->captcha))
			return Yii::app()->getModule('user')->captcha[$place];
		return false;
	}
	
	/**
	 * Return admin status.
	 * @return boolean
	 */
	public static function isAdmin() {
		if(Yii::app()->user->isGuest)
			return false;
		else {
			if (!isset(self::$_admin)) {
				if(self::user()->role == 1)
					self::$_admin = true;
				else
					self::$_admin = false;	
			}
			return self::$_admin;
		}
	}


	/**
	 * Return admins.
	 * @return array syperusers names
	 */	
	public static function getAdmins() {
		if (!self::$_admins) {
			$admins = User::model()->active()->findAllByAttributes(array('role'=>1));
			$return_name = array();
			foreach ($admins as $admin)
				array_push($return_name,$admin->username);
			self::$_admins = ($return_name)?$return_name:array('');
		}
		return self::$_admins;
	}

    /**
     * Send email with attachments
     */

    public static function mailAttachments($to, $from, $subject, $message, $attachments = array(), $headers = array(), $additional_parameters = '') {
        $headers['From'] = $from;

        // Define the boundray we're going to use to separate our data with.
        $mime_boundary = '==MIME_BOUNDARY_' . md5(time());

        // Define attachment-specific headers
        $headers['MIME-Version'] = '1.0';
        $headers['Content-Type'] = 'multipart/mixed; boundary="' . $mime_boundary . '"';

        // Convert the array of header data into a single string.
        $headers_string = '';
        foreach($headers as $header_name => $header_value) {
            if(!empty($headers_string)) {
                $headers_string .= "\r\n";
            }
            $headers_string .= $header_name . ': ' . $header_value;
        }

        // Message Body
        $message_string  = '--' . $mime_boundary;
        $message_string .= "\r\n";
        $message_string .= 'Content-Type: text/html; charset="utf-8"';
        $message_string .= "\r\n";
        $message_string .= 'Content-Transfer-Encoding: 7bit';
        $message_string .= "\r\n";
        $message_string .= "\r\n";
        $message_string .= $message;
        $message_string .= "\r\n";
        $message_string .= "\r\n";

        // Add attachments to message body
        foreach($attachments as $local_filename => $attachment_filename) {
            if(is_file($local_filename)) {
                $message_string .= '--' . $mime_boundary;
                $message_string .= "\r\n";
                $message_string .= 'Content-Type: application/octet-stream; name="' . $attachment_filename . '"';
                $message_string .= "\r\n";
                $message_string .= 'Content-Description: ' . $attachment_filename;
                $message_string .= "\r\n";

                $fp = @fopen($local_filename, 'rb'); // Create pointer to file
                $file_size = filesize($local_filename); // Read size of file
                $data = @fread($fp, $file_size); // Read file contents
                $data = chunk_split(base64_encode($data)); // Encode file contents for plain text sending

                $message_string .= 'Content-Disposition: attachment; filename="' . $attachment_filename . '"; size=' . $file_size.  ';';
                $message_string .= "\r\n";
                $message_string .= 'Content-Transfer-Encoding: base64';
                $message_string .= "\r\n\r\n";
                $message_string .= $data;
                $message_string .= "\r\n\r\n";
            }
        }

        // Signal end of message
        $message_string .= '--' . $mime_boundary . '--';

        $data = array(
            'from' => $from,
            'headers' => $headers_string,
            'name' => $subject
        );

        // Send the e-mail.
        return Yii::app()->apostle->send($message_string, $to, $data);
    }
	
	/**
	 * Return safe user data.
	 * @param user id not required
	 * @return user object or false
	 */
	public static function user($id=0,$clearCache=false) {
        if (!$id&&!Yii::app()->user->isGuest)
            $id = Yii::app()->user->id;
		if ($id) {
            if (!isset(self::$_users[$id])||$clearCache)
                self::$_users[$id] = User::model()->findbyPk($id);
			return self::$_users[$id];
        } else return false;
	}
	
	/**
	 * Return safe user data.
	 * @param user name
	 * @return user object or false
	 */
	public static function getUserByName($username) {
		if (!isset(self::$_userByName[$username])) {
			$_userByName[$username] = User::model()->findByAttributes(array('username'=>$username));
		}
		return $_userByName[$username];
	}
	
	/**
	 * Return safe user data.
	 * @param user id not required
	 * @return user object or false
	 */
	public function users() {
		return User;
	}
}
