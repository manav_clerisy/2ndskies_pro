<?php

// DO NOT include this file!
// It is created only to improve IDE autosuggest.

/**
 * @property Paypal $paypal
 * 
 * @property HttpClient $httpClient console application only 
 * 
 * CWebApplication modules:
 * @property CClientScript $clientScript
 * @property WebUser $user
 * @property CHttpSession $session
 * @property CFormatter $format
 */
class CApplication
{}
