<?php

class CoursesController extends Controller
{

    public $defaultAction = 'list';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array( 'sectionlist', 'getcourses', 'search', 'changeCalendarPublicStatus','changeVisiblePublicStatus','changeTimeZone','changeEconomicEvents','videoLessons','quiz'),
                'users'=>array('@'),
            ),
            array('allow',
                'actions'=>array('FetchOrder','callback','list','buycoursepreview','startPayment','PayCreditCard','finishPaymentPaypal','finishPaymentCreditCard','endPayment','checkCouponCode','CreditCardError'),
                'users'=>array('*')
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     * @return Courses
     */
    public function loadModel($id)
    {
        $model=Courses::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='courses-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionList()
    {
        $this->render('list');
    }

    /**
     * Start payment process
     * @param integer $id - ID of buying course
     * @param string $type - chosen payment type, 'creditCard' or 'paypal'
     */
    public function actionStartPayment()
    {
        $id   = Yii::app()->request->getParam('id', null);
        Yii::app()->session['currentCourseId'] = $id;
        $type = Yii::app()->request->getParam('type','creditCard');

        if(isset($_GET['orid'])){
            Yii::app()->session['aff_id'] = $_GET['orid'];
        }
        if((!Yii::app()->user->isGuest) || (isset(Yii::app()->session['newUserId']) && !empty(Yii::app()->session['newUserId'])))
        {

            if (empty($id))
            {
                if(isset(Yii::app()->session['courseId']) && !empty(Yii::app()->session['courseId']))
                    $id = Yii::app()->session['courseId'];
                else
                    $this->response('error', '<strong>Warning!</strong> Can\'t get course ID');
            }

            if(isset(Yii::app()->user->id) && (!isset(Yii::app()->session['newUserId']) || is_null(Yii::app()->session['newUserId'])))
            {
                $userCourse = UserCourse::model()->findByAttributes(array('user_id'=>Yii::app()->user->id, 'course_id'=>$id));
                if(isset($userCourse) && !empty($userCourse))
                    $this->responseError('It looks like you have already purchased this course.
                    Please proceed to login to the course member area <a href="'.Yii::app()->createUrl("/user/login").'">here</a>.
                    If you have just purchased this course but are unable to login, you will need to activate your account first.
                    To do so, click the link in the email you should\'ve received after registering.
                    If you are still having problems, please <a href="mailto:chris@2ndskiesforex.com">contact support</a>.');
            }

            $model = Courses::model()->findByPk($id);
            $price = $model->price;

            if (empty($price))
                $this->response('error', '<sctrong>Warning!</strong> Can\'t get course PRICE');
            if($type == 'paypal')
                $this->paypalPrepare($model);
            elseif($type == 'creditCard')
                $this->creditCardPrepare($id,$model);
        }
        else
        {
            Yii::app()->session['courseId'] = $id;
            $this->redirect('/user/registration');
        }
    }


    /**
     * prepare details for payment via Paypal
     *
     * set attributes, which will be sent to the Paypal,
     * redirect to the Paypal site
     *
     * @param Courses $model
     */
    public function paypalPrepare($model)
    {
        $name = $model->name;
        $courseId = $model->id;
        $couponCode = Yii::app()->request->getPost('couponCode',null);

        if(!empty($couponCode))
        {
            $nowTime = time();
            $discount = DiscountCode::model()->findByAttributes(array('code'=>$couponCode));
            if(isset($discount) && !empty($discount) && $nowTime > strtotime($discount->date_start) && $nowTime < strtotime('+1 day', strtotime($discount->date_end)-1))
            {
                $price = $discount->getPriceWithDiscount($model->price);
                $couponId = $discount->id;
            } else {
                $price = $model->price;
                $couponId = null;
            }
        }
        else
        {
            $price = $model->price;
            $couponId = null;
        }

        $params = array(
            'RETURNURL' => $this->createAbsoluteUrl('courses/finishPaymentPaypal',array('id' => $courseId, 'couponId'=>$couponId)),
            'CANCELURL' => $this->createAbsoluteUrl('courses/list'),
            'PAYMENTREQUEST_0_AMT' => $price,
            'PAYMENTREQUEST_0_ITEMAMT' => $price,
            'L_PAYMENTREQUEST_0_QTY0'=>0,
            'L_PAYMENTREQUEST_0_AMT0' => $price,
            'L_PAYMENTREQUEST_0_DESC0' => '$'.$price .' '.$name,
            'L_PAYMENTREQUEST_0_NAME0'=>$name,
            'BRANDNAME' => 'Second Skies LLC'
        );
        $url = Yii::app()->paypal->setExpressCheckout($params);

        $this->redirect($url);
    }

    /**
     * prepare details for payment via credit card
     *
     * create form for paying via credit card
     *
     * @param integer $id - buying course id
     * @param CreditCard $creditCard
     */
    public function creditCardPrepare($id,$model)
    {
        $creditCard = Yii::app()->paypalSDK->createCreditCardModel();

        $userId = !empty(Yii::app()->user->id) ? Yii::app()->user->id : Yii::app()->session['newUserId'];

        $profile = Profile::model()->findByPk($userId);
        $user = User::model()->findByPk($userId);

        $creditCard->first_name = $profile->first_name;
        $creditCard->billing_zip = $profile->billing_zip;
        $creditCard->billing_state = $profile->billing_state;
        $creditCard->billing_address = $profile->billing_address;
        $creditCard->billing_country = $profile->location;
        $creditCard->billing_city = $profile->billing_city;
        if(!empty($profile->last_name))
            $creditCard->last_name = $profile->last_name;

        $this->render('payment/doPayment', array(
            'model'=>$creditCard,
            'courseId'=>$id,
            'userEmail'=>$user->email,

        ));
    }

    function randomPrefix($length)
    {
        $random= "";

        srand((double)microtime()*1000000);

        $data = "AbcDE123IJKLMN67QRSTUVWXYZ";
        $data .= "aBCdefghijklmn123opq45rs67tuv89wxyz";
        $data .= "0FGH45OP89";

        for($i = 0; $i < $length; $i++)
        {
            $random .= substr($data, (rand()%(strlen($data))), 1);
        }

        return $random;
    }


    /**
     * payment via paypal
     *
     * manage transaction and data, which were returned by it
     */
    public function actionFinishPaymentPaypal()
    {

        try {
            $res = Yii::app()->paypal->finishExpressCheckoutPayment();
        } catch (Exception $e){
            $this->responseError('Transaction failed');
        }

        if (!isset($res) || ($res == false))
            $this->responseError('Transaction failed');

        $id = Yii::app()->request->getParam('id', null);
        if (empty($id))
            $this->responseError('Can\'t get course ID');

        $price = Courses::model()->findByPk($id)->getAttribute('price');
        if (empty($price))
            $this->responseError('Can\'t get course PRICE');

        $userId = (Yii::app()->user->id) ? Yii::app()->user->id : Yii::app()->session['newUserId'];
        $model = new UserCourse;
        $model->user_id   = $userId;
        $model->course_id = $id;
        $model->paypal_id = $res['PAYMENTINFO_0_TRANSACTIONID'];
        $model->price     = $price;
        $model->payment_type = 0;

        $couponId = Yii::app()->request->getParam('couponId', null);

        if ($model->save(false))
            $this->responseSuccess($couponId,$id);
        else
            $this->responseError('Can\'t save into database');

    }

    /**
     * payment via credit card
     *
     * create payment process, manage response
     */

    public function actionFinishPaymentCreditCard()
    {
        if (isset($_POST['CreditCard'],$_POST['courseId'])) {
            $model = Yii::app()->paypalSDK->createCreditCardModel();
            $model->attributes = $_POST['CreditCard'];

            $courseId = Yii::app()->request->getPost('courseId', null);
            $couponCode = $couponId = Yii::app()->request->getPost('couponCode', null);
            $userEmail = Yii::app()->request->getPost('userEmail', null);

            $course = Courses::model()->findByPk($courseId);

            if(!empty($couponCode))
            {
                $discount = DiscountCode::model()->findByAttributes(array('code'=>$couponCode));
                $nowTime = time();
                if(isset($discount) && !empty($discount) && $nowTime > strtotime($discount->date_start) && $nowTime < strtotime('+1 day', strtotime($discount->date_end)-1))
                {
                    $price = $discount->getPriceWithDiscount($course->price);
                    $couponId = $discount->id;
                }
                else {
                    $price = $course->price;
                }
            }
            else
                $price = $course->price;

            $userId = !empty(Yii::app()->user->id) ? Yii::app()->user->id : Yii::app()->session['newUserId'];
            $user = User::model()->findByPk($userId);
            $userProfile = Profile::model()->findByPk($userId);

            if(empty($userProfile->last_name))
            {
                $userProfile->last_name = $_POST['CreditCard']['last_name'];
                $userProfile->billing_address = $_POST['CreditCard']['billing_address'];
                $userProfile->billing_city = $_POST['CreditCard']['billing_city'];
                $userProfile->billing_state = $_POST['CreditCard']['billing_state'];
                $userProfile->billing_zip = $_POST['CreditCard']['billing_zip'];
                $userProfile->location = $_POST['CreditCard']['billing_country'];
                $fields['firstname'] = $userProfile->first_name;
                $fields['lastname'] = $userProfile->last_name;
                //$fields['state'] = $userProfile->billing_state; this is not compatible with ontraport states
                $fields['zip'] = $userProfile->billing_zip;
                $fields['city'] = $userProfile->billing_city;
                $fields['country'] = $this->countriesListOntraport($userProfile->location);
                $fields['address'] = $userProfile->billing_address;
                $fields['id'] = $user->ontraportId;
                $fields['objectID'] = 0;
                $update = $this->curlPut('https://api.ontraport.com/1/objects', $fields, array(0 => 'Api-Appid: '.Yii::app()->params->ontraportAppId,1 => 'Api-Key: '.Yii::app()->params->ontraportAppKey));
                $userProfile->save(false);
            }

            if ($model->validate())
            {

                if(isset($_POST) && !empty($_POST))
                {
                    $token=  $this->randomPrefix(28);
                    $post=$_POST;
                    $this->render('payment/doPayment',array(
                        'model_result'=>$model,
                        'courseId'=>$courseId,
                        'courseName'=>$course->name,
                        'userEmail'=>$user->email,
                        'token'=>	$token,
                        'post'=>$post,
                        'amount'=>$price
                    ));
                }

            }

            $this->render('payment/doPayment',array(
                'model'=>$model,
                'courseId'=>$courseId,
                'userEmail'=>$user->email
            ));
        }
        elseif((isset($_GET['SECURETOKEN']) && trim($_GET['SECURETOKEN']) != '') && (isset(Yii::app()->session['SECURETOKEN']) && Yii::app()->session['SECURETOKEN'] != ''))
        {
            if($_GET['SECURETOKEN']== Yii::app()->session['SECURETOKEN'])
            {
                if(isset($_GET['RESULT']))
                {
                    if(!empty($_GET) && $_GET['RESULT'] == 0 )
                    {
                        $subject="Result approved";
                        $this->actionCreateActionLog(json_encode($_GET),$subject);

                        $courseId=   Yii::app()->session['currentCourseId'];

                        $userId = !empty(Yii::app()->user->id) ? Yii::app()->user->id : Yii::app()->session['newUserId'];
                        $model = new UserCourse;
                        $model->user_id   = $userId;
                        $model->course_id = $courseId;
                        $model->paypal_id = $_GET['PNREF'];
                        $model->price     = $_GET['AMT'];
                        $model->payment_type = 1;

                        $course = Courses::model()->findByPk($courseId);
                        $courseName=$course->id;
                        if ($model->save(false))
                        {
                            $subject="Result approved and saved".$_GET['PNREF'];
                            $this->actionCreateActionLog(json_encode($_GET),$subject);
                            $couponId = Yii::app()->request->getParam('couponId', null);
                            $this->responseSuccess($couponId,$courseName);
                        }
                        else
                        {
                            $subject="Result approved and not saved";
                            $this->actionCreateActionLog(json_encode($_GET),$subject);
                            $this->responseError('Can\'t save into database');

                        }
                    }
                    else
                    {
                        $subject="Result not approved";
                        $this->actionCreateActionLog(json_encode($_GET),$subject);

                        $cid=Yii::app()->session['currentCourseId'];
                        $msg=$_GET['RESULT'];
                        if($msg==23)
                        {
                            $mssg="Unfortunately the credit card number you entered isn't working. Please double check the number and try to enter again. If you continue to have problems, please contact  <a href='mailto:chris@2ndskiesforex.com'>support</a>";
                        }
                        elseif($msg==24)
                        {
                            $mssg=("Unfortunately the expiry date on your credit card seems to be wrong. Please double check the number and try to enter again. If you continue to have problems, please contact <a href='mailto:chris@2ndskiesforex.com'>support</a>" );
                        }

                        else
                        {
                            $mssg=$_GET['RESPMSG'];
                        }
                        $this->responseErrorMessageCreditCard($mssg,$cid);

                    }
                }
                else
                {
                    $subject="Result not set";
                    $this->actionCreateActionLog(json_encode($_GET),$subject);
                    $this->responseError('Can\'t complete transaction');
                }
            }
            else
            {
                $subject="Secure Token Mismatch";
                $this->actionCreateActionLog(json_encode($_GET),$subject);
                $this->responseError('Secure Token Mismatch');
            }
        }
        else
        {
            $subject="Secure Token Missing";
            $this->actionCreateActionLog(json_encode($_GET),$subject);
            $this->responseError('Secure Token Mismatch');
        }
    }
    public function responseErrorMessageCreditCard($text,$cid)
    {
        $text = '<strong>Warning!</strong> ' . $text;
        Yii::app()->user->setFlash('error', $text);
        $this->redirect(array('courses/startPayment?id='.$cid.'&type=creditCard'));
    }

    public function actionCreateActionLog($details,$subject)
    {
        if(Yii::app()->params['actionLog']==true)
        {
            $path=Yii::getPathOfAlias('action_log');
            //$fp = fopen($path,"a");
            //fwrite($fp,$subject."\n".$details." Created On: ".date("Y-m-d H:m:i")."\n ********************************************** \n");

        }
        return;
    }

    /**
     * handle exception response during payment via credit card
     *
     * manage response data
     *
     * @param Exception $e
     * @param integer $courseID
     */
    public function handleExceptionResponse($e, $courseID)
    {
        $errors = json_decode($e->getData());

        if(!empty($errors))
            $message = $this->getExceptionMessage($errors->details);
        else
            $message = 'Transaction failed, please try again.';


        Yii::app()->user->setFlash('error', $message);
        $this->redirect(Yii::app()->createUrl('courses/startPayment',array('type'=>'creditCard','id'=>$courseID)));
    }

    /**
     * handle exception message
     *
     * create user friendly error message
     *
     * @param array $exceptionDetails
     * @return string $message
     */
    public function getExceptionMessage($exceptionDetails)
    {
        $message = '';
        foreach($exceptionDetails as $line)
            $message .= $line->issue . '. ';

        return $message;
    }

    /**
     * manage success response
     *
     * set coupon code as used, redirect to endPayment action
     *
     * @param string $couponCode
     * @param integer $courseId
     */
    public function responseSuccess($couponId,$courseId)
    {
        /*  if(isset($couponId) && !empty($couponId))
          {
              $discount = DiscountCode::model()->findByPk($couponId);

              if(!empty($discount))
              {
                  $discount->is_used = 1;
                  $discount->save();
              }
          }*/

        $this->redirect(array('courses/endPayment?courseId='.$courseId));
    }

    /**
     * manage error response
     *
     * set user friendly error message
     *
     * @param string $text
     */
    public function responseError($text)
    {
        $text = '<sctrong>Warning!</strong> ' . $text;

        $this->response('error', $text);
    }

    /**
     * set response message
     *
     * @param string $class - 'success' or 'error'
     * @param string $text - message text
     */
    public function response($class, $text)
    {
        Yii::app()->user->setFlash($class, $text);

        $this->redirect(array('courses/list'));
    }

    /**
     * end payment
     *
     * @param integer $courseId
     */
    public function actionEndPayment($courseId)
    {
        if(!empty($courseId))
        {
            $courseName = Courses::model()->findByPk($courseId);
            if(!empty($courseName))
            {
                $userId = !empty(Yii::app()->user->id) ? Yii::app()->user->id : Yii::app()->session['newUserId'];
                $user = User::model()->findByPk($userId);
                $result=Courses::model()->sendWelcomeCourseEmail($courseId,$userId);
                if(isset($result))
                {
                    $model = new MailNotification;
                    $model->user_id=$userId;$model->to=$result['to'];
                    $model->from=$result['from'];
                    $model->to=$result['to'];
                    $model->subject=$result['subject'];
                    $model->created_date=time();
                    $model->attachments=$result['attachments'];
                    $model->message=$result['message'];
                    $model->save();

                    //Send purchase to ontraport after payment
                    $fields['objectID'] = 17;
                    if(isset(Yii::app()->session['aff_id'])){
                        $fields['affiliate_id'] = Yii::app()->session['aff_id'];
                    }
                    $fields['contact_id'] = $user->ontraportId;
                    if(isset(Yii::app()->session['aff_id'])){
                        $fields['affiliate_id'] = Yii::app()->session['aff_id'];
                    }
                    $fields['quantity'] = 1;
                    $fields['id'] = 0;
                    $fields['price'] = $courseName->price;
                    $fields['product_id'] = $courseName->ontraportPid;
                    $fields['status'] = 1;
                    $fields['name'] = $courseName->name;
                    $fields['date'] = time();
                    $fields['dlm'] = time();
                    $fields['owner'] = 1;
                    $purchase = $this->curlPost('https://api.ontraport.com/1/objects', $fields,array(0 => 'Api-Appid: '.Yii::app()->params->ontraportAppId,1 => 'Api-Key: '.Yii::app()->params->ontraportAppKey));
                    if(isset(Yii::app()->session['aff_id'])){
                        unset(Yii::app()->session['aff_id']);
                    }

                }
                $this->render('payment/endPayment',array('course'=>$courseName->name));
            }
            else
            {
                $this->responseError('Invalid Course Id');
                $this->redirect(array('courses/list'));
            }
        }
        else
        {
            $this->responseError('Course Id Empty');
            $this->redirect(array('courses/list'));
        }
    }


    /**
     * courses name autosuggest
     *
     * using in admin area in update or create user subscription for a course
     * finding course by it's name
     *
     * @param string $query  - chars in courses name
     * @return JSON array - {string: course id| course name}
     */
    public function actionGetCourses()
    {
        $query  = Yii::app()->request->getParam('query');

        $criteria = new CDbCriteria;
        $criteria->addSearchCondition('name', $query);
        $criteria->limit = 20;

        $items = Courses::model()->findAll($criteria);

        $res = array();
        foreach ($items as $item) {
            $res[] = array(
                'string' => implode('|', array(
                    $item->id,
                    $item->name,
                ))
            );
        }

        Yii::app()->end(CJSON::encode($res));
    }

    /**
     * @param int $courseId Course id
     * @param int $sectionId Section id
     * @throws CHttpException
     */
    public function actionSectionList($courseId, $sectionId = 0, $isMenu = false)
    {
        $this->assertCourseAccessible($courseId);
        Yii::app()->session['courseId'] = $courseId;

        $section = null;
        if ($sectionId) {
            $section = Sections::model()->findByPk($sectionId);
            /* @var $section Sections */
            if (!$section || $section->course_id != $courseId)
                throw new CException('Section not found');
            $this->setActiveSection($section);
        }

        $course = $this->loadModel($courseId);

        $sections = $course->findSectionsBy($sectionId);

        if($section->name == 'Daily Market Commentary' || $section->name == 'Market Commentary & Quizzes')
            $this->render('sections/daily_market/_view', array(
                'course'    => $course,
                'sections'  => $sections,
                'currentSection' => $section,
                'isMenu'=>$isMenu
            ));
        else
            $this->render('sections/view', array(
                'course'    => $course,
                'sections'  => $sections,
                'currentSection' => $section,
                'isMenu'=>$isMenu
            ));
    }

    /**
     * get data for video lessons
     *
     * gets data of a section, put them in the special view for video lessons
     *
     * @param integer $courseId, $sectionId, $topicId
     */
    public function actionVideoLessons($courseId, $sectionId = 0, $topicId = 0)
    {
        $this->assertCourseAccessible($courseId);
        $course = $this->loadModel($courseId);

        if(!$topicId)
        {
            $section = null;
            if ($sectionId) {
                $section = Sections::model()->findByPk($sectionId);
                /* @var $section Sections */
                if (!$section || $section->course_id != $courseId)
                    throw new CException('Section not found');
                $this->setActiveSection($section);
            }

            $sections = $course->findSectionsBy($sectionId);

            $this->render('video_lessons/view', array(
                'course'    => $course,
                'sections'  => $sections,
                'currentSection' => $section,
            ));
        }
        else
        {
            $topic = Topic::model()->findByPk($topicId);
            if (!Yii::app()->user->isAdmin()) {
                $userCourse = UserCourse::model()->findByAttributes(array('user_id' => Yii::app()->user->id, 'course_id' => $course->id));
            }
            if (Yii::app()->user->isAdmin() || time() > strtotime("+$topic->time_release day", strtotime($userCourse->created))) {
                $this->render('video_lessons/_topic', array(
                    'course'    => $course,
                    'topic'  => $topic,
                ));
            } else {
                throw new CHttpException(404, 'This page is not available.');
            }
        }
    }


    /**
     * search keyword in topics comments
     *
     * @param string $keyword
     * @param integer $courseId
     */
    public function actionSearch()
    {
        $keyword  = Yii::app()->request->getParam('keyword', null);
        $courseId = Yii::app()->request->getParam('courseId', null);

        $doSearch = true;
        $comments =  array();
        $topics   =  array();

        if (empty($keyword)) {
            $doSearch = false;
            Yii::app()->user->setFlash('error', '<strong>Warning</strong> Please specify right keyword');
        }

        $isSubscribed = Yii::app()->user->model()->isSubscribedOnCourse($courseId);
        if (! $isSubscribed) {
            $doSearch = false;
            Yii::app()->user->setFlash('error', '<strong>Warning</strong> You don\'t have access to this course');
        }

        if ($doSearch) {

            $comments = Yii::app()->db->createCommand()
                ->select('c.*')
                ->from('comments as c')
                ->join('topics as t', 't.id = c.topic_id')
                ->join('sections as s', 's.id = t.section_id')
                ->where('s.course_id = :COURSE_ID and MATCH (stripped_tag_text) AGAINST (:KEYWORD IN BOOLEAN MODE)',
                    array(
                        ':COURSE_ID' => $courseId,
                        ':KEYWORD'   => $keyword.'*'
                    )
                )
                ->limit('20')
                ->queryAll();

            $comments = $this->wrapSearchResults($comments, 'text', $keyword);


            $topics = Yii::app()->db->createCommand()
                ->select('t.*')
                ->from('topics as t')
                ->join('sections as s', 's.id = t.section_id')
                ->where('s.course_id = :COURSE_ID and subject LIKE :KEYWORD',
                    array(
                        ':COURSE_ID' => $courseId,
                        ':KEYWORD'   => '%'.$keyword.'%'
                    )
                )
                ->limit('20')
                ->queryAll();

            $topics = $this->wrapSearchResults($topics, 'subject', $keyword);
        }

        $this->render('search_results', array(
            'comments' => $comments,
            'topics'   => $topics,
        ));
    }

    /**
     * wrap search results
     *
     * @param array $topics
     * @param string $fieldName
     * @param string $keyword
     * @return array $item
     */
    private function wrapSearchResults($items, $fieldName, $keyword)
    {
        foreach ($items as &$item) {
            $item[$fieldName] = str_replace($keyword, "<span class='label label-warning pad-lr-0'>$keyword</span>", $item[$fieldName]);
        }

        return $items;
    }

    /**
     * Change Calendar Public Status
     *
     * set calendar attribute is_calendar_public
     *
     * @throw CHttpException 404
     */
    public function actionChangeCalendarPublicStatus()
    {
        if(!Yii::app()->request->isAjaxRequest) throw new CHttpException(404);

        $id = Yii::app()->request->getParam('id', null);
        if ($id == null)
            throw new CHttpException(404);

        $model = Courses::model()->findByPk($id);

        $model->saveAttributes( array('is_calendar_public'=> 1-$model->is_calendar_public));
    }

    /**
     * Change Visible Public Status
     *
     * @throw CHttpException 404
     */
    public function actionChangeVisiblePublicStatus()
    {
        if(!Yii::app()->request->isAjaxRequest) throw new CHttpException(404);

        $id = Yii::app()->request->getParam('id', null);
        if ($id == null)
            throw new CHttpException(404);

        $model = Courses::model()->findByPk($id);

        $model->saveAttributes( array('visible'=> 1-$model->visible));
    }

    /**
     * Change time zone
     *
     * change time zone for economic events greed on the home page
     *
     * @param string $time in format like 'Etc/GMT+10'
     * @return html $timeDropDown
     */
    public function actionChangeTimeZone()
    {
        if(isset($_POST['time']))
        {    $timeDropDown = Courses::model()->timezone($_POST['time']);
            echo $timeDropDown;
            Yii::app()->end();
        }

    }

    /**
     * send discont code to the secondskies frontend WP
     *
     * @return JSON
     */
    public function actionCallback()
    {
        $model = new DiscountCode;

        $model->code = time();
        $model->is_used = 0;
        $model->save();

        header('Content-type: application/json');
        echo $_GET['callback'] . "(";
        echo CJSON::encode($model->code);
        echo ")";
        Yii::app()->end();
    }

    /**
     * manage quiz
     *
     * @param integer $sectionId
     * @param array $_POST['Answers'] - answers to the quiz questions
     */
    public function actionQuiz()
    {
        $sectionId = Yii::app()->request->getQuery('sectionId',null);

        if(isset($_POST['Answers']) && !empty($_POST['Answers']))
        {
            $answers = $_POST['Answers'];
            $response = null;
            $model = Quiz::model()->findAllByAttributes(array('section_id'=>$sectionId));

            foreach($model as $key => $item)
            {
                $correctAnswers = '';
                if(isset($answers[$key]))
                    $correctAnswers = implode(';', $answers[$key]);

                if((!isset($answers[$key])) || ($item->correct_answers != $correctAnswers))
                {
                    $response .= '<br>' . $item->question;
                    $answArr = explode(';', $item->answers);

                    foreach($answArr as $k => $answ)
                        if(!empty($answ))
                            $response .= $k . ' - ' . $answ . '<br>';

                    $response .= '<b>Correct answer: #' . str_replace(';', ' and #', $item->correct_answers) . '</b><br>';
                }
            }
            if((!isset($response)) && (!empty($answers)) && (count($answers) == count($model)))
                $response = 'all correct';

            $this->render('quiz',array(
                'response'=>$response
            ));
        }
        else{
            $model = Quiz::model()->findAllByAttributes(array('section_id' => $sectionId));
            $this->render('quiz',array(
                'model' => $model
            ));
        }
    }

    /**
     * Check Coupon Code if it was already used in the system
     *
     * @param integer $courseId
     * @param string $code
     * @return string
     */
    public function actionCheckCouponCode()
    {
        $code = Yii::app()->request->getPost('code', null);
        $courseId = Yii::app()->request->getPost('courseId', null);
        $model = Courses::model()->findByPk($courseId);

        if($code)
        {
            $discount = DiscountCode::model()->findByAttributes(array('code'=>$code));
            $nowTime = time();

            if (empty($discount))
                $response = array('price'=>$model->price, 'message'=>'Error. The code is not found.');
            /*elseif($discount->is_used == 1)
             $response = array('price'=>$model->price, 'message'=>'Error. This code has been used already.');*/
            elseif ($nowTime < strtotime($discount->date_start)) {
                $response = array('price'=>$model->price, 'message'=>'Error. This code is not started.');
            }
            elseif ($nowTime > strtotime('+1 day', strtotime($discount->date_end)-1)) {
                $response = array('price'=>$model->price, 'message'=>'Error. This code is ended.');
            }
            else
            {
                $price = $discount->getPriceWithDiscount($model->price);

                $response = array('price'=>$price, 'message'=>'');
            }
        }
        else
            $response = array('price'=>$model->price, 'message'=>'');

        $response = json_encode($response);
        echo $response;
        Yii::app()->end();
    }

    public function actionFetchOrder()
    {
        $post = implode('==',$_POST);
        file_put_contents('/var/www/courses.2ndskies/httpdocs/protected/runtime/IPN.log',$post);
        /*  $transactionID=$_POST["txn_id"];
          $item=$_POST["item_name"];
          $amount=$_POST["mc_gross"];
          $currency=$_POST["mc_currency"];
          $datefields=explode(" ",$_POST["payment_date"]);
          $time=$datefields[0];
          $date=str_replace(",","",$datefields[2])." ".$datefields[1]." ".$datefields[3];
          $timestamp=strtotime($date." ".$time);
          $status=$_POST["payment_status"];
          $firstname=$_POST["first_name"];
          $lastname=$_POST["last_name"];
          $email=$_POST["payer_email"];
          $custom=$_POST["option_selection1"];
          if ($transactionID AND $amount)
          {
              // query to save data
              return $this->insertID;
          }
          else
          {
              return 0;
          }*/
    }
    public function actionCreditCardError()
    {
        if(isset($_GET))
        {
            $msg=$_GET['RESPMSG']; print_r($_GET); die();
            $this->responseError($msg);
        }
    }
}
