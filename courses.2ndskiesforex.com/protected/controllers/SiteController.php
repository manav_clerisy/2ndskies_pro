<?php

class SiteController extends Controller
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */

    public function accessRules()
    {
        return array(
            array('allow', 'actions'=>array('index,sendMailTest'),'users'=>array('*')),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * homepage
     *
     * @param integer $courseId
     * @param integer $_GET[days] - number of days from today, user would like to get economic events for
     * @param string $_GET[event_time] - time zone offset
     * @param string $_GET[posts] - number of new post user want to show
     */
	public function actionIndex($courseId = null)
	{
        if (Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->createUrl('courses/list'));
            
        if(empty($courseId)) {
            if(Yii::app()->user->isAdmin()) {
                $courseId = 1;
            } else {
                $userCourse = UserCourse::model()->findByAttributes(array('user_id'=>Yii::app()->user->id));

                if(empty($userCourse))
                    $this->redirect('courses/list');
                else
                    $courseId = $userCourse->course_id;
            }
        }

        Yii::app()->session['courseId'] =  $courseId;

        $userTimezone = Profile::model()->findByPk(Yii::app()->user->id)->getAttribute('timezone');
        if(empty($userTimezone))
            $userTimezone = 'America/Santiago';

        $time = Courses::model()->timezone($userTimezone);


        if(isset($_GET['event_time'])) {
            $userTimezone = $_GET['event_time'];
            Profile::model()->findByPk(Yii::app()->user->id)->saveAttributes(array('timezone'=>$userTimezone));
        }

        if (isset($_GET['days']))
            $economicEvents = EconomicEvents::model()->getTodaysEconomicEvents($userTimezone,$_GET['days']);
        else
            $economicEvents = EconomicEvents::model()->getTodaysEconomicEvents($userTimezone);


        $announcements = Comment::model()->getAnnouncements($courseId);


        $postsCount = 10;
        if(isset($_GET['posts']))
            $postsCount = $_GET['posts'];

        $mostRecentPosts = Topic::model()->searchLastTopics($courseId,$postsCount);

        if (Yii::app()->request->isAjaxRequest && isset($_GET['posts'])) {
            $this->renderPartial('most_recent', array(
                'mostRecentPosts' => $mostRecentPosts,
            ));
            Yii::app()->end();
        }

        $subscriptions = Topic::model()->searchSubscriptions($courseId);

        $this->render('index',array(
            'time'=>$time,
            'offset'=>$userTimezone,
            'economicEvents' => $economicEvents,
            'announcements' =>$announcements,
            'mostRecentPosts'=>$mostRecentPosts,
            'subscriptions'=>$subscriptions
        ));
	}


   /* public function actionSendMailTest()
    {
        User::model()->sendActivationEmailForNewUser();
       // Yii::app()->getResponseMailer->sendMail('svetlana.aliferova@techs.com.ua','test','test');
       // Yii::app()->getResponseMailer->addContact('email@mail.ru','tester');
    }*/


	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}


    public function actionEmbed($id = null)
    {
        if (!Yii::app()->user->isGuest && !is_null($id) && !is_null(Yii::app()->request->urlReferrer)) {
            $video = Comment::model()->findByAttributes(array('topic_id' => $id, 'is_first' => 1));
            preg_match('|<iframe [^>]*src="([^"]+)"[^>]*|', $video->text, $matches);
            if (!empty($matches)) {
                $src = $matches[1];
                $src = str_replace('http:', 'https:', $src);
                $this->redirect($src);
            }
        } else {
            $this->redirect('/site/index');
        }
    }

}