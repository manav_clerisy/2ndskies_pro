<?php

class TopicController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('view','comment','create','upload','imageUpload','reply','editComment','subscribe','unsubscribe','subscriptionNotify'),
				'users'=>array('@'),
			),
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('deleteComment', 'deleteTopic', 'replaceTopic'),
                'users'=>Yii::app()->getModule('user')->getAdmins(),
            ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */

	public function actionCreate($section_id = null)
	{
        $section = Sections::model()->findByPk($section_id);
        $this->setActiveSection($section);

        $userId = Yii::app()->user->id;
        $date = new CDbExpression('NOW()');

        $modelTopic=new Topic;
        $modelTopic->section_id = $section_id;
        $modelTopic->user_id = $userId;
        $modelTopic->date_changed = $modelTopic->date_created = $date;

        $modelComment = new Comment;
        $modelComment->is_first = 1;
        $modelComment->user_id = $userId;
        $modelComment->date_changed = $modelComment->date_created = $date;

		if(isset($_POST['Topic']))
        {
            $modelTopic->attributes=$_POST['Topic'];

            $modelTopic->save();

            if(isset($_POST['Comment']))
            {
                $modelComment->text = $_POST['Comment'];

                $modelComment->topic_id = $modelTopic->id;
                if($modelComment->save()){
                    $modelTopic->first_comment_id = $modelComment->id;
                    $modelTopic->save(false,array('first_comment_id'));

                    if($modelTopic->status != 1)
                        Subscriptions::model()->newSubscription($modelTopic->id);

                    $this->redirect(array('view','id'=>$modelTopic->id));
                }
            }
        }

		$this->render('create',array(
			'model'=>$modelTopic
		));
	}

    /**
     * Creates a new comment for a topic.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param integer $topic_id
     */
    public function actionComment($topic_id)
    {
        $topic = $this->loadModel($topic_id);

        $this->assertCourseAccessible($topic->section->course_id);

        $modelComment = new Comment;
        $modelComment->topic_id = $topic_id;
        $modelComment->is_first = 0;
        $modelComment->user_id = Yii::app()->user->id;
        $modelComment->date_changed = $modelComment->date_created = new CDbExpression('NOW()');

        $topic->date_changed = new CDbExpression('NOW()');
        if(isset($_POST['Comment']))
        {
            $text = $_POST['Comment']['text'];
            //$text = preg_replace('/(iframe\s+[^>]*src=")(http:|https:)([^"]*"[^>])*/','$1$3',$text);
            //$text = preg_replace('/(src=")(http:|https:)([^"]*"*)/','$1$3',$text);

            $modelComment->attributes = $_POST['Comment'];
            $modelComment->text = $text;
           $topic->save();
           if($modelComment->save())
           {
               $this->sendNotification($topic->subject, $modelComment->user->username, $topic_id, $topic->user_id);
               $this->redirect($modelComment->getCommentUrl($modelComment->id));
           }
        }
        $this->redirect(Yii::app()->createUrl('topic/view',array('id'=>$topic_id)));

    }

    /**
     * Edit a comment for a topic.
     * If editing is successful, the browser will be redirected to the 'view' page.
     * @param integer $id - comment id
     * @throw CHttpException 404 if user does not have access to a course
     */
    public function actionEditComment($id)
    {
        $comment = Comment::model()->findByPk($id);
        $topic = $this->loadModel($comment->topic_id);

        $section = Sections::model()->findByPk($topic->section_id);
        if (! Yii::app()->user->model()->isSubscribedOnCourse($section->course_id))
            throw new CHttpException(404,'You don\'t have access on this course');

        if(isset($_POST['Comment']))
        {
            $text = $_POST['Comment']['text'];
           // $text = preg_replace('/(iframe\s+[^>]*src=")(http:|https:)([^"]*"[^>])*/','$1$3',$text);
           // $text = preg_replace('/(src=")(http:|https:)([^"]*"*)/','$1$3',$text);

            $comment->attributes = $_POST['Comment'];
            $comment->text = $text;
            if($comment->save())
                $this->redirect($comment->getCommentUrl($comment->id));
        }

        $this->redirect(Yii::app()->createUrl('topic/view',array('id'=>$comment->topic_id,'#'=>$comment->id)));

    }

    /**
     * Delete a comment for a topic.
     * @param integer $commentId
     */
    public function actionDeleteComment()
    {
        $commentId = Yii::app()->request->getPost('commentId',null);
        Comment::model()->findByPk($commentId)->delete();
    }

    /**
     * Delete a topic.
     * If deleting is successful, the browser will be redirected to the 'view' page of next topic.
     * @param integer topicId
     */
    public function actionDeleteTopic()
    {
        $topicId = Yii::app()->request->getPost('topicId',null);

        $topic = Topic::model()->findByPk($topicId);
        if ($topic->delete())
            Comment::model()->deleteAllByAttributes(array('topic_id' => $topic->id));

        $nextTopicId = Topic::model()->getNextTopicId($topic->id, $topic->section_id);
        if ($nextTopicId) {
            echo Yii::app()->createAbsoluteUrl('/topic/view', array('id' => $nextTopicId));
        } else {
            $courseId = Yii::app()->session['courseId'];
            echo Yii::app()->createAbsoluteUrl('/courses/sectionlist', array('courseId' => $courseId, 'sectionId' => $topic->section_id, 'isMenu' => true));
        }
    }

    /**
     * Move a topic to another section.
     *
     * @params integer $sectionId, $topicId
     * @return string
     */
    public function actionReplaceTopic()
    {
        $sectionId = Yii::app()->request->getPost('sectionId',null);
        $topicId = Yii::app()->request->getPost('topicId',null);

        if($sectionId && $topicId)
        {
            $topic = Topic::model()->findByPk($topicId);
            $topic->section_id = $sectionId;
            if($topic->save())
                echo '<div class="alert alert-block alert-success" id="user-message">You have successfully moved topic!</div>';
            else
                echo '<div class="alert alert-block alert-error" id="user-message">The topic was not moved. Please, try again.</div>';

        }

        Yii::app()->cache->flush();
        Yii::app()->end();
    }

    /**
     * View a topic.
     *
     * @params integer $id - topic id, $isSingleVideoLayout
     */
	public function actionView($id, $isSingleVideoLayout = null)
    {
        $topic = $this->loadModel($id);

        $this->setActiveSection($topic->section);
        
        $model = new Comment;
        $model->topic_id = $id;

        $this->saveLastCommentToSubscriptions($id, Yii::app()->user->id);

        $this->render('view',array(
            'model'=>$model,
            'dataProvider' => $model->searchComments($isSingleVideoLayout),
            'topic'=>$topic,
        ));
    }

    /**
     * Subscribe to a topic.
     *
     * @params integer $topicId
     * @return string
     */
    public function actionSubscribe()
    {
        $topicId = Yii::app()->request->getPost('id',null);
        $userId = Yii::app()->user->id;
        $courseId = Yii::app()->session['courseId'];

        $lastCommentId = Topic::model()->findByPk($topicId)->getAttribute('last_comment_id');

        $userSubscriptions = Subscriptions::model()->findByAttributes(array('user_id'=>$userId, 'course_id'=>$courseId));

        if(!empty($userSubscriptions))
            $userSubscriptions->topic_id_arr .= ','.$topicId.'=>'.$lastCommentId.',';
        else
        {
            $userSubscriptions = new Subscriptions();
            $userSubscriptions->user_id = $userId;
            $userSubscriptions->course_id = $courseId;
            $userSubscriptions->topic_id_arr = ','.$topicId.'=>'.$lastCommentId.',';
        }

        if($userSubscriptions->save())
            echo '<div class="alert alert-block alert-success" id="user-message">You have successfully subscribed to the topic!</div>';
        else
            echo '<div class="alert alert-block alert-error" id="user-message">You subscription was not saved. Please, try again.</div>';

        Yii::app()->end();
    }

    /**
     * Unsubscribe to a topic.
     *
     * @params integer $topicId
     * @return string
     */
    public function actionUnsubscribe()
    {
        $topicId = Yii::app()->request->getPost('id',null);

        $lastCommentId = Topic::model()->findByPk($topicId)->getAttribute('last_comment_id');
        $userSubscriptions = Subscriptions::model()->findByAttributes(array('user_id'=>Yii::app()->user->id, 'course_id'=>Yii::app()->session['courseId']));

        $subscriptions = str_replace(array(','.$topicId.'=>'.$lastCommentId.',',','.$topicId.','), '',$userSubscriptions->topic_id_arr);

        $userSubscriptions->topic_id_arr = $subscriptions;

        if($userSubscriptions->save(false))
            echo '<div class="alert alert-block alert-success" id="user-message">You have successfully unsubscribed form this topic!</div>';
        else
            echo '<div class="alert alert-block alert-error" id="user-message">You unsubscription was not saved. Please, try again.</div>';

        Yii::app()->end();
    }

    /**
     * Set parameters of subscription notification
     *
     * @params array $notify
     * @return string
     */
    public function actionSubscriptionNotify()
    {
        $notify = Yii::app()->request->getPost('form',null);
        $userId = Yii::app()->user->id;

        $userNotifications = UserNotifications::model()->findByAttributes(array('user_id'=>$userId));

        if(!empty($userNotifications))
            $userNotifications->notify = $notify[0]['value'];
        else
        {
            $userNotifications = new UserNotifications();
            $userNotifications->user_id = $userId;
            $userNotifications->notify = $notify[0]['value'];
        }
        if($userNotifications->save())
            echo '<div class="alert alert-block alert-success" id="user-message">Saved</div>';
        else
            echo '<div class="alert alert-block alert-error" id="user-message">Was not saved. Please, try again.</div>';

        Yii::app()->end();
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
     * @return Topic
	 */
	public function loadModel($id)
	{
		$model=Topic::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='topic-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    /**
     * Uploading files in wysiwyg
     */
    public function actionUpload()
    {
        $directory = Yii::getPathOfAlias('web.uploads.files').'/';
        $file = md5(date('YmdHis')).'.'.pathinfo(@$_FILES['file']['name'], PATHINFO_EXTENSION);

        if (move_uploaded_file(@$_FILES['file']['tmp_name'], $directory.$file)) {
            $array = array(
                'filelink' => '/uploads/files/'.$file
            );
        }
        header("Content-type: application/json; charset=utf-8");
        echo CJSON::encode($array);
    }

    /**
     * Send notifications to admin about new comment for a topic.
     *
     * @params string $topicSubject, $userName
     * @params integer $topicId
     */
    public function sendNotificationToAdmin($topicSubject, $userName, $topicId)
    {
        $admins = User::model()->findAllByAttributes(array('role'=>1));
        $subject = 'New comment to the topic "'.$topicSubject.'"';
        $link = Yii::app()->createAbsoluteUrl('topic/view',array('id'=>$topicId));

        Apostle::setup(Yii::app()->params->apostleKey);

        $mail = new Apostle\Mail('admin-notification');
        $mail->from = Yii::app()->params->adminEmail;
        $mail->__set('subject', $topicSubject);
        $mail->__set('name', $userName);
        $mail->__set('link', $link);

        foreach($admins as $admin)
        {
            $mail->email = $admin->email;
            $mail->deliver();
        }
    }

    /**
     * Send notification to a user about new comment for a topic user is subscribed to.
     *
     * @params string $topicSubject, $userName
     * @params integer $topicId, $authorId
     */
    public function sendNotification($topicSubject, $userName, $topicId, $authorId)
    {
        $criteria = new CDbCriteria();
        $criteria->addSearchCondition('topic_id_arr',','.$topicId.'=>');

        $userSubscribed = Subscriptions::model()->findAll($criteria);

        if(!empty($userSubscribed))
        {
            foreach($userSubscribed as $user)
            {
                $userIdArr[] = $user->user_id;
            }

            $criteria = new CDbCriteria();
            $criteria->addInCondition('user_id',$userIdArr);
            $criteria->compare('notify',1);
            $userNotifications = UserNotifications::model()->findAll($criteria);

            $subject = 'Topic "'.$topicSubject.'" update on 2ndSkies';
            $topicAuthor = User::model()->findByAttributes(array('id'=>$authorId));

            Apostle::setup(Yii::app()->params->apostleKey);

            $mail = new Apostle\Mail('topic-notification');

            $mail->from = Yii::app()->params->adminEmail;
            $mail->__set('link', Yii::app()->createAbsoluteUrl('topic/view',array('id'=>$topicId)));

            foreach($userNotifications as $user)
            {
                $userModel = User::model()->findByAttributes(array('id'=>$user->user_id));

                $mail->email = $user->user->email;
                $mail->__set('name', $userModel->username);
                $mail->__set('author', $topicAuthor->username);
                $mail->__set('sender', $userName);
                $mail->__set('subject', $topicSubject);

                $mail->deliver();
            }
        }

    }

    /**
     * Save last comments ids of a topics user is subscribed to.
     *
     * @params integer $topicId, $userId
     */
    public function saveLastCommentToSubscriptions($topicId, $userId)
    {
        $criteria = new CDbCriteria();
        $criteria->addSearchCondition('topic_id_arr',','.$topicId.'=>');
        $criteria->compare('user_id',$userId);

        $subscriptions = Subscriptions::model()->findAll($criteria);

        $wasChanged = false;

        foreach($subscriptions as $subscription)
        {
            $topics = explode(',',$subscription->topic_id_arr);

            foreach($topics as $topic)
            {
                if(!empty($topic))
                {
                    if(strrpos($topic,$topicId.'=>') !== false)
                    {
                        $lastCommentId = Topic::model()->findByPk($topicId)->getAttribute('last_comment_id');

                        if($topic != $topicId.'=>'.$lastCommentId)
                        {
                            $subscription->topic_id_arr = str_replace($topic,$topicId.'=>'.$lastCommentId,$subscription->topic_id_arr);
                            $wasChanged = true;
                            break;
                        }
                    }
                }
            }

            if($wasChanged)
            {
                $subscription->save();
                break;
            }
        }
    }

    public function actionImageUpload($attr = '')
    {
        $name=strtolower($this->getId());
        $attribute=strtolower((string)$attr);

        if ($this->uploadPath===null) {
            $path=Yii::app()->basePath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'uploads';
            $this->uploadPath=realpath($path);
            if ($this->uploadPath===false && $this->uploadCreate===true) {
                if (!mkdir($path,$this->permissions,true)) {
                    throw new CHttpException(500,CJSON::encode(
                        array('error'=>'Could not create upload folder "'.$path.'".')
                    ));
                }
            }
        }
        if ($this->uploadUrl===null) {
            $this->uploadUrl=Yii::app()->request->baseUrl .'/uploads';
        }

        // Make Yii think this is a AJAX request.
        $_SERVER['HTTP_X_REQUESTED_WITH']='XMLHttpRequest';

        $file=CUploadedFile::getInstanceByName('file');
        if ($file instanceof CUploadedFile) {
            $attributePath=$this->uploadPath.DIRECTORY_SEPARATOR.$name.DIRECTORY_SEPARATOR.$attribute;
            if (!in_array(strtolower($file->getExtensionName()),array('gif','png','jpg','jpeg'))) {
                throw new CHttpException(500,CJSON::encode(
                    array('error'=>'Invalid file extension '. $file->getExtensionName().'.')
                ));
            }
            $fileName=trim(md5($attribute.time().uniqid(rand(),true))).'.'.$file->getExtensionName();
            if (!is_dir($attributePath)) {
                if (!mkdir($attributePath,$this->permissions,true)) {
                    throw new CHttpException(500,CJSON::encode(
                        array('error'=>'Could not create folder "'.$attributePath.'". Make sure "uploads" folder is writable.')
                    ));
                }
            }
            $path=$attributePath.DIRECTORY_SEPARATOR.$fileName;
            if (file_exists($path) || !$file->saveAs($path)) {
                throw new CHttpException(500,CJSON::encode(
                    array('error'=>'Could not save file or file exists: "'.$path.'".')
                ));
            }
            $attributeUrl=$this->uploadUrl.'/'.$name.'/'.$attribute.'/'.$fileName;
            $data = array(
                'filelink'=>$attributeUrl,
            );
            echo CJSON::encode($data);
            exit;
        } else {
            throw new CHttpException(500,CJSON::encode(
                array('error'=>'Could not upload file.')
            ));
        }
    }


}
