﻿<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php
		Yii::app()->bootstrap->register();
	?>
	<link rel="stylesheet" type="text/css" href="/2ndskiesdev-project/live/courses.2ndskiesforex.com/css/styles.css">
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <script src="/2ndskiesdev-project/live/courses.2ndskiesforex.com/js/chosen.jquery.min.js"></script>
    <script src="/2ndskiesdev-project/live/courses.2ndskiesforex.com/js/main.js" ></script>
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:400,300' rel='stylesheet' type='text/css'>
<script src="https://cdn.optimizely.com/js/5767624972.js"></script>
	<!-- Start Visual Website Optimizer Asynchronous Code -->
    <script type='text/javascript'>
    var _vwo_code=(function(){
    var account_id=61633,
    settings_tolerance=2000,
    library_tolerance=2500,
    use_existing_jquery=false,
    // DO NOT EDIT BELOW THIS LINE
    f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
    </script>
    <!-- End Visual Website Optimizer Asynchronous Code -->
	<!--script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-6226756-13', 'auto');
	  ga('send', 'pageview');

	</script-->
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-78643158-1', 'auto');
  ga('send', 'pageview');

</script>
    </head>

<?php
/*
if(isset(Yii::app()->user->email) && !empty(Yii::app()->user->email)) { ?>
    <script>
        window.intercomSettings = {
            // TODO: The current logged in user's full name
            name: "<?php echo Yii::app()->user->name; ?>",
            // TODO: The current logged in user's email address.
            email: "<?php echo Yii::app()->user->email; ?>",
            // TODO: The current logged in user's sign-up date as a Unix timestamp.
            created_at: <?php echo time(); ?>,
            app_id: "98aqmhat"
        };
    </script>
    <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/98aqmhat';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
    <?php }*/?>

<body>

<?php if(strpos(Yii::app()->request->requestUri,'endPayment') != false) {
    $courseId = Yii::app()->request->getQuery('courseId', false);
    if ($courseId) {
        $courseId = (int)$courseId;
        switch ($courseId) {
            case 1: $offerId = 2;
                break;
            case 2: $offerId = 4;
                break;
            case 3: $offerId = 6;
                break;
            default:
                $offerId = null;
        }
        if (!is_null($offerId)) {
            echo "<!-- Offer Conversion: 2ndSkies Advanced Ichimoku Course -->
                  <iframe src='https://2ndskiesforex.go2cloud.org/aff_l?offer_id=$offerId' scrolling='no' frameborder='0' width='1' height='1'></iframe>
                  <!-- // End Offer Conversion -->";
        }
    } ?>
    <script type='text/javascript'>
        !function(d,s) {
            var rc = d.location.protocol + "//go.referralcandy.com/purchase/hxot5xdldvolthexcxr8wbpfw.js";
            var js = d.createElement(s);
            js.src = rc;
            var fjs = d.getElementsByTagName(s)[0];
            fjs.parentNode.insertBefore(js,fjs);
        }(document,"script");
    </script>
<?php }?>

<?php if (strpos(Yii::app()->request->requestUri,'registration') != false ||
          strpos(Yii::app()->request->requestUri,'startPayment') != false ||
          strpos(Yii::app()->request->requestUri,'endPayment') != false) :?>
		  
<?php
$rawData_course = (Yii::app()->user->isAdmin()) ? Courses::model()->findAll() : Courses::model()->findAllByAttributes(array('visible' => 1));
//print_r($rawData_course);
$courseArray=array();
foreach($rawData_course as $key=>$course)
{
	if($course->isUserSubscribed())
	{
		$courseArray[$course->id]['label'] = $course->name;
		$courseArray[$course->id]['url'] = Yii::app()->createUrl("/site/index", array("courseId" => $course->id));

	}
	elseif ($course->id != 5 && $course->id != 3)
	{
		$courseArray[$course->id]['name'] = $course->name;
		$courseArray[$course->id]['price'] =$course->price;
		$courseArray[$course->id]['slug'] =$course->slug;
		
	}
}

if(isset($_GET["id"])):
$courseId=strip_tags($_GET["id"]);
$courseData=$courseArray[$courseId];
$price=isset($courseData["price"])?$courseData["price"]:'';
$name=isset($courseData["name"])?$courseData["name"]:'';
$slug=isset($courseData["slug"])?$courseData["slug"]:'';

?>
<script>
dataLayer = [{
 'transactionId': '1234',
 'transactionAffiliation': 'Acme Clothing',
 'transactionTotal': <?php echo $price; ?>,
 'transactionTax': 0.00,
 'transactionShipping': 1,
 'transactionProducts': [{
 'sku': '<?php echo $slug; ?>',
 'name': '<?php echo $name; ?>',
 'category': 'course',
 'price':  <?php echo $price; ?>,
 'quantity': 1
 },{
 'sku': '<?php echo $slug; ?>',
 'name': '<?php echo $name; ?>',
 'category': 'course',
 'price': <?php echo $price; ?>,
 'quantity': 1
 }]
}];
</script>
<?php
endif;


if(isset($_GET["courseId"])):
$courseId=strip_tags($_GET["courseId"]);
$courseData=$courseArray[$courseId];
$price=isset($courseData["price"])?$courseData["price"]:'';
$name=isset($courseData["name"])?$courseData["name"]:'';
$slug=isset($courseData["slug"])?$courseData["slug"]:'';

?>
<script>
dataLayer = [{
 'transactionId': '<?php echo $courseId.'-'.$slug; ?>',
 'transactionAffiliation': 'Acme Clothing',
 'transactionTotal': <?php echo $price; ?>,
 'transactionTax': 0.00,
 'transactionShipping': 1,
 'transactionProducts': [{
 'sku': '<?php echo $slug; ?>',
 'name': '<?php echo $name; ?>',
 'category': 'course',
 'price':  <?php echo $price; ?>,
 'quantity': 1
 },{
 'sku': '<?php echo $slug; ?>',
 'name': '<?php echo $name; ?>',
 'category': 'course',
 'price': <?php echo $price; ?>,
 'quantity': 1
 }]
}];
</script>
<?php
endif;


$connection = Yii::app()->db;
$command = $connection->createCommand('select * from users where id="'.Yii::app()->user->id.'"');
$row = $command->queryRow(); 
if(isset($row['email']) && !empty($row['email']))
{
?>
<script src="//static.tapfiliate.com/tapfiliate.js" type="text/javascript" async></script> 
<script type="text/javascript"> 
window['TapfiliateObject'] = i = 'tap'; 
window[i] = window[i] || function () { 
(window[i].q = window[i].q || []).push(arguments); 
}; 

tap('create', '2257-f01c5f'); 
tap('conversion', '<?php echo $row['email']; ?>'); 
</script> 

<?php
}
?>
		  
		  
		  	
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WLRNLV"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WLRNLV');</script>
<!-- End Google Tag Manager -->

<?php endif;?>

<div id="wrapper">
	<div class="w1">
		<?php
        $dependency = new CDbCacheDependency('SELECT id FROM user_course WHERE user_id='.Yii::app()->user->id);
//        if (Cache::begin(array('mainmenu-user-', Yii::app()->user->id), 30*24*60*60,$dependency))
//        {

            $rawData = (Yii::app()->user->isAdmin()) ? Courses::model()->findAll() : Courses::model()->findAllByAttributes(array('visible' => 1));

            $courseArr = array();

            foreach($rawData as $key=>$course)
            {
                if($course->isUserSubscribed())
                {
                    $courseArr[$key]['label'] = $course->name;
                    $courseArr[$key]['url'] = Yii::app()->createUrl("/site/index", array("courseId" => $course->id));

                }
                elseif ($course->id != 5 && $course->id != 3)
                {
                    $courseArr[$key]['label'] = $course->name;
                    $courseArr[$key]['url'] = Yii::app()->createUrl("/courses/list");
                }
            }

            if((Yii::app()->controller->action->id == 'startPayment') || (Yii::app()->controller->action->id == 'registration') || !Yii::app()->user->isGuest)
                $loginVisible = false;
            else
                $loginVisible = true;

            if((Yii::app()->controller->action->id == 'startPayment') || (Yii::app()->controller->action->id == 'registration') || Yii::app()->user->isGuest)
                $searchForm = '';
            else
                $searchForm ='<form id="searchForm" class="pull-right search-form" type="search" action="'.Yii::app()->createUrl("courses/search", array("courseId" => Yii::app()->session["courseId"])).'" method="get">
                                        <div class="input-prepend">
                                            <span class="add-on"><i class="icon-search"></i></span>'.
                                        CHtml::textField("keyword", isset($_GET["keyword"])?$_GET["keyword"]:"", array("class"=>"input-medium", "placeholder" => "Search")).
                                        '</div>
                                        <button type="submit" class="btn">Go</button>
                                </form>';


             $this->widget('bootstrap.widgets.TbNavbar',array(
                    'items'=>array(
                        array(
                            'class'=>'bootstrap.widgets.TbMenu',
                            'htmlOptions'=>array('class'=>'clearfix'),
                            'items'=>array(
                                array('label'=>'Home', 'icon'=>'home', 'url'=>array('/site/index'), 'visible'=>!Yii::app()->user->isGuest),
                                array('label'=>'Course', 'icon'=>'icon-play-circle', 'url'=>array('#'), 'visible'=>!Yii::app()->user->isGuest,'items'=>$courseArr),
                                array(
                                    'label'=>'Messages',
                                    'url'=>array('/messages/messagesUsersList'),
                                    'visible'=>!Yii::app()->user->isGuest,
                                    'template'=>Yii::app()->user->isGuest
                                                    ? '{menu}'
                                                    : '{menu}<span class="label label-important">'.Yii::app()->user->model()->amountOfUnreadMessages.'</span>',
                                    'icon' => 'envelope'
                                ),
                                array('label'=> (isset(Yii::app()->session['courseId']) && Yii::app()->session['courseId'] == '4' && !Yii::app()->user->isAdmin()) ? 'Mental Performance Journal' : 'Calendar', 'icon'=>'calendar', 'url'=>array('/calendar/index'), 'visible'=>Yii::app()->user->isAdmin() || (Yii::app()->user->model() && Yii::app()->user->model()->getHasAccessToCalendarOfCourse(Yii::app()->session['courseId']))),
                                array('label'=>'Admin', 'icon'=>'cog', 'url'=>array('/admin/default/index'), 'visible'=>Yii::app()->user->isAdmin()),
                                array('label'=>'Edit Economic Events', 'icon'=>'cog', 'url'=>array('/admin/economicEvents/manage'), 'visible'=>Yii::app()->user->isEconomicEventsEditor()),
                                array('label'=>'Profile','icon'=>'user', 'url'=>array('/user/profile'), 'visible'=>!Yii::app()->user->isGuest),

                                array('label'=>'Login', 'icon'=>'ok', 'url'=>Yii::app()->getModule('user')->loginUrl, 'visible'=>$loginVisible),

                                array('label'=>'Logout ('.Yii::app()->user->name.')', 'icon'=>'off', 'url'=>Yii::app()->getModule('user')->logoutUrl, 'visible'=>!Yii::app()->user->isGuest),
                            ),
                        ),
                        //search form
                        $searchForm
                    ),
                ));
//            Cache::end();
//        }

        ?>



		<div class="container" id="page">
			<div class="menu-container">
			<?php
            if((!Yii::app()->user->isGuest) && (!empty(Yii::app()->session['courseId'])) && (!strpos(Yii::app()->request->url,'admin')))//if user not a guest and we are not in the admin area
            {
                /*$dependency = new CDbCacheDependency('SELECT max(sections.id),max(topics.id) FROM sections,topics WHERE sections.course_id='.Yii::app()->session['courseId']);

                if (Cache::begin(array('submenu-user-', Yii::app()->user->id.'-course-'.Yii::app()->session['courseId']), 30*24*60*60,$dependency))
                {*/
                    $subMenu = Courses::model()->createMenu();

                    if(isset($subMenu))
                        $this->widget('application.components.MyMenu',array(
                            'type'=>'pills',
                            'encodeLabel' => false,
                            'htmlOptions'=>array('id'=>'courseMenu'),

                            'items'=>$subMenu
                        ));
                   /* Cache::end();
                }*/
            }
			?>

			</div>

			<?php
            // Course sections header
            if (isset($this->section)) {
                if (Cache::begin(array('course-header', $this->section->id), 0, 'course-sections-' . $this->section->course_id)) {
                    $items = array();
                    foreach ($this->section->course->topSections as $section) {
                        $items[] = array(
                            'label' => $section->name,
                            'url' => $section->createRouteFor(),
                            'active' => $this->section->isInside($section),
                        );
                    }
                   Cache::end();
                }
            }

            // Breadcrumbs
            if (isset($this->breadcrumbs)) {
                $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                    'homeLink' => false,
                    'encodeLabel' => false,
                ));
            }

		    $this->widget('bootstrap.widgets.TbAlert', array(
		        'block'=>true, // display a larger alert block?
		        'fade'=>true, // use transitions?
		        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
		    ));

			echo $content;

            ?>
		</div>
	</div>

	<!-- footer -->
	<footer id="footer">
		<div class="footer-holder">
			<div class="footer-frame">
				<div class="container">
					<p>Copyright © 2007 - <?php echo date('Y');?> 2ndSkies Forex. All rights reserved. </p>
				</div>
			</div>
		</div>
	</footer>
</div><!-- page -->

</body>
</html>
