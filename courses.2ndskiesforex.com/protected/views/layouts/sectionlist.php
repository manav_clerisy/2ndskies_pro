<?php $this->beginContent('//layouts/main')?>


<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'searchForm',
    'type'=>'search',
    'action' => Yii::app()->createUrl('courses/search', array('courseId' =>Yii::app()->session['courseId']/* isset($_GET['courseId'])?$_GET['courseId']:''*/)),
    'htmlOptions'=>array('class'=>'well'),
    'method'=>'get'
));?>

<div class="input-prepend">
    <span class="add-on"><i class="icon-search"></i></span>
    <?=CHtml::textField('keyword', isset($_GET['keyword'])?$_GET['keyword']:'', array('class'=>'input-medium', 'placeholder' => 'Search'))?>
</div>

<button type="submit" class="btn">Go</button>

<?php $this->endWidget(); ?>

<?=$content?>

<?php $this->endContent()?>