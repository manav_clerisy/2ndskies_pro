<?php
$course = array(
    'Courses'=>Yii::app()->createUrl('courses/list'),
);
$section = $model->section->generateBreadcrumbs(true);

$this->breadcrumbs = array_merge($course, $section);

?>

<h3>Create Topic</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>