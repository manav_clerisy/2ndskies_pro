<?php

/* @var $topic Topic */

$this->layout ='//layouts/main';

Yii::app()->clientScript->registerScriptFile('/js/admin/topics/topic.js', CClientScript::POS_END);
?>


<div id="header"><h3><?=$topic->subject ?></h3></div>

<div>
<?php

$deleteTopicHtml = '';

if(Yii::app()->user->isAdmin())
{
    echo Sections::model()->getSectionsForCourse($topic->section_id);
    echo CHtml::Button('Move Topic', array('class'=>'btn btn-warning pull-left', 'id'=>'moveTopic'));

    echo CHtml::hiddenField('topicId',$topic->id, array('id'=>'topicId'));

    $deleteTopicHtml =
        $this->widget('bootstrap.widgets.TbButton', array(
            'type'=>'danger',
            'htmlOptions' => array(
                'class' => 'pull-right',
                'id'=>'deleteTopic',
                'name'=>$topic->id,
                'href'=>'#'
            ),
            'label'=>'Delete Topic',
        ), true) ;
}
?>
</div>

<br><br>

<?php
# Reply button and WYSIWYG
$replyHtml =
    $this->widget('bootstrap.widgets.TbButton', array(
        'type'=>'info',
        'htmlOptions' => array(
            'class' => 'pull-right post-reply',
            'id'=>'post-reply',
            'href'=>'#comment'
        ),
        'label'=>'Post reply',
    ), true) ;

//subscribe to topic button
$isUserSubscribed = Subscriptions::model()->isUserSubscribed($topic->id);

$subscribeHtml =
    $this->widget('bootstrap.widgets.TbButton', array(
        'type'=>'info',
        'htmlOptions' => array(
            'class' => ($isUserSubscribed) ? 'pull-right hidden':'pull-right',
            'id'=>'subscribe',
            'name'=>$topic->id,
            'href'=>'#'
        ),
        'label'=>'Subscribe',
    ), true) ;

$unsubscribeHtml =
    $this->widget('bootstrap.widgets.TbButton', array(
        'type'=>'success',
        'htmlOptions' => array(
            'class' => ($isUserSubscribed) ? 'pull-right': 'pull-right hidden',
            'id'=>'unsubscribe',
            'name'=>$topic->id,
            'href'=>'#'
        ),
        'label'=>'Unsubscribe',
    ), true) ;


?>


<?php

    $this->widget('bootstrap.widgets.TbListView', array(
        'dataProvider'=>$dataProvider,
        'itemView'=>'/comment/view',
        'summaryText'=>false,
        'ajaxUpdate' => false,
        'htmlOptions' => array(
            'class' => 'topic-list'
        ),
        'template' => "<div class='topic-panel'>$deleteTopicHtml$replyHtml$subscribeHtml$unsubscribeHtml{pager}</div>\n{items}\n<div class='topic-panel'>$replyHtml{pager}</div>"
    ));



    $this->renderPartial('/comment/_form',array('model'=>new Comment,'topic_id'=>$topic->id));
?>
