<?php
$this->breadcrumbs=array(
    'Topics',
);
?>

<h3>Section's name</h3>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
        'id'=>'topic-grid',
        'htmlOptions' => array('class' => ''),
        //'filter'=>$model,
        'dataProvider'=> $dataProvider,
        'columns'=>array(
            array(
                'name' => 'subject',
                'value' => '$data->theme',
                'type'=>'html',
                'htmlOptions' => array(
                    'class' => 'text-left bold'
                ),
            ),
            array(
                'name' => 'replies',
                'value' => '$data->replies',
                'htmlOptions' => array(
                    'width' => '70px',
                    'class' => 'text-left bold'
                ),
            ),
            array(
                'name' => 'lastPost',
                'value' => '$data->lastPost',
                'type' => 'html',
                'htmlOptions' => array(
                    'width' => '200px',
                    'class' => 'text-left bold'
                ),
            ),
            array(
                'name' => 'date_created',
                'htmlOptions' => array('width' => '150px')
            )
        ),
    ));
?>
<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'type'=>'primary',
        'label'=>'New topic',
        'url'=>Yii::app()->createUrl('topic/create')
    )); ?>
</div>