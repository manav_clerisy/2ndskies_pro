<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'topic-form',
	'enableAjaxValidation'=>false,
));

Yii::app()->clientScript->registerLocalScript('admin/topics/topic.js', CClientScript::POS_END);

?>
    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'subject',array('class'=>'span8','maxlength'=>255)); ?>

	<?php
        $this->widget('ext.redactorWidget.ImperaviRedactorWidget',array(
            'name'=>'Comment',
            'options'=>array(
                'minHeight'=>200,
                'convertVideoLinks'=> true,
                'convertImageLinks'=> true,
                'dragUpload'=>true,
                'fileUpload'=>Yii::app()->createUrl('topic/upload'),
                'fileUploadErrorCallback'=>new CJavaScriptExpression(
                    'function(obj,json) { alert(json.error); }'
                ),
//                'imageUpload'=>Yii::app()->createUrl('topic/upload'),
//                'imageUploadErrorCallback'=>new CJavaScriptExpression(
//                    'function(obj,json) { alert(json.error); }'
//                ),
            ),
        ));
    ?>

    <?php if (Yii::app()->user->isAdmin()) { ?>
        <b>Post topic as:</b>
        <?php echo $form->radioButtonList($model, 'status', $model->getThreadProperties()); ?>
    <?php } ?> 


	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
