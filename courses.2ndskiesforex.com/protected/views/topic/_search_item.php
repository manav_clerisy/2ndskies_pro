<?php
// for global searching
if (is_array($data)) {
    $obj = new Topic();
    $obj->id = $data['id'];
    $obj->attributes = $data;
    $data = $obj;
}
?>
<div class="alert alert-block alert-success" xmlns="//www.w3.org/1999/html">
    <div class="media">
        <div class="span11">
            <p><?=$data->theme?></p>
        </div>
    </div>
</div>