<div class="well">
    <div class="row-fluid" id="c<?= $data->id ?>">
        <?php
        /* @var $data Comment */
        $isCurrentUser = $data->user_id == Yii::app()->user->id;

        // Assertion
        if (!$data->user)
            throw new Exception('User #' . $data->user_id . ' for comment #' . $data->id . ' not found');

        $topic = Topic::model()->findByPk($data->topic_id);

        $isMiscNewsSection = ($data->topic->section_id == 6) || ($data->topic->section_id == 61) || ($data->topic->section_id == 85);

        if ($topic->status == 1 && $data->is_first == 1 && !$isMiscNewsSection)
            $showInCenter = true;
        else
            $showInCenter = false;

        if (!$showInCenter) {
            $cacheId = array('user-stats', $data->user_id, $isCurrentUser);
            foreach ($data->user->badges as $badge)
                $cacheId[] = $badge->id;
            if (Cache::begin($cacheId, 0, 'user-stats-' . $data->user_id)) {
                ?>
                <div class="span3">
                    <strong class="<?= (substr_count($data->user->username, 'Chris Capre')) ? 'chris user-title' : 'user-title'?>"><a
                            href="<?= Yii::app()->createURL('user/profile/userProfile', array('id' => $data->user_id)) ?>"> <?=$data->user->username ?></a></strong>

                    <?php
                    if (!empty($data->userProfile->imageUrl))
                        echo '<img class="img-rounded" alt="" src="' . $data->userProfile->imageUrl . '">';
                    ?>
                    <ul class="topic-info">
                        <span class="muted">
                            <?php
                            echo $data->user->BadgeTitles
                            ?>
                        </span>
                        <?php if (!$isCurrentUser) echo '<li class="link-send"><a href="' . Yii::app()->createURL('messages/conversationWith', array('userId' => $data->user->id)) . '"><i class="icon-envelope" rel="tooltip" title="Send message"></i> Send message</a></li>'; ?>
                        <li>Posts: <span class="muted"><?= $data->posts ?></span></li>
                        <li>Joined: <span class="muted"><?= $data->getDate($data->user->create_at) ?></span></li>
                    </ul>
                </div>
                <?php
                Cache::end();
            }
        }
        $span = !$showInCenter ? 9 : '8 pull-center';
        ?>
        <div class="span<?php echo $span ?>">
            <?php if (!$showInCenter) { ?>
                <div class="topic-heading">

                    <span class="muted">
                    	<i class="icon-time"></i> <?php if (isset($data->date_created)) echo $data->getDate($data->date_created) ?>
                        <span class="pull-right">
                        	<?php if (!$showInCenter) { ?>
            					<a id="reply" rel="tooltip" title="Reply with quote" class="btn btn-mini btn-success pull-left reply" href="#comment" name="<?= $data->id ?>"><i class="icon-comment"></i> Reply</a>
					        <?php }

                            if ($isCurrentUser || UserModule::isAdmin()) {?>
                                <a rel="tooltip" title="Edit this post" class="btn btn-mini btn-success pull-left edit" href="#comment" name="<?=$data->id?>">Edit</a>
                            <?php }
                                if(!$data->is_first && UserModule::isAdmin()) {?>
                                    <a rel="tooltip" title="Delete this post" class="btn btn-mini btn-danger pull-left deleteComment" href="#" name="<?=$data->id?>">Delete Comment</a>
                            <?php }?>
            			</span>
                    </span>
                </div>
            <?php } ?>

            <div class="content"><?php
                $text = $data->text;
                $text = $data->hideSrcIframe($text, $topic->id);
                $text = preg_replace('/(iframe\s+[^>]*)(src=")(http:|https:)([^"]*"[^>]*)/','$1$2$4',$text);
                //$text = preg_replace('/(src=")(http:|https:)([^"]*"*)/','$1$3',$text);
                echo $text; ?>
            </div>

            <?php if (!empty($data->signature) && !$showInCenter) echo '<div class="topic-signature">' . $data->signature . '</div>'; ?>
        	
        	<?php if ($showInCenter) { ?>
				<div class="topic-signature">
					<?php
                    if ($isCurrentUser || UserModule::isAdmin())
						echo '<a rel="tooltip" title="Edit this post" class="btn btn-mini btn-success pull-left edit" href="#comment" name="' . $data->id . '">Edit</a>';
                    ?>
					</div>
            <?php } ?>
        </div>
    </div>
</div>
