<?php
$course = array(
    'Courses'=>Yii::app()->createUrl('courses/list'),
);
$section = $topic->section->generateBreadcrumbs(true);
$breadcrumbs = array_merge($course, $section, array($topic->subject => Yii::app()->createUrl('topic/view',array('id'=>$topic->id))));

$this->breadcrumbs = $breadcrumbs;

?>

<h3>Create Comment</h3>

<?php echo $this->renderPartial('/comment/_form', array('model'=>$model)); ?>