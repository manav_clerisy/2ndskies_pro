<?php
// for global searching
if (is_array($data)) {
    $obj = new Comment();
    $obj->id = $data['id'];
    $obj->attributes = $data;
    $data = $obj;
}

$url = $data->getCommentUrl($data->id);
?>
<div class="alert alert-block alert-info" xmlns="//www.w3.org/1999/html">
    <div class="media">
        <h4><?=CHtml::link($data->topic->subject, $url)?></h4>
        <div class="span8" style="word-wrap:break-word;">
            <div>
                <small><span >Posted by <b><?=CHtml::link($data->user->username, Yii::app()->createURL('user/profile/userProfile',array('id'=>$data->user->id)))?></a></b><i class="muted"> >> </i><?php if(isset($data->date_created)) echo $data->getDate($data->date_created) ?></span></small>
            </div>
            <p><?=$data->text ?></p>
        </div>
        <div class="span0.5"></div>
        <div class="media-body">
            <div class="span5">
            <img class="img-rounded" alt="" src="<?=$data->userImage ?>">
            <p>
                <b><?=$data->user->username ?></b><br>
                <br><small>Posts: <i class="muted"><?=$data->posts ?></i></small><br>
                <small>Joined: <i class="muted"><?=$data->user->create_at ?></i></small>
            </p>
            </div>
        </div>
        <?php if(!empty($data->signature)) echo
            '<hr><small>'.$data->signature.'</small>';?>
    </div>
</div>



