<div class="hidden" id="comment">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'comment-form',
	'enableAjaxValidation'=>false,
    'action'=>Yii::app()->createUrl('topic/comment',array('topic_id'=>$topic_id,))
));

?>

	<?php echo $form->errorSummary($model); ?>

    <?php $this->widget('ext.redactorWidget.ImperaviRedactorWidget',array(
        'model'=>$model,
        'attribute'=>'text',
        'name'=>'redactor',
        'options'=>array(
            'minHeight'=>200,
            'convertVideoLinks'=> true,
            'convertImageLinks'=> true,
            'fileUpload'=>Yii::app()->createUrl('topic/upload'),
            'convertLinks'=>true,
            'dragUpload'=>true,
            'fileUploadErrorCallback'=>new CJavaScriptExpression(
                'function(obj,json) { alert(json.error); }'
            ),
//            'imageUpload'=>Yii::app()->createUrl('topic/upload'),
//            'imageUploadErrorCallback'=>new CJavaScriptExpression(
//                'function(obj,json) { alert(json.error); }'
//            )
        ),
    ));
    ?>


	<div class="form-actions">
		<?php
        $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Reply' : 'Save',
            'htmlOptions' => array(
                'id'=>'save-btn'
            ),
		)); ?>
        <?php
        $this->widget('bootstrap.widgets.TbButton', array(
            'type'=>'primary',
            'label'=>'Cancel',
            'htmlOptions' => array(
                'id'=>'close-redactor',
                'name'=>$topic_id
		        ),
        )); ?>
	</div>



<?php $this->endWidget(); ?>
</div>