<?php
/* @var $this CalendarController */

    Yii::app()->clientScript->registerPackage('calendar');

    Yii::app()->clientScript->registerScriptFile('/js/calendar.js');

    Yii::app()->clientScript->registerScript('events','
        events = '.json_encode($events).';
    ', CClientScript::POS_BEGIN);
?>


<div class="row-fluid">
    <div id="div-for-replace" class="span6">
        <?php $this->renderPartial('_create_form', array('model' => $model));?>
    </div>

    <div id='calendar' class="span6"></div>
</div>

<!--<div class="modal hide fade modal-calendar" id="modal"></div>-->
