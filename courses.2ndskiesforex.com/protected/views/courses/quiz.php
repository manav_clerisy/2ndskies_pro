<?php
if(isset($model) && !empty($model)){
    echo '<h3>Quiz</h3>';

    $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'quizForm',
        'action'=>Yii::app()->createUrl("/courses/quiz",array('sectionId'=>$_GET['sectionId'])),
        'type'=>'horizontal',
        'enableAjaxValidation'=>false,
    ));

         foreach($model as $questionNumber=>$item)
        {
            echo '<br><div>'.$item->question.'</div>';

            $answers = explode(';',$item->answers);

            foreach($answers as $key=>$answer) {
                if(!empty($answer))
                    echo
                        CHtml::CheckBox('Answers['.$questionNumber.']['.$key.']','', array (
                        'value'=>$key,
                    )).
                    '&nbsp;' . $answer . '<br>';
            }
            echo '<br>';
        }
        echo CHtml::submitButton('Check the Answers', array('class'=>'btn btn-info'));

    $this->endWidget();
 }elseif(isset($response) && $response == 'all correct'){ ?>
    <h3>Quiz Results</h3>

    <p><strong>Congratulations!</strong></p>

    <p>All of your quiz answers were correct! Now it's time to take the next step and shift your theoretical understanding into practical experience.</p>

    <p>You can start by practicing the concepts and methods you've learned by placing live trades in the market (with small position sizing at first) or, if you're not sure you want to commit real capital yet, practice in Forex Tester 2.</p>

    <p>You can get a $50 Forex Tester 2 discount for being a course member of 2nd Skies by using the following link:</p>

    <p><a href="https://secure.payproglobal.com/orderpage.aspx?products=142463" target="_blank">https://secure.payproglobal.com/orderpage.aspx?products=142463</a></p>
<?php } else {
    echo '<h3>Quiz Wrong Answers</h3>'.$response;
 }
