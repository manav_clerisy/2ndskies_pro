<?php
    $this->layout = 'sectionlist';



    $dataProvider=new CArrayDataProvider($topics);

    echo '<h4 style="margin:5% 0 0 0">Topics:</h4>';
    $this->widget('bootstrap.widgets.TbListView', array(
        'dataProvider'=>$dataProvider,
        'itemView'=>'//topic/_search_item',
        'summaryText'=>false,
    ));

    $dataProvider=new CArrayDataProvider($comments);

    echo '<h4 style="margin:5% 0 0 0">Comments:</h4>';
    $this->widget('bootstrap.widgets.TbListView', array(
        'dataProvider'=>$dataProvider,
        'itemView'=>'//comment/_search_item',
        'summaryText'=>false,
    ));


?>