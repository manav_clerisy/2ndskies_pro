<?php

// there is a selected section
$sectionShown = !empty($currentSection);

if ($sectionShown) {
    echo '<h4>'.$currentSection->getAttribute('name').'</h4>';
} else {
    echo '<h1>' . $course->name . '</h1>';
}

echo '<div class="well well-blue">';

if (UserModule::isAdmin())
{
    $this->widget('bootstrap.widgets.TbButton', array(
        'type'=>'info',
        'htmlOptions' => array(
            'class' => 'pull-right',
            'id'=>'daily-add-new',
            'href'=>'/topic/create?section_id='.$currentSection->getAttribute('id')
        ),
        'label'=>'Add New',
    )) ;
}
echo '<br><br>';
// Topics block
$this->widget('bootstrap.widgets.TbListView', array(
    'id'=>'topic-grid',
    'template'=>"{items}{pager}",
    'dataProvider'=> $currentSection->getTopicsProvider(array(
        'select' => 'id, subject, date_created',
        'condition' => 'comments.is_first = 1',
        'order'=> 't.date_created DESC',
        'with' => array(
            'user' => array('select' => 'id, username'),
            'comments',
            'userProfile',
        ),
    )),
    'itemView'=>'/courses/sections/daily_market/_item_view',
    'summaryText'=>false,
    'ajaxUpdate' => false,
    'htmlOptions' => array(
        'class' => 'topic-list'
    ),
));


echo '</div>';