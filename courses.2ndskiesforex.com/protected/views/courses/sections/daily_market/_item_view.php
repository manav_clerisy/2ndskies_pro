<div class="well">
    <div class="row-fluid" id="c<?= $data->id ?>">
              <div class="span3">
                    <strong class="user-title">
                        <a href="<?= Yii::app()->createURL('user/profile/userProfile', array('id' => $data->user_id)) ?>">
                            <?=$data->user->username ?>
                        </a>
                    </strong>
                    <?php
                    if (!empty($data->userProfile->imageUrl))
                        echo '<img class="img-rounded" alt="" src="' . $data->userProfile->imageUrl . '">';
                    ?>
                </div>
        <div class="span9">
                <div class="topic-heading">
                    <h4><?=$data->subject?></h4>
                    <span class="muted">
                    	<i class="icon-time"></i> <?php if (isset($data->date_created)) echo $data->getDate($data->date_created) ?>
                    </span>
                </div>
            <div class="content"><?php

                $text = explode("<br /><br />",$data->comments->text);

                if(isset($text) && !empty($text))
                {
                    if(count($text)>1)  echo array_shift($text)."<br ><br />".array_shift($text)."<br ><br />";
                    else echo $data->comments->text;
                }

                 ?>
            </div>
            <div align="right"><a class="btn btn-info" href="<?=Yii::app()->createUrl('/topic/'.$data->id)?>">Read More</a></div>
        </div>
    </div>
</div>
