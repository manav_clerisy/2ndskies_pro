<?php

/* @var $currentSection Sections */

// there is a selected section
$sectionShown = !empty($currentSection);

if ($sectionShown)
{
    echo '<h4>Section - '.$currentSection->getAttribute('name').'</h4>';
}
else
{
    echo '<h1>' . $course->name . '</h1>';
}

echo '<div class="well well-blue">';

if ((!empty($sections)) && (!$isMenu)) {
    // Section block
    $dataProvider=new CArrayDataProvider($sections);
    $this->widget('bootstrap.widgets.TbListView', array(
        'htmlOptions'=>array('class'=>'table-blue'),
        'dataProvider'=>$dataProvider,
        'viewData' => array('courseId' => $course->id),
        'itemView'=>'//courses/sections/_list_item',
        'summaryText'=>'Sections',
        'summaryCssClass'=>'list-summary',
        'itemsTagName' => 'table',
        'itemsCssClass'=> 'items table',
    ));
}
// Topics block

if ((isset($currentSection)) && (!$isMenu)) {
    $announsementsCount = Topic::model()->count("status = 1 AND section_id = $currentSection->id");
    if ($announsementsCount) {
        $this->widget('bootstrap.widgets.TbGridView', array(
            'htmlOptions' => array('class' => 'table-blue'),
            'template' => "{items}",
            'dataProvider' => $currentSection->getTopicsProvider(array(
                    'select' => 'id, subject, date_created, thumb',
                    'condition' => 't.status = 1',
                    'with' => array(
                        'user' => array('select' => 'id, username'),
                        'replies',
                        'lastComment' => array(
                            'select' => 'date_created',
                            'with' => array(
                                'user' => array(
                                    'select' => 'username',
                                    'alias' => 'lcu',
                                ),
                            ),
                        ),
                        'firstComment' => array(
                            'select' => 'text, stripped_tag_text',
                        ),
                    ),
                )),
            'columns' => array(
                array(
                    'name' => 'Image',
                    'value' => '(!empty($data->imageUrl)) ? CHtml::image($data->imageUrl,"",array("width"=>"200", "height"=>"170")) : $data->image',
                    'type' => 'html',
                    'sortable' => false,

                ),
                array(
                    'name' => 'subject',
                    'value' => '$data->themeForAnnouncement',
                    'type' => 'html',
                    'sortable' => false,


                ),
                array(
                    'name' => 'lastPost',
                    'value' => '$data->lastPostWithCount',
                    'type' => 'html',
                    'htmlOptions' => array(
                        'width' => '150px'
                    ),
                ),

            ),
        ));

    }
}
if ($sectionShown) {

    echo '<h5> Topics</h5>';

    $dataProvider = $currentSection->getTopicsProvider(array(
        'select' => 'id, subject, date_created',
        // 'condition' => 't.status != 1',
        'with' => array(
            'user' => array('select' => 'id, username'),
            'replies',
            'lastComment' => array(
                'select' => 'date_created',
                'with' => array(
                    'user' => array(
                        'select' => 'username',
                        'alias' => 'lcu',
                    ),
                ),
            ),
        ),
    ));

    $this->widget('bootstrap.widgets.TbGridView',array(
        'id'=>'topic-grid',
        'template'=>"{items}{pager}",
        'dataProvider'=> $currentSection->getTopicsProvider(array(
                'select' => 'id, subject, date_created, status',
               // 'condition' => 't.status != 1',
                'with' => array(
                    'user' => array('select' => 'id, username'),
                    'replies',
                    'lastComment' => array(
                        'select' => 'date_created',
                        'with' => array(
                            'user' => array(
                                'select' => 'username',
                                'alias' => 'lcu',
                            ),
                        ),
                    ),
                ),
            )),
        'columns'=>array(
            array(
                'name' => 'subject',
                'value' => function($data) {
                   if ($data->status == Topic::STICKY_TOPIC) {
                       echo "<img src='/images/paperclip.png' title='Sticky'> ";
                   }
                    echo $data->theme;
                },
                'type' => 'html',
                'sortable' => false,
                'htmlOptions' => array(
                    'class' => 'text-left bold'
                ),
            ),
            array(
                'name' => 'replies',
                'value' => '$data->replies',
                'htmlOptions' => array(
                    'width' => '70px',
                    'class' => 'text-left bold'
                ),
            ),
            array(
                'name' => 'lastPost',
                'value' => '$data->lastPost',
                'type' => 'html',
                'htmlOptions' => array(
                    'width' => '200px',
                    'class' => 'text-left bold'
                ),
            ),
            array(
                'name' => 'date_created',
                'htmlOptions' => array('width' => '150px')
            )
        ),
    ));

    $this->widget('bootstrap.widgets.TbButton', array(
        'type'=>'primary',
        'label'=>'New topic',
        'url'=>Yii::app()->createUrl('topic/create', array('section_id' => $currentSection->id))
    ));
}

echo '</div>';