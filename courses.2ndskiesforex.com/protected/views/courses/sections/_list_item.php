<tr>
    <td>
        <?=CHtml::link($data->name, Yii::app()->createUrl("courses/sectionlist", array("courseId" => $courseId, "sectionId" => $data->id)))?>
        <?php

            $announceTopics = $data->getAnnounces(array('select' => 'id, subject'));

            if (! empty($announceTopics)) {
                $dataProvider = new CArrayDataProvider($announceTopics,
                    array(
                        'pagination'=>false
                    )
                );
                $this->widget('bootstrap.widgets.TbListView', array(
                    'dataProvider'=>$dataProvider,
                    'itemView'=>'//topic/_announce_list_item',
                    'summaryText'=>false,
                    'itemsTagName' => 'table',
                    'itemsCssClass'=> 'items table table-striped announce-topics-list',
                    'emptyText' => '',
                ));
            }
        ?>
    </td>
</tr>