<?php
//$this->layout = 'sectionlist';
?>

<div class="well well-blue">
    <div>
        <div class="row-fluid" id="c<?= $topic->id ?>">
            <div class="span8 pull-center">
                <div class="content">
                    <h3><?=$topic->subject?></h3>
                        <?php
                        $text = $topic->comments->text;
                        $text = $topic->comments->hideSrcIframe($text, $topic->id);
                        $text = preg_replace('/(iframe\s+[^>]*)(src=")(http:|https:)([^"]*"[^>]*)/','$1$2$4',$text);
                        //$text = preg_replace('/(src=")(http:|https:)([^"]*"*)/','$1$3',$text);
                        echo $text;?>
                </div>
            </div>
        </div>
    </div>
    <?php
    if ($topic->discussion == 1) {
        echo '<h5> Lessons Discussions</h5>';

        $this->widget('bootstrap.widgets.TbGridView', array(
            'template' => "{items}{pager}",
            'dataProvider' => $topic->search(),
            'columns' => array(
                array(
                    'name' => 'subject',
                    'value' => '$data->getTheme(1)',
                    'type' => 'html',
                    'htmlOptions' => array(
                        'class' => 'text-left bold'
                    ),
                ),
                array(
                    'name' => 'replies',
                    'value' => '$data->replies',
                    'htmlOptions' => array(
                        'class' => 'text-left bold'
                    ),
                ),
                array(
                    'name' => 'lastPost',
                    'value' => '$data->lastPost',
                    'type' => 'html',
                    'htmlOptions' => array(
                        'class' => 'text-left bold'
                    ),
                ),


            ),
        ));
    }
    ?>

</div>

<script type="text/javascript">
function myFunction() {
    var startTime = "<?php echo date("Y-m-d h:i a") ?>";	
		$.ajax({
				url : "http://courses.2nd-skies-forex.com/courses/usertrack/",
				type : "POST",
				data : {
					action : "ignore_user_track",
					startTime : startTime,
					logInsertID : "<?php echo $logInsertID; ?>"
				},
				success : function(response){
				}
			});
}
</script>
<script>
function IntervalFunction() {
    setInterval(function(){ myFunction() }, 10000);
}
IntervalFunction();
</script>
