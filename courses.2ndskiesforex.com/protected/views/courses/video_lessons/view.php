<?php

/* @var $currentSection Sections */

// there is a selected section
$sectionShown = !empty($currentSection);

echo '<div class="well well-blue">';

if (!empty($sections)) {
    // Section block
    $dataProvider=new CArrayDataProvider($sections);
    $this->widget('bootstrap.widgets.TbListView', array(
        'htmlOptions'=>array('class'=>'table-blue'),
        'dataProvider'=>$dataProvider,
        'viewData' => array('courseId' => $course->id),
        'itemView'=>'//courses/sections/_list_item',
        'summaryText'=>'Sections',
        'summaryCssClass'=>'list-summary',
        'itemsTagName' => 'table',
        'itemsCssClass'=> 'items table',
    ));
}
// Topics block

if (isset($currentSection)) {

    $announsementsCount = Topic::model()->count("status = 1 AND section_id = $currentSection->id");

    if ($announsementsCount) {
        $topics = Topic::model()->findAllByAttributes(array('status'=>1,'section_id'=>$currentSection->id),array('order'=>'t.order ASC'));

        if (!Yii::app()->user->isAdmin()) {
            $userCourse = UserCourse::model()->findByAttributes(array('user_id' => Yii::app()->user->id, 'course_id' => $course->id));
        }

    foreach($topics as $topic) {?>
        <?php
        if (Yii::app()->user->isAdmin() || time() > strtotime("+$topic->time_release day", strtotime($userCourse->created))) {
            $video = Comment::model()->findByAttributes(array('topic_id'=>$topic->id, 'is_first'=>1));?>
            <br>
            <div>
                <div class="row-fluid" id="c<?= $video->id ?>">
                    <div class="span8 pull-center">
                        <div class="content">
                            <h3><?=$topic->subject?></h3>
                            <?php
                            $text = $video->text;
                            $text = $video->hideSrcIframe($text, $topic->id);
                            $text = preg_replace('/(iframe\s+[^>]*)(src=")(http:|https:)([^"]*"[^>]*)/','$1$2$4',$text);
                            //$text = preg_replace('/(src=")(http:|https:)([^"]*"*)/','$1$3',$text);
                            echo $text;

//                            if (Yii::app()->session['courseId'] != 3 && Yii::app()->session['courseId'] != 4)
//                                echo '<center><a class="btn btn-large btn-info" href="/courses/quiz?sectionId='.$currentSection->parent_id.'"><i class="icon-hand-up"></i> Quiz</a></center>';
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        <?php }?>

    <?php }
        echo '<h5> Lessons Discussions</h5>';

        $this->widget('bootstrap.widgets.TbGridView', array(
            'template' => "{items}{pager}",
            'dataProvider' => $currentSection->getTopicsProvider(array(
                'select' => 'id, subject, date_created',
                'condition' => 't.status = 1 AND t.discussion = 1',
                'with' => array(
                    'user' => array('select' => 'id, username'),
                    'replies',
                    'lastComment' => array(
                        'select' => 'date_created',
                        'with' => array(
                            'user' => array(
                                'select' => 'username',
                                'alias' => 'lcu',
                            ),
                        ),
                    ),
                    'firstComment' => array(
                        'select' => 'text, stripped_tag_text',
                    ),
                ),
                'order'=>'t.order ASC'
            )),
            'columns' => array(
                array(
                    'name' => 'subject',
                    'value' => '$data->getTheme(1)',
                    'type' => 'html',
                    'sortable' => false,
                    'htmlOptions' => array(
                        'class' => 'text-left bold'
                    ),
                ),
                array(
                    'name' => 'replies',
                    'value' => '$data->replies',
                    'htmlOptions' => array(
                        'class' => 'text-left bold'
                    ),
                ),
                array(
                    'name' => 'lastPost',
                    'value' => '$data->lastPost',
                    'type' => 'html',
                    'htmlOptions' => array(
                        'class' => 'text-left bold'
                    ),
                ),
            ),
        ));
    }
}
if ($sectionShown) {

    echo '<h5> General Discussions</h5>';

    $this->widget('bootstrap.widgets.TbGridView',array(
        'id'=>'topic-grid',
        'template'=>"{items}{pager}",
        'dataProvider'=> $currentSection->getTopicsProvider(array(
            'select' => 'id, subject, date_created',
            'condition' => 't.status != 1 AND t.discussion = 1',
            'with' => array(
                'user' => array('select' => 'id, username'),
                'replies',
                'lastComment' => array(
                    'select' => 'date_created',
                    'with' => array(
                        'user' => array(
                            'select' => 'username',
                            'alias' => 'lcu',
                        ),
                    ),
                ),
            ),
        )),
        'columns'=>array(
            array(
                'name' => 'subject',
                'value' => '$data->theme',
                'type' => 'html',
                'sortable' => false,
                'htmlOptions' => array(
                    'class' => 'text-left bold'
                ),
            ),
            array(
                'name' => 'replies',
                'value' => '$data->replies',
                'htmlOptions' => array(
                    'width' => '70px',
                    'class' => 'text-left bold'
                ),
            ),
            array(
                'name' => 'lastPost',
                'value' => '$data->lastPost',
                'type' => 'html',
                'htmlOptions' => array(
                    'width' => '200px',
                    'class' => 'text-left bold'
                ),
            ),
            array(
                'name' => 'date_created',
                'htmlOptions' => array('width' => '150px')
            )
        ),
    ));

    $this->widget('bootstrap.widgets.TbButton', array(
        'type'=>'primary',
        'label'=>'New topic',
        'url'=>Yii::app()->createUrl('topic/create', array('section_id' => $currentSection->id))
    ));
}

echo '</div>';
echo '
<script type="text/javascript">
function myFunction() {
    var startTime = "'.date("Y-m-d h:i a").'";	
		$.ajax({
				url : "http://courses.2nd-skies-forex.com/courses/usertrack/",
				type : "POST",
				data : {
					action : "ignore_user_track",
					startTime : startTime,
					logInsertID : "'.$logInsertID.'"
				},
				success : function(response){
				}
			});
}
</script>
<script>
function IntervalFunction() {
    setInterval(function(){ myFunction() }, 10000);
}
IntervalFunction();
</script>';