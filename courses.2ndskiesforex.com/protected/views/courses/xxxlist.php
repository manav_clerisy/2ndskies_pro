<?php
        $rawData=(Yii::app()->user->isAdmin()) ? Courses::model()->findAll() : Courses::model()->findAll('visible=:status AND id NOT IN (400,5, 3)', array(':status' => 1));
        $is_subscribed_to_a_course = false;
        foreach ($rawData as $course) {
                if ($course->isUserSubscribed()) {
                        $is_subscribed_to_a_course = true;
                }
        }

?><div class="well home-intro well-blue">
 <h1>Welcome to <?php echo CHtml::encode(Yii::app()->name); ?></h1>
 <h2>Courses:</h2>
 <div class="table-blue" id="yw0">
  <table class="items table">
   <thead>
    <tr>
     <th id="yw0_c0">Name</th><th id="yw0_c1">Price</th><th class="button-column" id="yw0_c2">&nbsp;</th>
    </tr>
   </thead>
   <tbody>
    <?php foreach ($rawData as $course): ?>
    <tr class="odd">
     <td>
     <?php if ($course->isUserSubscribed()): ?>
      <a href="<?= Yii::app()->createUrl("/site/index", array("courseId" => $data->id)) ?>"><?= $course->name ?></a>
     <?php else: ?>
      <?= $course->name ?>
     <?php endif; ?>
     <td><?= $course->price ?></td>
     <td class="button-column">
       <?php if ($course->id == 4 && !$is_subscribed_to_a_course): ?>
         Available to existing course members.
       <?php elseif ($course->isUserSubscribed()) : ?>
         <a class="label label-success" title="" rel="tooltip" data-original-title="Subscribed">Subscribed</a>
       <?php else: ?>
         <a class="btn btn-primary" title="Buy" rel="tooltip" href="<?= Yii::app()->createAbsoluteUrl("courses/startPayment", array("id" => $course->id, "type"=>"creditCard")) ?>">
           Buy
         </a>
       <?php endif; ?>
     </td>
    </tr>
    <?php endforeach; ?>
   </tbody>
  </table>
 </div>
</div>
