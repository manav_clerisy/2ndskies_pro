<?php

if(isset(Yii::app()->user->id) && !empty(Yii::app()->user->id))
	$username = Profile::model()->findByPk(Yii::app()->user->id)->getAttribute('first_name');
else
	$username = Profile::model()->findByPk(Yii::app()->session['newUserId'])->getAttribute('first_name');

?>
<div class="buy-box">
	<ul class="steps-list">
		<li><a href="#"><i class="counter"><b>1</b></i><span class="text">Registration</span></a></li>
		<li><a href="#"><i class="counter"><b>2</b></i><span class="text">Payment info</span></a></li>
		<li class="active"><a href="#"><i class="counter"><b>3</b></i><span class="text">Finish</span></a></li>
	</ul>
	<div class="buy-box-holder">
		<div class="finish-box">
			<h1>Congratulations!</h1>
			<h2>You have successfully purchased the <span class="blue"><?=$course ?>!</span></h2>
			<div class="logo-box">
				<img src="/images/2nd-skies.png" alt=""/>
				<strong class="logo-title">Forex</strong>
			</div>
			<?php  if(Yii::app()->user->isGuest) { ?>
				<div class="clearfix">
					<img class="pull-left" src="/images/check.png" alt=""/>
					<p class="text-overflow">In the next few minutes you should receive an account activation email, as well as a welcome email for the course you've just purchased. If you do not receive these in your inbox shortly, please check your spam folder. If after 10 minutes you still have not received the emails, please contact support at <a href="mailto:support@2ndskiesforex.com">support@2ndskiesforex.com</a> and we will get you setup as soon as possible.</p>
				</div>
                        <?php } ?>
		</div>
	</div>
</div>
