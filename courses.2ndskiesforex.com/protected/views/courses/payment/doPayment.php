<?php

Yii::app()->clientScript->registerScript('hasOffers', "<iframe src='http://2ndskiesforex.go2cloud.org/aff_l?offer_id=2' scrolling='no' frameborder='0' width='1' height='1'></iframe>", CClientScript::POS_BEGIN);
if(empty($model_result))
{?>

	<div class="buy-box">
		<?php
		Yii::app()->clientScript
			->registerLocalScript('admin/courses/doPayment.js',CClientScript::POS_END)
			->registerScript(
				'assignment',
				'var courseId = "'.$courseId.'"',
				CClientScript::POS_HEAD
			);

		$course = Courses::model()->findByPk($courseId);

		?>
		<ul class="steps-list">
			<li><a href="#"><i class="counter"><b>1</b></i><span class="text">Registration</span></a></li>
			<li class="active"><a href="#"><i class="counter"><b>2</b></i><span class="text">Payment info</span></a></li>
			<li><a href="#"><i class="counter"><b>3</b></i><span class="text">Finish</span></a></li>
		</ul>
		<div class="buy-box-heading">
			<h2>You are buying the <strong class="blue-title"><?=$course->name ?></strong></h2>
		</div>

		<div class="buy-box-holder">
			<div class="pay-holder">
				<!--label class="custom-radio">
					<input type="radio" name="payment" value="creditCard" id="creditCardRadio" checked><span>Pay with Debit/Credit Card</span>
				</label-->
				<label class="custom-radio">
					<input type="radio" name="payment" value="creditCard" id="creditCardRadio" checked><span>Pay with Debit/Credit Card</span>
				</label>
				<label class="custom-radio">
					<input type="radio" name="payment" value="paypal" id="paypalRadio"><span>Pay with PayPal</span>
				</label>
			</div>

			<div class="text-right hidden half-box-holder" id="paypalPayment">
				<?php  $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
					'action' => Yii::app()->createUrl('courses/startPayment', array("id" => $courseId, "type"=>"paypal")),
				));?>
				<div class="coupon">
					<div class="heading-box">
						<label class="block-label" for="couponCode">Coupon Code</label>
					</div>
					<input id="couponCodePaypal" name="couponCode" type="text">
					<?php echo '<div class="help-text">'.UserModule::t("If you have a discount or coupon code, please enter above.").'</div>'; ?>

					<span class="total-text price">Total USD <?=$course->price ?></span>

					<?php echo CHtml::submitButton('Buy Now', array('class'=>'btn btn-gold'));?>
				</div>

				<?php $this->endWidget();?>

			</div>

			<div id="creditCardArea">
				<?php

				/* @var $model \yii\PayPal\Form\CreditCard */
				$form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
					'id'=>'payment-info',
					'type'=>'horizontal',
					'enableAjaxValidation'=>false,
					'action' => Yii::app()->createUrl('courses/FinishPaymentCreditCard'),
					'htmlOptions' => array('class' => 'main-form'),
				));

				/* @var $form CActiveForm */

				CHtml::setModelNameConverter(function($model) {
					$className=is_object($model) ? get_class($model) : (string)$model;
					$parts = explode('\\', $className);
					return trim(end($parts),'_');
				});
				?>

				<input type="hidden" value="<?=$courseId?>" name="courseId">

				<?php if($model->getErrors()) echo '<div class="alert alert-error">Please fix the following input errors.</div>' ?>
				<div class="half-box-holder">
					<div class="half-box">
						<div class="control-group <?php if ($model->getError('first_name')) echo 'error'?>">
							<div class="heading-box">
								<?php echo $form->error($model,'first_name').
									$form->labelEx($model, 'first_name',array('class'=>'block-label','label'=>'First name')) ?>
							</div>
							<?php echo $form->textField($model, 'first_name') ?>
						</div>
						<div class="control-group <?php if ($model->getError('last_name')) echo 'error'?>">
							<div class="heading-box">
								<?php echo $form->error($model,'last_name').
									$form->labelEx($model, 'last_name',array('class'=>'block-label','label'=>'Last name')) ?>
							</div>
							<?php echo $form->textField($model, 'last_name') ?>
						</div>

						<div class="control-group <?php if ($model->getError('billing_address')) echo 'error'?>">
							<div class="heading-box">
								<?php echo $form->error($model,'billing_address').
									$form->labelEx($model, 'billing_address',array('class'=>'block-label','label'=>'Address')) ?>
							</div>
							<?php echo $form->textField($model, 'billing_address') ?>
						</div>
						<div class="control-group <?php if ($model->getError('billing_city')) echo 'error'?>">
							<div class="heading-box">
								<?php echo $form->error($model,'billing_city').
									$form->labelEx($model, 'billing_city',array('class'=>'block-label','label'=>'City')) ?>
							</div>
							<?php echo $form->textField($model, 'billing_city') ?>
						</div>
						<div class="control-group <?php if ($model->getError('billing_state')) echo 'error'?>">
							<div class="heading-box">
								<?php echo $form->error($model,'billing_state').
									$form->labelEx($model, 'billing_state',array('class'=>'block-label','label'=>'State')) ?>
							</div>
							<?php echo $form->textField($model, 'billing_state') ?>
						</div>
						<div class="control-group <?php if ($model->getError('billing_zip')) echo 'error'?>">
							<div class="heading-box">
								<?php echo $form->error($model,'billing_zip').
									$form->labelEx($model, 'billing_zip',array('class'=>'block-label','label'=>'Zip code')) ?>
							</div>
							<?php echo $form->textField($model, 'billing_zip') ?>
						</div>

						<div class="control-group <?php if ($model->getError('billing_country')) echo 'error'?>">
							<div class="heading-box">
								<?php echo $form->error($model,'billing_country').
									$form->labelEx($model, 'billing_country',array('class'=>'block-label','label'=>'Country')) ?>
							</div>
                                                    <?php
                                                        $profileFields=Profile::getFields();
                                                        echo $form->dropDownList($model,'billing_country',Profile::range($profileFields[3]->range));
                                                    ?>
						</div>
					</div>
					<div class="half-box">

						<div class="control-group <?php if ($model->getError('number')) echo 'error'?>">
							<div class="heading-box">
								<?php echo $form->error($model,'number').
									$form->labelEx($model, 'number',array('class'=>'block-label')) ?>
							</div>
							<?php echo $form->textField($model, 'number', array('id'=>'cardNumber')) ?>
						</div>

						<div class="control-group clearfix">
							<div class="input-holder pull-left  <?php if ($model->getError('expire_month') || $model->getError('expire_year')) echo 'error'?>">
								<div class="heading-box">
									<label for="CreditCard_expire" class="block-label required">Expiry date <span class="required">*</span></label>
								</div>
								<?php echo $form->dropDownList($model, 'expire_month',$model->expireMonth(),array('class'=>'small')).
									$form->dropDownList($model, 'expire_year',$model->expireYearLabels(),array('class'=>'small'));?>
							</div>
							<div class="input-holder cvv-input pull-right <?php if ($model->getError('cvv2')) echo 'error'?>">
								<div class="heading-box">

									<div class="hint-holder">
										<span class="question-icon">?</span>
										<div class="hint">
                                        <span class="hint-text">
                                            <div id='cvv2Info'><p><img src='/images/CVC2SampleVisaNew.png'></p>
												<p>The card security code is located on the back of MasterCard, Visa and Discover credit or debit cards and is typically a separate group of 3 digits to the right of the signature strip.</p>
												<p><img src='/images/CIDSampleAmex.png'></p>
												<p>On American Express cards, the card security code is a printed, not embossed, group of four digits on the front towards the right.</p>
											</div>
                                        </span>
										</div>
									</div>

									<?php echo $form->labelEx($model, 'cvv2',array('class'=>'block-label','label'=>'CVV2')); ?>
								</div>
								<?php echo $form->textField($model, 'cvv2'); ?>
							</div>
						</div>

						<div class="control-group">
							<div class="heading-box">
								<label class="block-label" for="couponCode">Coupon code</label>
							</div>
							<input id="couponCodeCard" name="couponCode" type="text">
							<?php echo '<div class="help-text">'.UserModule::t("If you have a discount or coupon code, please enter above.").'</div>'; ?>
						</div>
						<span class="total-text price"><b>Total USD <?=$course->price ?></b></span>
						<?php echo	$form->hiddenField($model, 'type', array('id'=>'creditCardTypeInput'));?>
						<input type="hidden" value="<?=$userEmail?>" name="userEmail">

					</div>

				</div>

				<div class="text-right mb20">
					<?php echo CHtml::submitButton('Buy Now', array('class'=>'btn btn-gold')); ?>
				</div>
				<div class="buy-box-bottom">
					<img src="/images/comodo.png" alt="" style="float: left;"/>
					<div style="float: left; width: 270px; margin-left: 15px;"><p><strong>Refunds</strong><br/>This is intellectual property and therefore all sales are final.</p></div>
					<img class="payment-img" src="/images/payment.png" alt=""/>
				</div>
				<?php

				$this->endWidget();
				CHtml::setModelNameConverter(null);?>
			</div>
		</div>
	</div>
	<ul class="list-testimonials">
		<li class="testimonial">
			<i class="icon-quote"></i>
			<p class="testimonial--quote">I've already paid for the courses using the systems risking only 1/2% per trade.</p>
			<h3 class="testimonial--title">John C</h3>
		</li>
	</ul>

<?php }
/*if(!empty($model_result) && $amount == 0){
    $this->redirect(array('courses/endPayment','courseId'=>$_POST['courseId']));
}*/
if(!empty($model_result))
{
	$course_name=$courseName;
	$courseId = $_POST['courseId'];
	$token=$token;
	$amt=$amount;
	$email=$post['userEmail'];
	$cvv2=$post['CreditCard']['cvv2'];
	$first_name=$post['CreditCard']['first_name'];
	$last_name=$post['CreditCard']['last_name'];
	$acc_number=$post['CreditCard']['number'];
	$expire_year=$post['CreditCard']['expire_year'];
	$month=$post['CreditCard']['expire_month'];
        $address = $post['CreditCard']['billing_address'];
        $city = $post['CreditCard']['billing_city'];
        $state = $post['CreditCard']['billing_state'];
        $country = $post['CreditCard']['billing_country'];
        $zip = $post['CreditCard']['billing_zip'];
	if(strlen($month) == 1){ $month='0'.$month; }
	$exp_date = $month.substr($expire_year, -2);

	if(isset(Yii::app()->params['mode']) )
	{
		$mode= Yii::app()->params['mode'];
		if($mode != 'LIVE' && $mode != 'TEST' )
		{
			echo "Invalid Transaction Mode"; exit();
		}
	}
	else
	{
		echo "Transaction Mode Not Set"; exit();
	}


	//$url = 'https://payflowpro.paypal.com/';
	$url = Yii::app()->params['paypal_url'];//'https://pilot-payflowpro.paypal.com/';
	$fields = array(
		'PARTNER' => 'PayPal',
		'LOGIN' =>  Yii::app()->params['login'],
		'VENDOR' => Yii::app()->params['vendor'],
		'USER' => Yii::app()->params['user'],
		'PWD' => Yii::app()->params['pwd'],
		'MODE' => $mode,
		'TRXTYPE' => 'S',
		'AMT'=>$amt,
		'CREATESECURETOKEN'=>'Y',
		'SECURETOKENID'=>$token,
		'EMAIL'=>$email,
		"SHIPTOFIRSTNAME"=>$first_name,
		"SHIPTOLASTNAME"=>$last_name,
		'BILLTOFIRSTNAME'=>$first_name,
                'BILLTOCITY' => $city,
                'BILLTOSTATE' => $state,
                'BILLTOZIP' => $zip,
                'BILLTOSTREET' => $address,
		'CVV2'=>$cvv2,
		'BILLTOLASTNAME'=>$last_name,
		'CSCREQUIRED'=>'TRUE',
		'SILENTTRAN'=>'TRUE',
	);
//url-ify the data for the POST
    $fields_string = '';
	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
	rtrim($fields_string, '&');

//open connection
	$ch = curl_init();

//set the url, number of POST vars, POST data
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);

//execute post
	$result = curl_exec($ch);

//close connection
	curl_close($ch);

	preg_match("/(.*)SECURETOKEN=((?P<SECURETOKEN>.*))&SECURETOKENID=(?P<SECURETOKENID>[^&]+)/",$result,$output);
	if(isset($output['SECURETOKENID']) && isset($output['SECURETOKEN']))
	{
		$SECURETOKENID=$output['SECURETOKENID'];
		$SECURETOKEN=$output['SECURETOKEN'];
		Yii::app()->session['SECURETOKEN']=$SECURETOKEN;

		?>
		<form name="paypal" id="paypal"  method="POST" action="https://payflowlink.paypal.com?MODE=<?php echo $mode;?>&SECURETOKENID=<?php echo $SECURETOKENID;?>&SECURETOKEN=<?php echo $SECURETOKEN;?>">
			<input type="hidden" name="TENDER" value="C">

			<input type="hidden" name="CVV2" value="<?php echo $cvv2; ?>">

			<input type="hidden" name="ACCTTYPE" value="0">

			<input type="hidden" name="DESC" value="<?php echo 'Product Name : '.$course_name; ?>">

			<input type="hidden" name="AMT" value="<?php echo $amt; ?>" >

			<input type="hidden" name="TYPE" value="C">

			<input type="hidden" name="EMAIL" value="<?php echo $email; ?>">

			<input type="hidden" name="EXPDATE" value="<?php echo $exp_date; ?>">

			<input type="hidden" name="ACCT" value="<?php echo $acc_number; ?>" >

			<input type="hidden" name="MODE" value="<?php echo $mode; ?>" >



		</form>

		<script>
			document.getElementById("paypal").submit();
		</script>

	<?php }
	else
	{
		echo "Secure Token Missing"; exit();
	}
}?>
