<?php
Yii::app()->clientScript
    ->registerLocalScript('homepage.js', CClientScript::POS_END)
    ->registerScriptFile('/js/admin/economicEvents/economicEvents.js', CClientScript::POS_END)
    ->registerPackage('timepicker')
    ->registerScript(
        'assignment',
        'var courseId = "'.Yii::app()->session['courseId'].'"',
        CClientScript::POS_HEAD
    );
/* @var $mostRecentPosts CActiveDataProvider */

?>

<div align="right" id="drop">
<?php echo $time?>
</div>

<!--<a href="site/sendMailTest">Email testing (dev server only)</a>-->

<?php 
if (Yii::app()->session['courseId'] != '4') :?>
<h3>Today's Economic Events</h3>


<div class="table-events">

<script type="text/javascript">
var fxcalendar_config = {
host: "http://calendar.fxstreet.com",
css: 'mini',
rows: 10,
pastevents: 3,
hoursbefore: 3, 
timezone: 'UTC',
showcountryname: 'false',
columns: 'date,time,country,event,consensus,previous,volatility,actual',
isfree: 'true',
countrycode: 'AU,CA,JP:EMU,NZ,CH,UK,US',
culture:'en-US'
};
</script>

  <?php /*?><script type="text/javascript">
    var fxcalendar_config = {
        host: "http://calendar.fxstreet.com",
        css: 'mini',
        rows: 10,
        pastevents: 5,
        hoursbefore: 20,        
        timezone: 'UTC',
        showcountryname: 'false',
        columns: 'date,time,country,event,consensus,previous,volatility,actual',
        isfree: 'true',
        countrycode: 'AU,CA,JP:EMU,NZ,CH,UK,US',
        culture:'en-US'
    };
    </script><?php */?>
    <script type="text/javascript" src="http://calendar.fxstreet.com/scripts/mini"></script>
    <div id="fxst_calendar" style="width: 1000px;margin: 4px 85px 15px;"></div>
	<div class="showMore" style="text-align:center;font-family:Arial, Helvetica, sans-serif;font-size:16px;color:#0088cc"><strong>
	<a href="javascript:void(0);" id="showMoreOrLess" data-status="show" onclick="return showMoreContent();">Show More</a>
	</strong></div>
	<style type="text/css">
	.fxst-calendarmini td
	{
	   border-bottom: solid 1px #ddd;
	}
	#fxst-thconsensus, #fxst-thconsensus, #fxst-thprevious
	{
		text-align:right;
	}
	</style>
	<script type="text/javascript">
	function showMoreContent()
	{
		
		var showStatus=$("#showMoreOrLess").attr("data-status");
		if(showStatus=="show")
		{	
			$(".fxit-eventrow").css("display","table-row");
			$("#showMoreOrLess").attr("data-status","hide");
			$("#showMoreOrLess").html("Show Less");
		}
		else
		{
			//$(".fxit-eventrow").css("display","none");
			showOnLoad();
			$("#showMoreOrLess").attr("data-status","show");
			$("#showMoreOrLess").html("Show More");
		}
	}
	</script>
	<script type="text/javascript">
	function showOnLoad()
	{		
		var i=0;
		$('#fxst-calendartable tbody tr').each(function() {
		if(i>7 && i!=0)
		{
			
			$(this).css("display","none");
		}
		else if(i!=0)
		{
			$(this).css("display","table-row");
		}
		
		i++;
    	});
	}

	
	$( document ).ready(function() {
		setTimeout(function(){ showOnLoad(); }, 1000);
	});
	</script>



<?php /*?>
	<ul class="events-pager">
		<li><a class="btn btn-success" href='#' id="previous"><span class="icon-double-angle-left"></span> Previous Day </a></li>
		<li><a class="btn btn-success" href='#' id='today'>Today </a></li>
		<li><a class="btn btn-success" href='#' id="next">Next Day <span class="icon-double-angle-right"></span></a></li>
	</ul>
    <input type="hidden" value=0 id="days">
    <div class="message-list">
        <?php
        $this->widget('bootstrap.widgets.TbGridView',array(
            'id'=>'economic-events-grid-index',
            'htmlOptions'=>array('class'=>'table-events grid-view'),
            'dataProvider'=>$economicEvents,
            'template' => '{items}',
            'ajaxUrl'=>"site/index",
            'afterAjaxUpdate' => 'js:function(){
             hideDuplicatedDates();
         }',
            'columns'=>array(
                array(
                    'header' => 'Date',
                    'type' => 'html',
                    'value' => '$data->getDateOnly("'.$offset.'")',
                ),
                array(
                    'header' => 'Time',
                    'type' => 'html',
                    'value' => '($data->getTimeOnly("'.$offset.'") == "00:00") ? "" : $data->getTimeOnly("'.$offset.'")',
                ),
                array(
                    'header' => 'Currency',
                    'type' => 'html',
                    'value' => '$data->currencyWithFlag',
                ),
                array(
                    'header' => 'Event',
                    'value' =>'$data->event',
                    'htmlOptions'=>array('class'=>'col-event'),
                ),
                array(
                    'header' => 'Importance',
                    'type' => 'html',
                    'value' => '$data->coloredImportance',
                ),
                array(
                    'header' => 'Actual',
                    'value' => '$data->actual',
                ),
                array(
                    'header' => 'Forecast',
                    'value' => '$data->forecast',
                ),
                array(
                    'header' => 'Previous',
                    'value' => '$data->previous',
                ),
               /* array(
                    'header' => 'Notes',
                    'type' => 'html',
                    'value' => '$data->hiddenNotes',
                ),*/
         /*   ),
        ));
        ?>
    </div><?php */?>
</div>
<?php endif;
?>


<div class="row">

    <div id="recentPosts" class="posts span6">
        <h3>Most Recent Posts</h3>
        <div class="well well-blue well-small">

            <div class="message-list" >
                <?php if (Yii::app()->request->isAjaxRequest){
                    Yii::app()->clientscript->scriptMap['jquery.js'] = false;
                    Yii::app()->clientscript->scriptMap['jquery-ui.min.js'] = false;
                    Yii::app()->clientscript->scriptMap['jquery.yiigridview.js'] = false;
                    Yii::app()->clientscript->scriptMap['homepage.js'] = false;
                }
                //$this->renderPartial('most_recent',array('mostRecentPosts'=>$mostRecentPosts));
                ?>

            </div>

            <div align="right">
                Show Posts <a href="#recentPosts" id="posts5">20</a>
                <a href="#recentPosts" id="posts10">30</a>
                <a href="#recentPosts" id="posts20">50</a>
                <a href="#recentPosts" id="posts50">100</a>
            </div>
        </div>
    </div>

    <div class="posts span6">
        <h3>Subscribed Topics</h3>
        <div class="well well-blue well-small">

                <?php if(isset($subscriptions) && !empty($subscriptions))
                { ?>
            <div class="message-list">
                <?php
                $subscriptions->criteria->with[] = 'lastComment';
                $subscriptions->criteria->with[] = 'lastComment.user';
                $this->widget('bootstrap.widgets.TbGridView', array(
                    'id'=>'subscribed topics',
                    'template'=>"{items}",
                    'dataProvider'=> $subscriptions,
                    'summaryText'=>false,
                    'ajaxUrl'=>"site/index",
                    'htmlOptions' => array(
                        'class' => 'topic-list'
                    ),
                    'columns'=>array(
                        array(
                            'header' => 'Topic',
                            'type' => 'html',
                            'value' => '$data->postedInTopic',
                            'htmlOptions'=>array('style'=>'width: 330px'),
                        ),
                        array(
                            'header' => 'By',
                            'type' => 'html',
                            'value' => '$data->lastPost',
                            //'htmlOptions'=>array('style'=>'width: 100px'),
                        ),
                    )
                ));
                ?>
            </div>
            <div align="right">
                <a href="<?php echo Yii::app()->createUrl('/user/profile/subscriptions')?>">Edit Subscriptions</a>
            </div>
            <?php
                }
            else
                echo "<div class='empty-box'>No results found.</div>"; ?>
        </div>
    </div>
</div>


<h3>Announcements from Chris</h3>
<div class="well well-blue">
<?php

//$dependency = new CDbCacheDependency('SELECT COUNT(id) FROM topics WHERE status=1 AND section_id IN(6,61,85,185)');
//
//if (Cache::begin(array('announcements-course-'.Yii::app()->session['courseId']), 30*24*60*60,$dependency)) {
    if(isset($announcements) && !empty($announcements))
    {    ?>
        <div class="message-list">
            <?php
            $this->widget('bootstrap.widgets.TbGridView', array(
                'id'=>'announcements-grid',
                'template'=>"{items}",
                'dataProvider'=> $announcements,
                'summaryText'=>false,
                'htmlOptions' => array(
                    'class' => 'topic-list'
                ),
                'columns'=>array(
                    array(
                        'header' => 'Topic',
                        'type' => 'html',
                        'value' => '$data->announcementTopic',
                    ),
                    array(
                        'header' => 'Posted Time',
                        'type' => 'html',
                        'value' => '$data->lastPost',
                    ),
                )
            ));
        ?>
    </div>
<?php //  }
//    Cache::end();

} 
?>
</div>
