<?php
/* @var $mostRecentPosts CActiveDataProvider */

$mostRecentPosts->criteria->with[] = 'lastComment.user';

$this->widget('bootstrap.widgets.TbGridView', array(
    'id'=>'recent-posts-grid',
    'template'=>"{items}",
    'dataProvider'=> $mostRecentPosts,
    'summaryText'=>false,
    'ajaxUrl'=>"site/index",
    'htmlOptions' => array(
        'class' => 'topic-list'
    ),
    'columns'=>array(
        array(
            'header' => 'Topic',
            'type' => 'html',
            'value' => '$data->postedInTopic',
            'htmlOptions'=>array('style'=>'width: 330px'),
        ),
        array(
            'header' => 'By',
            'type' => 'html',
            'value' => '$data->lastPost',
           // 'htmlOptions'=>array('style'=>'width: 100x'),
            ),
        )
    )
);
?>