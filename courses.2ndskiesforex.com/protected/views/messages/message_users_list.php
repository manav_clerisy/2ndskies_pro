<?php
/**
 *
 * @var MessagesController $this
 */
Yii::app()->clientScript->registerLocalScript('/admin/messages/messages.js',CClientScript::POS_END);

if (!empty($users)) {
    $provider = new CArrayDataProvider($users,array(
        'pagination'=>array(
            'pageSize'=>10,
        ),
    ));

    $this->widget('zii.widgets.CListView', array(
        'id' => 'user-list',
        'dataProvider' => $provider,
        'template' => '{items}{pager}',
        'itemView' => '_message_user_item',
        'enableSorting' => false,
        'enablePagination' => true,
        'emptyText' => '',
        'cssFile' => false,
    ));
} else {
    echo '<p class="no-messages">You have no messages</p>';
}
