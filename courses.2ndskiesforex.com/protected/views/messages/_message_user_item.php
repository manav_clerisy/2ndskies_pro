<?php
    $UnreadMessagesAmount = $data->getAmountOfUnreadMessages($data->id)
?>
<div class="user-item well <?=($UnreadMessagesAmount)? 'well-blue':''?>">
	<?php
    $messages = new Messages;
    $lastMessage = $messages->getLastMessage($data->id,Yii::app()->user->id);
    if(!empty($lastMessage))
    {
        $lastMessageDate = Topic::model()->getDate($lastMessage->created);
        $messageId = $lastMessage->id;
    }
    else
    {
        $lastMessageDate = '';
        $messageId = '';
    }
	?>
	<div class="photo-holder">
		<a href="<?=Yii::app()->createUrl('messages/conversationwith', array('userId' => $data->id, '#'=> $messageId))?>">
			<?php echo CHtml::image($data->profile->imageUrl);?>
		</a>
	</div>
	<div class="text-holder">
		<strong class="user-name">
			<a href="<?=Yii::app()->createUrl('messages/conversationwith', array('userId' => $data->id,'#'=> $messageId))?>"><?=$data->username?></a> <?= '<span class="label label-important">'.$UnreadMessagesAmount.'</span>' ?>
		</strong>
		<p><strong>Registration date:  </strong><?=Topic::model()->getDate($data->create_at) ?></p>
		<p><strong>Last message:  </strong><?=$lastMessageDate ?></p>
        <p><strong>Posted by: </strong><?php if($lastMessage->user_from == $data->id) echo $data->username; else echo 'Me'?></p>
		<p><strong>Messages from <?=$data->username?> : </strong><?=$messages->countByAttributes(array('user_from'=>$data->id,'user_to'=>Yii::app()->user->id))?></p>
	</div>
    <div align="right">
        <a href="#page" class="btn btn-info deleteAllMessages" id="<?=$data->id ?>">Delete All Messages</a>
    </div>
</div>
