<?php

/**
 * This is the model class for table "sections".
 *
 * The followings are the available columns in table 'sections':
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 * @property integer $order
 * @property integer $course_id
 * 
 * @property AllIdsBehavior $allIdsBehavior
 * 
 * @property Sections $parentSection
 */
class Sections extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    public $str;

	public function tableName()
	{
		return 'sections';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, parent_id, course_id', 'required'),
			array('parent_id, order, course_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>200),
            array('order', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, parent_id, order, course_id', 'safe', 'on'=>'search'),
		);
	}

    public function behaviors()
	{
		return array(
            'dropCacheBehavior' => array(
                'class' => 'ext.behaviors.DropCacheBehavior',
                'tags' => 'sections,course-sections-{course_id},section-{id}',
            ),
            'allIdsBehavior' => array(
                'class' => 'ext.behaviors.AllIdsBehavior',
            ),
		);
	}
    
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'course' => array(self::BELONGS_TO, 'Courses', 'course_id'),
            'topics' => array(self::HAS_MANY, 'Topic', 'section_id'),
            'parentSection'=>array(self::BELONGS_TO, 'Sections', 'parent_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'parent_id' => 'Parent',
			'order' => 'Order',
			'course_id' => 'Course',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('`order`',$this->order);
		$criteria->compare('course_id',$this->course_id);
        return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Sections the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function generateBreadcrumbs($lastIsLink = false)
    {
        $cacheId = __METHOD__ . '-' . $this->id;
        $breadcrumbs = Yii::app()->cache->get($cacheId);
        if ($breadcrumbs === false) {
            
            $section = $this;

            $breadcrumbs = array(
                $section->name => $this->createRouteFor($section)
            );
/*
            while ($section->parent_id) {
                // parent node
                $section = Sections::model()->findByPk($section->parent_id);//die(var_dump($section));
                $breadcrumbs[$section->name] = $this->createRouteFor($section);
            }*/

            // Course link
            $breadcrumbs[$this->course->name] = array('courses/sectionlist', 'courseId' => $this->course_id);

            $breadcrumbs = array_reverse($breadcrumbs, true);
            
            Yii::app()->cache->set(
                $cacheId, $breadcrumbs, 0,
                new CacheTags('course-' . $this->course_id, 'course-sections-' . $this->course_id)
            );
        }

        if (!$lastIsLink) {
            end($breadcrumbs); // last element
            $lastKey = key($breadcrumbs); // it's key
            unset($breadcrumbs[$lastKey]);
            $breadcrumbs[] = $lastKey;
        }

        return $breadcrumbs;
    }

    public function createRouteFor($section = null)
    {
        if ($section === null)
            $section = $this;
        
        return array(
            'courses/sectionlist',
            'courseId'  => $section->course_id,
            'sectionId' => $section->id
        );
    }

    public function getTopicsProvider($criteria = array())
    {
        $topic = new Topic();
        $topic->unsetAttributes();
        $topic->section_id = $this->id;
        return $topic->search($criteria);
    }

    public function getChildrensField($name = 'id')
    {
        $self = $this;
        
        $calc = function() use ($name, $self) {
            $res = array($self->$name);
            $parentIds = array($self->id);
            do {
                $sectionsIds = Yii::app()->db->createCommand()
                    ->select($name)
                    ->from($self->tableName())
                    ->where('parent_id IN (' . implode(',', $parentIds) . ')')
                    ->queryColumn();

                $res = array_merge($res, $sectionsIds);
                
                $parentIds = $sectionsIds;
            } while (!empty($parentIds));

            return $res;
        };
        
        return Cache::wrap(array(__METHOD__, $name, $this->id), $calc, 0, 'course-sections-' . $this->course_id);
    }

    /**
     * Returns announce topics
     * @param CDbCriteria|array $criteria query criteria
     * @return Topic[]
     */
    public function getAnnounces($criteria = array())
    {
        if (is_array($criteria))
            $criteria = new CDbCriteria($criteria);
        
        $criteria->compare('section_id', $this->getChildrensField('id'));
        $criteria->compare('status', Topic::ANNOUNCE_TOPIC);
        $criteria->order = '`order`, `date_created`';
        
        return Topic::model()->findAll($criteria);
    }

    public function getAnnouncesProvider($criteria = array())
    {
        if (is_array($criteria))
            $criteria = new CDbCriteria($criteria);

        $criteria->compare('section_id', $this->getChildrensField('id'));
        $criteria->compare('status', Topic::ANNOUNCE_TOPIC);
        $criteria->order = '`order`, `date_created`';

        $dataProvider = new CActiveDataProvider('Topic', array(
            'pagination' => false,
            'criteria' => $criteria,
        ));
        return $dataProvider;
    }

    public function getSectionsForCourse($currentSectionId)
    {
        $course = Courses::model()->findByPk(Yii::app()->session['courseId']);
        $sections = json_decode($course->sectionTree);

        $strBegin = '<select id="sectionList" class="pull-left" name="section">';
        $strBegin .= $this->buildList($sections, $currentSectionId);

        $strBegin .='</select>';
        return $strBegin;
    }

    public function buildList($sections, $currentSectionId)
    {
        $str = '';
        foreach($sections->children as $section)
        {
            $class = $selected = '';

            if($section->parent_id == 0)
                $class = 'class="main-section"';

            if($section->id == $currentSectionId)
                $selected = ' selected';

            $str .= '<option value="'.$section->id.'" '. $class . $selected. '>'.$section->title.'</option>';
            if(!empty($section->children))
            {
                $str .=$this->buildList($section, $currentSectionId);
            }

        }

        return $str;
    }

    /**
     * Returns true if this section is inside $section or equals to it.
     * @param Sections $section
     * @return boolean
     */
    public function isInside($section)
    {
        return
            $this->id == $section->id ||
            in_array($this->id, $section->getChildrensField());
    }
}
