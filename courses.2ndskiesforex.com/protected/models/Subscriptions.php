<?php

/**
 * This is the model class for table "subscriptions".
 *
 * The followings are the available columns in table 'subscriptions':
 * @property integer $id
 * @property integer $user_id
 * @property integer $course_id
 * @property string $topic_id_arr
 */
class Subscriptions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'subscriptions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, course_id, topic_id_arr', 'required'),
			array('user_id, course_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, course_id, topic_id_arr', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'course_id' => 'Course',
			'topic_id_arr' => 'Topic Id Arr',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('course_id',$this->course_id);
		$criteria->compare('topic_id_arr',$this->topic_id_arr,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Subscriptions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function isUserSubscribed($id)
    {
        $userSubscriptions = $this->findByAttributes(array('user_id'=>Yii::app()->user->id, 'course_id'=>Yii::app()->session['courseId']));

        if(!empty($userSubscriptions)){

            $key = strpos($userSubscriptions->topic_id_arr,','.$id.'=>');

            if ($key !== false)
                return true;
            else
                return false;
        }
        else
            return false;
    }

    public function getSubscriptions()
    {
        $subscriptions = Subscriptions::model()->findAllByAttributes(array('user_id'=>Yii::app()->user->id));
        $subscriptionArr = array();

        if(!empty($subscriptions)){
            foreach($subscriptions as $subscription)
            {
                $topicIdStr = str_replace(array('{','}'),'',$subscription->topic_id_arr);
                $subscriptionArrTemp = explode(',',$topicIdStr);
                $subscriptionArr = array_merge($subscriptionArr,$subscriptionArrTemp);
            }

            $criteria = new CDbCriteria;
            $criteria->addInCondition('id',$subscriptionArr);

            return Topic::model()->findAll($criteria);
        }
        else return null;

    }

    public function newSubscription($id)
    {
        $userId = Yii::app()->user->id;
        $courseId = Yii::app()->session['courseId'];

        $userSubscriptions = $this->findByAttributes(array('user_id'=>$userId, 'course_id'=>$courseId));

        if(!empty($userSubscriptions))
        {
            $userSubscriptions->topic_id_arr .= $id.',';
            $userSubscriptions->save();
        }
        else
        {
            $subscriptions = new Subscriptions();
            $subscriptions->user_id = $userId;
            $subscriptions->course_id = $courseId;
            $subscriptions->topic_id_arr = $id.',';
            $subscriptions->save();
        }
    }
}
