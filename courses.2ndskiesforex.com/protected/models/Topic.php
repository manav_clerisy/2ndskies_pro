<?php

/**
 * This is the model class for table "topics".
 *
 * The followings are the available columns in table 'topics':
 * @property integer $id
 * @property integer $section_id
 * @property integer $parent_id
 * @property integer $user_id
 * @property string $subject
 * @property string $text
 * @property string $notify
 * @property string $date_created
 * @property string $date_changed
 * @property integer $status Topic priority (higher number is lower priority)
 * @property integer $order Order for announce topics
 * @property integer $last_comment_id Cached last comment ID value
 * @property integer $first_comment_id First comment ID value
 * @property string $thumb First comment ID value
 * @property integer $time_release
 * @property integer $discussion
 *
 * @property User $user
 * @property Sections $section
 * @property Comment $lastComment
 * @property Comment $firstComment
 * @property integer $replies
 */
class Topic extends CActiveRecord
{
    const NORMAL_TOPIC = 3;
    const STICKY_TOPIC = 2;
    const ANNOUNCE_TOPIC = 1;
    const GLOBAL_TOPIC = 0;

    public function afterSave()
    {
        if ($this->thumb instanceof EUploadedImage) {
            $image = $this->thumb;
            /* @var $image EUploadedImage */

            $this->setImageName($image->getName());
            if (!$image->saveAs($this->getImagePath(true)))
                throw new CException('Could not save image as ' . $this->getImagePath(true));

            $model = Topic::model()->findByPk($this->id);
            $model->thumb = $this->thumb;
            $model->save(false);
            //$this->save(false, array('image'));
        }

        return parent::afterSave();
    }

    public function setAttributes($values, $safeOnly = true) //1 by create
    {
        if (isset($values['thumb'])) {
            // Process avatar uploading

            $image = $this->uploadedImage();

            if ($image !== null)
                $values['thumb'] = $image;
            else
                unset($values['thumb']);
        }

        parent::setAttributes($values, $safeOnly);
    }
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'topics';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('section_id, user_id, status', 'numerical', 'integerOnly' => true),
            array('subject, date_created, date_changed', 'safe'),
            //array('subject','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
            array('subject', 'required'),
            array('thumb', 'file', 'types'=>'jpg, gif, png', 'maxSize' => 500 * 500, 'allowEmpty' => true, 'safe' => true),

            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, section_id, user_id, subject, date_created, date_changed, status, thumb', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'userProfile' => array(self::BELONGS_TO, 'Profile', 'user_id'),
            'section' => array(self::BELONGS_TO, 'Sections', 'section_id'),
            'lastComment' => array(self::BELONGS_TO, 'Comment', 'last_comment_id'),
            'replies' => array(self::STAT, 'Comment', 'topic_id'),
            'roles'=>array(self::HAS_ONE,'Topic','id'),
            'comments' => array(self::HAS_ONE, 'Comment', array('id'=>'topic_id'),'through'=>'roles'),
            'firstComment' => array(self::BELONGS_TO, 'Comment', 'first_comment_id'),

		);
	}

    public function behaviors()
	{
		return array(
            'dropCacheBehavior' => array(
                'class' => 'ext.behaviors.DropCacheBehavior',
                'tags' => 'topics,section-topics-{section_id},topics-status-{status}',
            ),
		);
	}
    
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'section_id' => 'Section',
			'user_id' => 'User',
			'subject' => 'Subject',
			'date_created' => 'Date Created',
			'date_changed' => 'Date Changed',
            'status' => 'Status',
            'thumb' => 'Thumb'
        );
    }


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($criteria = array())
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        if (is_array($criteria))
            $criteria = new CDbCriteria($criteria);

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.section_id', $this->section_id);
        $criteria->compare('t.user_id', $this->user_id);
        $criteria->compare('t.subject', $this->subject, true);
        $criteria->compare('t.date_created', $this->date_created, true);
        $criteria->compare('t.date_changed', $this->date_changed, true);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.thumb', $this->thumb);

        $sort = new CSort();
        $const = self::STICKY_TOPIC. ',' . self::NORMAL_TOPIC . ',' . self::ANNOUNCE_TOPIC . ',' . self::GLOBAL_TOPIC;
        $sort->defaultOrder = "FIELD(t.status, {$const}), t.date_created DESC";
        $sort->attributes = array('date_created' => array(
           "asc"=>"FIELD(t.status, {$const}), t.date_created ASC",
           "desc"=>"FIELD(t.status, {$const}), t.date_created DESC",
	       "label"=>"Date"
        ));
        $sort->multiSort = true;

//        if(!isset($criteria->order) || empty($criteria->order))
//            $criteria->order = 't.status';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageVar' => 'page',
            ),
        ));
    }

    public function searchLastTopics($currentCourse, $limit = 10)
    {
        if (!isset($currentCourse))
            return;
        
        $sectionIds = Sections::model()->allIdsBehavior->allIdsByAttributes(array('course_id'=>$currentCourse));

        $criteria = new CDbCriteria;
        $criteria->addInCondition('t.section_id', $sectionIds);
        $criteria->with = array(
            'lastComment' => array(
                'joinType' => 'INNER JOIN',
            ),
        );
        $criteria->order = 't.date_changed DESC';
        $criteria->limit = $limit;
        
        return new CActiveDataProvider($this,array('criteria'=>$criteria,'pagination'=>false));
    }
    
    /**
     * @param integer $currentCourse - id
     * @return CActiveDataProvider|null
     */
    public function searchSubscriptions($currentCourse)
    {
        if(!isset($currentCourse)) return;

        $userSubscriptions = Subscriptions::model()->findByAttributes(array('user_id'=> Yii::app()->user->id,'course_id'=>$currentCourse));

        if(!empty($userSubscriptions)){

            $topicIdsArr = array();
            $topicsArr = explode(',',$userSubscriptions->topic_id_arr);
            foreach($topicsArr as $topic)
            {
                $tempArr = explode('=>',$topic);
                $topicIdsArr[] = $tempArr[0];
            }

            $criteria = new CDbCriteria;

            $criteria->addInCondition('t.id',$topicIdsArr);
            $criteria->order = 't.date_created DESC';

            return new CActiveDataProvider($this,array('criteria'=>$criteria,'pagination'=>false));
        }
        else return null;
    }

    
    //GETTERS

    public function getTheme($isSingleVideoLayout = null)
    {
        if(isset($isSingleVideoLayout))
            $parametersArr = array('id'=>$this->id,'isSingleVideoLayout'=>$isSingleVideoLayout);
        else
            $parametersArr = array('id'=>$this->id);

        $theme = "<b><a href=" .Yii::app()->createUrl('topic/view',$parametersArr). ">" .$this->subject. "</a></b>
        <p><small>by <a href='".Yii::app()->createURL('user/profile/userProfile',array('id'=>$this->user->id))."'>" .$this->user->username. '</a> >> ' .$this->getDate($this->date_created). "</small></p>";
        return $theme;
    }

    public function getThemeForAnnouncement()
    {
        $model = $this->firstComment;
        $text = isset($model->stripped_tag_text) && $model->stripped_tag_text != '' ? substr($model->stripped_tag_text, 0, 300) . '...' : '';
        $theme = "<b><a href=" .Yii::app()->createUrl('topic/view',array('id'=>$this->id)). ">" .$this->subject. "</a></b>
        <p><small>" . $text . "</small></p>";
        return $theme;
    }

    public function getSubject()
    {
        $model = $this->firstComment;
        $text = isset($model->stripped_tag_text) && $model->stripped_tag_text != '' ? substr($model->stripped_tag_text, 0, 300) . '...' : '';
        $theme = "<b><a href=" . Yii::app()->createUrl('topic/view', array('id' => $this->id)) . ">" . $this->subject . "</a></b>
        <p><small>" . $text . "</small></p>";
        return $theme;
    }

    public function getImage()
    {
        $dir = DIRECTORY_SEPARATOR;
        $noImageUrl = $dir.'images'.$dir.'no-image.JPG';
        if($this->thumb)
            $topicImage = $this->thumb;
        else
            $topicImage = $noImageUrl;
        $img = CHtml::image($topicImage,'topic image');

        return $img;
    }

    public function getUsername()
    {
        return $this->user->username;
    }

    /**
     * Get a section name topic is posted in
     * @return string
     */
    public function getPostedInTopic()
    {
        if ($this->lastComment) {
            $url = $this->lastComment->getCommentUrl();
        } else {
            $url = Yii::app()->createUrl('topic/view',array('id'=>$this->id));
        }
        
        return
            '<a href="'.$url.'">'.$this->subject.'</a><br/>' .
            '<small>Posted in: <b>'.$this->getTopSectionName().'</b></small>';
    }
    
    public function getLastPost()
    {
        $model = $this->lastComment;

        if ($model !== null) {
            $name = (substr_count($model->user->username, 'Chris Capre')) ? '<b class="chris">'.$model->user->username.'</b>' : '<b>'.$model->user->username.'</b>';
            return '<small>by '.$name.'<br> '.$this->getDate($model->date_created).'</small>';
        }
        return null;
    }

    public function getLastPostWithCount()
    {
        $model = $this->lastComment;

        $result = '<small>Posts: ' . $this->replies . '<br>';
        if ($model !== null) {
            $result .= 'Last Post: by <b>' . $model->user->username . '</b><br>' . $this->getDate($model->date_created) . '</small>';
        }
        $result .= '</small>';
        return $result;
    }

    public function getDate($dateConverted)
    {
        if (isset($dateConverted)) {
            $arr = explode(' ', $dateConverted);
            $time = $arr[1];
            $date = $arr[0];
            $timeArr = explode(':', $time);
            $dateArr = explode('-', $date);

            $unixTime = mktime($timeArr[0], $timeArr[1], $timeArr[2], $dateArr[1], $dateArr[2], $dateArr[0]);
            $formattedDate = date('D M j, Y g:i a', $unixTime);

            return $formattedDate;
        }
        return null;
    }

    public function getPosts()
    {
        return Comment::model()->countByAttributes(array('user_id' => $this->user_id));
    }

    public function getJoined()
    {
        return $this->getDate($this->user->create_at);
    }

    public function getAnnouncementTopic()
    {
        return '<b><a href="/topic/'.$this->id.'">'.$this->subject.'</a></b>';
    }

    public function getNextTopicId($topicId, $sectionId)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('id > '.$topicId);
        $criteria->addCondition('section_id = '.$sectionId);
        $criteria->limit = 1;
        $criteria->order = 'id ASC';
        $criteria->select = 'id';

        $nextTopic = Topic::model()->find($criteria);
        if (!is_null($nextTopic))
            return $nextTopic->id;
        else
            return false;
    }
    
    public function getTopSectionName()
    {
        $id = __METHOD__ . $this->section_id;
        $name = Yii::app()->cache->get($id);
        if (!$name) {
            if (!$this->section)
                return '';
            
            $section = $this->section;
            while ($section->parent_id > 0) {
                $section = $section->parentSection;
            }

            $name = $section->name;
            
            Yii::app()->cache->set($id, $name, 0, new CacheTags('section-' . $this->section_id));
        }
        
        return $name;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Topic the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function updateLastCommentId()
    {
        $comment = Comment::model()->findByAttributes(
            array('topic_id' => $this->id),
            array(
                'select' => array('id'),
                'order' => 't.date_created DESC',
            )
        );

        $this->saveAttributes(array('last_comment_id' => $comment->id));
    }



    public function setImageName($originalName)
    {
        if (($pos=strrpos($originalName,'.')) !== false)
            $ext = (string) substr($originalName, $pos+1);
        else
            $ext = 'jpg';

        $this->thumb = $this->id . '.' . $ext;
    }


    /**
     * Returns badge image url.
     * @return string
     */
    public function getImageUrl()
    {
        return isset($this->thumb)
            ? '/uploads/topic-thumb' . '/' . $this->thumb
            : '';
    }

    /**
     * Returns uploaded image handler instance
     * @return EUploadedImage
     */
    protected function uploadedImage()
    {
        Yii::import('ext.EUploadedImage');

        try {
            $image = EUploadedImage::getInstance($this, 'thumb');
            if ($image !== null) {
                $image->maxWidth = 200;
                $image->maxHeight = 170;
            }
            return $image;
        }
        catch(Exception $e){
        }
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return Yii::getPathOfAlias('web') . '/' . $this->getImageUrl();
    }

    public static function getThreadProperties()
    {
        return array(
            self::NORMAL_TOPIC  => 'Normal',
            self::STICKY_TOPIC  => 'Sticky',
            self::ANNOUNCE_TOPIC => 'Announce',
            self::GLOBAL_TOPIC => 'Global',
        );
    }
}
