<?php

/**
 * This is the model class for table "user_course".
 *
 * The followings are the available columns in table 'user_course':
 * @property string $id
 * @property integer $user_id
 * @property integer $course_id
 * @property string $paypal_id
 * @property double $price
 * @property string $created
 * @property integer $payment_type
 */
class UserCourse extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_course';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, course_id', 'required'),
			array('user_id, course_id, payment_type', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('paypal_id', 'length', 'max'=>200),
            array('created, userName, courseName', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, course_id, paypal_id, price, created, payment_type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'course' => array(self::BELONGS_TO, 'Courses', 'course_id'),
        );
    }


    /**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'course_id' => 'Course',
			'paypal_id' => 'Paypal ID',
			'price' => 'Price',
			'created' => 'Created',
            'payment_type' => 'Payment Type'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.
        $with = array();

        $criteria=new CDbCriteria;

        $criteria->alias = 'uc';

        $criteria->compare('uc.id',$this->id,true);
        $criteria->compare('uc.user_id',$this->user_id);
        $criteria->compare('uc.course_id',$this->course_id);
        $criteria->compare('uc.price',$this->price);
        $criteria->addSearchCondition('uc.paypal_id',$this->paypal_id);
        $criteria->compare('uc.created',$this->created,true);
        $criteria->compare('uc.payment_type',$this->payment_type,true);

        // user
        $filterByUserName = isset($_GET['userName']) &&
            !empty($_GET['userName']);

        $joinUser = isset($_GET['UserCourse_sort']) &&
            stristr($_GET['UserCourse_sort'], 'userName') ||
            $filterByUserName;

        if ($joinUser)
            $with[] = 'user';

        if ($filterByUserName) {
            $criteria->addCondition('user.username LIKE :USER_NAME');
            $criteria->params[':USER_NAME'] = '%'.addcslashes(trim($_GET['userName']), '%_').'%';
        }


        // course
        $filterByCourseName = isset($_GET['courseName']) &&
            !empty($_GET['courseName']);

        $joinCourse = isset($_GET['UserCourse_sort']) &&
            stristr($_GET['UserCourse_sort'], 'courseName') ||
            $filterByCourseName;

        if ($joinCourse)
            $with[] = 'course';

        if ($filterByCourseName) {
            $criteria->addCondition('course.name LIKE :COURSE_NAME');
            $criteria->params[':COURSE_NAME'] = '%'.addcslashes(trim($_GET['courseName']), '%_').'%';
        }

        if (count($with) > 0)
            $criteria->with = $with;


        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>array(
                'attributes'=>array(
                    'id',
                    'userName'=>array(
                        'asc'=>'user.username ASC',
                        'desc'=>'user.username DESC',
                    ),
                    'courseName'=>array(
                        'asc'=>'course.name ASC',
                        'desc'=>'course.name DESC',
                    ),
                    'price'
                ),
            )
        ));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserCourse the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave()
    {
        if ($this->isNewRecord)
            $this->created = new CDbExpression('NOW()');

        return parent::beforeSave();
    }

    public function getUserName()
    {
        if (isset($this->user))
            return $this->user->username;

        return '';
    }

    public function getCourseName()
    {
        if (isset($this->course))
            return $this->course->name;

        return '';
    }

    public function getPaymentType()
    {
        if(isset($this->payment_type))
        {
            switch($this->payment_type){
                case '0': return 'Paypal';
                case '1': return 'Credit Card';
            }
        }
        else return ' - ';

    }

    public function getPaymentTypesArr()
    {
        return array('0'=>'Paypal', '1'=>'Credit Card');
    }

}
