<?php

/**
 * This is the model class for table "economic_events".
 *
 * The followings are the available columns in table 'economic_events':
 * @property integer $id
 * @property string $event_date
 * @property string $currency
 * @property string $event
 * @property string $importance
 * @property string $actual
 * @property string $forecast
 * @property string $previous
 * @property string $notes
 */
class EconomicEvents extends CActiveRecord
{
    protected function beforeSave()
    {
        $this->event_date = date('Y-m-d H:i', strtotime($this->event_date));
        return parent::beforeSave();
    }



    public static $currencies = array(
        'EUR','USD','JPY','GBP','CHF','AUD','CAD','NZD','CNY'
    );

    public static $importance = array(
        'Low','Medium','High'
    );
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'economic_events';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('event_date, currency, event, importance', 'required'),
			array('currency', 'length', 'max'=>3),
			array('event_date, actual, forecast, previous', 'length', 'max'=>250),
			array('importance', 'length', 'max'=>6),
            array('notes', 'filter', 'filter' => array(new CHtmlPurifier(), 'purify')),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, event_date, currency, event, importance, actual, forecast, previous, notes', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'event_date' => 'Date',
			'currency' => 'Currency',
			'event' => 'Event',
			'importance' => 'Importance',
			'actual' => 'Actual',
			'forecast' => 'Forecast',
			'previous' => 'Previous',
			'notes' => 'Notes',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('event_date',$this->event_date,true);
		$criteria->compare('currency',$this->currency,true);
		$criteria->compare('event',$this->event,true);
		$criteria->compare('importance',$this->importance,true);
		$criteria->compare('actual',$this->actual,true);
		$criteria->compare('forecast',$this->forecast,true);
		$criteria->compare('previous',$this->previous,true);
		$criteria->compare('notes',$this->notes,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'event_date ASC'
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EconomicEvents the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getCurrencyWithFlag()
    {
        return '<div class="flag-32-'.strtolower($this->currency).'"></div>';
    }

    public function getDateOnly($offset = 'GMT')
    {
        $temp = date_create($this->event_date,timezone_open($offset));
        $offset = date_offset_get($temp);

        $date = date("D M d", strtotime($this->event_date)+($offset));
        return  '<div class="cal-date"><span>'.$date.'</span></div>';
    }

    public function getTimeOnly($timeZone = null)
    {
        if($timeZone) return $this->getTimeZone($timeZone, $this->event_date);

        return date("H:i",strtotime($this->event_date));
    }

    public function getColoredImportance()
    {
	    $importance = $this->importance;
	    $map = array(
		    'Low' => 'label-info',
		    'Medium' => 'label-warning',
		    'High' => 'label-important',
	    );
	    if (isset($map[$importance]))
		    $importance = $map[$importance];

        return  '<span class="label '.$importance.'">'.$this->importance.'</span>';
    }

    public function getHiddenNotes()
    {
        if (empty($this->notes))
            return '';

        // for some reason attribute data can not be added to an element,
        // so to avoid this restriction i am forced to create a link with href containing id
        return  '<div class="arrow arrow-active"><a href="'.$this->id.'"></a></div>';
    }

    public function getTodaysEconomicEvents($offset, $days = 0) //Today's economic events widget
    {
        if( $days != 0 )
            $timePeriod = date('Y-m-d',strtotime($days.' days'));
        else
            $timePeriod = date('Y-m-d',strtotime('today'));

        $temp = date_create($timePeriod,timezone_open($offset));
        $offset = date_offset_get($temp);
        $begin = date("Y-m-d H:i:s", strtotime($timePeriod)-($offset));
        $end = date("Y-m-d H:i:s", strtotime($timePeriod.'+24 hours')-($offset));

        $criteria = new CDbCriteria;

        $criteria->addCondition('event_date>="'.$begin.'" AND '.'event_date<"'.$end.'"');
        $criteria->order = 'event_date ASC';

        return new CActiveDataProvider($this,array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>200,
            ),
        ));
    }

    public function getTimeZone($timeZone, $date)
    {
        $today = date('Y-m-d',strtotime('today'));

        $temp = date_create($today,timezone_open($timeZone));
        $offset = date_offset_get($temp);

        $time = explode(' ',$date);
        $hour = explode(':',$time[1]);
        $todayArr = explode('-',$today);

        date_default_timezone_set("UTC"); // Set the time zone UTC
        $time = mktime($hour[0], $hour[1], 0, $todayArr[1], $todayArr[0], $todayArr[2]);

        $time += $offset; // add offset to time zone UTC
        $hours = date("H:i", $time);

        return $hours;
    }


}
