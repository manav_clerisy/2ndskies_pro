<?php

/**
 * This is the model class for table "courses".
 *
 * The followings are the available columns in table 'courses':
 * @property string $id
 * @property string $slug
 * @property string $name
 * @property double $price
 * @property int $is_calendar_public
 * @property int $visible
 * @property integer $ontraportPid
 * @property Sections[] $sections
 * @property Sections[] $topSections
 */
class Courses extends CActiveRecord
{
    /**
     * @var array ids of all new inserted sections
     */
    public $newSectionsIds = array();

    const DEFAULT_SECTION_TREE = '[{"title":"Course","isFolder":true,"activate":"true","expand":"true","key":"root"}]';

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'courses';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('slug, name, price, ontraportPid', 'required'),
            array('price, is_calendar_public', 'numerical'),
            array('slug, name', 'length', 'max'=>255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, slug, name, price, ontraportPid', 'safe', 'on'=>'search'),
        );
    }

    public function behaviors()
    {
        return array(
            'dropCacheBehavior' => array(
                'class' => 'ext.behaviors.DropCacheBehavior',
                'tags' => 'course-{id}',
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'sections' => array(self::HAS_MANY, 'Sections', 'course_id', 'order'=>'sections.order'),
            'topSections' => array(self::HAS_MANY, 'Sections', 'course_id', 'order'=>'topSections.order', 'on' => 'topSections.parent_id = 0'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'slug' => 'Slug',
            'name' => 'Name',
            'price' => 'Price',
            'ontraportPid' => 'Ontraport product id',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('slug',$this->slug,true);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('price',$this->price);
        $criteria->compare('ontraportPid',$this->ontraportPid);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Courses the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @param int $userId User id. 0 - current authorized user
     * @return bool if user subscribed on course
     */
    public function isUserSubscribed($userId = 0)
    {
        $userModel = Yii::app()->user->model($userId);
        if(isset($userModel) && !empty($userModel))
            return
                Yii::app()->user->model($userId)->isSubscribedOnCourse($this->id);
        else return false;
    }

    /**
     * Checks if course could be deleted. Course that has subscribed user couldn't be removed
     *
     * @return bool
     */
    public function couldBeDeleted()
    {
        return !$this->amountOfSubscription;
    }

    /**
     * @return CDbDataReader|mixed|resource|string
     */
    public function getAmountOfSubscription()
    {
        $count = UserCourse::model()->countByAttributes(array('course_id' => $this->id));

        if ($count == 0)
            return '';

        return $count;
    }

    /**
     * @param $value
     */
    public function setAmountOfSubscription($value)
    {
        $this->amountOfSubscription = $value;
    }

    /**
     * @param $sections
     * @param int $parent_id
     * @param int $order
     * @param array $ids
     */
    public function saveSectionTree($sections, $parent_id = 0, &$order = 0, &$ids = array())
    {
        //ini_set('xdebug.var_display_max_depth', '-1');
        //ini_set('xdebug.var_display_max_children', '-1');
        //ini_set('xdebug.var_display_max_data', '-1');

        foreach ($sections as $section) {

            $isRoot = $section['key'] == 'root';

            if (! $isRoot)
                $order++;

            $model = isset($section['id'])
                ? Sections::model()->findByPk($section['id'])
                : new Sections;
            $model->name      = $section['title'];
            $model->course_id = $this->id;
            $model->parent_id = $parent_id;
            $model->order     = $order;

            if (! $isRoot) {
                $model->save();
                $id = $model->id;
                $this->newSectionsIds[] = $id;
            } else
                $id = 0;

            if (isset($section['children'])) {
                $this->saveSectionTree($section['children'], $id, $order, $ids);
            }

        }

    }

    /**
     * @return string
     */
    public function getSectionTree()
    {
        $tree = array();
        $links = array(
            0 => &$tree,
        );

        $list = $this->sections;

        while (!empty($list)) {
            $nextList = array();

            foreach ($list as $section) {
                /* @var $section Sections */

                if (isset($links[$section->parent_id])) {

                    $node = array('id' => $section->id, 'title' => $section->name, 'parent_id'=>$section->parent_id,'isFolder' => 'true', 'expand' => true, 'children' => array());

                    $links[$section->parent_id][] = $node;

                    $key = count($links[$section->parent_id]) - 1;

                    $links[$section->id] = &$links[$section->parent_id][$key]['children'];

                } else {
                    $nextList[] = $section;
                }
            }

            if (count($list) == count($nextList))
                break;

            $list = $nextList;
        }

        $res = array(
            'title'    => 'Course',
            'key'      => 'root',
            'isFolder' => true,
            'children' => $tree,
            'expand'   => true,
            'activate' => true,
        );


        if (empty($tree))
            return $this::DEFAULT_SECTION_TREE;

        return json_encode($res);
    }

    public function deleteSections()
    {
        $ids = implode(',', $this->newSectionsIds);
        Sections::model()->deleteAll("id NOT IN ($ids) and course_id = ".$this->id);// course_id is redundant condition but it is safer
    }

    public function findSectionsBy($id = 0)
    {
        $criteria = new CDbCriteria();
        $criteria->compare('course_id', $this->id);
        $criteria->compare('parent_id', $id);
        $criteria->order = 't.order';

        return Sections::model()
            ->cache(86400*30, new CacheTags('course-sections-' . $this->id))
            ->findAll($criteria);
    }

    public function timezone($zone = null)
    {
        $timeZones = '<select name="userTimeZone" id="time">';

        if($zone)
        {
            date_default_timezone_set("UTC");
            $timeZones .= '<option selected="selected" value='.$zone.'>'.EconomicEvents::model()->getTimeZone($zone,date('Y-m-d H:i:s')).'</option>';
        }
        else
            $timeZones .= '<option selected="selected" value="GMT">'.date('H:i').'</option>';

        $timeZones .=
            '<option value="Pacific/Midway">(GMT-11:00) Midway Island, Samoa</option>
            <option value="America/Adak">(GMT-10:00) Hawaii-Aleutian</option>
            <option value="Etc/GMT+10">(GMT-10:00) Hawaii</option>
            <option value="Pacific/Marquesas">(GMT-09:30) Marquesas Islands</option>
            <option value="Pacific/Gambier">(GMT-09:00) Gambier Islands</option>
            <option value="America/Anchorage">(GMT-09:00) Alaska</option>
            <option value="America/Ensenada">(GMT-08:00) Tijuana, Baja California</option>
            <option value="Etc/GMT+8">(GMT-08:00) Pitcairn Islands</option>
            <option value="America/Los_Angeles">(GMT-08:00) Pacific Time (US & Canada)</option>
            <option value="America/Denver">(GMT-07:00) Mountain Time (US & Canada)</option>
            <option value="America/Chihuahua">(GMT-07:00) Chihuahua, La Paz, Mazatlan</option>
            <option value="America/Dawson_Creek">(GMT-07:00) Arizona</option>
            <option value="America/Belize">(GMT-06:00) Saskatchewan, Central America</option>
            <option value="America/Cancun">(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>
            <option value="Chile/EasterIsland">(GMT-06:00) Easter Island</option>
            <option value="America/Chicago">(GMT-06:00) Central Time (US & Canada)</option>
            <option value="America/New_York">(GMT-05:00) Eastern Time (US & Canada)</option>
            <option value="America/Havana">(GMT-05:00) Cuba</option>
            <option value="America/Bogota">(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>
            <option value="America/Caracas">(GMT-04:30) Caracas</option>
            <option value="America/Santiago">(GMT-04:00) Santiago</option>
            <option value="America/La_Paz">(GMT-04:00) La Paz</option>
            <option value="Atlantic/Stanley">(GMT-04:00) Faukland Islands</option>
            <option value="America/Campo_Grande">(GMT-04:00) Brazil</option>
            <option value="America/Goose_Bay">(GMT-04:00) Atlantic Time (Goose Bay)</option>
            <option value="America/Glace_Bay">(GMT-04:00) Atlantic Time (Canada)</option>
            <option value="America/St_Johns">(GMT-03:30) Newfoundland</option>
            <option value="America/Araguaina">(GMT-03:00) UTC-3</option>
            <option value="America/Montevideo">(GMT-03:00) Montevideo</option>
            <option value="America/Miquelon">(GMT-03:00) Miquelon, St. Pierre</option>
            <option value="America/Godthab">(GMT-03:00) Greenland</option>
            <option value="America/Argentina/Buenos_Aires">(GMT-03:00) Buenos Aires</option>
            <option value="America/Sao_Paulo">(GMT-03:00) Brasilia</option>
            <option value="America/Noronha">(GMT-02:00) Mid-Atlantic</option>
            <option value="Atlantic/Cape_Verde">(GMT-01:00) Cape Verde Is.</option>
            <option value="Atlantic/Azores">(GMT-01:00) Azores</option>
            <option value="Europe/Belfast">(GMT) Greenwich Mean Time : Belfast</option>
            <option value="Europe/Dublin">(GMT) Greenwich Mean Time : Dublin</option>
            <option value="Europe/Lisbon">(GMT) Greenwich Mean Time : Lisbon</option>
            <option value="Europe/London">(GMT) Greenwich Mean Time : London</option>
            <option value="Africa/Abidjan">(GMT) Monrovia, Reykjavik</option>
            <option value="Europe/Amsterdam">(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
            <option value="Europe/Belgrade">(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
            <option value="Europe/Brussels">(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>
            <option value="Africa/Algiers">(GMT+01:00) West Central Africa</option>
            <option value="Africa/Windhoek">(GMT+01:00) Windhoek</option>
            <option value="Asia/Beirut">(GMT+02:00) Beirut</option>
            <option value="Africa/Cairo">(GMT+02:00) Cairo</option>
            <option value="Asia/Gaza">(GMT+02:00) Gaza</option>
            <option value="Africa/Blantyre">(GMT+02:00) Harare, Pretoria</option>
            <option value="Asia/Jerusalem">(GMT+02:00) Jerusalem</option>
            <option value="Europe/Minsk">(GMT+02:00) Minsk</option>
            <option value="Asia/Damascus">(GMT+02:00) Syria</option>
            <option value="Europe/Moscow">(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>
            <option value="Africa/Addis_Ababa">(GMT+03:00) Nairobi</option>
            <option value="Asia/Tehran">(GMT+03:30) Tehran</option>
            <option value="Asia/Dubai">(GMT+04:00) Abu Dhabi, Muscat</option>
            <option value="Asia/Yerevan">(GMT+04:00) Yerevan</option>
            <option value="Asia/Kabul">(GMT+04:30) Kabul</option>
            <option value="Asia/Yekaterinburg">(GMT+05:00) Ekaterinburg</option>
            <option value="Asia/Tashkent">(GMT+05:00) Tashkent</option>
            <option value="Asia/Kolkata">(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
            <option value="Asia/Katmandu">(GMT+05:45) Kathmandu</option>
            <option value="Asia/Dhaka">(GMT+06:00) Astana, Dhaka</option>
            <option value="Asia/Novosibirsk">(GMT+06:00) Novosibirsk</option>
            <option value="Asia/Rangoon">(GMT+06:30) Yangon (Rangoon)</option>
            <option value="Asia/Bangkok">(GMT+07:00) Bangkok, Hanoi, Jakarta</option>
            <option value="Asia/Krasnoyarsk">(GMT+07:00) Krasnoyarsk</option>
            <option value="Asia/Hong_Kong">(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
            <option value="Asia/Irkutsk">(GMT+08:00) Irkutsk, Ulaan Bataar</option>
            <option value="Australia/Perth">(GMT+08:00) Perth</option>
            <option value="Australia/Eucla">(GMT+08:45) Eucla</option>
            <option value="Asia/Tokyo">(GMT+09:00) Osaka, Sapporo, Tokyo</option>
            <option value="Asia/Seoul">(GMT+09:00) Seoul</option>
            <option value="Asia/Yakutsk">(GMT+09:00) Yakutsk</option>
            <option value="Australia/Adelaide">(GMT+09:30) Adelaide</option>
            <option value="Australia/Darwin">(GMT+09:30) Darwin</option>
            <option value="Australia/Brisbane">(GMT+10:00) Brisbane</option>
            <option value="Australia/Hobart">(GMT+10:00) Hobart</option>
            <option value="Asia/Vladivostok">(GMT+10:00) Vladivostok</option>
            <option value="Australia/Lord_Howe">(GMT+10:30) Lord Howe Island</option>
            <option value="Etc/GMT-11">(GMT+11:00) Solomon Is., New Caledonia</option>
            <option value="Asia/Magadan">(GMT+11:00) Magadan</option>
            <option value="Pacific/Norfolk">(GMT+11:30) Norfolk Island</option>
            <option value="Asia/Anadyr">(GMT+12:00) Anadyr, Kamchatka</option>
            <option value="Pacific/Auckland">(GMT+12:00) Auckland, Wellington</option>
            <option value="Etc/GMT-12">(GMT+12:00) Fiji, Kamchatka, Marshall Is.</option>
            <option value="Pacific/Chatham">(GMT+12:45) Chatham Islands</option>
            <option value="Pacific/Tongatapu">(GMT+13:00) Nukualofa</option>
            <option value="Pacific/Kiritimati">(GMT+14:00) Kiritimati</option>
            </select>';

        return $timeZones;
    }


    public function createMenu()
    {
        if(isset(Yii::app()->session['courseId']) && !empty(Yii::app()->session['courseId']))
        {
            $course = Courses::model()->findByPk(Yii::app()->session['courseId']);

            if($course->isUserSubscribed())
                $currentCourse = Yii::app()->session['courseId'];
        }

        if(!isset($currentCourse))
        {
            $courses = (Yii::app()->user->isAdmin()) ? Courses::model()->findAll() : Courses::model()->findAllByAttributes(array('visible' => 1));
            foreach($courses as $course)
                if($course->isUserSubscribed())
                {
                    $currentCourse = $course->id;
                    Yii::app()->session['courseId'] = $currentCourse;
                    break;
                }
            if(!isset($currentCourse)) return;
        }

        $courseUrl = '/courses/sectionlist/'.$currentCourse;

        $sections = $course->findSectionsBy();

        $arr = array();

        foreach($sections as $key => $section)
        {
		
		if($section->id != 129)
		{
            if(($section->name == 'Misc. News') || ($section->id == 6) || ($section->id == 61) || ($section->id == 85))
            {
                $arr[$key]['label']= $section->name;
                $arr[$key]['url']= array($courseUrl.'?sectionId='.$section->id.'&isMenu=true');
                continue;
            }

            $arr[$key]['label']= $section->name;
            $arr[$key]['url']= array($courseUrl.'?sectionId='.$section->id);

            $subSections = $course->findSectionsBy($section->id);
            $url = $courseUrl.'?sectionId=';

            if(($section->id == 129) || ($section->id == 130) || ( $section->id == 131) || ($section->name == 'Daily Market Commentary') || ($section->name === 'Market Commentary & Quizzes')) {
                continue;
            }

            if(!isset($subSections) || empty($subSections))
            {
                $subSections = $course->findTopicsBy($section->id);
                $url = '/topic/';
            }

            if(isset($subSections) && !empty($subSections))
            {
                $arrSubSections = array();

                foreach($subSections as $subKey => $subSection)
                {
                    $urlVideo = null;
                    if ($section->name == 'ATM v1' || $section->name == 'ATM v2' || $section->name == 'Course Lessons' || $section->name == 'Meditation Series') { //Price Action Course
                        if ($currentCourse == 1) {
                            $arrSubMenuItems1 = array();
                            $arrSubMenuItems2 = array();

                            $subMenuItems = $course->findSectionsBy($subSection->id);

                            foreach($subMenuItems as $itemKey=>$item)
                            {
                                $arrSubMenuItems1[$itemKey]['label']= $item->name;
                                $arrSubMenuItems1[$itemKey]['url']= Yii::app()->createUrl('/courses/videoLessons',array('courseId'=>$currentCourse,'sectionId'=>$item->id));
                            }

                            if($subSection->id != 25) // 1000th Member Webinars
                            {
                                $subMenuItems = $course->findTopicsBy($subSection->id);

                                foreach($subMenuItems as $itemKey=>$item)
                                {
                                    $arrSubMenuItems2[$itemKey]['label']= $item->subject;
                                    $arrSubMenuItems2[$itemKey]['url']= Yii::app()->createUrl('/courses/videoLessons',array('courseId'=>$currentCourse,'topicId'=>$item->id));
                                }
                            }
                            else
                            {
                                $arrSubSections[$subKey]['label']= isset($subSection->name) ? $subSection->name : $subSection->subject;
                                $arrSubSections[$subKey]['url']= Yii::app()->createUrl('/courses/videoLessons',array('courseId'=>$currentCourse,'sectionId'=>$subSection->id));;
                                continue;
                            }

                            $existQuiz = Quiz::model()->findByAttributes(array('section_id'=>$subSection->id));
                            $arrQuiz = array();

                            if(!empty($existQuiz))
                            {
                                $arrQuiz[0]['label']= 'Quiz';
                                $arrQuiz[0]['url']= Yii::app()->createUrl('/courses/quiz',array('sectionId'=>$subSection->id));
                            }

                            $arrSubSections[$subKey]['items']=array_merge($arrSubMenuItems1,$arrSubMenuItems2,$arrQuiz);
                        } elseif ($currentCourse == 3) {                       //Pro Forex Trading Course
                            $urlVideo = Yii::app()->createUrl('/courses/videoLessons',array('courseId'=>$currentCourse,'sectionId'=>$subSection->id));
                        } else {
                            $arrSubMenuItems1 = array();
                            $subMenuItems = $course->findSectionsBy($subSection->id);

                            foreach($subMenuItems as $itemKey=>$item)
                            {
                                $arrSubMenuItems1[$itemKey]['label']= $item->name;
                                $arrSubMenuItems1[$itemKey]['url']= Yii::app()->createUrl('/courses/videoLessons',array('courseId'=>$currentCourse,'sectionId'=>$item->id));
                            }

                            $arrSubSections[$subKey]['items']=array_merge($arrSubMenuItems1);
                        }
                    }

                    if ($section->id == 185 && ($subSection->id != 5289 || $subSection->subject != 'Open Canvas')) // Course #4 Open Canvas section
                            continue;

                    if(($subSection->id != 69)) //Ichimoku Course section "3) Strategy 1 - New Instruments 2012 Inverse TKx"
                    {
                        $arrSubSections[$subKey]['label']= isset($subSection->name) ? $subSection->name : $subSection->subject;
                        $arrSubSections[$subKey]['url']= array(isset($urlVideo)? $urlVideo : $url.$subSection->id);
                    }
                
				}
				

                $lastKey = key(end($arrSubSections));

                $arrSubSections[$lastKey]['label'] = 'General Discussions';
                $arrSubSections[$lastKey]['url']= array($courseUrl.'?sectionId='.$section->id.'&isMenu=true');

                $arr[$key]['items']= $arrSubSections;
            }
        }
		}
        return $arr;

    }

    public function getIsCalendarPublic()
    {
        return (bool)$this->is_calendar_public;
    }

    public function findTopicsBy($sectionId = null)
    {
        $criteria = new CDbCriteria;

        $criteria->compare('section_id', $sectionId);

        $criteria->compare('status', Topic::ANNOUNCE_TOPIC);
        $criteria->order = '`order`, `date_created`';

        return Topic::model()
//            ->cache(86400*30, new CacheTags('section-topics-' . $sectionId))
            ->findAll($criteria);
    }

    public function sendWelcomeCourseEmail($courseId, $userId)
    {
        $user = User::model()->findByPk($userId);
        $course = $this->model()->findByPk($courseId);

        Apostle::setup(Yii::app()->params->apostleKey);

        $mail = new Apostle\Mail(null);

        $mail->email = $user->email;
        $mail->from = Yii::app()->params->adminEmail;
        $mail->replyTo = Yii::app()->params->adminEmail;
        $mail->__set('name', $user->profile->first_name);

        switch ($courseId) {
            case 1:
                $mail->template = 'advanced-price-action-course'; // Advanced Price Action Course
                break;
            case 2:
                $mail->template = 'advanced-ichimoku-course'; // Advanced Ichimoku Course
                break;
            case 3:
                $mail->template = 'pro-forex-trading-course'; // Pro Forex Trading Course
                break;
            case 4:
                $mail->template = 'advanced-traders-mindset-course'; // Advanced Traders Mindset Course

                $mainPath = Yii::getPathOfAlias('data').'/'.$course->slug;

                $attPath = $mainPath.'/att/';
                $attachmentsList = scandir($attPath);

                // Define a list of FILES to send along with the e-mail. Key = File to be sent. Value = Name of file as seen in the e-mail.
                foreach($attachmentsList as $att)
                    $mail->addAttachment($attPath.$att, $att);

                break;
        }

        return $mail->deliver();
    }

}
