<?php

/**
 * This is the model class for table "discount_codes".
 *
 * The followings are the available columns in table 'discount_codes':
 * @property integer $id
 * @property string $code
 * @property integer $discount
 * @property string $date_start
 * @property string $date_end
 * @property integer $percentage
 * @property string $note
 */
class DiscountCode extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'discount_codes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('code, is_used', 'required'),
			array('discount', 'numerical'),
			array('code', 'length', 'max'=>250),
			array('note', 'length', 'max'=>500),
            array('code', 'existcode', 'on'=>'registration'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, code, discount, date_start, date_end, percentage, note', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'code' => 'Coupon Code',
			'discount' => 'Discount',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'note' => 'Note',
			'percentage' => 'Percentage',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('discount',$this->discount);
        $criteria->compare('date_start',$this->date_start,true);
        $criteria->compare('date_end',$this->date_end,true);
        $criteria->compare('note',$this->note,true);
		$criteria->compare('percentage',$this->percentage);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DiscountCode the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function existcode($attribute)
    {
        if(!empty($this->$attribute))
        {
            //$model = $this->find('code=:code AND is_used=0', array(
            $model = $this->find('code=:code', array(
                ':code' => $this->$attribute,
            ));

            if ($model == null)
                $this->addError($attribute, "You discount code is invalid.");
        }
    }

	public function getPriceWithDiscount($price = 0)
	{
		if ((bool)$this->percentage) {
			$result = $price - (($price * $this->discount)/100);
		} else {
			$result = $price - $this->discount;
		}

		return number_format($result, 2);
	}

}
