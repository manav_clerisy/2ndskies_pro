<?php

/**
 * This is the model class for table "comments".
 *
 * The followings are the available columns in table 'comments':
 * @property integer $id
 * @property integer $topic_id
 * @property integer $user_id
 * @property string $text
 * @property string $stripped_tag_text
 * @property string $date_created
 * @property string $date_changed
 * 
 * @property User $user
 * @property Profile $userProfile
 * @property Topic $topic
 */
class Comment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
        $p = new CHtmlPurifier();
        $p->options = array(
            'HTML.SafeIframe'=> true,
            'URI.SafeIframeRegexp'=>'%^http://videos.sproutvideo.com/embed/|^http://c.sproutvideo.com/MediaPlayer.swf%',
        );

		return array(
			array('topic_id, user_id', 'numerical', 'integerOnly'=>true),
			array('date_created, date_changed', 'safe'),
           // array('text, stripped_tag_text', 'filter', 'filter' => array($p, 'purify')),
            array('text', 'length', 'min' => 2, 'allowEmpty' => false),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, topic_id, user_id, text, date_created, date_changed, is_first', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function behaviors()
	{
		return array(
            'dropCacheBehavior' => array(
                'class' => 'ext.behaviors.DropCacheBehavior',
                'tags' => 'user-stats-{user_id},topic-comments-{topic_id}',
            ),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'userProfile' => array(self::BELONGS_TO, 'Profile', 'user_id'),
            'topic' => array(self::BELONGS_TO, 'Topic', 'topic_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'topic_id' => 'Topic ID',
			'user_id' => 'User',
			'text' => 'Text',
			'date_created' => 'Date Created',
			'date_changed' => 'Date Changed',
            'is_first' => 'Is First Comment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
        $criteria->compare('topic_id',$this->topic_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('text',$this->text,true);
		$criteria->addSearchCondition('stripped_tag_text',$this->stripped_tag_text);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_changed',$this->date_changed,true);
        $criteria->compare('is_first',$this->is_first,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function searchComments($isSingleVideoLayout = null)
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('topic_id',$this->topic_id);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('text',$this->text,true);
        $criteria->addSearchCondition('stripped_tag_text',$this->stripped_tag_text);
        $criteria->compare('date_created',$this->date_created,true);
        $criteria->compare('date_changed',$this->date_changed,true);

        if(!empty($isSingleVideoLayout)) $criteria->addCondition('is_first!=1');
        
        $criteria->order = 'date_created';

        $criteria->with = array('user', 'userProfile');

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageVar' => 'page',
            ),
        ));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Comment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getDate($date)
    {
        return Topic::model()->getDate($date);
    }

    public function getUserImage()
    {
        return $this->userProfile->imageUrl;
    }

    public function getPosts()
    {
        return Comment::model()->countByAttributes(array('user_id'=>$this->user_id));
    }

    public function getSignature()
    {
        return $this->userProfile->signature;
    }

    public function beforeSave()
    {
        $this->stripped_tag_text = strip_tags($this->text);

        return parent::beforeSave();
    }

    public function afterSave()
    {
        if($this->is_first){
            if(strpos($this->text,'<iframe class="sproutvideo-player"') !== false){
                $uploadVideoPath = Yii::getPathOfAlias('uploads.topic-thumb') . DIRECTORY_SEPARATOR;
                $uploadsUrl = '/uploads/topic-thumb/';
                $http = Yii::app()->httpClient;

                $find = 'src="//videos.sproutvideo.com/embed/';
                $from = strpos($this->text,$find)+strlen($find);
                $to = strpos($this->text,'/',$from);
                $videoId = substr($this->text,$from,$to-$from);
                if (file_exists($uploadVideoPath . $videoId . '.jpg')) {

                    $json = $http->get('//api.sproutvideo.com/v1/videos/' . $videoId, array('httpHeader' => array('SproutVideo-Api-Key: ' . Yii::app()->params['SproutVideo-Api-Key'])));
                    $video = json_decode($json, true);
                    if (!$http->download($video['assets']['thumbnails'][0], $uploadVideoPath . $videoId . '.jpg', array('httpHeader' => array('SproutVideo-Api-Key: ' . Yii::app()->params['SproutVideo-Api-Key']))))
                        Yii::log('Error downloading video thumb ' . $video['assets']['thumbnails'][0] . ' to ' . $uploadVideoPath . $videoId . '.jpg');
                    $topic = Topic::model()->findByPk($this->topic_id);
                    $topic->first_comment_id = $this->id;
                    $topic->thumb = $videoId . '.jpg';
                    $topic->save(false,array('thumb'));
                    //echo ('<pre>'.var_export($topic->attributes).'</pre>'."\n");
                }
            }
        }
        parent::afterSave();
        
        if ($this->isNewRecord) {
            Topic::model()->updateByPk($this->topic_id, array('last_comment_id' => $this->id));
        }
    }
    
    public function afterDelete()
    {
        parent::afterDelete();
        
        $this->topic->updateLastCommentId();
    }

    /**
     * @param integer $id - comment id
     * @return string $url
     */
    public function getCommentUrl($id = null)
    {
        if($id != null)
            $comment = Comment::model()->findByPk($id);
        else
            $comment = $this;

        return Yii::app()->createUrl('topic/view',array(
            'id'=>$comment->topic_id,
            'page'=>$this->getPageNumber(),
            '#'=>'c'.$comment->id,
        ));
    }

    /**
     * @param integer $currentCourse - id
     * @return CActiveDataProvider|null
     */
    public function getAnnouncements($currentCourse)
    {
        if(!isset($currentCourse)) return;

        $criteria = new CDbCriteria;

        $criteria->compare('course_id',$currentCourse);
        $criteria->select = 'id';

        $sectionIdArr = array(6,61,85,185); //Misc. News section IDs
        $criteria->addInCondition('id',$sectionIdArr);

        $sections = Sections::model()->findAll($criteria);

        foreach($sections as $section)
            $sectionIdArr[] = $section->id;

        if(!empty($sectionIdArr))
        {
            $criteria = new CDbCriteria;
            $criteria->with = array('comments');
            $criteria->limit = 20;
            $criteria->addInCondition('t.section_id',$sectionIdArr);
            $criteria->compare('t.status', 1);
            $criteria->compare('comments.is_first',true);
            $criteria->order='t.date_created DESC';
//            var_dump($criteria);exit;
            return new CActiveDataProvider(new Topic,array('criteria'=>$criteria,'pagination'=>false));//Topic::model()->findAll($criteria);
        }
        else
            return null;
    }

    /**
     * Get last comments for topics, user is subscribed to
     * @return array
     */
    public function getSubscriptionLastCommentsIds()
    {
        $userSubscriptions = Subscriptions::model()->findByAttributes(array(
            'user_id'=> Yii::app()->user->id,
            'course_id'=>Yii::app()->session['courseId']
        ));

        $topicsLastCommentIdArr = array();
        $topicsArr = explode(',',$userSubscriptions->topic_id_arr);
        foreach($topicsArr as $topic)
        {
            if(!empty($topic))
            {
                $tempArr = explode('=>',$topic);

                if(!empty($tempArr[1]))
                    $lastComment = $tempArr[1];
                else
                    $lastComment = Topic::model()->findByPk($tempArr[0])->getAttribute('last_comment_id');

                $topicsLastCommentIdArr[$tempArr[0]] = $lastComment;
            }

        }

        return $topicsLastCommentIdArr;
    }

    /**
     * Get a section name topic is posted in (for index page greedview)
     * @return string
     */
    public function getPostedIn()
    {
        $str = '';

        if($this->topic->section->parent_id)
            $str = '<a href="/courses/sectionlist/'.Yii::app()->session['courseId'].'?sectionId='.$this->topic->section->parent_id.'">'.$this->topic->section->parentSection->name.'</a> - ';

        return '<b>'.$str.'<a href="/courses/sectionlist/'.Yii::app()->session['courseId'].'?sectionId='.$this->topic->section->id.'">'.$this->topic->section->name.'</a></b>';
    }


    /**
     * @return string
     */
    public function getAuthorLastPost()
    {
        $name = (substr_count($this->user->username, 'Chris Capre')) ? '<b class="chris">'.$this->user->username.'</b>' : '<b>'.$this->user->username.'</b>';
        return '<small>by '.$name.'<br> '.$this->getDate($this->date_created).'</small>';
    }
    
    /**
     * @return integer
     */
    public function getPageNumber()
    {
        $id = __METHOD__ . $this->id;
        
        $number = Yii::app()->cache->get($id);
        if (!$number) {
            $commentsBefore = $this->countByAttributes(
                array('topic_id'=>$this->topic_id),
                "date_created<='".$this->date_created."'"
            );

            $number = ceil(($commentsBefore+1)/10);
            
            Yii::app()->cache->set($id, $number, 0, new CacheTags('topic-comments-' . $this->topic_id));
        }
        
        return $number;
    }


    public function hideSrcIframe($text = null, $id)
    {
        $hideUrl = Yii::app()->createUrl('/site/embed', array('id' => $id));
        $newText = preg_replace(array('/src="(?:http:\/\/)?(?:www\.)?(\/\/)?videos\.sproutvideo\.com\/embed\/([^"]+)"/i', '/src="(?:https:\/\/)?(?:http:\/\/)?(\/\/)?(?:www\.)?w\.soundcloud\.com\/player\/([^"]+)"/i'), "src='$hideUrl'", $text);
        return $newText;
    }
}
