<?php

/**
 * This is the model class for table "badges".
 *
 * The followings are the available columns in table 'badges':
 * @property integer $id
 * @property string $image
 * @property string $title
 * @property integer $minimum_posts
 */
class Badges extends CActiveRecord
{
    public function afterSave()
    {
        if ($this->image instanceof EUploadedImage) {
            $image = $this->image;
            /* @var $image EUploadedImage */

            $this->setImageName($image->getName());
            if (!$image->saveAs($this->getImagePath(true)))
                throw new CException('Could not save badge image as ' . $this->getImagePath(true));

            $model = Badges::model()->findByPk($this->id);
            $model->image = $this->image;
            $model->save(false);
            //$this->save(false, array('image'));
        }

        return parent::afterSave();
    }

    /**
     * Extends setAttributes to handle active date fields
     *
     * @param $values array
     * @param $safeOnly boolean
     */
    public function setAttributes($values, $safeOnly = true) //1 by create
    {
        if (isset($values['image'])) {
            // Process avatar uploading

            $image = $this->uploadedImage();
            if ($image !== null)
                $values['image'] = $image;
            else
                unset($values['image']);
        }

        parent::setAttributes($values, $safeOnly);
    }






	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'badges';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('minimum_posts', 'numerical', 'integerOnly'=>true, 'max'=>2147483646),//max value for INT
			array('title', 'length', 'max'=>255),
            array('image', 'file', 'types'=>'jpg, gif, png', /*'maxSize' => 25 * 25,*/ 'allowEmpty' => true, 'safe' => true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, image, title, minimum_posts', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'image' => 'Image',
			'title' => 'Title',
			'minimum_posts' => 'Minimum Posts',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('minimum_posts',$this->minimum_posts);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Badges the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


    /**
     * Sets proper image name for this file.
     */
    public function setImageName($originalName)
    {
        if (($pos=strrpos($originalName,'.')) !== false)
            $ext = (string) substr($originalName, $pos+1);
        else
            $ext = 'jpg';

        $this->image = $this->id . '.' . $ext;
    }


    /**
     * Returns badge image url.
     * @return string
     */
    public function getImageUrl()
    {
        return isset($this->image)
            ? '/uploads/badges' . '/' . $this->image
            : '';
    }

    /**
     * Returns uploaded image handler instance
     * @return EUploadedImage
     */
    protected function uploadedImage()
    {
        Yii::import('ext.EUploadedImage');

            try {
                $image = EUploadedImage::getInstance($this, 'image');
               /* if ($image !== null) {
                    $image->maxWidth = 25;
                    $image->maxHeight = 25;
            }*/
            return $image;
            }
            catch(Exception $e){
            }
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return Yii::getPathOfAlias('web') . '/' . $this->getImageUrl();
    }

    public function getAllInArray()
    {
        $models = $this->findAll();

        foreach($models as $badge)
            $badges[$badge->id] = $badge->title;

        return $badges;
    }

    public function getOptionsWithoutPosts()
    {
        $badges = $this->findAll('minimum_posts IS NOT NULL');
        $options = array();
        foreach ($badges as $badge) {
            $options[$badge->id] = array('disabled'=>'disabled');
        }
        return $options;
    }
}
