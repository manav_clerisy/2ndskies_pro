<?php

class m131119_161242_default_birthday extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `profiles` CHANGE `birthday` `birthday` DATE NULL DEFAULT NULL ;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131119_161242_default_birthday does not support migration down.\\n";
		return false;
	}
}