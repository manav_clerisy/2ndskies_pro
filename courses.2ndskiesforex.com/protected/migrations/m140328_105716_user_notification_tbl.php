<?php

class m140328_105716_user_notification_tbl extends CDbMigration
{
    protected $MySqlOptions = 'ENGINE=InnoDB CHARSET=utf8';

	public function safeUp()
	{
        $this->createTable('user_notifications',array(
            'id'=>'pk',
            'user_id' => 'int(11) NOT NULL',
            'notify'=>'int(10) NOT NULL DEFAULT 0',
        ),$this->MySqlOptions);
	}

	public function safeDown()
	{
		echo "m140328_105716_user_notification_tbl does not support migration down.\\n";
		return false;
	}
}