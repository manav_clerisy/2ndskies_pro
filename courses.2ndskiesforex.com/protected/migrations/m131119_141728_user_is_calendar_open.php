<?php

class m131119_141728_user_is_calendar_open extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `profiles` ADD `is_calendar_public` TINYINT NOT NULL DEFAULT '0' COMMENT 'calendar is public 0-no 1-yes';
INSERT INTO `profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`)
 VALUES (NULL, 'is_calendar_public', 'Is Calendar Public', 'TINYINT', '4', '0', '2', '', '', '', '', '', '', '', '13', '3');
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131119_141728_user_is_calendar_open does not support migration down.\\n";
		return false;
	}
}