<?php

class m131218_084217_insert_into_badges extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
        INSERT INTO `badges` ( `image`, `title`, `minimum_posts`) VALUES
        ( NULL, 'Veteran Course Member', NULL),
        ( NULL, 'Helpful Member', NULL),
        ( NULL, 'Funniest Comment of the Month Award', NULL),
        ( NULL, 'Expert Student', NULL),
        ( NULL, 'Yoda', NULL),
        ( NULL, 'Initiate', 0),
        ( NULL, 'Padawan PA Initiate', 15),
        ( NULL, 'Padawan PA Learner', 61),
        ( NULL, 'Jedi Initiate PA Trader', 109),
        ( NULL, 'Jedi PA Trader', 201),
        ( NULL, 'Jedi Guardian PA Trader', 350),
        ( NULL, 'Jedi Knight PA Trader', 501),
        ( NULL, 'Jedi Master', 751);
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131218_084217_insert_into_badges does not support migration down.\\n";
		return false;
	}
}