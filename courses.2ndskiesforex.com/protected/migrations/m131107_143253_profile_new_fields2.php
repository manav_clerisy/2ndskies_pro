<?php

class m131107_143253_profile_new_fields2 extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL

  alter table profiles
ADD (icq varchar(255) NOT NULL DEFAULT '',
aol_instant_messanger varchar(255) NOT NULL DEFAULT '',
wl_msn_messenger varchar(255) NOT NULL DEFAULT '',
yahoo_messenger varchar(255) NOT NULL DEFAULT '',
jabber_address varchar(255) NOT NULL DEFAULT '',
website varchar(255) NOT NULL DEFAULT '',
location varchar(255) NOT NULL DEFAULT '',
occupation varchar(255) NOT NULL DEFAULT '',
interests varchar(255) NOT NULL DEFAULT '',
birthday date NOT NULL DEFAULT '0000-00-00');
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131107_143253_profile_new_fields2 does not support migration down.\\n";
		return false;
	}
}