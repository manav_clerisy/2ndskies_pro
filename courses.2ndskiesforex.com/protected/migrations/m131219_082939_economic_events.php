<?php

class m131219_082939_economic_events extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS `economic_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_date` datetime NOT NULL,
  `currency` enum('EUR','USD','JPY','GBP','CHF','AUD','CAD','NZD','CNY') NOT NULL,
  `event` varchar(250) NOT NULL,
  `importance` enum('High','Medium','Low') NOT NULL,
  `actual` varchar(250) DEFAULT NULL,
  `forecast` varchar(250) DEFAULT NULL,
  `previous` varchar(250) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131219_082939_economic_events does not support migration down.\\n";
		return false;
	}
}