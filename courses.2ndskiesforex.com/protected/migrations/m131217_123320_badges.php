<?php

class m131217_123320_badges extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS `badges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE `badges` ADD `minimum_posts` INT NOT NULL ;
ALTER TABLE `badges` CHANGE `minimum_posts` `minimum_posts` INT( 11 ) NULL DEFAULT NULL ;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131217_123320_badges does not support migration down.\\n";
		return false;
	}
}