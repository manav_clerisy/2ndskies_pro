<?php

class m140114_161859_change_discount_tbls extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
DROP TABLE IF EXISTS `discount_types`;
ALTER TABLE `discount_codes` CHANGE COLUMN `type` `percentage` INT(11) NOT NULL DEFAULT 15;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m140114_161859_change_discount_tbls does not support migration down.\\n";
		return false;
	}
}