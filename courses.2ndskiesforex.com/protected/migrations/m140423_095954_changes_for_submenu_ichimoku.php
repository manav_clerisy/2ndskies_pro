<?php

class m140423_095954_changes_for_submenu_ichimoku extends CDbMigration
{
	public function safeUp()
	{
		$this->update('sections', array(
            'name'=>'If You Are New To Ichimoku, Read This',
        ), 'id=:id', array(':id'=>68));

        $this->insert('sections', array(
        'name'=>'Understanding Ichimoku Theory Beyond the 5 Lines',
        'parent_id'=>58,
        'order'=>4,
        'course_id'=>2
        ));

        $row = Yii::app()->db->createCommand(array(
            'select' => 'id',
            'from' => 'sections',
            'where' => 'name=:name',
            'params' => array(':name'=>'Understanding Ichimoku Theory Beyond the 5 Lines'),
        ))->queryRow();

        $parentId1 = $row['id'];

        $this->update('sections', array(
            'parent_id'=>$parentId1,
            'name'=>'Video Lesson 4) Reading the Chikou Span'
        ), 'id=:id', array(':id'=>70));

        $this->update('sections', array(
            'parent_id'=>$parentId1,
            'name'=>'Video Lesson 1) Introduction to Ichimoku Time Theory'
        ), 'id=:id', array(':id'=>71));

        $this->update('sections', array(
            'parent_id'=>$parentId1,
            'name'=>'Video Lesson 2) Introduction to Ichimoku Wave Theory'
        ), 'id=:id', array(':id'=>72));

        $this->update('sections', array(
            'parent_id'=>$parentId1,
            'name'=>'Video Lesson 3) Introduction to Ichimoku Price Theory'
        ), 'id=:id', array(':id'=>73));


        $this->insert('sections', array(
            'name'=>'Six Ichimoku Strategies Beyond the Theories',
            'parent_id'=>58,
            'order'=>5,
            'course_id'=>2
        ));

        $row = Yii::app()->db->createCommand(array(
            'select' => array('id'),
            'from' => 'sections',
            'where' => 'name=:name',
            'params' => array(':name'=>'Six Ichimoku Strategies Beyond the Theories'),
        ))->queryRow();

        $parentId2 = $row['id'];

        $this->update('sections', array(
            'parent_id'=>$parentId2,
            'name'=>'Strategy #1: Inverse TKx Trading'
        ), 'id=:id', array(':id'=>63));

        $this->update('sections', array(
            'parent_id'=>$parentId2,
            'name'=>'Strategy #2: Advanced Kumo Breaks '
        ), 'id=:id', array(':id'=>65));

        $this->update('sections', array(
            'parent_id'=>$parentId2,
            'name'=>'Strategy #3: The Tenkan OCx'
        ), 'id=:id', array(':id'=>62));

        $this->update('sections', array(
            'parent_id'=>$parentId2,
            'name'=>'Strategy #4: The Kijun ATR Cross'
        ), 'id=:id', array(':id'=>66));

        $this->update('sections', array(
            'parent_id'=>$parentId2,
            'name'=>'Strategy #5: Enhanced TKx Trading'
        ), 'id=:id', array(':id'=>64));

        $this->update('sections', array(
            'parent_id'=>$parentId2,
            'name'=>'Strategy #6: The WX Shadow'
        ), 'id=:id', array(':id'=>67));
	}

	public function safeDown()
	{
		echo "m140423_095954_changes_for_submenu_ichimoku does not support migration down.\\n";
		return false;
	}
}