<?php

class m140707_132137_profile_last_name_min_size extends CDbMigration
{
	public function safeUp()
	{
		$this->update('profiles_fields', array(
            'field_size_min'=>'2',
        ), 'varname=:varname', array(':varname'=>'first_name'));

        $this->update('profiles_fields', array(
            'field_size_min'=>'2',
        ), 'varname=:varname', array(':varname'=>'last_name'));
	}

	public function safeDown()
	{
		echo "m140707_132137_profile_last_name_min_size does not support migration down.\\n";
		return false;
	}
}