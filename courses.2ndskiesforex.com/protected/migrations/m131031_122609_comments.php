<?php

class m131031_122609_comments extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `comments` ENGINE = MYISAM;
alter table comments add FULLTEXT KEY `text_index` (`stripped_tag_text`)
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131031_122609_comments does not support migration down.\\n";
		return false;
	}
}