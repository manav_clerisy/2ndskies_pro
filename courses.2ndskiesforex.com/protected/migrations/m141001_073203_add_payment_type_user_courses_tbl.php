<?php

class m141001_073203_add_payment_type_user_courses_tbl extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('user_course','payment_type','SMALLINT(4) NULL DEFAULT NULL');
	}

	public function safeDown()
	{
		echo "m141001_073203_add_payment_type_user_courses_tbl does not support migration down.\\n";
		return false;
	}
}