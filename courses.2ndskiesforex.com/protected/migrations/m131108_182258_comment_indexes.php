<?php

class m131108_182258_comment_indexes extends CDbMigration
{
	public function safeUp()
	{
        $this->createIndex('topic_and_creation', 'comments', 'topic_id, date_created');
        $this->createIndex('topic_id', 'comments', 'topic_id');
	}

	public function safeDown()
	{
        $this->dropIndex('topic_and_creation', 'comments');
        $this->dropIndex('topic_id', 'comments');
	}
}