<?php

class m131121_130432_topic_last_post extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `topics` ADD `last_comment_id` INT UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Cached last comment ID value';
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
        $this->dropColumn('topics', 'last_comment_id');
	}
}