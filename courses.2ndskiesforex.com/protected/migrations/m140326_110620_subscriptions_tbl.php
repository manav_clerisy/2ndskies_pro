<?php

class m140326_110620_subscriptions_tbl extends CDbMigration
{
    protected $MySqlOptions = 'ENGINE=InnoDB CHARSET=utf8';

    public function safeUp()
	{
		$this->createTable('subscriptions',array(
            'id'=>'pk',
            'user_id' => 'int(11) NOT NULL',
            'course_id'=>'int(10) NOT NULL',
            'topic_id_arr'=>'text NOT NULL'
        ),$this->MySqlOptions);
	}

	public function safeDown()
	{
		echo "m140326_110620_subscriptions_tbl does not support migration down.\\n";
		return false;
	}
}