<?php

class m131023_140835_purchase_user_id extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `purchases` ADD `user_id` INT NOT NULL AFTER `id` ;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131023_140835_purchase_user_id does not support migration down.\\n";
		return false;
	}
}