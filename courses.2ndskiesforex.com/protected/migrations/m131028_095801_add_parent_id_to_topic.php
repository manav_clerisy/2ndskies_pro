<?php

class m131028_095801_add_parent_id_to_topic extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `topics` ADD `parent_id` INT NOT NULL AFTER `section_id` ;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131028_095801_add_parent_id_to_topic does not support migration down.\\n";
		return false;
	}
}