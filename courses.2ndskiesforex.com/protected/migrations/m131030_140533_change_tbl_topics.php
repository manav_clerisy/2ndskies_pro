<?php

class m131030_140533_change_tbl_topics extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `topics` DROP `text`,DROP `parent_id`,DROP `notify`;
DROP TABLE IF EXISTS `topic_files`;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131030_140533_change_tbl_topics does not support migration down.\\n";
		return false;
	}
}