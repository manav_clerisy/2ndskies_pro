<?php

class m140109_110145_discount_code_tbl extends CDbMigration
{
    public function safeUp()
    {
        $sql = <<<SQL
CREATE TABLE IF NOT EXISTS `discount_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(250) NOT NULL,
  `is_used` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `discount_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_percentage` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
SQL;
        $this->execute($sql);
    }

    public function safeDown()
    {
        echo "m140109_110145_discount_code_tbl does not support migration down.\\n";
        return false;
    }
}