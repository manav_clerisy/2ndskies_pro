<?php

class m131031_103413_user_course_updates extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `user_course` CHANGE `subscribed` `created` DATETIME NOT NULL;
ALTER TABLE `user_course` ADD `paypal_id` VARCHAR( 200 ) NULL DEFAULT NULL AFTER `course_id` ,
ADD `price` DOUBLE NULL DEFAULT NULL AFTER `paypal_id`;

DROP table if exists `purchases`
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131031_103413_user_course_updates does not support migration down.\\n";
		return false;
	}
}