<?php

class m131025_112008_create_tbl_topics extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS `topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `subject` text DEFAULT NULL,
  `text` text DEFAULT NULL,
  `options` varchar(255) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `date_changed` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131025_112008_create_tbl_topics does not support migration down.\\n";
		return false;
	}
}