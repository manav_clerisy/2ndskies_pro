<?php

class m131030_135930_create_tbl_comments extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `text` text,
  `date_created` datetime DEFAULT NULL,
  `date_changed` datetime DEFAULT NULL,
  `is_first` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131030_135930_create_tbl_comments does not support migration down.\\n";
		return false;
	}
}