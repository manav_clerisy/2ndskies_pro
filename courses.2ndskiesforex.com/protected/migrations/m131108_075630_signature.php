<?php

class m131108_075630_signature extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `profiles` ADD `signature` TEXT NOT NULL;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131108_075630_signature does not support migration down.\\n";
		return false;
	}
}