<?php

class m131119_090051_journal_events_user_id extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `journal_events` ADD `user_id` INT NOT NULL AFTER `id` ;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131119_090051_journal_events_user_id does not support migration down.\\n";
		return false;
	}
}