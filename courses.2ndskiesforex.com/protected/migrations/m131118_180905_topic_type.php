<?php

class m131118_180905_topic_type extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `topics` CHANGE `status` `status` TINYINT UNSIGNED NOT NULL DEFAULT '3' COMMENT 'Topic priority (higher number is lower priority)';
SQL;
		$this->execute($sql);
        
            $this->update('topics', array('status' => new CDbExpression('3 - `status`')));
	}

	public function safeDown()
	{
		
	}
}