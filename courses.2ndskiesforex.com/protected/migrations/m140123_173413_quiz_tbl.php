<?php

class m140123_173413_quiz_tbl extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS `quiz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL,
  `question` text NULL,
  `answers` text NULL,
  `correct_answers` text NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m140123_173413_quiz_tbl does not support migration down.\\n";
		return false;
	}
}