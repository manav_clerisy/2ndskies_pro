<?php

class m141002_164880_time_release extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('topics', 'time_release', 'tinyint(3) NOT NULL DEFAULT 0');
        $this->addColumn('topics', 'discussion', 'tinyint(3) NOT NULL DEFAULT 1');
    }

    public function safeDown()
    {
        $this->dropColumn('topics', 'time_release');
        $this->dropColumn('topics', 'discussion');
    }
} 