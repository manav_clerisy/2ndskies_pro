<?php

class m131218_120504_update_user_badges extends CDbMigration
{
	public function safeUp()
	{
        // phpbb_Ichiusers
        $emails = array(
            '"info@2ndskiesforex.com"',
            '"info@2ndskies.com"'
        );
        $this->doQuery('Yoda', $emails);

        $emails = array(
            '"xavier.de.buck@blackberry.mobistar.be"',
            '"mike@skeff.com"',
            '"ryanoubre@yahoo.com"',
            '"judd2_98@yahoo.com"',
            '"gabriel.petcu@yahoo.com"',
            '"andrew2010is@live.co.uk"'
        );
        $this->doQuery('Veteran Course Member', $emails);

        $emails = array(
            '"knuckleheadmovies@gmail.com"'
        );
        $this->doQuery('Expert Student', $emails);


        // phpbb_Price_users
        $emails = array(
            '"alastair.sharp@sky.com"',
            '"fxrecon@gmail.com"',
            '"sg-sulman@sbcglobal.net"',
            '"nel.rebuldela@gmail.com"',
            '"gabriel.petcu@yahoo.com"',
            '"mantasgalvosas@gmail.com"',
            '"jmintegr1@gmail.com"',
        );
        $this->doQuery('Veteran Course Member', $emails);


        // phpbb_Pro_users
        $emails = array(
            '"sanket@elitechsystems.com"',
        );
        $this->doQuery('Yoda', $emails);
	}

	public function safeDown()
	{
		echo "m131218_120504_update_user_badges does not support migration down.\\n";
		return false;
	}

    private function doQuery($badgeName, $emails)
    {
        $sql = <<<SQL
update user_badge AS ub
INNER JOIN users AS u ON ub.user_id = u.id
set ub.badge_id = (select id from badges where title="#badgeName#")
where u.email in (#emails#)
SQL;
        $emails = implode(',', $emails);
        $query = str_replace(array('#badgeName#', '#emails#'), array($badgeName, $emails), $sql);
        $this->execute($query);
    }
}