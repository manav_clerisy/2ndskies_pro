<?php

class m150130_095738_add_note_to_discount_codes extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `discount_codes` ADD `note` VARCHAR(500) NULL DEFAULT NULL ;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m150130_095738_add_note_to_discount_codes does not support migration down.\\n";
		return false;
	}
}