<?php

class m140709_161907_change_required_fields_in_registration extends CDbMigration
{
	public function safeUp()
	{
        $this->update('profiles_fields', array(
            'required'=>'1',
        ), 'varname=:varname', array(':varname'=>'first_name'));

        $this->update('profiles_fields', array(
            'required'=>'0',
        ), 'id=:id', array(':id'=>10));

        $this->update('profiles_fields', array(
            'required'=>'0',
        ), 'id=:id', array(':id'=>12));
	}

	public function safeDown()
	{
		echo "m140709_161907_change_required_fields_in_registration does not support migration down.\\n";
		return false;
	}
}