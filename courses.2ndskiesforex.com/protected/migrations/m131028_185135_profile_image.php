<?php

class m131028_185135_profile_image extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `profiles` CHANGE `avatar_path` `image` VARCHAR( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Avatar image file name';
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131028_185135_profile_image does not support migration down.\\n";
		return false;
	}
}