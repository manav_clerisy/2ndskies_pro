<?php

class m150424_071411_change_email_chris extends CDbMigration
{
	public function safeUp()
	{
		$this->update('users', array('email' => 'chris@2ndskiesforex.com'), 'email=:oldEmail', array(':oldEmail' => 'info@2ndskies.com'));
	}

	public function safeDown()
	{
        $this->update('users', array('email' => 'info@2ndskies.com'), 'email=:newEmail', array(':newEmail' => 'chris@2ndskiesforex.com'));
	}
}