<?php

class m150328_117600_initiate_badge_change extends CDbMigration
{
	public function safeUp()
	{
        $sql = <<<SQL
UPDATE `badges` SET `minimum_posts` = 1 WHERE `title` = 'Initiate';
SQL;
        $this->execute($sql);
	}

	public function safeDown()
	{
        $sql = <<<SQL
UPDATE `badges` SET `minimum_posts` = 0 WHERE `title` = 'Initiate';
SQL;
        $this->execute($sql);
	}
}