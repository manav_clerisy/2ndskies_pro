<?php

class m140104_112422_initiate_badge extends CDbMigration
{
	public function safeUp()
	{
        $exist = (bool)Yii::app()->db->createCommand('select id from badges where title = "Initiate"')->queryScalar();

        if (! $exist) {

		    $sql = <<<SQL
                INSERT IGNORE INTO badges( `image`, `title`, `minimum_posts`) VALUES ( NULL, 'Initiate', 0);
SQL;
		    $this->execute($sql);
        }
	}

	public function safeDown()
	{
		echo "m140104_112422_initiate_badge does not support migration down.\\n";
		return false;
	}
}