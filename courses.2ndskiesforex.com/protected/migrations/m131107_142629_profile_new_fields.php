<?php

class m131107_142629_profile_new_fields extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
INSERT INTO `profiles_fields`
(`varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES
('icq', 'ICQ Number', 'VARCHAR', 255, 3, 2, '', '', '', '', '', '', '', 3, 3),
('aol_instant_messanger', 'AOL Instant Messenger', 'VARCHAR', 255, 0, 2, '', '', '', '', '', '', '', 4, 3),
('wl_msn_messenger', 'WL/MSN Messenger', 'VARCHAR', 255, 0, 2, '', '', '', '', '', '', '', 5, 3),
('yahoo_messenger', 'Yahoo Messenger', 'VARCHAR', 255, 0, 2, '', '', '', '', '', '', '', 6, 3),
('jabber_address', 'Jabber address', 'VARCHAR', 255, 0, 2, '', '', '', '', '', '', '', 7, 3),
('website', 'Website', 'VARCHAR', 255, 0, 2, '', '', '', '', '', '', '', 8, 3),
('location', 'Location', 'VARCHAR', 255, 0, 2, '', '', '', '', '', '', '', 9, 3),
('occupation', 'Occupation', 'VARCHAR', 255, 0, 2, '', '', '', '', '', '', '', 10, 3),
('interests', 'Interests', 'VARCHAR', 255, 0, 2, '', '', '', '', '', '', '', 11, 3),
('birthday', 'Birthday', 'DATE', 0, 0, 2, '', '', '', '', '', 'UWjuidate', '', 12, 3);

,
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131107_142629_profile_new_fields does not support migration down.\\n";
		return false;
	}
}