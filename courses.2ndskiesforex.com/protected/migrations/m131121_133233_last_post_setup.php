<?php

class m131121_133233_last_post_setup extends CDbMigration
{
	public function safeUp()
	{
		$reader = $this->dbConnection->createCommand()
            ->select('id, topic_id')
            ->from('comments')
            ->order('date_created DESC')
            ->query();
        
        $reader->setFetchMode(PDO::FETCH_NUM);
        
        $updated = array();
        foreach ($reader as $row) {
            list($id, $topicId) = $row;
            
            if (!isset($updated[$topicId])) {
                $this->update(
                    'topics',
                    array('last_comment_id' => $id),
                    'id = ' . $topicId
                );
                $updated[$topicId] = true;
            }
        }
	}

	public function safeDown()
	{
	}
}