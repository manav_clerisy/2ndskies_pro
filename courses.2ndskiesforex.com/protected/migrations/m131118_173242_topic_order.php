<?php

class m131118_173242_topic_order extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `topics` ADD `order` TINYINT UNSIGNED NOT NULL DEFAULT '255' COMMENT 'Order for announce topics';
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
        $this->dropColumn('topics', 'order');
	}
}