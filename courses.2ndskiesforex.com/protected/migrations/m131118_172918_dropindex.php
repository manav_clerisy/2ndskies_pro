<?php

class m131118_172918_dropindex extends CDbMigration
{
	public function safeUp()
	{
		$this->dropIndex('topic_id', 'comments');
	}

	public function safeDown()
	{
		$this->createIndex('topic_id', 'comments', 'topic_id');
	}
}