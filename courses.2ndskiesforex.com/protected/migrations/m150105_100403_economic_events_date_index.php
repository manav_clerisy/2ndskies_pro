<?php

class m150105_100403_economic_events_date_index extends CDbMigration
{
	public function safeUp()
	{
        $this->createIndex('economic_events_date', 'economic_events', 'event_date');
	}

	public function safeDown()
	{
        $this->dropIndex('economic_events_date', 'economic_events');
	}
}