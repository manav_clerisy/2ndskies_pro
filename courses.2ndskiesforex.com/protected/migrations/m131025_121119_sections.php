<?php

class m131025_121119_sections extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

ALTER TABLE `sections` ADD `course_id` INT NOT NULL ;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131025_121119_sections does not support migration down.\\n";
		return false;
	}
}