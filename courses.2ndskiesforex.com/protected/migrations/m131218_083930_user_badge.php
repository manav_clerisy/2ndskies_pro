<?php

class m131218_083930_user_badge extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS `user_badge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `badge_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
SQL;
		$this->execute($sql);

        $sql = <<<SQL
 INSERT INTO user_badge (user_id)
  SELECT id
  FROM users
  order by id
SQL;
        $this->execute($sql);


        $sql = <<<SQL
 update user_badge set badge_id = (SELECT id FROM badges where title = "Initiate")
SQL;
        $this->execute($sql);


        $sql = <<<SQL
 ALTER TABLE `user_badge` ADD `set_manually` INT NOT NULL;
SQL;
        $this->execute($sql);


	}

	public function safeDown()
	{
		echo "m131218_083930_user_badge does not support migration down.\\n";
		return false;
	}
}