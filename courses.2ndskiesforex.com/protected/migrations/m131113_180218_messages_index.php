<?php

class m131113_180218_messages_index extends CDbMigration
{
	public function safeUp()
	{
        $this->createIndex('unread', 'messages', 'user_to, is_read');
	}

	public function safeDown()
	{
        $this->dropIndex('unread', 'messages');
	}
}