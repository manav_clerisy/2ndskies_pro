<?php

class m131028_132648_add_status_and_notify_to_topic extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `topics` CHANGE COLUMN `options` `notify` INT(10) NULL DEFAULT 0;
ALTER TABLE `topics` ADD `status` INT NULL AFTER `date_changed`;
ALTER TABLE `topics` MODIFY `date_created` datetime NULL;
ALTER TABLE `topics` MODIFY `date_changed` datetime NULL;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131028_132648_add_status_and_notify_to_topic does not support migration down.\\n";
		return false;
	}
}