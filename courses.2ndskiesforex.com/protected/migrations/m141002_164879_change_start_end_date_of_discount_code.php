<?php

class m141002_164879_change_start_end_date_of_discount_code extends CDbMigration
{
    public function safeUp()
    {
        $time = new DateTime();

        $this->update('discount_codes', array('date_start'=>'2014-01-01'), 'date_start=:null', array(':null' => '0000-00-00 00:00:00'));
        $this->update('discount_codes', array('date_end'=>'2014-01-02'), 'date_end=:null', array('null' => '0000-00-00 00:00:00'));
    }

    public function safeDown()
    {
        $this->update('discount_codes', array('date_start'=>'0000-00-00'), 'date_start=:date_start', array(':date_start' => '2014-01-01'));
        $this->update('discount_codes', array('date_end'=>'0000-00-00'), 'date_end=:date_end', array(':date_end' => '2015-01-01'));
    }
} 