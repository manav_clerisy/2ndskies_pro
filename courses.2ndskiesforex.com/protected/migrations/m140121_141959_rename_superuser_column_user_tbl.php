<?php

class m140121_141959_rename_superuser_column_user_tbl extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `users` CHANGE COLUMN `superuser` `role` INT(11) NOT NULL DEFAULT 0;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m140121_141959_rename_superuser_column_user_tbl does not support migration down.\\n";
		return false;
	}
}