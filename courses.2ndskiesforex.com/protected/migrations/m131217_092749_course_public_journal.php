<?php

class m131217_092749_course_public_journal extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
		  DELETE FROM `profiles_fields` WHERE `profiles_fields`.`id` = 13;
SQL;
        $this->execute($sql);

        $sql = <<<SQL
                  ALTER TABLE `profiles` DROP `is_calendar_public` ;
SQL;
		$this->execute($sql);

        $sql = <<<SQL
               ALTER TABLE `courses` ADD `is_calendar_public` TINYINT( 1 ) NOT NULL DEFAULT '0' COMMENT 'calendar is public 0-no 1-yes';
SQL;
        $this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131217_092749_course_public_journal does not support migration down.\\n";
		return false;
	}
}