<?php

class m150105_145647_topic_index_dch_si extends CDbMigration
{
	public function safeUp()
	{
        $this->createIndex('dch_si', 'topics', 'date_changed,section_id');
	}

	public function safeDown()
	{
        $this->dropIndex('dch_si', 'topics');
	}
}