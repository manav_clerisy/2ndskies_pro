<?php

class m150311_080816_change_type_field extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `comments` CHANGE `text` `text` MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
        $sql = <<<SQL
ALTER TABLE `comments` CHANGE `text` `text` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;
SQL;
        $this->execute($sql);
	}
}