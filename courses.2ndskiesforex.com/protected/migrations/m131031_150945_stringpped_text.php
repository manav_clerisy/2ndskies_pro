<?php

class m131031_150945_stringpped_text extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `comments` ADD `stripped_tag_text` TEXT NOT NULL COMMENT 'there is fulltext index on this column' AFTER `text`;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131031_150945_stringpped_text does not support migration down.\\n";
		return false;
	}
}