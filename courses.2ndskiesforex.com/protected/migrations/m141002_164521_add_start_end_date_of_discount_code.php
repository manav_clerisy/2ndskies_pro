<?php

class m141002_164521_add_start_end_date_of_discount_code extends CDbMigration
{
    public function safeUp()
    {
        $this->dropColumn('discount_codes', 'is_used');
        $this->addColumn('discount_codes', 'date_start', 'datetime NOT NULL');
        $this->addColumn('discount_codes', 'date_end', 'datetime NOT NULL');
    }

    public function safeDown()
    {
        $this->dropColumn('discount_codes', 'date_start');
        $this->dropColumn('discount_codes', 'date_end');
        $this->addColumn('discount_codes', 'is_used', 'int(11) NOT NULL');
    }
} 