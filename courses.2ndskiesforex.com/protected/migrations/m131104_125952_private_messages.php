<?php

class m131104_125952_private_messages extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL,
  `text` text NOT NULL,
  `created` datetime NOT NULL,
  `is_read` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0-unread, 1-read',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='user private messages' AUTO_INCREMENT=1 ;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131104_125952_private_messages does not support migration down.\\n";
		return false;
	}
}