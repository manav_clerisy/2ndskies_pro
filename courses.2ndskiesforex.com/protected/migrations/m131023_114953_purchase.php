<?php

class m131023_114953_purchase extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS `purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `price` double NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131023_114953_purchase does not support migration down.\\n";
		return false;
	}
}