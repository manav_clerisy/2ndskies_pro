<?php

class m131105_113609_tbl_topics_change_subject_field extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `topics` CHANGE `subject` `subject` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131105_113609_tbl_topics_change_subject_field does not support migration down.\\n";
		return false;
	}
}