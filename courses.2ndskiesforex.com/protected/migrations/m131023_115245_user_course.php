<?php

class m131023_115245_user_course extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS `user_course` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `subscribed` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131023_115245_user_course does not support migration down.\\n";
		return false;
	}
}