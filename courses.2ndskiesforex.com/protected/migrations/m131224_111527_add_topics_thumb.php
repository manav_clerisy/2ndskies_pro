<?php

class m131224_111527_add_topics_thumb extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `topics` ADD `thumb` VARCHAR( 100 ) NULL COMMENT 'url for topic thumb'
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131224_111527_add_topics_thumb does not support migration down.\\n";
		return false;
	}
}