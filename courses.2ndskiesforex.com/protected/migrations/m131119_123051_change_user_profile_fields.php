<?php

class m131119_123051_change_user_profile_fields extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
		DELETE FROM profiles_fields WHERE varname IN('icq','aol_instant_messanger', 'wl_msn_messenger', 'yahoo_messenger', 'jabber_address');
		ALTER TABLE profiles DROP icq, DROP aol_instant_messanger, DROP wl_msn_messenger, DROP yahoo_messenger, DROP jabber_address;
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131119_123051_change_user_profile_fields does not support migration down.\\n";
		return false;
	}
}