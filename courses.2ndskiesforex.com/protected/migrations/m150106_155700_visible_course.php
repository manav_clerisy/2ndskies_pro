<?php

class m150106_155700_visible_course extends CDbMigration
{
	public function safeUp()
	{
        $this->addColumn('courses', 'visible', 'tinyint(1) NOT NULL DEFAULT 1');
	}

	public function safeDown()
	{
		$this->dropColumn('courses', 'visible');
	}
}