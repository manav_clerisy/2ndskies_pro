<?php

class m131022_120733_tbl_courses_add_info extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
INSERT INTO `courses` (`id`, `slug`, `name`, `price`) VALUES ('1', 'advanced-price-action-course', 'Advanced Price Action Course', '399.00');
INSERT INTO `courses` (`id`, `slug`, `name`, `price`) VALUES ('2', 'advanced-ichimoku-course', 'Advanced Ichimoku Course', '399.00');
INSERT INTO `courses` (`id`, `slug`, `name`, `price`) VALUES ('3', 'pro-forex-trading-course', 'Pro Forex Trading Course', '399.00');
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131022_120733_tbl_courses_add_info does not support migration down.\\n";
		return false;
	}
}