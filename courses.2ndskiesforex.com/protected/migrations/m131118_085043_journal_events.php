<?php

class m131118_085043_journal_events extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS `journal_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity` enum('Trading','Study/Review','Simulation') NOT NULL COMMENT 'Activity For the Day',
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `location` enum('Home office','Laptop','Traveling') NOT NULL,
  `conditions` enum('Trading Day','Holiday','Weekend','') NOT NULL,
  `objective` text NOT NULL,
  `percentage_programm` double DEFAULT NULL COMMENT '% Ran My Mind Program For the Day',
  `percentage_system` double DEFAULT NULL COMMENT '% Ran My Technical Systems Correctly',
  `what_did` text COMMENT 'What I Did Today & What I Learned',
  `what_did_well` text COMMENT 'What I Did Well Today',
  `solution` text COMMENT 'I Am Looking For A Solution To',
  `statement` text COMMENT 'Directive Affirmation Statement',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `journal_events` CHANGE `activity` `activity` SET( 'Trading', 'Study/Review', 'Simulation' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Activity For the Day';
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
		echo "m131118_085043_journal_events does not support migration down.\\n";
		return false;
	}
}