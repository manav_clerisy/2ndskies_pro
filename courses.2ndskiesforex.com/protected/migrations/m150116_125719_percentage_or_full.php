<?php

class m150116_125719_percentage_or_full extends CDbMigration
{
	public function safeUp()
	{
        $this->renameColumn('discount_codes', 'percentage', 'discount');
		$this->addColumn('discount_codes', 'percentage', 'tinyint(1) NOT NULL DEFAULT 1');
	}

	public function safeDown()
	{
		$this->dropColumn('discount_codes', 'percentage');
		$this->renameColumn('discount_codes', 'discount', 'percentage');
	}
}