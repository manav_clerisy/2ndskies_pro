<?php

class m150402_141953_add_index_user_id extends CDbMigration
{
    public function safeUp()
    {
        $this->createIndex('comments_user_id', 'comments', 'user_id');
        $this->createIndex('topics_user_id', 'topics', 'user_id');
    }

    public function safeDown()
    {
        $this->dropIndex('comments_user_id', 'comments');
        $this->dropIndex('topics_user_id', 'topics');
    }
}