<?php

class m131219_113417_topic_first_comment extends CDbMigration
{
	public function safeUp()
	{
        $sql = <<<SQL
ALTER TABLE `topics` ADD `first_comment_id` INT UNSIGNED NOT NULL DEFAULT '0' COMMENT 'First comment ID value';
SQL;
$this->execute($sql);

        $reader = $this->dbConnection->createCommand()
            ->select('id, topic_id')
            ->from('comments')
            ->where('is_first = 1')
            ->query();

        $reader->setFetchMode(PDO::FETCH_NUM);

        $updated = array();
        foreach ($reader as $row) {
            list($id, $topicId) = $row;

            if (!isset($updated[$topicId])) {
                $this->update(
                    'topics',
                    array('first_comment_id' => $id),
                    'id = ' . $topicId
                );
                $updated[$topicId] = true;
            }
        }


	}

	public function safeDown()
	{
		echo "m131219_113417_topic_first_comment does not support migration down.\\n";
		return false;
	}
}