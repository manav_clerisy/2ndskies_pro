<?php

class m131025_175234_avatarPath extends CDbMigration
{
	public function safeUp()
	{
		$sql = <<<SQL
ALTER TABLE `profiles` ADD `avatar_path` VARCHAR( 200 ) NULL COMMENT 'Avater image filename';
SQL;
		$this->execute($sql);
	}

	public function safeDown()
	{
        $this->dropColumn('profiles', 'avatar_path');
	}
}