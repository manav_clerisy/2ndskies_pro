# Setup directory permissions
chmod 777 ./assets
chmod 777 ./protected/runtime
chmod 777 ./protected/runtime/cache
chmod 777 ./uploads/files
chmod 777 ./uploads/user-full
chmod 777 ./uploads/user-thumb

# Setup executables permissions
chmod 755 ./protected/yiic
chmod 755 ./protected/test.sh
chmod 755 ./protected/init.sh

# Create local config file if not exits
test -f ./protected/config/main-local.php || cp ./protected/config/main-local.example.php ./protected/config/main-local.php
