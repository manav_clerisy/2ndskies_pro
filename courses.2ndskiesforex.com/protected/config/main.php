<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

Yii::setPathOfAlias('web', dirname(__FILE__).'/../..');
Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
Yii::setPathOfAlias('data', dirname(__FILE__).'/../data');
Yii::setPathOfAlias('runtime', dirname(__FILE__).'/../runtime');

/* Paypal Manager Action Log */
Yii::setPathOfAlias('action_log', dirname(__FILE__).'/../../actionLog/actionLog.txt');

Yii::setPathOfAlias('Apostle', dirname(__FILE__).'/../vendor/apostle/apostle-php/src/Apostle');
Yii::setPathOfAlias('Guzzle', dirname(__FILE__).'/../vendor/guzzle/guzzle/src/Guzzle');
Yii::setPathOfAlias('Symfony', dirname(__FILE__).'/../vendor/symfony/event-dispatcher/Symfony');


$config = array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'2nd Skies',
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.components.scrapers.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
		'ext.EScriptBoost.*',
        'application.widgets.TDatePicker.TDatePicker',
        'application.vendor.apostle.apostle-php.src.*',
	),

	'modules'=>array(
		'admin',
        'user'=>array(
            # encrypting method (php hash function)
            'hash' => 'md5',

            # send activation email
            'sendActivationMail' => true,

            # allow access for non-activated users
            'loginNotActiv' => false,

            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => false,

            # automatically login from registration
            'autoLogin' => true,

            # registration path
            'registrationUrl' => array('/user/registration'),

            # recovery password path
            'recoveryUrl' => array('/user/recovery'),

            # login form path
            'loginUrl' => array('/user/login'),

            # page after login
            'returnUrl' => array('/user/profile'),

            # page after logout
            'returnLogoutUrl' => array('/user/login'),

            'tableUsers'    => 'users',
            'tableProfiles' => 'profiles',
            'tableProfileFields'    => 'profiles_fields',
        ),
        'cal'=>array('debug'=>true),
	),

	// application components
	'components'=>array(
        'paypal' => array(
            'class' => 'ext.paypal.Paypal',

            'sandbox' => true,

            'email'     => 'ztwetbkw@sharklasers.com',
            'username'  => 'ztwetbkw_api1.sharklasers.com',
            'password'  => '1382452481',
            'signature' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31ARCuY-N3ZhSHWSyf5THlh4IdsxkY',
        ),
        'paypalSDK' => array(
            'class' => 'ext.paypal.PaypalSDK',
            'clientId' => 'AR0IPBAO2rD5tVMrt9AbxgtM_DMl7u89Z1vucwD0-cz4wrNYXOAQ70A_Z0e0',
            'clientSecret' => 'EOi5xBBuQLgv8OefHBNOXNurt6Tt-dlu4nXW2pe3sZSDG5t4YV-4ZQir_cAv',
        ),
      /*  'getResponseMailer' =>array(
            'class' => 'ext.getResponseMailer.GetResponseMailer' ,
            'api_key'=>'dc26e626badc7d1a6988011ff04593fa',
            'campaign_name'=>'dergartendesnichts'
        ),*/
        'httpClient' => array(
            'class' => 'ext.httpclient.HttpClient',
            'useRandomCookieFile' => false,
        ),
        'user'=>array(
            'class' => 'WebUser',
            'allowAutoLogin'=>true,
            'loginUrl' => array('/user/login'),
		),
		'urlManager'=>array(
			'showScriptName' => false,
			'urlFormat' => 'path',
			'rules'=>array(
				'' => 'site/index',
				'<controller:\w+>/<id:\d+>/page<page:\d+>'=>'<controller>/view',
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<courseId:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>'
			),
		),
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=courses_2ndskies',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'root',
			'charset' => 'utf8',
			'schemaCachingDuration' => !YII_DEBUG ? 86400 : 0,
			'enableParamLogging' => true,
		),
		'cache' => array(
			'class' => 'CFileCache',
            'behaviors' => array(
                array(
                    'class' => 'ext.behaviors.TaggingBehavior',
                ),
            ),
		),
		'assetManager' => array(
			'class' => 'ext.EAssetManagerBoostGz',
			'minifiedExtensionFlags' => array('min.js', 'minified.js', 'packed.js', '.'),
		),
        'bootstrap' => array(
            'class'=>'bootstrap.components.Bootstrap',
        ),
		'clientScript'=>array(
            'packages' => array(
				'jquery' => array( // jQuery CDN - provided by (mt) Media Temple
					'baseUrl' => '//code.jquery.com/',
					'js' => array(YII_DEBUG ? 'jquery-1.8.2.js' : 'jquery-1.8.2.min.js'),
				),
                'jquery.ui' => array( // jQuery CDN - provided by (mt) Media Temple
                    'baseUrl' => '//ajax.googleapis.com/ajax/libs/jqueryui/1.8/',
                    'css' => array('themes/smoothness/jquery-ui.css'),
                    'js' => array(YII_DEBUG ? 'jquery-ui.js' : 'jquery-ui.min.js'),
                    'depends' => array('jquery'),
                ),
                'jquery.cookie' => array(
                    'baseUrl' => '/js',
                    'js' => array('jquery.cookie.js'),
                    'depends' => array('jquery'),
                ),
                'jquery.dynatree' => array(
                    'baseUrl' => '/',
                    'js' => array('js/jquery.dynatree-1.2.4.js'),
                    'css' => array('css/ui.dynatree.css'),
                    'depends' => array('jquery', 'jquery.ui', 'jquery.cookie'),
                ),
                'jquery.bootbox' => array(
                    'baseUrl' => '/js',
                    'js' => array('jquery.bootbox.min.js'),
                    'depends' => array('jquery'),
                ),
                'calendar' => array(
                    'baseUrl' => '/js/fullcalendar',
                    'js' => array('fullcalendar.min.js', 'gcal.js'),
                    'css' => array('fullcalendar.css'),
                    'depends' => array('jquery', 'jquery.ui'),
                ),
                'timepicker' => array(
                    'baseUrl' => '/',
                    'js' => array('js/timepicker.js'),
                    'css' => array('css/timepicker.css', 'css/dfx.min.css'),
                    'depends' => array('jquery', 'jquery.ui'),
                ),
			),
			'behaviors' => array(
				array(
					'class' => 'ext.behaviors.localscripts.LocalScriptsBehavior',
					'publishJs' => !YII_DEBUG,
					// Uncomment this if your css don't use relative links
					// 'publishCss' => !YII_DEBUG,
				),
			),
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
                array(
                    'class'   => 'ext.formattedfilelogroute.FormattedFileLogRoute',
                    'format'  => "{time}\t{ip}\t{category}\t{uri}\t{message}",
                    'levels'  => 'warning',
                    'logFile' => 'warning.log',
                ),
                array(
                    'class'   => 'ext.formattedfilelogroute.FormattedFileLogRoute',
                    'format'  => "{time}\t{ip}\t{category}\t{uri}\t{message}",
                    'except'  => 'exception.CHttpException.404',
                    'levels'  => 'error',
				    'logFile' => 'error.log',
                ),
                array(
                    'class'      => 'ext.formattedfilelogroute.FormattedFileLogRoute',
                    'format'     => "{time}\t{ip}\t{uri}\t{sref}",
                    'categories' => 'exception.CHttpException.404',
                    'logFile'    => 'error404.log',
                ),
			),
		),
        'session'=>array(
            'class'=>'HttpSession'
        ),
        'authManager' => array(
            'class' => 'PhpAuthManager',
        ),

	),
    'params'=>array(
        // this is used in contact page
        'adminEmail'=>'chris@2ndskiesforex.com',
        'SproutVideo-Api-Key' => 'af757048550e111b3a07cf8b3ace33c6',
        'ontraportAppId' => '2_26856_8fklzY3Jy', // test account
        'ontraportAppKey' => 'OSc6qaKL2M01m5H',
        'actionLog' => true,
        'paypal_url' => 'https://payflowpro.paypal.com/',	//https://payflowpro.paypal.com/ or https://pilot-payflowpro.paypal.com/
        'mode' => 'LIVE', 					                /* LIVE or TEST */
        'login' => '2ndskiesforex', 				        /* 2ndskiesforex(LIVE) or 2ndskiesforex12(TEST) */
        'vendor' => '2ndskiesforex', 			            /* 2ndskiesforex(LIVE) or 2ndskiesforex12(TEST) */
        'user' => 'developer',				                /* same for both*/
        'pwd' => 'Ws6e7h^fEKwsDo^94n',                      /* same for both*/
        'apostleKey' => 'ce738559a63b383bfb579b0d007fa4700ab4f23a', // Apostle.io secret key
    ),
);

// Apply local config modifications
@include dirname(__FILE__) . '/main-local.php';

return $config;
