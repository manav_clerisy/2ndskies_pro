<?php

$mainConfig = include dirname(__FILE__) . '/main.php';

return array(
	'basePath' => $mainConfig['basePath'],
	'name'     => $mainConfig['name'],
	'import'   => $mainConfig['import'],
	
	'preload' => array('log'),

	'components' => array(
		'cache' => $mainConfig['components']['cache'],
		'db'    => $mainConfig['components']['db'],
		'log'   => $mainConfig['components']['log'],
        'httpClient' => array(
            'class' => 'ext.httpclient.HttpClient',
            'useRandomCookieFile' => false,
        ),
	),

    'modules' => array(
        'user' => $mainConfig['modules']['user'],
    ),

	'commandMap' => array(
		'migrate' => array(
			'class'        => 'application.commands.ChMigrateCommand',
			'templateFile' => 'application.migrations.template',
		),
	),
	
	'params' => $mainConfig['params'],
);