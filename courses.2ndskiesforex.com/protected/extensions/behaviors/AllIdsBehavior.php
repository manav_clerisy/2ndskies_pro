<?php

/**
 * @author urmaul <urmaul@codesex.org>
 */
class AllIdsBehavior extends CActiveRecordBehavior
{
    public $cacheExpire = 60;
    
    /**
     * Finds all ids array.
     * @param mixed $condition query condition or criteria.
     * @param array $params parameters to be bound to an SQL statement.
     * @return array
     */
    public function allIds($condition='', $params=array())
    {
        $owner = $this->getOwner();
        /* @var $owner CActiveRecord */
        
        $cacheId = get_class($owner) . '-' . json_encode($condition) . '-' . json_encode($params);
        $result = Yii::app()->cache->get($cacheId);
        if ($result === false) {
            $criteria = $owner->getCommandBuilder()->createCriteria($condition, $params);

            $pk = $owner->getMetaData()->tableSchema->primaryKey;

            $cmd = $this->owner->dbConnection
                ->createCommand()
                ->select($pk)
                ->from($owner->tableName() . ' ' . $criteria->alias);

            $cmd->where  = $criteria->condition;
            $cmd->order  = $criteria->order;
            $cmd->params = $criteria->params;

            $result = is_string($pk) ? $cmd->queryColumn() : $cmd->queryAll();
            
            Yii::app()->cache->set($cacheId, $result, $this->cacheExpire);
        }
        
        return $result;
    }
    
    /**
     * Finds all ids array by attributes.
     * @param array $attributes list of attribute values (indexed by attribute names) that the active records should match. An attribute value can be an array which will be used to generate an IN condition.
     * @param mixed $condition query condition or criteria.
     * @param array $params parameters to be bound to an SQL statement.
     * @return array
     */
    public function allIdsByAttributes($attributes, $condition='', $params=array())
    {
        $owner = $this->getOwner();
        /* @var $owner CActiveRecord */
        
        $prefix = $owner->getTableAlias(true).'.';
        $criteria = $owner->getCommandBuilder()->createColumnCriteria($owner->getTableSchema(),$attributes,$condition,$params,$prefix);
        $criteria->alias = $owner->getTableAlias();
        
        return $this->allIds($criteria);
    }
}

/**
 * @property AllIdsBehavior $allIdsBehavior
 * @method array allIds($criteria) 
 * @method array allIdsByAttributes($criteria) 
 */
