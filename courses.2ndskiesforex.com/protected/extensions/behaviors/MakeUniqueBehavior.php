<?php

class MakeUniqueBehavior extends CActiveRecordBehavior
{
    /**
     * Adds number to the end of attribute value until it is unique.
     */
	public function makeUnique($attribute, $postfix = '')
    {
        /* @var $owner CActiveRecord */
        $owner = $this->owner;
            
        if ( !empty($postfix) ) {
        	$pos =  strpos($owner->{$attribute}, $postfix);
        	$original = substr($owner->{$attribute}, 0, $pos);
        	
        } else {
        	$original = $owner->{$attribute};
        }
        
        $i = 1;
        
        $criteria = new CDbCriteria();
        
        $criteria->condition = $attribute . ' = :x';
        $criteria->params[':x'] = $owner->getAttribute($attribute);
        
        if ( !$owner->isNewRecord ) {
            $pk = $owner->primaryKey;
            
            if ( !is_array($pk) )
                $pk = array($owner->metaData->tableSchema->primaryKey => $pk);
            
            foreach ($pk as $column => $value) {
                $criteria->addNotInCondition($column, array($value));
            }
        }
        
        while ( $owner->exists($criteria) ) {
            $owner->{$attribute} = $original . (++$i) . $postfix;
            $criteria->params = array(':x' => $owner->{$attribute});
        }
    }
    
    /**
     * Adds number to the end of file name stored id attribute until it is unique.
     */
	public function makeUniqueFile($attribute)
    {
    	$postfix = strstr($this->owner->{$attribute}, '.');
    	
        $this->makeUnique($attribute, $postfix);
    }
}

/**
 * @property MakeUniqueBehavior $makeUniqueBehavior
 * @method void makeUnique($attribute, $postfix = '') adds number to the end of attribute login till it is unique.
 * @method void makeUniqueFile($attribute) adds number to the end of attribute login till it is unique.
 */
