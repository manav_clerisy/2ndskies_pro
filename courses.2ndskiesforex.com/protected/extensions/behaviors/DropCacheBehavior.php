<?php

class DropCacheBehavior extends CActiveRecordBehavior
{
    public $tags = '';
    
    public function afterSave($event)
    {
        parent::afterSave($event);
        $this->dropCache();
    }
    
    public function afterDelete($event)
    {
        parent::afterDelete($event);
        $this->dropCache();
    }
    
    protected function dropCache()
    {
        $cache = Yii::app()->cache;
        
        foreach ($this->tagsArray() as $tag) {
            $cache->clear($this->tagName($tag));
        }
    }
    
    protected function tagsArray()
    {
        return array_filter(array_map('trim', explode(',', $this->tags)));
    }
    
    protected function tagName($tag)
    {
        $owner = $this->getOwner();
        return preg_replace_callback(
            '~\{(.*)\}~U',
            function ($match) use ($owner) {
                return $owner->{$match[1]};
            },
            $tag
        );
    }
}
