<?php

class GetResponseMailer extends CApplicationComponent
{
    # your API key is available at
    # https://app.getresponse.com/my_api_key.html
    public $api_key;

    public $campaign_name;
    public $campaignId;

    # API 2.x URL
    public $apiUrl = 'http://api2.getresponse.com';

    public function init()
    {
        # JSON::RPC module is required
        # available at http://github.com/GetResponse/DevZone/blob/master/API/lib/jsonRPCClient.php

        require_once 'jsonRPCClient.php';

        $this->campaignId = $this->getCampaignId();

    }

    public function sendMail($email,$subject,$message)
    {
        # initialize JSON-RPC client
        $client = new jsonRPCClient('http://api2.getresponse.com');

       // $CAMPAIGN_ID = $this->getCampaignId();

        # add contact to the campaign
        $result = $client->send_newsletter(
            $this->api_key,
            array (
                # identifier of 'test' campaign
                'campaign' => $this->campaignId,

               // "from_field"=>,
              //  "reply_to_field"=>,
                "subject"   => $subject,
                "name"      => "Say hi",
                "contents"  => array(
                    "plain" => "Hello there",
                    "html"  => "<h1>Hello</h1>there"
                ),
               /* "attachments" => array(
                    array(
                        "data" => "WmHFvMOzxYLEhyBnxJnFm2zEhSBqYcW6xYQu==",
                        "name" => "order.txt",
                        "mime" => "application/txt"
                    ),
                    array(
                        "data" => "QWxhLGtvdA==",
                        "name" => "people.csv",
                        "mime" => "application/csv"
                    ),
                ),
                "flags" => array("clicktrack", "openrate"),// [ "clicktrack", "openrate" ],
                "contacts"=>array( $CONTACT_ID ),*/
                "get_contacts" =>  array(
                    'email'=>array("EQUALS" => $email)
                ),
            /*  "segments" => array( "SEGMENT_ID", "SEGMENT_ID" ),
                "get_segments"      : { get_segments conditions },
                "suppressions"      : [ "SUPPRESSION_ID", "SUPPRESSION_ID" ],
                "get_suppressions"  : { get_suppressions conditions }*/
            )
        );
    }

    public function addContact($email,$name)
    {
        $client = new jsonRPCClient('http://api2.getresponse.com');

        $result = $client->add_contact(
            $this->api_key,
            array (
                'campaign' => $this->campaignId,
                'name'=> $name,
                'email'=>$email
            )
        );
    }

    public function getCampaignId()
    {
        $CAMPAIGN_ID = '';
        $client = new jsonRPCClient('http://api2.getresponse.com');

        if($client)
        {
            $result = $client->get_campaigns(
                $this->api_key,
                array (
                    # find by name literally
                    'name' => array ( 'EQUALS' => $this->campaign_name ),
                )
            );

            # because there can be only one campaign of this name
            # first key is the CAMPAIGN_ID required by next method
            # (this ID is constant and should be cached for future use)

            $campaigns = array_keys($result);
            $CAMPAIGN_ID = array_pop($campaigns);
        }

        return $CAMPAIGN_ID;
    }

    public function getContactIdByEmail($client, $email)
    {
        $CONTACT_ID = '';

        if($client)
        {
            $result = $client->get_contacts(
                $this->api_key,
                array(
                    'email'=>array("EQUALS" => $email)
                )
            );

            $contacts = array_keys($result);
            $CONTACT_ID = array_pop($contacts);
        }

        return $CONTACT_ID;
    }




}