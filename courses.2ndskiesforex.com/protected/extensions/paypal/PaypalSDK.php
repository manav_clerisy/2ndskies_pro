<?php

use yii\PayPal\Form\CreditCard;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\PayerInfo;
use PayPal\Api\Payment;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;


define('PP_CONFIG_PATH',(($_SERVER['REMOTE_ADDR'] === '127.0.0.1') ||
    ($_SERVER['REMOTE_ADDR'] === '10.10.100.3') ||
    ($_SERVER['REMOTE_ADDR'] === '::1')) ? dirname(__FILE__).DIRECTORY_SEPARATOR.'config_local' : dirname(__FILE__));

class PaypalSDK extends CApplicationComponent
{

    # Paypal user credentials #
    /**
     * your clientId and client secret from Pay Pal account
     */

    public $clientId;
    public $clientSecret;

    /**
     * @param $creditCard CreditCard
     * @param $name
     * @param $price
     */

    # Paypal default settings #

    /**
     * Default currency code
     * @var string
     */
    public $currency = 'USD';

    /**
     * Default quantity
     * @var int
     */
    public $quantity = 1;


    public function createCreditCardModel()
    {
        file_put_contents(Yii::getPathOfAlias('ext').'/../runtime/config_sdk.txt',' Config path = '.PP_CONFIG_PATH.' $clientId = '.$this->clientId.' $clientSecret = '.$this->clientSecret);
        return new CreditCard;
    }
    # FundingInstrument #

    /* A resource representing a Payer's funding instrument.
    * For direct credit card payments, set the CreditCard
    * field on this object.
    */

    public function setFundingInstrument($creditCard)
    {
        $fi = new FundingInstrument();
        $fi->setCreditCard($creditCard);

        return $fi;
    }


    # Payer #

    /* A resource representing a Payer that funds a payment
    * For direct credit card payments, set payment method
    * to 'credit_card' and add an array of funding instruments.
    */

    public function setPayer($fi, $payerInfo = null, $paymentMethod = "credit_card" )
    {
        $payer = new Payer();

        $payer->setPaymentMethod($paymentMethod)
            ->setFundingInstruments(array($fi))
            ->setPayerInfo($payerInfo);

        return $payer;
    }


    # Itemized information #

    /* (Optional) Lets you specify item wise
    * information
    */

    public function setItem($name = 'Payment', $price, $sku = null)
    {
        $item = new Item();
        $item->setName($name)
            ->setCurrency($this->currency)
            ->setQuantity($this->quantity)
            ->setPrice($price)
            ->setSku($sku);

        return $item;
    }

    public function setItemList($items)
    {
        $itemList = new ItemList();
        $itemList->setItems(array($items));

        return $itemList;
    }

    # Amount #

    /* Lets you specify a payment amount.
    * You can also specify additional details
    * such as shipping, tax.
    */

    public function setAmount($price, $details = null)
    {
        $amount = new Amount();
        $amount->setCurrency($this->currency)
            ->setTotal($price)
            ->setDetails($details);

        return $amount;
    }

    # Transaction #

    /* A transaction defines the contract of a
    * payment - what is the payment for and who
    * is fulfilling it.
    */

    public function setTransaction($amount, $itemList, $payee = null, $description= null, $relatedResources = null, $transactions = null)
    {
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setPayee($payee)
            ->setDescription($description)
            ->setItemList($itemList)
            ->setRelatedResources($relatedResources)
            ->setTransactions($transactions);

        return $transaction;
    }


    # Payment #

    /* A Payment Resource; create one using
    * the above types and intent set to sale 'sale'
    */

    public function setPayment($payer, $transaction, $intent = "sale", $state = null, $urls = null, $links = null )
    {
        $payment = new Payment();
        $payment->setIntent($intent)
            ->setPayer($payer)
            ->setTransactions(array($transaction))
            ->setState($state)
            ->setRedirectUrls($urls)
            ->setLinks($links);

        return $payment;
    }


    /*
     * set Api context using user credentials
     */

    public function setApiContext()
    {
        return new ApiContext(new OAuthTokenCredential($this->clientId, $this->clientSecret));
    }


    public function runPayment($creditCard, $name, $price, $userEmail)
    {
        $fi = $this->setFundingInstrument($creditCard);
        $payer = $this->setPayer($fi,array('email'=>$userEmail));

        $item = $this->setItem($name, $price);
        $itemList = $this->setItemList($item);

        $amount = $this->setAmount($price);
        $transaction = $this->setTransaction($amount,$itemList);

        $payment = $this->setPayment($payer,$transaction);
//die('<pre>'.var_export($payment, true)."</pre>\n");
        $apiContext = $this->setApiContext();

//        try {
            $res = $res = $payment->create($apiContext);//die(var_dump($res));
  //      } catch (PayPal\Exception\PPConnectionException $ex) {
    //        die(var_dump($ex->getData()));
        //    throw new Exception($ex->getData());
      //  }

        return $res;
    }


}
