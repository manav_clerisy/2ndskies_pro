<?php
class CrawlCommand extends ConsoleCommand
{
    public  function actionIndex()
    {
        echo 'Date: '.date('Y-m-d H:i',time()).'. Start page 1. ';
        $scraper = new ScraperDailyfx(array('url'=>'http://www.dailyfx.com/calendar'));
        $scraper->setStartDate('last Sunday');
        $scraper->cleanData();
        $scraper->doParse();
        $nextWeekUrl = $scraper->getNextWeekUrl();
        echo 'Done.'."\n".'Start page 2.';
        $scraper = new ScraperDailyfx(array('url'=>'http://www.dailyfx.com/calendar?week='.$nextWeekUrl));
        $scraper->doParse();
        echo 'Done.';
    }

}