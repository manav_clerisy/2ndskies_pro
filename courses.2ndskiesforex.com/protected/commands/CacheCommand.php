<?php

class CacheCommand extends CConsoleCommand
{
    public function run($args)
    {
        $this->actionFlush();
    }
    
    public function actionFlush()
    {
        Yii::app()->cache->flush();
    }
}
