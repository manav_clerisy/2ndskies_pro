<?php

class CleanupCommand extends CConsoleCommand
{
    public function actionIndex()
    {
        $users = User::model();
        $comments = Comment::model();
        $topics = Topic::model();
        $messages = Messages::model();
        
        // Cleanup all comments, topics and messages without users
        $userIds = Yii::app()->db->createCommand()
            ->select('id')
            ->from($users->tableName())
            ->queryColumn();
        $ids = implode(',', $userIds);
        
        $out = $comments->deleteAll('user_id NOT IN(' . $ids . ')');
        echo "Deleted $out comments without users.\n";
        
        $out = $topics->deleteAll('user_id NOT IN(' . $ids . ')');
        echo "Deleted $out topics without users.\n";
        
        $out = $messages->deleteAll('user_from NOT IN(' . $ids . ') OR user_to NOT IN(' . $ids . ')');
        echo "Deleted $out messages without users.\n";
        
        // Cleanup all comments without topics
        $topicIds = Yii::app()->db->createCommand()
            ->select('id')
            ->from($topics->tableName())
            ->queryColumn();
        $ids = implode(',', $topicIds);
        
        $out = $comments->deleteAll('topic_id NOT IN(' . $ids . ')');
        echo "Deleted $out comments without topics.\n";
    }
}
