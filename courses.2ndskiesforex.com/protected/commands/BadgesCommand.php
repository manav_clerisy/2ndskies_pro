<?php

class BadgesCommand extends CConsoleCommand
{
    public function actionIndex()
    {
        $updateIds = array();
        $insertIds = array();

        $badgesItem = Yii::app()->db->createCommand()
            ->select('id, minimum_posts')
            ->from(Badges::model()->tableName())
            ->where('minimum_posts > 0')
            ->order('minimum_posts DESC')
            ->queryAll();

        foreach (User::model()->findAll() as $user) {

            $apartments = Yii::app()->db->createCommand()
                ->select("count(*) as num")
                ->from(Topic::model()->tableName())
                ->where("user_id=:user_id AND user_id IS NOT NULL", array(
                    ':user_id' => $user->id
                ))
                ->getText();

            $posts = Yii::app()->db->createCommand()
                ->select('count(*) as num')
                ->from(Comment::model()->tableName())
                ->where('user_id=:user_id', array(
                    ':user_id' => $user->id
                ))
                ->union($apartments)
                ->queryAll();

            $countPosts = 0;
            foreach ($posts as $item)
                $countPosts += $item['num'];


            if ($countPosts == 0)
                continue;

            $currentBadgeId = null;
            foreach ($badgesItem as $badge) {
                if ($countPosts >= $badge['minimum_posts']) {
                    $currentBadgeId = $badge['id'];
                    break;
                }
            }

            $userBadge = Yii::app()->db->createCommand()
                ->select('badge_id')
                ->from(UserBadge::model()->tableName())
                ->where("user_id={$user->id} AND set_manually = 0")
                ->queryRow();

            if ($userBadge && $userBadge['badge_id'] != $currentBadgeId) {
                $updateIds[$currentBadgeId][] = $user->id;
            } elseif (!$userBadge) {
                $insertIds[] = array(
                    'badge_id' => $currentBadgeId,
                    'user_id' => $user->id,
                    'set_manually' => 0
                );
            }
        }

        foreach ($updateIds as $badgeId => $userIds) {
            Yii::app()->db->createCommand()->update(
                UserBadge::model()->tableName(),
                array(
                    'badge_id' => $badgeId
                ),
                'user_id in ('.implode(',', $userIds).') and set_manually = 0'
            );
            echo count($userIds)." users updated to badge with id - {$badgeId}\n";
        }

        if (!empty($insertIds)) {
            foreach ($insertIds as $data) {
                Yii::app()->db->createCommand()->insert(
                    UserBadge::model()->tableName(),
                    $data
                );
            }
            echo count($insertIds) . " users added badges\n";
        }
    }
}
