<?php

class DailyNotificationCommand extends CConsoleCommand
{
    public function actionIndex()
    {
        $emailSubject = '2ndSkies topic subscription updates';
        $commentsArr = array();

        $users = UserNotifications::model()->findAllByAttributes(array('notify' => 2));

        foreach($users as $user)
        {
            $allUserSubscriptions = Subscriptions::model()->findAllByAttributes(array('user_id'=>$user->id));

            foreach($allUserSubscriptions as $courseUserSubscription)
            {

                $subscribedTopics = explode(',',$courseUserSubscription->topic_id_arr);

                foreach($subscribedTopics as $subscribedTopic)
                {
                    if(!empty($subscribedTopic))
                    {
                        $itemArr = explode('=>',$subscribedTopic);

                        $topic = Topic::model()->findByPk($itemArr[0]);

                        $lastCommentId = $topic->last_comment_id;

                        $lastComment = Comment::model()->findByPk($lastCommentId);

                        if($lastComment->date_created > date('Y-m-d',strtotime('today')).' 00:00:00')
                        {
                            $topicAuthor = User::model()->findByAttributes(array('id'=>$topic->user_id));
                            $lastCommentAuthor = User::model()->findByAttributes(array('id'=>$lastComment->user_id));

                            $commentsArr[]= '"'.$topic->subject.'" by '.$topicAuthor->username.', the latest comment was by '.$lastCommentAuthor->username.' - view the thread here: <a href="'.Yii::app()->createUrl('topic/view',array('id'=>$topic->id)).'">'.$topic->subject.'</a> <br>';
                        }
                    }
                }

            }

            if(!empty($commentsArr))
            {
                $userModel = User::model()->findByPk($user->user_id);

                Apostle::setup(Yii::app()->params->apostleKey);

                $mail = new Apostle\Mail('daily-notification');

                $mail->from = Yii::app()->params->adminEmail;
                $mail->email = $user->user->email;
                $mail->__set('name', $userModel->username);
                $mail->__set('comments', $commentsArr);

                return $mail->deliver();
            }

        }
    }
}