<?php

class ImportCommand extends ConsoleCommand
{
    /**
     * @var CDBConnection
     */
    protected $source;
    
    protected $prefix;
    protected $baseUrl;
    protected $courseId;
    
    /**
     * @var HttpClient
     */
    protected $http;
    
    public function actionIndex($from = 't2forum')
    {
        $db = Yii::app()->db;
        $this->source = Yii::createComponent(array(
            'class' => 'CDbConnection',
            'connectionString' => preg_replace('~dbname=([0-9a-zA-Z_\.]+)~', 'dbname=' . $from, $db->connectionString),
			'emulatePrepare' => true,
			'username' => $db->username,
			'password' => $db->password,
			'charset' => 'utf8',
		));
        
        $this->http = Yii::app()->httpClient;
        
        $this->prefix = 'phpbb_Price_';
        $this->baseUrl = 'http://2ndskiesforex.com/price-course/';
        $this->courseId = 1;
        $this->importCourse();
        
        $this->prefix = 'phpbb_Ichi';
        $this->baseUrl = 'http://2ndskiesforex.com/ichimoku-course/';
        $this->courseId = 2;
        $this->importCourse();
        
        $this->prefix = 'phpbb_Pro_';
        $this->baseUrl = 'http://2ndskiesforex.com/pro-course/';
        $this->courseId = 3;
        $this->importCourse();
    }
    
    public function actionCleanup()
    {
        $query = <<<SQL
DELETE FROM `users` WHERE id > 1;
TRUNCATE `sections`;
TRUNCATE `topics`;
TRUNCATE `comments`;
TRUNCATE `user_badge`;
SQL;
        Yii::app()->db->createCommand($query)->execute();
    }
    
    protected function importCourse()
    {
        echo "Importing {$this->baseUrl}...\n";
        
        // Login
        $this->http->setRandomCookieFile();
        $this->http->post($this->baseUrl . 'ucp.php?mode=login', array(
            'username' => 'Chris Capre',
            'password' => 'karmadakini1',
            'login' => 'Login',
            'redirect' => './index.php?',
        ));
        
        
        $usersMap = $this->importUsers();
        //echo '<pre>' . var_export($usersMap, true) . "</pre>\n";
        
        $sectionsMap = $this->importSections();
        //echo '<pre>' . var_export($sectionsMap, true) . "</pre>\n";
        
        $topicsMap = $this->importTopics($usersMap, $sectionsMap);
        
        $this->importComments($usersMap, $topicsMap);

        $this->deleteSectionsForAnouncementTopics();

        $this->renameSections();

    }
    
    /**
     * Imports phpBB users
     * @return array map between phpBB user ids and yii user ids
     * @throws CException
     */
    protected function importUsers()
    {
        // Email => Id map
        $users = $this->renderMap('User', 'email', 'id');
        
        $userCourses = $this->renderMap('UserCourse', 'user_id', 'id', 'course_id = ' . $this->courseId);
        
        $reader = $this->source->createCommand()
            ->select()
            ->from($this->prefix . 'users')
            ->query();
        
        echo "Got " . $reader->count() . " users\n";
        $i = 0;
        
        $map = array();
        foreach ($reader as $row) {
            if (empty($row['user_email']))
                continue;
            
            $created = $this->date($row['user_regdate']);
            
            $id = @$users[$row['user_email']];
            
            if ($id === null) {
                // Create new user
                
                $user = new User();
                $user->setAttributes(array(
                    'username' => $row['username'],
                    'password' => $row['user_password'],
                    'email'    => $row['user_email'],
                    'activkey' => $row['user_actkey'],
                    'status' => 1,
                    'create_at'    => $created,
                    'lastvisit_at' => $this->date($row['user_lastvisit']),
                ), false);
                
                try {
                    $user->save(false);

                    $ub = new UserBadge();
                    $ub->user_id  = $user->id;
                    $ub->badge_id = Badges::model()->findByAttributes(array('minimum_posts' => 0))->getAttribute('id');
                    $ub->set_manually = 0;
                    $ub->save(false);
                    
                } catch (CDbException $exc) {
                    if (preg_match("~Duplicate entry '.*' for key ~", $exc->getMessage())) {
                        $user->makeUnique('username');
                        $user->save(false);
                        
                    } else {
                        throw $exc;
                    }
                }
                
                $profile = new Profile();
                $profile->user_id = $user->id;
                //$profile->icq = $row['user_icq'];
                //$profile->aol_instant_messanger = $row['user_aim'];
                //$profile->wl_msn_messenger = $row['user_msnm'];
                //$profile->jabber_address = $row['user_jabber'];
                $profile->website = $row['user_website'];
                $profile->location = $row['user_from'];
                $profile->occupation = $row['user_occ'];
                $profile->interests = $row['user_interests'];
                $profile->birthday = $this->date($row['user_birthday']);
                $profile->signature = $this->text($row['user_sig'], $row['user_sig_bbcode_uid']);
                if (!empty($row['user_avatar'])) {
                    $url = $this->baseUrl . 'download/file.php?avatar=' . $row['user_avatar'];
                    
                    $profile->setImageName($url);
                    
                    $fullPath = $profile->getImagePath(true);
                    $thumbPath = $profile->getImagePath(false);
                    
                    $this->download('avatar', $row['user_avatar'], $fullPath);
                    copy($fullPath, $thumbPath);
                }
                
                $profile->save(false);
                
                $id = $user->id;
            }
            
            $map[$row['user_id']] = $id;
            
            // Add user-course connection
            if (!isset($userCourses[$id])) {
                $subscription = new UserCourse();
                $subscription->user_id = $id;
                $subscription->course_id = $this->courseId;
                $subscription->created = $created;
                $subscription->save(false);
            }
            
            if (++$i == 100) {
                echo '.';
                $i = 0;
            }
        }
        
        echo "\n";
        
        return $map;
    }
    
    protected function importSections()
    {
        // Section name => Id map
        $sections = $this->renderMap('Sections', 'name', 'id', 'course_id = ' . $this->courseId);
        
        $rows = $this->source->createCommand()
            ->select()
            ->from($this->prefix . 'forums')
            ->order('parent_id, left_id')
            ->queryAll();
        
        $map = array();
        
        while (!empty($rows)) {
            $nextRows = array();
            $lastParentId = 0;
            $lastOrder = 1;
            
            foreach ($rows as $row) {
                $id = @$sections[$row['forum_name']];

                if ($id === null) {
                    $parentId = $row['parent_id'] == 0 ? 0 : @$map[$row['parent_id']];

                    if ($parentId === null) {
                        // If parent is not added yet, let's add this section later
                        $nextRows[] = $row;
                        continue;
                    }
                    
                    if ($parentId != $lastParentId) {
                        $lastParentId = $parentId;
                        $lastOrder = 1;
                    }

                    $section = new Sections();
                    $section->name = $row['forum_name'];
                    $section->parent_id = $parentId;
                    $section->order = $lastOrder++;
                    $section->course_id = $this->courseId;
                    $section->save(false);
                    
                    $id = $section->id;
                }

                $map[$row['forum_id']] = $id;
            }
            
            $rows = $nextRows;
        }
        
        return $map;
    }
    
    protected function importTopics($usersMap, $sectionsMap)
    {
        $topics = $this->renderMap('Topic', 'CONCAT(section_id, "-", user_id, "-", subject)', 'id', 'section_id IN(' . implode(', ', $sectionsMap) . ')');
        
        $reader = $this->source->createCommand()
            ->select()
            ->from($this->prefix . 'topics')
            ->query();
        
        echo "Got " . $reader->count() . " topics\n";
        $i = 0;
        
        $map = array();
        foreach ($reader as $row) {
            $sectionId = $sectionsMap[$row['forum_id']];
            $userId = $usersMap[$row['topic_poster']];
            $key = $sectionId . '-' . $userId . '-' . $row['topic_title'];
            
            $id = @$topics[$key];

            if ($id === null) {
                $topic = new Topic();
                $topic->section_id = $sectionId;
                $topic->user_id = $userId;
                $topic->subject = $row['topic_title'];
                $topic->date_created = $this->date($row['topic_time']);
                $topic->date_changed = $this->date($row['topic_time']);
                $topic->status = 3 - $row['topic_type'];
                $topic->save(false);

                $id = $topic->id;
            }

            $map[$row['topic_id']] = $id;
            
            if (++$i == 100) {
                echo '.';
                $i = 0;
            }
        }
        
        echo "\n";
        
        return $map;
    }
    
    protected function importComments($usersMap, $topicsMap)
    {
        // Attachments
        $reader = $this->source->createCommand()
            ->select()
            ->from($this->prefix . 'attachments')
            ->query();
        
        $attachments = array();
        foreach ($reader as $row) {
            $attachments[$row['post_msg_id']][] = $row;
        }
        
        
        $uploadsPath = Yii::getPathOfAlias('web.uploads.files') . '/';
        $uploadsUrl = '/uploads/files/';
        
        // Topics with comments ids
        $filledTopics = Comment::model()->dbConnection->createCommand()
            ->selectDistinct('topic_id')
            ->from(Comment::model()->tableName())
            ->queryColumn();
        $filledTopics = array_fill_keys($filledTopics, true);
        
        // Posts
        $comments = $this->renderMap('Comment', 'CONCAT(topic_id, "-", user_id, "-", date_created)', 'id', 'topic_id IN(' . implode(', ', $topicsMap) . ')');
        
        $reader = $this->source->createCommand()
            ->select()
            ->from($this->prefix . 'posts')
            ->order('post_time')
            ->query();
        
        echo "Got " . $reader->count() . " posts\n";
        $i = 0;
        
        foreach ($reader as $row) {
            $topicId = $topicsMap[$row['topic_id']];
            $userId  = $usersMap[$row['poster_id']];
            $created = $this->date($row['post_time']);
            
            $key = $topicId . '-' . $userId . '-' . $created;
            
            if (!isset($comments[$key])) {
                
                // Text
                $text = $this->text($row['post_text'], $row['bbcode_uid']);
                
                if (isset($attachments[$row['post_id']])) {
                    $text .= '<br/><strong>Attachments:</strong>';
                    
                    foreach ($attachments[$row['post_id']] as $attachment) {
                        $name = $attachment['real_filename'];

                        $this->download('id', $attachment['attach_id'], $uploadsPath . $name);

                        $url = $uploadsUrl . $name;
                        if (strncmp($attachment['mimetype'], 'image/', 6) == 0) {
                            $text .= '<br/>' . CHtml::image($url, $name);
                        } else {
                            $text .= '<br/>' . CHtml::link($name, $url);
                        }
                    }
                }
                
                // Filling model
                $comment = new Comment();
                $comment->topic_id = $topicId;
                $comment->user_id = $userId;
                $comment->text = $text;
                $comment->date_created = $created;
                $comment->date_changed = $created;
                $comment->is_first = !isset($filledTopics[$topicId]);
                $comment->save(false);
                
                $filledTopics[$topicId] = true;
            }
            
            if (++$i == 100) {
                echo '.';
                $i = 0;
            }
        }
        
        echo "\n";
    }
    
    protected function renderMap($class, $key, $value, $condition = null)
    {
        $model = CActiveRecord::model($class);
        $command = $model->dbConnection->createCommand()
            ->select(array($key, $value))
            ->from($model->tableName());
        if ($condition !== null)
            $command->where($condition);
        $reader = $command
            ->query();
        
        $reader->fetchMode = PDO::FETCH_NUM;
        
        $map = array();
        foreach ($reader as $row) {
            $map[$row[0]] = $row[1];
        }
        
        return $map;
    }

    protected function download($name, $value, $dst)
    {
        if (file_exists($dst))
            return; // File already downloaded
        
        $sourceUrl = $this->baseUrl . 'download/file.php?' . $name . '=' . $value;
        $this->http->download($sourceUrl, $dst);
    }
    
    protected function date($time)
    {
        return is_numeric($time) ? date('Y-m-d H:i:s', $time) : '0000-00-00';
    }
    
    protected function text($text, $bbcode_uid = '')
    {
        $text = BBCode::toHtml($text, $bbcode_uid);
        $text = str_replace('{SMILIES_PATH}', '/images/phpbb-smilies', $text);
        return $text;
    }


    protected function renameSections()
    {
        $namesOld = array('Trade Signals and Setups','Course Lessons & Tutorials' ,'Trading Psychology','Money Management','News, Randoms, Announcements and Just Plain Entertainment','News, Random Announcements and Just Plain Entertainment');
        $namesNew = array('Trade Setups','Course Videos & Lessons', 'Your Trading Mindset','Money & Risk Management','Odds & Ends','Odds & Ends');

        for($i = 0; $i<count($namesOld); $i++)
        {
            $sections = Sections::model()->findAllByAttributes(array('name'=>$namesOld[$i]));
            foreach($sections as $section)
            {
                $section->name = $namesNew[$i];
                $section->save(false);
            }
        }

    }

    protected function deleteSectionsForAnouncementTopics()
    {

        $reader = Yii::app()->db->createCommand()
            ->select('id, section_id')
            ->from('topics')
            ->where('status = 1')
            ->query();

        $reader->setFetchMode(PDO::FETCH_NUM);

        foreach ($reader as $row) {
            list($id, $sectionId) = $row;
            if (isset($sectionId)) {

                $criteria = new CDbCriteria();
                $criteria->addCondition("section_id = $sectionId");
                $countTopicsForSection = Topic::model()->count($criteria);

                if ($countTopicsForSection == 1) {
                    //echo(var_export($countTopicsForSection) . ' -> ');
                    $parentSectionId = Yii::app()->db->createCommand()
                        ->select('parent_id')
                        ->from('sections')
                        ->where('id = :id', array(':id' => $sectionId))
                        ->queryRow();
                    $parentId = $parentSectionId['parent_id'];
                    //echo(var_export($sectionId) . "\n");

                    Yii::app()->db->createCommand()->update(
                        'topics',
                        array('section_id' => $parentId),
                        'id = ' . $id);

                    Yii::app()->db->createCommand()->delete(
                        'sections',
                        'id = ' . $sectionId
                    );
                }
            }
        }
    }



}
