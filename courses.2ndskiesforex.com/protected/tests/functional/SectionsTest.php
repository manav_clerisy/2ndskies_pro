<?php

class SectionsTest extends WebTestCase
{
    public $captureScreenshotOnFailure = FALSE;

    public function testSectionTree()
    {
        $sections =  Yii::app()->db->createCommand()
                        ->select('id, parent_id, course_id')
                        ->from('sections')
                        ->queryAll();

        // formatted section array
        // key = section id
        $nodesArr = array();
        foreach ($sections as $section) {
            $nodesArr[$section['id']] = array(
                'parent_id' => $section['parent_id'],
                'course_id' => $section['course_id'],
            );
        }

        $errors = array();
        foreach ($nodesArr as $key => $node) {

            if ($node['parent_id'] != 0) {

                $parentNode = $nodesArr[$node['parent_id']];

                if ($parentNode['course_id'] != $node['course_id'])
                    $errors[] = 'parentId => '.$node['parent_id'].' childrenId => '.$key;
            }
        }

        if (count($errors) > 0) {
            $msg = implode("\n", $errors);
            $msg = "Next pairs are assigned to different course:\n".$msg;
            $this->fail($msg);
        }

    }
    
}
