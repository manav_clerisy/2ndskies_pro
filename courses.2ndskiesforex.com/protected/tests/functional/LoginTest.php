<?php

class LoginTest extends WebTestCase
{
    public function testChangePassword()
    {
        $url = 'user/profile/changepassword';
        
        $this->open($url);
        $this->assertUrl($this->urls['login']);
        
        $this->login();
        
        $this->open($url);
        $this->assertUrl($url);
    }
    
    public function testUserIsReturnedAfterLogin()
    {
        $url = 'user/profile/changepassword';
        
        $this->open($url);
        $this->assertUrl($this->urls['login']);
        
        $this->login();
        
        $this->assertUrl($url);
    }
}
