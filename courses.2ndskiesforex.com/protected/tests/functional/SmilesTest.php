<?php

class SmilesTest extends WebTestCase
{
    public $captureScreenshotOnFailure = FALSE;

    public function testSmilesInMessages()
    {
        $this->login();

        $this->clickAndWait('link=Messages');

        $xpath = '//strong[@class="user-name"]/a';

        // checks if the list of friends isn't empty
        if (! $this->isElementPresent("xpath=".$xpath))
            $this->markTestSkipped('User doesn\'t have any friend he chatted with');

        $href = $this->getAttribute($xpath.'/@href'); // first friend

        $parts = parse_url($href);
        $userId = explode('=',$parts['query']);

        $this->clickAndWait($xpath);

        // redactor button
        $this->click('//a[@class="redactor_btn redactor_btn_smile"]');

        // smile button
        $this->click("//*[@id='redactor_toolbar_0']/div[1]/a[1]");

        // submit button
        $this->clickAndWait("//form[@id='horizontalForm']//button[@type='submit']");

        // remove comment that was just created
        $id =  Yii::app()->db->createCommand()
                ->select('id')
                ->from('messages')
                ->where('user_to=:USER_ID', array(':USER_ID' => $userId[1]))
                ->order('created DESC')
                ->limit('1')
                ->queryScalar();

        Yii::app()->db->createCommand()
            ->delete('messages', 'id=:id', array(':id'=>$id));
    }
    
}
