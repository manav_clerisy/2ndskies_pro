<?php

class ProfileTest extends CTestCase
{
    public function testSetImageName()
    {
        $profile = new Profile();
        $profile->user_id = 123;
        
        $source = 'http://2ndskiesforex.com/pro-course/download/file.php?avatar=127_1350650758.gif';
        $profile->setImageName($source);
        $this->assertSame('123.gif', $profile->image, 'Parsing ' . $source);
        
        $source = 'http://2ndskiesforex.com/pro-course/127_1350650758.gif';
        $profile->setImageName($source);
        $this->assertSame('123.gif', $profile->image, 'Parsing ' . $source);
        
        $source = '/pro-course/127_1350650758.gif';
        $profile->setImageName($source);
        $this->assertSame('123.gif', $profile->image, 'Parsing ' . $source);
        
        $source = 'http://2ndskiesforex.com/pro-course/download/file.php?avatar=app.com.png';
        $profile->setImageName($source);
        $this->assertSame('123.png', $profile->image, 'Parsing ' . $source);
        
        $source = 'http://2ndskiesforex.com/pro-course/app.com.png';
        $profile->setImageName($source);
        $this->assertSame('123.png', $profile->image, 'Parsing ' . $source);
        
        $source = '/pro-course/app.com.png';
        $profile->setImageName($source);
        $this->assertSame('123.png', $profile->image, 'Parsing ' . $source);
    }
}
