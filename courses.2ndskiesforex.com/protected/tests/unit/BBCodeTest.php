<?php

class BBCodeTest extends CTestCase
{
    public function testSize()
    {
        $source = '[size=150]Risk of Ruin Formula using 10% Risk / Trade[/size]';
        $result = '<span style="font-size: 150%;">Risk of Ruin Formula using 10% Risk / Trade</span>';
        $this->assertSame($result, BBCode::toHtml($source));
    }

    public function testTable()
    {
        $source = <<<BB
[table=800,1][tr][td=100,white]Win Ratio %[/td][td=100,white]Payoff Ratio 1:1[/td][td=100,white]PR 2:1[/td][td=100,white]PR 3:1[/td][td=100,white]PR 4:1[/td][td=100,white]PR 5:1[/td][/tr][tr][td=100,white]Win Ratio 10%[/td][td=100,white]100[/td][td=100,white]100[/td][td=100,white]100[/td][td=100,white]100[/td][td=100,white]100[/td][/tr][/table]
BB;
        $result = <<<HTML
<table valign="top" width="800" border="1" cellpadding="1" cellspacing="0"><tr><td width="100" bgcolor="white">Win Ratio %</td><td width="100" bgcolor="white">Payoff Ratio 1:1</td><td width="100" bgcolor="white">PR 2:1</td><td width="100" bgcolor="white">PR 3:1</td><td width="100" bgcolor="white">PR 4:1</td><td width="100" bgcolor="white">PR 5:1</td></tr><tr><td width="100" bgcolor="white">Win Ratio 10%</td><td width="100" bgcolor="white">100</td><td width="100" bgcolor="white">100</td><td width="100" bgcolor="white">100</td><td width="100" bgcolor="white">100</td><td width="100" bgcolor="white">100</td></tr></table>
HTML;

        $this->assertSame($result, BBCode::toHtml($source));
    }

    public function testQuote()
    {
        $source = <<<BB
[quote="Chris Capre"]So we meet again[/quote]
BB;
        $result = <<<HTML
<blockquote><em><small><cite>Chris Capre wrote:</cite></small>So we meet again</em></blockquote>
HTML;
        $this->assertSame($result, BBCode::toHtml($source));
        
        $source = <<<BB
[quote=&quot;Chris Capre&quot;]So we meet again[/quote]
BB;
        $this->assertSame($result, BBCode::toHtml($source));
        
        $source = <<<BB
[quote="slimeyflux"]Thanks chris look forward to learnig from you,
will need to be disciplined in regards to my journal ect :| am just playing with an alpari demo account at moment
thanks Dave[/quote]
BB;
        $result = <<<HTML
<blockquote><em><small><cite>slimeyflux wrote:</cite></small>Thanks chris look forward to learnig from you,<br />will need to be disciplined in regards to my journal ect :| am just playing with an alpari demo account at moment<br />thanks Dave</em></blockquote>
HTML;
        $this->assertSame($result, BBCode::toHtml($source));
    }

    public function testNestedQuotes()
    {
        $source = <<<BB
[quote="Chris Capre"][quote="kmotte"]first[/quote]second[/quote]
BB;
        $result = <<<HTML
<blockquote><em><small><cite>Chris Capre wrote:</cite></small><blockquote><em><small><cite>kmotte wrote:</cite></small>first</em></blockquote>second</em></blockquote>
HTML;
        $this->assertSame($result, BBCode::toHtml($source));
        
        $source = <<<BB
[quote=&quot;Chris Capre&quot;][quote=&quot;kmotte&quot;]first[/quote]second[/quote]
BB;
        $this->assertSame($result, BBCode::toHtml($source));
    }

    public function testSprout()
    {
        $source = <<<BB
[sprout=a09bd8b41c1ce5c628]6a152d0bcca56e61[/sprout]
BB;
        $result = <<<HTML
<iframe class="sproutvideo-player" type="text/html" src="http://videos.sproutvideo.com/embed/a09bd8b41c1ce5c628/6a152d0bcca56e61?type=hd" width="640" height="480" frameborder="0"></iframe>
HTML;
        $this->assertSame($result, BBCode::toHtml($source));
    }
    
    public function testUrlImg()
    {
        $source = <<<BB
[url=http://postimage.org/image/j9iszsak9/][img]http://s12.postimage.org/j9iszsak9/usdjpyh4.jpg[/img][/url]
BB;
        $result = <<<HTML
<a href="http://postimage.org/image/j9iszsak9/"><img src="http://s12.postimage.org/j9iszsak9/usdjpyh4.jpg" alt="" /></a>
HTML;
        $this->assertSame($result, BBCode::toHtml($source));
        
        $source = <<<BB
[url=http&#58;//postimage&#46;org/image/j9iszsak9/:1mlr0h1t][img:1mlr0h1t]http&#58;//s12&#46;postimage&#46;org/j9iszsak9/usdjpyh4&#46;jpg[/img:1mlr0h1t][/url:1mlr0h1t]
BB;
        $this->assertSame($result, BBCode::toHtml($source, '1mlr0h1t'));
    }

    public function testAmp()
    {
        $source = <<<BB
Course Lessons &amp; Tutorials
BB;
        $result = <<<HTML
Course Lessons & Tutorials
HTML;
        $this->assertSame($result, BBCode::toHtml($source));
    }
}
