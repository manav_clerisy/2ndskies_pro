<?php

class DropCacheTest extends CTestCase
{
    /**
     * @var Comment
     */
    protected $model;
    
    /**
     * @var DropCacheBehavior
     */
    protected $behavior;
    
    public function setUp()
    {
        $this->behavior = Yii::createComponent('ext.behaviors.DropCacheBehavior');
        $this->model = new Comment();
        
        $this->behavior->attach($this->model);
    }
    
    public function testTagsArray()
    {
        $method = $this->getMethod('tagsArray');
        
        $this->behavior->tags = '';
        $result = array();
        $this->assertEquals($result, $method->invoke($this->behavior));
        
        $this->behavior->tags = 'user-stats-{user_id}';
        $result = array('user-stats-{user_id}');
        $this->assertEquals($result, $method->invoke($this->behavior));
        
        $this->behavior->tags = 'user-stats-{user_id},user-{user_id}';
        $result = array('user-stats-{user_id}', 'user-{user_id}');
        $this->assertEquals($result, $method->invoke($this->behavior));
        
        $this->behavior->tags = 'user-stats-{user_id}, user-{user_id}';
        $result = array('user-stats-{user_id}', 'user-{user_id}');
        $this->assertEquals($result, $method->invoke($this->behavior));
    }
    
    public function testTagName()
    {
        $method = $this->getMethod('tagName');
        
        $tag = 'user-stats';
        $result = 'user-stats';
        $this->assertEquals($result, $method->invokeArgs($this->behavior, array($tag)));
        
        $tag = 'user-stats-{user_id}';
        $this->model->user_id = 5;
        $result = 'user-stats-5';
        $this->assertEquals($result, $method->invokeArgs($this->behavior, array($tag)));
    }
    
    /**
     * 
     * @param string $name
     * @return ReflectionClass
     */
    protected function getMethod($name) {
        $class = new ReflectionClass('DropCacheBehavior');
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method;
    }
}
