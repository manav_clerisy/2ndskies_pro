<?php

class CacheTest extends CTestCase
{
    public function testId()
    {
        $this->assertSame('hello-1', Cache::id('hello', 1));
        $this->assertSame('hello--1', Cache::id('hello', false, true));
    }
}
