<?php

/**
 * The base class for functional test cases.
 * In this class, we set the base URL for the test application.
 * We also provide some common methods to be used by concrete test classes.
 */
class WebTestCase extends CWebTestCase
{
	protected $baseUrl;

    protected $captureScreenshotOnFailure = TRUE;

    protected $screenshotPath = '';

    protected $screenshotUrl = '';

    private $defaultTimeoutWaitForPageToLoad = 60000;
    
    protected $urls = array(
        'login' => 'user/login',
    );

    public function waitForPageToLoad($timeout = null)
    {
        if (empty($timeout))
            $timeout = $this->defaultTimeoutWaitForPageToLoad;
        parent::waitForPageToLoad($timeout);
    }

    protected function setUp()
	{
		parent::setUp();

        global $yiiConfig;
        $yiiConfig = include dirname(__FILE__) . '/../config/main.php';

        $this->baseUrl = $yiiConfig['components']['request']['hostInfo'].'/';

        $this->screenshotPath = dirname(__FILE__).'/../../screenshots';
        $this->screenshotUrl  = $this->baseUrl . '/screenshots';

		$this->setBrowserUrl($this->baseUrl);
	}

    public function waitForAjax($timeout = 5000)
    {
        $js_condition = "selenium.browserbot.getCurrentWindow().jQuery.active == 0";
        $this->waitForCondition($js_condition, $timeout);
    }
    
    public function assertHtmlSourceContains($needle)
    {
        $html = $this->getHtmlSource();
        
        if (strpos($html, $needle) === false)
            $this->fail('Failed asserting that html page source contains "' . $needle . '".');
    }

    public function login()
    {
        if (!$this->urlIs($this->urls['login']))
            $this->open($this->urls['login']);

        $this->type('css=#UserLogin_username', 'admin');
        $this->type('css=#UserLogin_password', 'admin');
        $this->clickAndWait('css=.btn-primary');
    }

    public function checkForYiiErrors()
    {
        $content = $this->getText("//body");
        $pattern = '~(Fatal error: .+) in (.+ on line \d+)~m';
        if (preg_match($pattern, $content, $match)) {
            $this->fail(
                'found in response' . "\n" .
                $match[1] . "\n" .
                'in ' . $match[2]
            );
        }

        $isYiiErrorPage =
            $this->isElementPresent("//div[@class='container']//h1") &&
            $this->isElementPresent("//p[@class='message']") &&
            $this->isElementPresent("//p[@class='file']");

        if ($isYiiErrorPage) {
            // We've got Yii error page

            $title = $this->getText("//div[@class='container']//h1");
            $message = $this->getText("//p[@class='message']");
            $file = $this->getText("//p[@class='file']");

            $this->fail(
                'found in response' . "\n" .
                $title . ': ' . $message . "\n" .
                'in ' . $file
            );
        }
    }

    public function open($url = '')
    {
        parent::open($url);
        $this->waitForPageToLoad();
        $this->checkForYiiErrors();
    }

    public function clickAndWait($locator = null, $timeout = null )
    {
        $this->click($locator);
        $this->waitForPageToLoad($timeout);
        $this->checkForYiiErrors();
    }

    public function assertUrl($url)
    {
        $this->assertLocation($this->baseUrl . $url);
    }

    public function urlIs($url)
    {
        return $this->getLocation() === $this->baseUrl . $url;
    }
}
