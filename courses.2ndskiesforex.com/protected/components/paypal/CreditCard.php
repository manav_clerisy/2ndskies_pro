<?php

namespace yii\PayPal\Form;

use PayPal\Api\CreditCard as PPModel;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Payer;
use IsoCodes\CreditCard as CreditCardValidator;

/**
 * @property Address $billing_address
 */
class CreditCard extends \CFormModel
{
    /**
     * Credit card number.
     * Numeric characters only with no spaces or punctuation.
     * The string must conform with modulo and length required by each credit card type.
     * Redacted in responses.
     * Required.
     * @var string
     */
    public $number;

    /**
     * Credit card type.
     * Valid types are: visa, mastercard, discover, amex Required.
     * @var string
     */
    public $type;

    /**
     * Expiration month with no leading zero.
     * Acceptable values are 1 through 12.
     * Required.
     * @var integer
     */
    public $expire_month;
    
    /**
     * 4-digit expiration year.
     * Required.
     * @var integer
     */
    public $expire_year;

    /**
     * 3-4 digit card validation code.
     * @var string
     */
    public $cvv2;

    /**
     * Cardholder’s first name.
     * @var string
     */
    public $first_name;

    /**
     * Cardholder’s last name.
     * @var string
     */
    public $last_name;
    
    /*
     * Billing address
     */
    public $billing_address;
    public $billing_city;
    public $billing_state;
    public $billing_zip;
    public $billing_country;

    public function rules()
    {
        return array(
            array('number, type, expire_month, expire_year, cvv2,first_name, last_name, billing_address, billing_city, billing_state, billing_zip, billing_country', 'required'),
            array('number', 'validateCreditcard'),
            array('type', 'in', 'range' => array_keys($this->typeLabels())),
            array('expire_month, expire_year, cvv2', 'numerical', 'integerOnly' => true),
            array('expire_month', 'numerical', 'min' => 1, 'max' => 13),
            array('expire_year', 'length', 'is' => 4),
            array('cvv2', 'length', 'min' => 3, 'max' => 4),
            array('first_name, last_name', 'safe'),
        );
    }

    public function validateCreditcard($attr)
    {
        if (!CreditCardValidator::validate($this->$attr)) {
            $msg = sprintf(
                '%s is not a valid credit card number.',
                $this->getAttributeLabel($attr)
            );
            $this->addError($attr, $msg);
        }
    }

    public function typeLabels()
    {
        return array(
            'visa' => 'Visa',
            'mastercard' => 'MasterCard',
            'discover' => 'Discover',
            'amex' => 'American Express',
        );
    }

    public function expireMonth()
    {
        return array('1'=>'01','2'=>'02','3'=>'03','4'=>'04','5'=>'05','6'=>'06','7'=>'07','8'=>'08','9'=>'09','10'=>'10','11'=>'11','12'=>'12');
    }

    public function expireYearLabels()
    {
        $currentYear = date('Y', time());
        $yearsArr = array();

        for($i = 0; $i<20; $i++)
            $yearsArr[$currentYear+$i] = $currentYear + $i;

        return $yearsArr;
    }

    /**
     * @return PPModel
     */
    public function paypalModel()
    {
        $model = new PPModel();
        $model->fromArray($this->attributes);

        return $model;
    }

    /**
     * @return Payer
     */
    public function paypalPayer()
    {
        $fi = new FundingInstrument();
        $fi->setCreditCard($this->paypalModel());

        $payer = new Payer();
        $payer->setPaymentMethod('credit_card')
            ->setFundingInstruments(array($fi));

        return $payer;
    }
}
