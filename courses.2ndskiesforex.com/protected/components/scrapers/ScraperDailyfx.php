<?php
class scraperDailyfx extends ScraperBase
{
    public $date;
    public $dataArray;
    public $note;
    public $rowCount;
    public $nextWeekUrl;

    public function doParse()
    {
        $this->loadPage($this->url);
        $this->parseNextWeekUrl();

        $table = pq('#e-cal-table tr');

        foreach($table as $tr)
        {
            $trClass = pq($tr)->attr('class');

            if($trClass == 'e-cal-row empty')
                $this->getData($tr);
            elseif($trClass == 'e-cal-row')
                $this->getData($tr, true);

        }
    }

    public function getData($tr, $noteExist = false)
    {
        $data = array();
        $names = array('date','time','currency','event','importance','actual','forecast','previous','notes');
        $date = null;
        $i = 0;

        $rows = pq($tr)->find('td');

        foreach($rows as $td)
        {
            $innerDiv = pq($td)->find('div');
            $value = '';
            if(!empty($innerDiv))
            {
                $innerDivClass = $innerDiv->attr('class');

                if(!empty($innerDivClass) && substr_count($innerDivClass,'flag'))//set country
                {
                    $classArr = explode('-',$innerDivClass);
                    $value = strtoupper(end($classArr));
                }
                if(!empty($innerDivClass) && ($innerDivClass == 'e-cal-date')) //set date
                {
                    $date = $innerDiv->html();
                    $date = explode('<br>',$date);
                    $date = str_replace(array("\t","\v","\r","  ","\n","</span>"),'',$date[1]);
                    $this->date = date('Y-m-d',strtotime($date));
                }
            }

            //set importance level
            $tdClass = pq($td)->attr('class');
            if(substr_count($tdClass,'evImportance'))
            {
                $classArr = explode(' ',$tdClass);
                $value = ucfirst(end($classArr));
            }

            $data[$names[$i]] = empty($value) ? str_replace(array("\t","\v","\r","  ","\n"),'',pq($td)->text()) : $value;
            $i++;
        }

        if(empty($data['time']))
            $data['time'] = '00:00';

        $data['time'] = preg_replace('/LIVE/','',$data['time']);
        $data['event_date'] = $this->date.' '.str_replace(' ','',$data['time']);

        /*if($noteExist)
            $data['notes'] = $this->getNote($tr);*/

        $this->saveData($data);

    }

    public function getNote($tr)
    {
        $onclick = pq($tr)->attr('onclick');
        $onclick = str_replace(',','',$onclick);
        $onclickArr = explode("'",$onclick);

        if(isset($onclickArr[1]) && !empty($onclickArr[1]))
        {
            $noteUrl = 'http://www.dailyfx.com'.$onclickArr[1].'?comment=true&meta=english';
            $note = $this->loadPageCode($noteUrl);
            $note = explode("\n\n",$note);

            if(isset($note[1]) && !empty($note[1]) && pq($note[1])->isHTML())
                return $note[1];
            else
                return null;
        }
        else
            return null;
    }

    public function setStartDate($str)
    {
        $startDate = strtotime($str);
        $this->date = date('Y-m-d',$startDate);
    }

    public function saveData($data)
    {
        if($data['importance'] == 'Low')
            return;

        $command = Yii::app()->db->createCommand();

        $command->insert('economic_events', array(
            'event_date'=> $data['event_date'],
            'currency'=> $data['currency'],
            'event'=> $data['event'],
            'importance'=> $data['importance'],
            'actual'=> str_replace("Â",'',$data['actual']),
            'forecast'=> str_replace("Â",'',$data['forecast']),
            'previous'=> str_replace("Â",'',$data['previous']),
            'notes'=> $data['notes']
        ));

        echo ".";
        $this->rowCount++;
    }

    public function cleanData()
    {
        $command = Yii::app()->db->createCommand();
        $command->delete('economic_events', 'event_date>=:event_date', array(':event_date'=>$this->date));
    }

    public function parseNextWeekUrl()
    {
        $nextWeek = null;
        $href = pq('#e-cal-control-top span:last a')->attr('href');
        $href = explode("'",$href);
        if(isset($href[1]) && !empty($href[1]))
            $nextWeek = str_replace('/','%2F',$href[1]);

        $this->nextWeekUrl = $nextWeek;
    }

    public function getNextWeekUrl()
    {
        return $this->nextWeekUrl;
    }
}