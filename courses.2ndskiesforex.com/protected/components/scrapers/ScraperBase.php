<?php

/**
 * Base class of scrapers modules
 */
abstract class ScraperBase extends CApplicationComponent
{
   /**
    * Url of page we are parsing.
    * @var string
    */
    public $url;

   /**
    * config from config/scrapers.php
    *
    * @var array
    */
    public $config;

   /**
    * page content
    *
    * @var string
    */
    public $html;

   /**
    * array scraper errors
    * @var array
    */
    public $errors;
    

    
   /**
    * Parses html page
    */
    
    abstract protected function doParse();

    /**
     * @param $url string
     * @param $config
     */
    public function __construct($config)
    {
        $this->config = $config;
        $this->url = $config['url'];
        Yii::import('application.extensions.phpQuery.phpQuery');

        $log = 'Calling Scraper: [' . get_class($this) . '], url: [' . $this->url . ']';
        Yii::log($log, CLogger::LEVEL_INFO);
    }

   /**
    * Gets page content by url
    *
    * @param string $url page url
    * @param string $cookies file where cookies should be wrote
    * @return string page content in utf8
    */
    protected function loadPageCode($url, $cookies = null, $post = null, $extra = null, $showHeaders = 1)
    {
        $url = explode('#', $url);
        $url = $url[0];

        //echo('<pre>'.var_dump(array($url, $post), true)."</pre>\n");
        
        $ch = curl_init();
        $timeout = 15;
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0');
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_ENCODING, '');

        if (! is_null($cookies)) {
            curl_setopt($ch, CURLOPT_COOKIEJAR,  $cookies);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $cookies);
        }

        if (! is_null($post)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS,  $post);
        }



        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSLVERSION, 3);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HEADER, $showHeaders);

        $extra[] = '';//'Accept-Language: en-us,en;q=0.5';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $extra);

        //curl_setopt($ch, CURLOPT_COOKIE, "AustinLocale=en_US");

        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        $data = curl_exec($ch);
        //(var_dump(curl_error($ch)));
        $headers = curl_getinfo($ch, CURLINFO_HEADER_OUT);
        //var_dump($headers);
        //file_put_contents("d:/111.txt", $data);
        $contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        $charset = null;
        preg_match( '@([-\w/+]+)(;\s+charset=([a-z0-9\-]+))?@i', $contentType, $matches );
        if ( isset( $matches[3] ) )
            $charset = trim($matches[3], '"');

        if($charset!=null && $charset!='utf-8' && $charset!='UTF-8')
        {
            $data = iconv($charset, "utf-8", $data);
        }
        curl_close($ch);

        $parts = explode("\n", $data, 2);
        if (isset($parts[0]) && stristr($parts[0], '404')) {
            throw new PageNotFoundException('404 Page Not Found');
        }
        $data = str_replace("\x0b", '', $data);
        return $data;
    }

   /**
    * Logs errores inbto scraper.errores.log file
    */
    protected function logError()
    {
        $log = 'url: [' . $this->url . '], Error codes: [' . implode(',',$this->errors) . '], ';
        Yii::log($log, CLogger::LEVEL_INFO);
    }

    /**
     * 
     * @param string $url
     * @return phpQueryObject
     */
    public function loadPage($url)
    {
        $html = $this->loadPageCode($url);
        
        // Remove zero chars
        $html = str_replace("\0", '', $html);
        
        $this->html = $html;
        
        return phpQuery::newDocument($this->html);
    }

    /**
     * Entry point
     */
    public function parseUrl($url = null)
    {
        $this->url = $url;
        
        $this->loadPage($url);

        $res = $this->doParse();

        if (count($this->errors) > 0) {
            $this->logError();
            $res['has_errors'] = true;
        }

        echo 'result: ' . var_export($res, true), CLogger::LEVEL_INFO, 'scrapers.' . get_class($this);
        
        return $res;
    }

    protected function makeAbsolute($url, $baseUrl)
    {
        $pos = strpos($url, '://');
		if ( $pos === false || $pos > 10 ) {
			$parsed = parse_url($baseUrl);
			$fullHost = $parsed['scheme'] . '://' . $parsed['host'];
			
			if (substr($url, 0, 1) == '/') {
				return $fullHost . $url;
			}
			
			$pathParts = explode('/', $parsed['path']);
			array_pop($pathParts);
			
			while (substr($url, 0, 3) == '../') {
				array_pop($pathParts);
				$url = substr($url, 3);
			}
			//echo $url . '<br />' . $this->baseUrl . '<br />' . $fullHost . '<b>' . implode('/', $pathParts) . '</b>' . '/' . $url . '<br />' . '<br />';
			
			return $fullHost . implode('/', $pathParts) . '/' . $url;
		}
		
		return $url;
    }
}

class PageNotFoundException extends CException
{

}
