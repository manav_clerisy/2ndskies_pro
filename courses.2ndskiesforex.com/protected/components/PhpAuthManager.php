<?php
class PhpAuthManager extends CPhpAuthManager{
    public function init(){
        if($this->authFile===null){
            $this->authFile=Yii::getPathOfAlias('application.config.auth').'.php';
        }

        parent::init();

        $this->revoke('economicEventsEditor', Yii::app()->user->id);//if admin has change user role
        $this->save();

        if(Yii::app()->user->isEconomicEventsEditor()){
            if(!$this->isAssigned('economicEventsEditor', Yii::app()->user->id))
            {
                $this->assign('economicEventsEditor', Yii::app()->user->id);
                $this->save();
            }
        }
    }
}