<?php

abstract class Cache
{
    /**
     * Creates cache id by merging all arguments into one line.
     * @return string generated cache id.
     */
    public static function id()
    {
        return self::toId(func_get_args());
    }
    
    /**
     * Creates cache id by merging all vars into one line.
     * @param array $vars
     * @return string generated cache id.
     */
    public static function toId($vars)
    {
        return implode('-', $vars);
    }
    
    public static function wrap($id, $callback, $expire=0, $dependency=NULL)
    {
        if (is_array($id))
            $id = self::toId($id);
        
        if (is_string($dependency))
            $dependency = new CacheTags($dependency);
        
        $value = Yii::app()->cache->get($id);
        if ($value === false) {
            $value = $callback();
            Yii::app()->cache->set($id, $value, $expire, $dependency);
        }

        return $value;
    }

    public static function begin($id, $expire=0, $dependency=NULL)
    {
        if (is_array($id))
            $id = self::toId($id);
        
        if ($expire === 0)
            $expire = 60*60*24*31;
        
        if (is_string($dependency))
            $dependency = new CacheTags($dependency);
        
        $params = array(
            'duration' => $expire,
            'dependency' => $dependency,
        );
        
        return Yii::app()->controller->beginCache($id, $params);
    }

    public static function end()
    {
        return Yii::app()->controller->endCache();
    }
}
