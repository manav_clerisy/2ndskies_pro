<?php

class BBCode
{
    public static function toHtml($text, $bbcode_uid = '')
    {
        $text = html_entity_decode($text);

        if ($bbcode_uid) {
            $match = array('<br />', "[/*:m:$bbcode_uid]", ":u:$bbcode_uid", ":o:$bbcode_uid", ":$bbcode_uid");
            $replace = array("\n", '', '', '', '');
        } else {
            $match = array('<br />');
            $replace = array("\n");
        }

        $text = str_replace($match, $replace, $text);
        
        $format_search =  array(
            '#\[b\](.*?)\[/b\]#is', // Bold ([b]text[/b]
            '#\[i\](.*?)\[/i\]#is', // Italics ([i]text[/i]
            '#\[u\](.*?)\[/u\]#is', // Underline ([u]text[/u])
            '#\[s\](.*?)\[/s\]#is', // Strikethrough ([s]text[/s])
            '#\[code\](.*?)\[/code\]#is', // Monospaced code [code]text[/code])
            '#\[size=(\d{1,3})\](.*?)\[/size\]#is', // Font size 1-20px [size=20]text[/size])
            '#\[color=\#?([A-F0-9]{3}|[A-F0-9]{6})\](.*?)\[/color\]#is', // Font color ([color=#00F]text[/color])
            '#\[url=((?:ftp|https?)://.*?)\](.*?)\[/url\]#i', // Hyperlink with descriptive text ([url=http://url]text[/url])
            '#\[url\]((?:ftp|https?)://.*?)\[/url\]#i', // Hyperlink ([url]http://url[/url])
            '#\[img\](https?://.*?\.(?:jpg|jpeg|gif|png|bmp))\[/img\]#i', // Image ([img]http://url_to_image[/img])
            '#\[sprout=([0-9a-f]+)\]([0-9a-f]+)\[/sprout\]#i', // Sprout video
            // quote
            '#\[quote\](.*?)\[/quote\]#is', // Quote ([quote]text[/quote])
            '#\[quote="([^"]*)"\](.*?)\[/quote\]#is',
            //tables
            '#\[table=(\d{1,4}),(\d{1,2})\](.*?)\[/table\]#is',//Table width 1-4 symbols, border 1-4 symbols ([table=800,1])
            '#\[tr\](.*?)\[/tr\]#is',// table line [tr]
            '#\[td=(\d{1,4}),([a-zA-Z]{3,10})\](.*?)\[/td\]#is',//table column width 1-4 symbols, bgcolor 3-10 letters

    );
        // The matching array of strings to replace matches with
        $format_replace = array(
            '<strong>$1</strong>',
            '<em>$1</em>',
            '<span style="text-decoration: underline;">$1</span>',
            '<span style="text-decoration: line-through;">$1</span>',
            '<pre>$1</'.'pre>',
            '<span style="font-size: $1%;">$2</span>',
            '<span style="color: #$1;">$2</span>',
            '<a href="$1">$2</a>',
            '<a href="$1">$1</a>',
            '<img src="$1" alt="" />',
            '<iframe class="sproutvideo-player" type="text/html" src="http://videos.sproutvideo.com/embed/$1/$2?type=hd" width="640" height="480" frameborder="0"></iframe>', // Sprout video
            // quote
            '<blockquote>$1</blockquote>',
            '<blockquote><em><small><cite>$1 wrote:</cite></small>$2</em></blockquote>',
            //tables
            '<table valign="top" width="$1" border="$2" cellpadding="1" cellspacing="0">$3</table>',
            '<tr>$1</tr>',
            '<td width="$1" bgcolor="$2">$3</td>',

        );
        // Perform the actual conversion
        do {
            $oldText = $text;
            $text = preg_replace($format_search, $format_replace, $text);
        } while ($text !== $oldText); // Replacing nested BBCodes
        
        
        $text = str_replace(array("\n", "\r"), array('<br />', "\n"), $text);
        
        
        return $text;
    }
}
