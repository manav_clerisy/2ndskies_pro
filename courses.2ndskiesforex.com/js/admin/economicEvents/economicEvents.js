$(document).ready(function(){
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });

    $('.search-form form').submit(function(){
        $.fn.yiiGridView.update('economic-events-grid', {
            data: $(this).serialize()
        });
        return false;
    });

    addIdForGridTable();

    $(document).on('click', 'table.items .arrow', function(){
        var id  = $(this).find('a').attr('href');
        var $tr = $(this).closest('tr');

        if ($(this).hasClass('up'))
            $tr.next().remove();
        else {
            $.post( "/admin/economicEvents/getNote", {id: id}, function( data ) {

                var $newTr = $('<tr>',
                                {class:'expand'}
                              )
                              .append('<td colspan="3"></td><td class="col-notes" colspan="7">'+data.note+'</td>');

                $tr.after($newTr);

            }, 'json');
        }

        $(this).toggleClass('up');
        $(this).parents('tr').toggleClass('open');

        return false;
    });

    hideDuplicatedDates();
});

function hideDuplicatedDates()
{
    var $divs = $('div.cal-date');

    var dates = [];
    $divs.each(function(index, element){
        dates.push($(element).text());
    });

    $(dates.unique()).each(function(ind, el){
        // hides every block except the first one
        $divs.find(':contains('+el+')')
             .filter(':gt(0)')
             .each(function(){
                $(this).closest('div').hide();
             });
    });

}

// this id was stolen from http://www.dailyfx.com/calendar
function addIdForGridTable()
{
    $('table.items').attr('id', 'e-cal-table');
}


Array.prototype.unique = function() {
    var unique = [];
    for (var i = 0; i < this.length; i++) {
        if (unique.indexOf(this[i]) == -1) {
            unique.push(this[i]);
        }
    }
    return unique;
};