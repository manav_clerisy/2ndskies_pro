$(document).ready(function () {

    $('.post-reply').on('click',function(){
       $('#comment').removeClass('hidden');
    });

    $('#close-redactor').on('click',function(){
        $('#comment').addClass('hidden');
        $('#save-btn').text('Create');
        var topicId = $('#close-redactor').attr('name');

        $('#comment-form').attr('action','/topic/comment?topic_id='+topicId);
        $('#redactor').redactor('set', '');
    });

    $('.reply').on('click',function(){
        var commentId = $(this).attr('name');
        var $comment = $('#c' + commentId);
        var content = $('.content', $comment).html();
        var username = $('.span3 .user-title a', $comment).text();
        var text = '<blockquote><em><small><cite>'+username+' wrote:</cite></small>'+content+'</em></blockquote><br>';

        $('#comment').removeClass('hidden');
        $('#redactor').redactor('insertHtml', text).redactor('focus');
    });

    $('.edit').on('click',function(){
        var commentId = $(this).attr('name');
        var $comment = $('#c' + commentId);
        var content = $('.content', $comment).html();

        $('#save-btn').text('Save');
        $('#comment-form').attr('action','/topic/editComment?id='+commentId);
        $('#comment').removeClass('hidden');
        $('#redactor').redactor('insertHtml', content).redactor('focus');
    });

    $('.deleteComment').on('click',function(){
        var commentId = $(this).attr('name');

        if (confirm("Delete this comment? After this action you are not able to restore removed information.") == true)
            $.post("/topic/deleteComment",
                { commentId: commentId },
                function(){
                    location.reload();
                });
    });

    $('#deleteTopic').on('click',function(){
        var topicId = $(this).attr('name');

        if (confirm("Delete this topic? After this action you are not able to restore removed information.") == true)
            $.post("/topic/deleteTopic", { topicId: topicId },
                function(data){
                    window.location.href = data;
            });

    });

    $('#moveTopic').on('click', function(){
        var sectionId = $('#sectionList').find(":selected").val();
        var topicId = $('#topicId').val();

        $.post("/topic/replaceTopic",
            { sectionId: sectionId, topicId: topicId },
            function(data){
                $('#header').before(data);
                setTimeout(function(){ $('#user-message').remove();}, 2500);
            });
    });

    $('#subscribe').on('click', function(){
        var topicId = $(this).attr('name');
        var btn = $(this);

        $.post("/topic/subscribe",
            {id:topicId},
            function(data){
                btn.addClass('hidden');
                $('#unsubscribe').removeClass('hidden');

                $('#header').before(data);
                setTimeout(function(){ $('#user-message').remove();}, 2500);

            });
    });

    $('#unsubscribe').on('click', function(){
        var topicId = $(this).attr('name');
        var btn = $(this);

        $.post("/topic/unsubscribe",
            {id:topicId},
            function(data){
                btn.addClass('hidden');
                $('#subscribe').removeClass('hidden');
                $('#header').before(data);
                setTimeout(function(){ $('#user-message').remove();}, 2500);
            });
    });

    $('#notify').on('click', function(){
        var form = $('#subscriptions-notify-form').serializeArray();

        $.post("/topic/subscriptionNotify",
            {form:form},
            function(data){
             $('#subscriptions-notify-form').before(data);
             setTimeout(function(){ $('#user-message').remove();}, 2500);
            });
    });

});