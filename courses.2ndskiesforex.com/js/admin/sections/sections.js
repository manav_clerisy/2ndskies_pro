$(function(){

    var $tree = $("#tree");

    // Initialize the tree inside the <div>element.
    // The tree structure is read from the contained <ul> tag.
    $tree.dynatree({
        title: "Programming Sample",
        children: $.parseJSON(sectionTree),
        onActivate: function(node) {
            var $inputRename = $("#activeNodeName");
            var $inputRemove = $("#btnRemoveNode");

            if (node.data.key == 'root') {
                $inputRename.attr('disabled', 'true')
                $inputRemove.attr('disabled', 'true')
            }
            else {
                $inputRename.removeAttr('disabled')
                $inputRemove.removeAttr('disabled')
            }

            $inputRename.val(node.data.title);
        },
        clickFolderMode: 1,
        debugLevel: 0,
        dnd: {
            preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
            onDragStart: function(node) {
                /** This function MUST be defined to enable dragging for the tree.
                 *  Return false to cancel dragging of node.
                 */
                return true;
            },
            onDragEnter: function(node, sourceNode) {
                /** sourceNode may be null for non-dynatree droppables.
                 *  Return false to disallow dropping on node. In this case
                 *  onDragOver and onDragLeave are not called.
                 *  Return 'over', 'before, or 'after' to force a hitMode.
                 *  Return ['before', 'after'] to restrict available hitModes.
                 *  Any other return value will calc the hitMode from the cursor position.
                 */
                // Prevent dropping a parent below another parent (only sort
                // nodes under the same parent)
                if(node.parent !== sourceNode.parent){
                    return false;
                }
                // Don't allow dropping *over* a node (would create a child)
                return ["before", "after"];
            },
            onDrop: function(node, sourceNode, hitMode, ui, draggable) {
                /** This function MUST be defined to enable dropping of items on
                 *  the tree.
                 */
                sourceNode.move(node, hitMode);
            }
        }
    });



    // Adds new section
    $("#btnAddNode").click(function(){
        // Sample: add an hierarchic branch using code.
        // This is how we would add tree nodes programatically
        var node = $tree.dynatree("getActiveNode");
        if (node == null)
            var node = $tree.dynatree('getTree').getNodeByKey('root');

        node.addChild({
            title: "New Section",
            tooltip: "Section node",
            isFolder: true
        });

        node.expand();
    });

    // Adds new topic
    // Bogdan said it was unnecessary
    /*$("#btnAddTopic").click(function(){
        // Sample: add an hierarchic branch using code.
        // This is how we would add tree nodes programatically
        var node = $tree.dynatree("getActiveNode");
        if (node == null)
            var node = $tree.dynatree('getTree').getNodeByKey('root');

        // topic could be added only for section
        if (node.data.isFolder != true)
            return;


        node.addChild({
            title: "New Topic",
            tooltip: "Topic node",
            isFolder: false
        });

        node.expand();
    });*/

    // renaming
    $("#activeNodeName").keyup(function(){
        var node = $tree.dynatree("getActiveNode");

        if (! node)
            return;

        node.data.title = $(this).val();
        node.render();
    });

    // removing
    $("#btnRemoveNode").click(function(){

        bootbox.confirm("Are you sure?", function(result) {
            if (! result)
                return;

            var node = $tree.dynatree("getActiveNode");

            if (! node)
                return;

            if (node.data.key != 'root')
                node.remove();

            $tree.dynatree('getTree').activateKey('root');
        });

    });

    // saving
    $("#btnSaveTree").click(function(){

        var tree = $tree.dynatree("getTree").toDict().children;

        $json = JSON.stringify(tree);

        $form =
            $('<form>', {
                method: 'POST'
            }).append(
                $('<input>',{
                    'type' : 'hidden',
                    'value': $json,
                    'name' : 'Courses[section_tree]'
                })
            );


        $('#content').append($form);

        $form.submit();
    });
});


