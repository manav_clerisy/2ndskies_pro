function changeCalendarStatusForCourse(id)
{
    $.ajax({
        url: '/courses/changeCalendarPublicStatus',
        data: {id: id},
        complete: function(){
            $("#courses-grid").yiiGridView("update");
        }
    });
}

function changeVisibleStatusForCourse(id)
{
    $.ajax({
        url: '/courses/changeVisiblePublicStatus',
        data: {id: id},
        complete: function(){
            $("#courses-grid").yiiGridView("update");
        }
    });
}

