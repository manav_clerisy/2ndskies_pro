$(document).ready(function() {

    var formId = '#event-form';

    var calendar = $('#calendar').fullCalendar({
        header: {
            left: 'prev,next',// today',
            center: 'title',
            right: ''//'month, agendaWeek,agendaDay'
        },
        selectable: true,
        unselectAuto: false,
        select: function(start, end, allDay) {

            resetForm(formId);

            $('#date-interval', $(formId))
                .text(formatDate(start) +' - '+ formatDate(end));
            $('#event-date-start', $(formId))
                .val(formatDate(start, 'y-m-d'));
            $('#event-date-end', $(formId))
                .val(formatDate(end, 'y-m-d'));

        },
        eventClick: function(event, element) {
            $.ajax({
                url: "/calendar/viewEvent",
                type: "POST",
                data: {id: event.id},
                dataType: "json",
                success:function(data){
                    $(formId).replaceWith($(formId, '<div>'+data.html+'</div>'))
                }
            });
        },
        editable: false,
        events: events
       /*[
                        {
                            title: 'All Day Event',
                            start: new Date(y, m, 1)
                            },
                        {
                            title: 'Long Event',
                            start: new Date(y, m, d-5),
                            end: new Date(y, m, d-2)
                            },
                        {
                            id: 999,
                            title: 'Repeating Event',
                            start: new Date(y, m, d-3, 16, 0),
                            allDay: false
                            },
                        {
                            id: 999,
                            title: 'Repeating Event',
                            start: new Date(y, m, d+4, 16, 0),
                            allDay: false
                            },
                        {
                            title: 'Meeting',
                            start: new Date(y, m, d, 10, 30),
                            allDay: false
                            },
                        {
                            title: 'Lunch',
                            start: new Date(y, m, d, 12, 0),
                            end: new Date(y, m, d, 14, 0),
                            allDay: false
                            },
                        {
                            title: 'Birthday Party',
                            start: new Date(y, m, d+1, 19, 0),
                            end: new Date(y, m, d+1, 22, 30),
                            allDay: false
                            },
                        {
                            title: 'Click for Google',
                            start: new Date(y, m, 28),
                            end: new Date(y, m, 29),
                            url: 'http://google.com/'
                            }
        ]*/
    });


    $(document).on('click', '#submitEventForm', function(){
        $.ajax({
            url: "/calendar/manageEvent",
            type: "POST",
            data: $(formId).serialize(),
            dataType: "json",
            success:function(data){
                if (data.success == true)
                    window.location.reload();
                else
                    $(formId).replaceWith($(formId, '<div>'+data.html+'</div>'))

            }
        });

    });


    $(document).on('click', '#removeEvent', function(){

        if (! confirm('Are you sure?'))
            return;


        $.ajax({
            url: "/calendar/deleteEvent",
            type: "POST",
            data: {id: $('#event-form #JournalEvents_id').val()},
            dataType: "json",
            success:function(data){
                if (data == 'ok')
                    window.location.reload();
            }
        });

    });


    $('#is_calendar_open').change(function(){

        var $val =  $(this).is(':checked')
                        ? 1
                        : 0;
        $.ajax({
            url: "/calendar/changeCalendarPublicStatus",
            type: "POST",
            data: {val: $val},
            dataType: "json",
            success:function(data){

            }
        });

    });

});



function formatDate(date, format)
{
    format = format || 'd/m/y';

    var $date = new Date(date);

    var d = $date.getDate();
    var m = $date.getMonth()+1;
    var y = $date.getFullYear();

    if (format == 'd/m/y')
        $newDate =  d+'/'+m+'/'+y;
    else if (format == 'y-m-d')
        $newDate =  y+'-'+m+'-'+d;

    return $newDate;
}

function resetForm(el)
{
    $(el)
        .find(':input')
            .each(function() {
                switch(this.type) {
                    case 'password':
                    case 'select-multiple':
                    case 'select-one':
                    case 'text':
                    case 'textarea':
                        $(this).val('');
                        break;
                    case 'checkbox':
                    case 'radio':
                        this.checked = false;
                    }
            });
}

