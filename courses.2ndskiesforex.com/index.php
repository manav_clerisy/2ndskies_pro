<?php
$maintenance = false;
$maintenance_allowed = array(
    '89.33.120.19', #IMC
    '89.33.120.20', #IMC
    '89.33.120.21', #IMC
    '192.171.45.17', #Jeff
    '81.196.139.6', #Gabriel
    '162.158.89.161',#alex
    '162.158.88.176'
);
if ($maintenance && !in_array($_SERVER['REMOTE_ADDR'], $maintenance_allowed)) {
    header('Location: https://courses.2ndskiesforex.com/maintenance.html');
}
//define('YII_DEBUG', true);
// Debug is on when remote address is localhost
defined('YII_DEBUG') or $_SERVER['REMOTE_ADDR'] === '127.0.0.1' and define('YII_DEBUG', true);
defined('YII_DEBUG') or $_SERVER['REMOTE_ADDR'] === '10.10.100.3' and define('YII_DEBUG', true);
defined('YII_DEBUG') or $_SERVER['REMOTE_ADDR'] === '::1' and define('YII_DEBUG', true);
defined('YII_DEBUG') or define('YII_DEBUG', false);
ini_set('display_errors',         YII_DEBUG ? 1 : 0);
ini_set('display_startup_errors', YII_DEBUG ? 1 : 0);
error_reporting(YII_DEBUG ? -1 : 0);

// change the following paths if necessary
$yii = dirname(__FILE__) . '/vendor/yiisoft/yii/framework/' . (YII_DEBUG ? 'yii.php' : 'yiilite.php');
$config = dirname(__FILE__) . '/protected/config/main.php';

// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once __DIR__ . '/vendor/autoload.php';
require_once($yii);
Yii::createWebApplication($config)->run();
