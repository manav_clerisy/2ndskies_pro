<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'PayPal' => array($vendorDir . '/paypal/rest-api-sdk-php/lib'),
    'IsoCodes' => array($vendorDir . '/ronanguilloux/isocodes/src'),
    'Guzzle\\Tests' => array($vendorDir . '/guzzle/guzzle/tests'),
    'Guzzle' => array($vendorDir . '/guzzle/guzzle/src'),
    'Apostle' => array($vendorDir . '/apostle/apostle-php/src'),
);
