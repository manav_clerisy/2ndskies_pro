<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'yii\\PayPal\\Form\\' => array($baseDir . '/protected/components/paypal'),
    'urmaul\\gravacage\\' => array($vendorDir . '/urmaul/gravacage/src'),
);
