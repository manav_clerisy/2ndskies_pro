--+------------------------------------------------------------------+
--|                                   Ichimoku Kinko Hyo signal.lua  |
--|                               Copyright © 2012, Gehtsoft USA LLC | 
--|                                            http://fxcodebase.com |
--|                                      Developed by : Mario Jemic  |                    
--|                                          mario.jemic@gmail.com   |
--+------------------------------------------------------------------+

function Init()
    strategy:name("IchimokuKinkoHyo Signal");
    strategy:description("");

	 strategy.parameters:addGroup("Selector");
	 strategy.parameters:addBoolean("ON" .. 1, "SL/TL", "", true);
	  strategy.parameters:addBoolean("ON" .. 2, "CS / Price", "", true);
	   strategy.parameters:addBoolean("ON" .. 3, "Cloud / Price", "", true);
	
     strategy.parameters:addGroup("Parameters");
  
    strategy.parameters:addString("Type", "Price type", "", "Bid");
    strategy.parameters:addStringAlternative("Type", "Bid", "", "Bid");
    strategy.parameters:addStringAlternative("Type", "Ask", "", "Ask");
	
	strategy.parameters:addInteger("T", "Tenkan Period", "Tenkan Period", 9);
    strategy.parameters:addInteger("K", "Kijun Period", "Kijun Period", 26);
    strategy.parameters:addInteger("S", "Senkou Period", "Senkou Period", 52);
	
		 
    strategy.parameters:addString("Type", "Price type", "", "Bid");
    strategy.parameters:addStringAlternative("Type", "Bid", "", "Bid");
    strategy.parameters:addStringAlternative("Type", "Ask", "", "Ask");

    strategy.parameters:addString("Period", "Timeframe", "", "m5");
    strategy.parameters:setFlag("Period", core.FLAG_PERIODS);

    strategy.parameters:addGroup("Signals");

    strategy.parameters:addBoolean("ShowAlert", "Show Alert", "", true);
    strategy.parameters:addBoolean("PlaySound", "Play Sound", "", false);
	 strategy.parameters:addBoolean("RecurrentSound", "Recurrent Sound", "", false);
    strategy.parameters:addFile("SoundFile", "Sound File", "", "");
    strategy.parameters:setFlag("SoundFile", core.FLAG_SOUND);
	
	 strategy.parameters:addBoolean("SendEmail", "Send Email", "", true);
    strategy.parameters:addString("Email", "Email", "", "");
    strategy.parameters:setFlag("Email", core.FLAG_EMAIL);
end

local ShowAlert;
local SoundFile;
local BUY, SELL;
local bSource = nil;         -- the source stream
local Email;
local SendEmail;

local Tenkan;
local Kijun;
local Senkou;

local first={};

local ICH;
local SHORT, LONG;
local ON={};
local RecurrentSound;
function Prepare()
    -- collect parameters	
	Tenkan = instance.parameters.T;
    Kijun = instance.parameters.K;
    Senkou = instance.parameters.S;
	PlaySound=instance.parameters.PlaySound;
	RecurrentSound=instance.parameters.RecurrentSound;
	ON[1] = instance.parameters:getBoolean ("ON"..1);
	ON[2] = instance.parameters:getBoolean ("ON"..2);
	ON[3] = instance.parameters:getBoolean ("ON"..3);
	
	
	 SendEmail = instance.parameters.SendEmail;
    if SendEmail then
        Email = instance.parameters.Email;
    else
        Email = nil;
    end
    assert(not(SendEmail) or (SendEmail and Email ~= ""), "Email address must be specified");
   
	   
    ShowAlert = instance.parameters.ShowAlert;
	
    if PlaySound then
        SoundFile = instance.parameters.SoundFile;
    else
        SoundFile = nil;
    end
	
	
     assert(not(PlaySound) or (PlaySound and SoundFile ~= " "), "Sound file must be specified");

    --ExtSetupSignal("IchimokuKinkoHyo", ShowAlert);

    bSource = ExtSubscribe(1, nil, instance.parameters.Period, instance.parameters.Type == "Bid", "bar");
	
	ICH  = core.indicators:create("ICH", bSource, Tenkan, Kijun, Senkou);
   
    
    first["SL"]=ICH.SL:first();
	 first["TL"]=ICH.TL:first();
	  first["SA"]=ICH.SA:first();
	   first["CS"]=ICH.SA:first();
	   first["SB"]=ICH.SB:first();
	    first["PRICE"]=bSource:first();
	
        local name = profile:id() .. "(" ..  instance.bid:instrument() .. ", " .. Tenkan .. ", ".. Kijun.. ", ".. Senkou  .. ")";
         instance:name(name);
 
    --ExtSetupSignal(profile:id() .. ":", ShowAlert);
    --ExtSetupSignalMail(name);		
	
end


-- when tick source is updated
function ExtUpdate(id, source, period)	

    if period <  first["PRICE"]+1 then
	return;
	end
      
       
     ICH:update(core.UpdateLast);
		   
		if ON[1] then
			if period-1 > math.max( first["SL"],  first["TL"])  then
					if core.crossesOver(ICH.SL, ICH.TL, period)   then
					   Alert("IchimokuKinkoHyo Signal", " SL / TL CrossOver ", period);					 
					end
					  
				    if core.crossesUnder(ICH.SL, ICH.TL, period)   then
					     Alert("IchimokuKinkoHyo Signal", " SSL / TL CrossUnder ", period);	
					   
					end
			end		
		end
        if ON[2] then 		
			if period-1 > math.max( first["CS"] +Kijun ,  first["PRICE"] +Kijun) then
					if  core.crossesOver(ICH.CS, bSource.close, period-Kijun, period-Kijun-1 ) then
					Alert("IchimokuKinkoHyo Signal", " CS / Price CrossOver ", period);	
				 
					end
					
					if core.crossesUnder(ICH.CS, bSource.close, period-Kijun, period-Kijun-1 ) then
					Alert("IchimokuKinkoHyo Signal", " CS / Price CrossUnder ", period);	
					 
					end
			end		
		end
        if ON[3] then		
			if period-1 > math.max( first["SA"],  first["SB"]) then		
		            if math.max(ICH.SA[period], ICH.SB[period])< bSource.close[period] and  math.max(ICH.SA[period-1], ICH.SB[period-1]) > bSource.close[period-1]  then
					Alert("IchimokuKinkoHyo Signal", " Cloud / Price CrossOver ", period);	
						 
					end
					  
				     if math.min(ICH.SA[period], ICH.SB[period])> bSource.close[period] and  math.min(ICH.SA[period-1], ICH.SB[period-1]) < bSource.close[period-1]  then
					 Alert("IchimokuKinkoHyo Signal", " Cloud / Price CrossUnder ", period);	
					 
					end
			end	
		end				  
							   
end



function Alert(Label,Subject, period)
    if ShowAlert then
        terminal:alertMessage(instance.bid:instrument(), instance.bid[NOW],  Label .. " : " .. Subject , instance.bid:date(NOW));
    end
	
	
    if SoundFile ~= nil then
        terminal:alertSound(SoundFile, RecurrentSound);
    end

    if Email ~= nil then
     		
	   local date = instance.bid:date(NOW)
	local DATA = core.dateToTable (date);
	
    
   local delim = "\013\010";  
   local Note=  profile:id().. delim.. " Label : " ..Label  .. delim .. " Alert : " .. Subject ;   
   local Symbol= "Instrument : " .. instance.bid:instrument()
   local TF= "Time Frame : " .. bSource:barSize();    
   local Time =  " Date : " .. DATA.month.." / ".. DATA.day .." Time:  ".. DATA.hour  .." / ".. DATA.min .." / ".. DATA.sec; 
   
     local text = Note  .. delim ..  Symbol .. delim .. TF  .. delim .. Time;
	 terminal:alertEmail(Email, Label, text);
    end
end


dofile(core.app_path() .. "\\strategies\\standard\\include\\helper.lua");
