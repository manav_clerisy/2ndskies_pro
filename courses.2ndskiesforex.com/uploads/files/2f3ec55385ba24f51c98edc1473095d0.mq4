//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

#property indicator_chart_window
extern double StopLoss    = 30;
extern double RiskPercent = 2;

//
//
//
//
//

int init()   { return(0); }
int deinit() { return(0); }
int start()  
{
   double pipValue = MarketInfo(Symbol(),MODE_TICKVALUE); if (Digits==3 || Digits==5) pipValue *= 10;
   double step     = MarketInfo(Symbol(),MODE_LOTSTEP);
      int norm     = 0;
            if (step==1)    norm = 0;
            if (step==0.1)  norm = 1;
            if (step==0.01) norm = 2;
   double minLot = MarketInfo(Symbol(),MODE_MINLOT);
   double maxLot = MarketInfo(Symbol(),MODE_MAXLOT);
   double lots   = AccountBalance()*(RiskPercent/100.0)/(StopLoss*pipValue);
          lots   = NormalizeDouble(lots,norm);
          
          //
          //
          //
          //
          //
          
          double actualRisk; 
          string comment = DoubleToStr(StopLoss,0)+"SL     "+DoubleToStr(RiskPercent,0)+"% risk      "+DoubleToStr(lots,norm);
          if (lots<minLot)
            {
               actualRisk = (100*minLot*StopLoss*pipValue)/AccountBalance();
               comment = "lot size less than minimal allowed lot for risk and stop loss setting\n"+
                         "calculated lot size : "+DoubleToStr(lots,norm)+" minimal allowed : "+DoubleToStr(minLot,norm)+"\n"+
                         "risk with minimal lot size and stop loss set to : "+DoubleToStr(StopLoss,2)+"pips is : "+DoubleToStr(actualRisk,2)+"%";
            }
          Comment(comment);
   return(0);
}