$(function(){
    $('.sortable').each(function(){
        var $list = $(this);
        var $section = $list .closest('.section');
        var $save = $('.save', $section);
        
        $list
            .sortable()
            .disableSelection()
            .on('sortupdate', function() {
                $save .removeClass('disabled');
            });
        
        $save .click(function() {
            if ($save .hasClass('disabled'))
                return;
            
            $save .addClass('disabled');
            
            $.post('/admin/courses/saveTopicsOrder', {
                ids: $list .sortable('toArray', {attribute: 'data-id'})
            });
        });
    });
});