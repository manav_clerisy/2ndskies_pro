$(document).on('ready',function(){
    $('#showInfo').on('click',function(){
        $( "<div id='cvv2Info'><p><img src='/images/CVC2SampleVisaNew.png'></p>" +
            "<p>The card security code is located on the back of MasterCard, Visa and Discover credit or debit cards and is typically a separate group of 3 digits to the right of the signature strip.</p>" +
            "<p><img src='/images/CIDSampleAmex.png'></p>" +
            "<p>On American Express cards, the card security code is a printed, not embossed, group of four digits on the front towards the right.</p>" +
            "</div>" )
            .insertAfter( "#showInfo" );
        $('#showInfo').attr('id','hide');
    });

    $('#hide').on('click',function(){
        $( "div#cvv2Info").remove();
        $('#hide').attr('id','showInfo');
    });

    $('#paypalRadio').on('change', function(){
        $('#paypalPayment').removeClass('hidden');
        $('#creditCardArea').addClass('hidden');
    });

    $('#creditCardRadio').on('change', function(){
        $('#paypalPayment').addClass('hidden');
        $('#creditCardArea').removeClass('hidden');
    });

    var couponCodePaypal = document.getElementById("couponCodePaypal");
    var couponCodeCard = document.getElementById("couponCodeCard");
    couponCodePaypal.addEventListener("change", updatePrice, false);
    couponCodeCard.addEventListener("change", updatePrice, false);

    var cardNumber = document.getElementById("cardNumber");
    cardNumber.addEventListener("change", handleEvent, false);
    cardNumber.addEventListener("blur", handleEvent, false);

});

function updatePrice(event)
{
    var code   = event.target.value;
    var element = $('#couponCodePaypal, #couponCodeCard');
    element.parent().removeClass('error');
    element.removeClass('error');

    $('.codeError').remove();
    element.after('<p class="checkingCode">Checking code ... </p>');

    $.post("/courses/checkCouponCode",
        { code: code, courseId:courseId},
        function(data){
            var response = JSON.parse(data);
            var error = response['message'].indexOf("Error");
            $('.checkingCode').remove();
            if(error != -1)
            {
                element.parent().addClass('error');
                element.addClass('error');
                element.before('<div class="help-inline error codeError">'+response['message']+'</div>');
            }
            else
                $('.price').text('Total USD '+response['price']);
        });

}

function handleEvent(event)
{
    var value   = event.target.value,
        type    = getCreditCardType(value);

    $('#creditCardTypeInput').val(type);
}

function getCreditCardType(accountNumber)
{

    //start without knowing the credit card type
    var result = "unknown";

    //first check for MasterCard
    if (/^5[1-5]/.test(accountNumber))
    {
        result = "mastercard";
    }

    //then check for Visa
    else if (/^4/.test(accountNumber))
    {
        result = "visa";
    }

    //then check for AmEx
    else if (/^3[47]/.test(accountNumber))
    {
        result = "amex";
    }

    //then check for Discover
    else if (/^6(?:011|5)/.test(accountNumber))
    {
        result = "discover";
    }

    return result;
}
