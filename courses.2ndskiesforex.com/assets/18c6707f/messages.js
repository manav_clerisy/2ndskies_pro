$('.deleteAllMessages').on('click',function(e){

    if (confirm("Are you sure you want to delete this conversation? You can not restore this information!") == true) {
        $.post('deleteAllMessages',
            {user_from:$(this).attr('id')},
            function(data){
                $('#user-list').before(data);
                setTimeout(function(){ location.reload();}, 2500);

            });
    }
});

$('.deleteMessage').on('click',function(e){

    if (confirm("Are you sure you want to delete this message? You can not restore it!") == true) {
        $.post('deleteMessage',
            {message_id:$(this).attr('id')},
            function(data){
                $('#messages-div').before(data);
                setTimeout(function(){ location.reload();}, 2500);

            });
    }
});