$('#time').live("change", function(){

    $.post("courses/changeTimeZone",
        {time:$('#time').attr("selected","selected").val()},
        function(data){
            $('#time').html(data);
            var timeZone = '"'+$('#time').attr("selected","selected").val()+'"';
            $.fn.yiiGridView.update('economic-events-grid-index',{ data:{event_time:timeZone,days:$('#days').val()}});
        });
});

$('#previous').live("click", function(){
    var days = $('#days').val();
    $('#days').val( days - 1);
    var timeZone = '"'+$('#time').attr("selected","selected").val()+'"';

    $.fn.yiiGridView.update('economic-events-grid-index',{ data:{event_time:timeZone,days:$('#days').val()}});
});

$('#next').live("click", function(){
    var days = $('#days').val();
    var sum = parseInt(days) + 1;
    $('#days').val( sum );
    var timeZone = '"'+$('#time').attr("selected","selected").val()+'"';

    $.fn.yiiGridView.update('economic-events-grid-index',{ data:{event_time:timeZone,days:$('#days').val()}});
});

$('#today').live("click", function(){
    $('#days').val(0);
    var timeZone = '"'+$('#time').attr("selected","selected").val()+'"';

    $.fn.yiiGridView.update('economic-events-grid-index',{ data:{event_time:timeZone,days:$('#days').val()}});
});


// *** Recent Posts Update ***

$('#posts5, #posts10, #posts20, #posts50').live("click", function(){
    var posts = $(this).html();
    $.fn.yiiGridView.update('recent-posts-grid',{ data:{posts: posts, courseId: courseId}});
});

