<?php

/**
 * @package Package name
 * @version 1.0
 */
/*
Plugin Name: Test App Question List
Plugin URI: http://wp-admin.com.ua
Armstrong: Test App Plugin.
Author: Svetlanija
Version: 1.0
Author URI: http://wp-admin.com.ua
*/

?>

<!--<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">

<script>
    $(function() {
        $( "#dialog" ).dialog();
    });
</script>-->

<?php
add_action('admin_menu', 'create_custom_panel');
createTables();
/*
if(isset($_POST['TQ']) && !empty($_POST['TQ']))
{
    foreach($_POST['TQ'] as $key=>$tq)
    {
        if($tq == "") break;
        $question_text = $tq;
        $parent_id = ($key == 0) ? 0 : $parent_id_fromBD;

        if($key == 0)
        {
            $sql = "SELECT * FROM `wp_test_questions`;";
            $objects = $wpdb->get_results($sql);
            $parent_id_fromBD = $wpdb->num_rows;

            $sql = "SELECT * FROM `wp_test_questions` WHERE `parent_id`=0;";
            $objects = $wpdb->get_results($sql);
            $question_number = ($wpdb->num_rows) ? $wpdb->num_rows : 0;
        }

        if(isset($tq[0]) && !empty($tq[0])) $link = $tq[0];
        else $link=null;

        if(isset($tq[1]) && !empty($tq[1])) $set_name = $tq[1];
        else $set_name=null;

        $sql = 'INSERT INTO wp_test_questions (question_text, question_number, parent_id, link, set_name)
                VALUES ("'.$question_text.'","'.$question_number.'","'.$parent_id.'","'.$link.'","'.$set_name.'");';
        $wpdb->query($sql);

    }
}

if(isset($_GET['action']) && !empty($_GET['action']))
{
    switch ($_GET['action']){
        case 'editQuestion': { editQuestion($_GET['id']); break;}
        case 'deleteQuestion': { deleteQuestion($_GET['id']); break;}
    }

}
*/
function create_custom_panel() {
    add_menu_page('menu page', 'Question List', 'manage_options', 'custom-panel', 'custom_panel');
}

function custom_panel(){
   // $arr = array();

/*    '<div class="wrap" id="new_questions_form">
    <h2>Questions Text For Test App</h2>

    <form method="post" action="admin.php?page=custom-panel">
    <table class="form-table">

    <tr valign="top">
    <th scope="row">Question</th>
    <td><input type="text" name="TQ[0]" value="'.$arr[0].'" /></td>
    </tr>

    <tr valign="top">
    <th scope="row">Answer 1</th>
    <td>
        <input type="text" name="TQ[1]" value="'.$arr[1].'" /> Redirect link after choosing this answer (Optional)
        <input type="text" name="TQ[1][0]" value="'.$arr[1][0].'" />Name of Set (Optional)
        <input type="text" name="TQ[1][1]" value="'.$arr[1][1].'" />
    </td>
    </tr>

    <tr valign="top">
    <th scope="row">Answer 2</th>
    <td>
        <input type="text" name="TQ[2]" value="'.$arr[2].'" /> Redirect link after choosing this answer (Optional)
        <input type="text" name="TQ[2][0]" value="'.$arr[2][0].'" />Name of Set (Optional)
        <input type="text" name="TQ[1][1]" value="'.$arr[2][1].'" />
    </td>
    </tr>

    <tr valign="top">
    <th scope="row">Answer 3</th>
    <td>
        <input type="text" name="TQ[3]" value="'.$arr[3].'" /> Redirect link after choosing this answer (Optional)
        <input type="text" name="TQ[3][0]" value="'.$arr[3][0].'" />Name of Set (Optional)
        <input type="text" name="TQ[1][1]" value="'.$arr[3][1].'" />
    </td>
    </tr>

    <tr valign="top">
    <th scope="row">Answer 4</th>
    <td>
        <input type="text" name="TQ[4]" value="'.$arr[4].'" /> Redirect link after choosing this answer (Optional)
        <input type="text" name="TQ[4][0]" value="'.$arr[4][0].'" />Name of Set (Optional)
        <input type="text" name="TQ[1][1]" value="'.$arr[4][1].'" />
    </td>
    </tr>

    </table>

    <p class="submit">
    <input type="submit" class="button-primary" value="Add Question" />
    </p>

    </form>
    </div>

*/
    echo '<h3>Test Questions Table</h3>';
    global $wpdb;

    $sql = "
		SELECT * FROM `wp_test_questions`;
	";
    $objects = $wpdb->get_results($sql);
    $num_objects = $wpdb->num_rows;
?>

    <table class="widefat post fixed" cellspacing="0" style="clear: none;">
        <thead>
        <tr>
            <th scope="col" class="manage-column column-title" >ID</th>
            <th scope="col" class="manage-column column-title">Question Text</th>
            <th scope="col" class="manage-column column-title" >Parent ID</th>
            <th scope="col" class="manage-column column-title" >Question Number</th>
            <th scope="col" class="manage-column column-title" >&nbsp;</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th scope="col" class="manage-column column-title" >ID</th>
            <th scope="col" class="manage-column column-title">Question Text</th>
            <th scope="col" class="manage-column column-title" >Parent ID</th>
            <th scope="col" class="manage-column column-title" >Question Number</th>
            <th scope="col" class="manage-column column-title" >&nbsp;</th>
        </tr>
        </tfoot>
        <tbody>
<?php
        if($num_objects>0):
            foreach($objects as $object): ?>
                <tr>
                    <td>
                        <?php echo $object->id; ?>
                    </td>
                    <td>
                        <?php echo $object->question_text; ?>
                    </td>
                    <td>
                        <?php echo $object->parent_id; ?>
                    </td>
                    <td>
                        <?php echo $object->question_number; ?>
                    </td>
                 <!--   <td><a id="edit_button" href="admin.php?page=custom-panel&amp;action=editQuestion&amp;id=<?php //echo $object->id; ?>">edit</a> | <a href="admin.php?page=custom-panel&amp;action=deleteQuestion&amp;id=<?php //echo $object->id; ?>">delete</a></td>-->
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr><td colspan="5">No questions were found.</td></tr>
        <?php endif; ?>

        </tbody>
    </table>
<?php

echo '<h3>Recommended Sets Table</h3>';
global $wpdb;

$sql = "
		SELECT * FROM `wp_recommended_sets`;
	";
$objects = $wpdb->get_results($sql);
$num_objects = $wpdb->num_rows;
?>

<table class="widefat post fixed" cellspacing="0" style="clear: none;">
    <thead>
    <tr>
        <th scope="col" class="manage-column column-title" >ID</th>
        <th scope="col" class="manage-column column-title">Question ID</th>
        <th scope="col" class="manage-column column-title" >Text</th>
        <th scope="col" class="manage-column column-title" >Links</th>
        <th scope="col" class="manage-column column-title" >Is Last</th>
        <th scope="col" class="manage-column column-title" >&nbsp;</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th scope="col" class="manage-column column-title" >ID</th>
        <th scope="col" class="manage-column column-title">Question ID</th>
        <th scope="col" class="manage-column column-title" >Text</th>
        <th scope="col" class="manage-column column-title" >Links</th>
        <th scope="col" class="manage-column column-title" >Is Last</th>
        <th scope="col" class="manage-column column-title" >&nbsp;</th>
    </tr>
    </tfoot>
    <tbody>
    <?php
    if($num_objects>0):
        foreach($objects as $object): ?>
            <tr>
                <td>
                    <?php echo $object->id; ?>
                </td>
                <td>
                    <?php echo $object->question_id; ?>
                </td>
                <td>
                    <?php echo ($object->text) ? $object->text : 'n\a'; ?>
                </td>
                <td>
                    <?php echo ($object->links) ? $object->links : 'n\a'; ?>
                </td>
                <td>
                    <?php echo $object->is_last; ?>
                </td>
                <!--   <td><a id="edit_button" href="admin.php?page=custom-panel&amp;action=editQuestion&amp;id=<?php //echo $object->id; ?>">edit</a> | <a href="admin.php?page=custom-panel&amp;action=deleteQuestion&amp;id=<?php //echo $object->id; ?>">delete</a></td>-->
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr><td colspan="5">No sets were found.</td></tr>
    <?php endif; ?>

    </tbody>
</table>
<?php
}

/*
function editQuestion($id){
    global $wpdb;

    if((isset($_POST['edited_text']) && !empty($_POST['edited_text'])) || (isset($_POST['edited_value']) && !empty($_POST['edited_value'])))
    {
        $sql = 'UPDATE wp_test_questions SET question_text="'.$_POST['edited_text'].'", link="'.$_POST['edited_link'].'", set_name="'.$_POST['edited_set_name'].'" WHERE id='.$id.';';//die(var_dump($sql));
        $wpdb->query($sql);
    }
    else{
    $sql = "SELECT * FROM `wp_test_questions` WHERE `id`=".$id.";";
    $object = $wpdb->get_results($sql);
        foreach($object as $o)
        {
            $text = $o->question_text;
            $link = $o->link;
            $set_name = $o->set_name;
        }

    echo
        '<div id="dialog" title="Edit Question">
        <div class="wrap">
        <h2>Questions Text For Test App</h2>

        <form method="post" action="admin.php?page=custom-panel&action=editQuestion&id='.$id.'">
        <table class="form-table">

        <tr valign="top">
        <th scope="row">Question</th>
        <td><input type="text" name="edited_text" value="'.$text.'" /></td>
        </tr>

        <tr valign="top">
        <th scope="row">Link</th>
        <td><input type="text" name="edited_link" value="'.$link.'" /></td>
        </tr>

        <tr valign="top">
        <th scope="row">Set Name</th>
        <td><input type="text" name="edited_set_name" value="'.$set_name.'" /></td>
        </tr>

        </table>

        <p class="submit">
        <input type="submit" class="button-primary" value="Save" />
        </p>

        </form>
        </div>
    </div>';
    }
}

function deleteQuestion($id){
    global $wpdb;

    if(isset($_POST['deleted']) && !empty($_POST['deleted']))
    {
        $sql = 'DELETE FROM wp_test_questions WHERE id='.$id.';';
        $wpdb->query($sql);
    }
    else{
        echo
        '<div id="dialog" title="Delete Question">
        <div class="wrap">
        <h2>Are you sure you wish to remove this question from the database?</h2>

        <form method="post" action="admin.php?page=custom-panel&action=deleteQuestion&id='.$id.'">
        <input class="hidden" value="1" name="deleted">
        <p class="submit">
        <input type="submit" class="button-primary" value="Delete" />
        </p>

        </form>
        </div>
    </div>';
    }
}
*/
//create tabl for Test App
function createTables(){
    global $wpdb, $table_prefix;
    $table_name = $wpdb->prefix . "test_questions";

    $sql = "
		CREATE TABLE IF NOT EXISTS `".$table_name."` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`question_text` text DEFAULT NULL,
			`parent_id` int(11) NOT NULL,
			`question_number` int(11) NOT NULL,
			PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
	";
    $wpdb->query($sql);

    $table_name = $wpdb->prefix . "recommended_sets";

    $sql = "
		CREATE TABLE IF NOT EXISTS `".$table_name."` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`question_id` int(11) NOT NULL,
			`text` text DEFAULT NULL,
			`links` text DEFAULT NULL,
			`is_last` int(11) NOT NULL,
			PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
	";
    $wpdb->query($sql);

}
?>

