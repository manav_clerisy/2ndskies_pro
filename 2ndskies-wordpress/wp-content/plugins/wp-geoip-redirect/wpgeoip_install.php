<?php
/**
 * Add Database Table `grules`
 */
function wpgeoip_install()
{
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	
	global $wpdb;
	
	$sql = 'CREATE TABLE IF NOT EXISTS `'.$wpdb->prefix.'grules` (
  `ruleID` int(10) unsigned NOT NULL auto_increment,
  `countryID` varchar(255) NOT NULL,
  `targetURL` varchar(255) NOT NULL,
  `catID` int(10) NOT NULL,
  `postID` int(10) NOT NULL,
  `home_rule` int(1) NOT NULL,
  PRIMARY KEY  (`ruleID`)
)DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
	';
	
	dbDelta($sql);
	
	$sql = "CREATE TABLE IF NOT EXISTS `wpgeoip_log` (
                `logID` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
                `post` VARCHAR( 255 ) NOT NULL ,
                `message` VARCHAR( 255 ) NOT NULL
                )DEFAULT CHARSET=utf8 ;";
     
    dbDelta($sql);
	
	add_option('wpgeoip_mass_redirect', '0');
	add_option('wpgeoip_mass_url', 'http://');
}

/**
 * Remove Database Table `grules`
 */
function wpgeoip_uninstall()
{
	global $wpdb;
	$wpdb->query("DROP TABLE IF EXISTS `".$wpdb->prefix."grules`");
	$wpdb->query("DROP TABLE IF EXISTS `wpgeoip_log`");
	delete_option('wpgeoip_mass_redirect');
	delete_option('wpgeoip_mass_url');
}
