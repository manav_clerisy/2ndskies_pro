<?php

function wpgeoip_ipexclusion() {

global $wpdb;
$excluded = get_option('wpgeoip_excluded', '');

?>
<div id="wrap">
	<br />
    <img src="<?= plugin_dir_url(__FILE__) ?>/assets/images/icon32x32.png" style="float:left;"/> 
	<h2 style="float:left;margin-top: 10px;margin-left:10px;">Excluded IP Addresses</h2>
    <div style="clear:both;"></div>
    <hr />
	
	<?php
	if(isset($_POST['sbExclude']))
	{
		update_option('wpgeoip_excluded', $_POST['excluded_redirect']);
		print '<div class="updated bellow-h2">Settings Updated</div>';
		$excluded = get_option('wpgeoip_excluded');
	}
	?>

	<div class="updated below-h2">
	<strong>AVOID REDIRECTS:</strong> Here you can enter as many IP addresses as you want to skip redirection rules.<br />
	Each IP address must be entered on a new line.
	</div>
	<br />

	<form method="post">
	Enter one IP address per line:
	<br />

	<textarea name="excluded_redirect" rows="20" cols="50"><?= $excluded ?></textarea>

	<br /><br />
	<input type="submit" name="sbExclude" value="Update Settings" class="button"/>
	</form>
</div>
<?php	


}