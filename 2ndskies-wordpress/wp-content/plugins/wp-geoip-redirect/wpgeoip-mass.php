<?php
/**
 * Function to MASS Redirect All Countries without Rules
 */
function wpgeoip_mass_redirect()
{
	global $wpdb;
	$enabled = get_option('wpgeoip_mass_redirect');
	$targetURL = get_option('wpgeoip_mass_url');
?>
<div id="wrap">
	<br />
    <img src="<?= plugin_dir_url(__FILE__) ?>/assets/images/icon32x32.png" style="float:left;"/> 
	<h2 style="float:left;margin-top: 10px;margin-left:10px;">Mass Redirect For Countries Without Rules</h2>
    <div style="clear:both;"></div>
    <hr />
	<br />
	
	<?php
	if(isset($_POST['sbMass']))
	{
		update_option('wpgeoip_mass_redirect', $_POST['mass_redirect']);
		update_option('wpgeoip_mass_url', $_POST['mass_url']);
		print '<div class="updated bellow-h2">Settings Updated</div>';
		$enabled = get_option('wpgeoip_mass_redirect');
		$targetURL = get_option('wpgeoip_mass_url');
	}
	?>
	
	<form method="post">
		Enable This Feature ? <input type="radio" name="mass_redirect" value="0" <?php if($enabled == "0") print 'checked'; ?>/> No 
							<input type="radio" name="mass_redirect" value="1" <?php if($enabled == "1") print 'checked'; ?>/> Yes
	    <br/>
	    Target URL : <input type="text" name="mass_url" value="<?php print $targetURL; ?>"/><br/>
	    <input type="submit" name="sbMass" value="Update Settings" class="button"/>
	</form>
</div>
<?php	
}
