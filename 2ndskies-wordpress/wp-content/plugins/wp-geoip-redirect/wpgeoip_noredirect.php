<?php

function wpgeoip_noredirect() {

global $wpdb;
$enabled = get_option('wpgeoip_no_redirect', 0);

?>
<div id="wrap">
	<br />
    <img src="<?= plugin_dir_url(__FILE__) ?>/assets/images/icon32x32.png" style="float:left;"/> 
	<h2 style="float:left;margin-top: 10px;margin-left:10px;">No Redirect</h2>
    <div style="clear:both;"></div>
    <hr />
	
	<?php
	if(isset($_POST['sbNoredirect']))
	{
		update_option('wpgeoip_no_redirect', $_POST['no_redirect']);
		print '<div class="updated bellow-h2">Settings Updated</div>';
		$enabled = get_option('wpgeoip_no_redirect');
	}
	?>

	<div class="updated below-h2">
	Append <strong>?noredirect=true</strong> to any URL to avoid being redirected.<br />
	<em>Example: <?php bloginfo('url') ?>/page/?noredirect=true</em>
	</div>
	<br />

	<form method="post">
	Enable <strong>?noredirect=true</strong> GET parameter?<br />
	<br />

	<input type="radio" name="no_redirect" value="0" <?php if($enabled == "0") print 'checked'; ?>/> No 
	<input type="radio" name="no_redirect" value="1" <?php if($enabled == "1") print 'checked'; ?>/> Yes

	<br /><br />
	<input type="submit" name="sbNoredirect" value="Update Settings" class="button"/>
	</form>
</div>
<?php	


}