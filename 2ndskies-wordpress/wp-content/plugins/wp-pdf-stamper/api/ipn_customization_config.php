<?php

//This is where a notification email will be sent after a purchase if you specify an email address below. Example eamil address: jon.doe@hotmail.com
$seller_eamil_address = "";
//Seller email subject
$seller_email_subject = "Notification of product sale"; 
//Seller email body. The {buyer_email} will be dynamically replaced with the actual buyer email. 
$seller_email_body = "Dear Seller".
			  "\n\nThis mail is to notify you of a product sale.".
			  "\n\nThe following email was sent to the buyer:".
			  "\n{buyer_email}".
			  "\n\nThanks";	

//If you want to stamp a custom PDF file regardless of what is getting passed via the custom field then
//specify the PDF file's URL (example: http://www.example.com/myfiles/product1.pdf) in the following variable
$pdf_stamper_paypal_custom_file = "";
