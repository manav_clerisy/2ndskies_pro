<?php
$pdf_stamper_clickbank_items = array(
'1' => 'http://www.example.com/myfiles/product1.pdf',
'2' => 'http://www.example.com/myfiles/product2.pdf',
'3' => 'http://www.example.com/myfiles/product3.pdf,http://www.example.com/myfiles/product4.pdf'
);

$pdf_stamper_clickbank_success_message = "Purchase processed successfully! An email has been sent to your email address.";
$pdf_stamper_clickbank_failure_message = "An error occured while trying to process the sale! Error details: ";

/*** Do not edit below this line ***/
$wp_ps_config = PDF_Stamper_Config::getInstance();

//The from email address that will be used to send emails to the buyer. By default it will use the paypal receiver email address.
$pdf_stamper_clickbank_from_email = $wp_ps_config->getValue('wp_pdf_stamper_from_email_address');

//Email subject
$pdf_stamper_clickbank_subject = $wp_ps_config->getValue('wp_pdf_stamper_buyer_email_subj');

//The buyer email body. The first name, last name and stamped file's URL will be replaced with the text with braces {} 
$pdf_stamper_clickbank_buyer_email_body = $wp_ps_config->getValue('wp_pdf_stamper_buyer_email_body');
