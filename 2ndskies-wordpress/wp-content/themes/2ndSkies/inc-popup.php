<style type="text/css">
@import url(//fonts.googleapis.com/css?family=Open+Sans:400,700,600,300);
@import url(//fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700);

.left {float: left;}
.right {float: right;}
.clear {display: block; height: 0; width: 0; clear: both;}
.clearfix:before,
.clearfix:after {content: ""; display: table;}
.clearfix:after {clear: both;}
.l-align-left {text-align: left;}
.l-align-center {text-align: center;}
.l-text-uppercase {text-transform: uppercase;}
.l-stacked {list-style-type: none; margin: 0; padding: 0;}
.l-stacked li {display: block;}
.moonray-form-clearfix .g-100, .moonray-form-clearfix .g-50, .moonray-form-clearfix .g-33, .moonray-form-clearfix .g-66 {
    float: left;
    padding-left: 10px !important;
    padding-right: 10px !important;
    overflow: hidden;
}
.moonray-form-clearfix .g-100 {width: 100% !important;}
.moonray-form-clearfix .g-50 {width:50% !important;}
.moonray-form-clearfix .g-33 {width:33.3333% !important;}
.moonray-form-clearfix .g-66 {width:66.6666% !important;}

.my-mfp-zoom-in .zoom-anim-dialog{opacity:0;-webkit-transition:all 0.2s ease-in-out;-moz-transition:all 0.2s ease-in-out;-o-transition:all 0.2s ease-in-out;transition:all 0.2s ease-in-out;-webkit-transform:scale(0.8);-moz-transform:scale(0.8);-ms-transform:scale(0.8);-o-transform:scale(0.8);transform:scale(0.8);}
.my-mfp-zoom-in.mfp-ready .zoom-anim-dialog{opacity:1;-webkit-transform:scale(1);-moz-transform:scale(1);-ms-transform:scale(1);-o-transform:scale(1);transform:scale(1);}
.my-mfp-zoom-in.mfp-removing .zoom-anim-dialog{-webkit-transform:scale(0.8);-moz-transform:scale(0.8);-ms-transform:scale(0.8);-o-transform:scale(0.8);transform:scale(0.8);opacity:0;}
.my-mfp-zoom-in.mfp-bg{opacity:0;-webkit-transition:opacity 0.3s ease-out;-moz-transition:opacity 0.3s ease-out;-o-transition:opacity 0.3s ease-out;transition:opacity 0.3s ease-out;}
.my-mfp-zoom-in.mfp-ready.mfp-bg{opacity:0.55;}
.my-mfp-zoom-in.mfp-removing.mfp-bg{opacity:0;}
.pop-call, .pop-call *{-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;}
.pop-call {
    position: relative;
    top: 50%;
    padding: 0;
    margin: auto;
    max-width: 670px;
    background-color: #cfdbe3;
    -webkit-transform: translateY(-50%);
    -moz-transform: translateY(-50%);
    -o-transform: translateY(-50%);
    -ms-transform: translateY(-50%);
    transform: translateY(-50%);
    -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1);
    -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1);
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1);
}
.pop-call .mfp-close {
    position: absolute;
    right: 2px !important;
    top: 2px !important;
    height: 26px;
    width: 26px;
    text-indent: -9999px;
    font: normal 22px/24px Arial, Baskerville, monospace;
    color: #5d7a8d;
    opacity: 1;
    background: #cfdbe3 url('<?php echo THEME_IMAGES ?>/ics-dark.png') center center no-repeat !important;
    border: 1px solid #5d7a8d;
    border-radius: 0 !important;
    overflow: hidden;
}
.pop-call .mfp-close:active,
.pop-call .mfp-close:hover {
    color: #cfdbe3;
    background: #5d7a8d url('<?php echo THEME_IMAGES ?>/ics.png') center center no-repeat !important;
}
.home .mfp-bg, .page-template-template-home-php .mfp-bg {opacity:0.55;}
/* popup header */
.popup-header {
    position: relative;
    padding: 30px;
    text-align: center;
    background-color: #cfdbe3;
}
.popup-header--title {
    margin-top: 0;
    margin-bottom: 0;
    font: 600 36px/37px 'Open Sans', sans-serif;
    color: #203b5b;
}
.popup-header--subtitle {
    margin-top: 15px;
    margin-bottom: 0;
    font: 400 27px/27px 'Open Sans', sans-serif;
    color: #5d7a8d;
}
/* popup body */
.popup-body {
    position: relative;
    padding: 40px 35px 15px 35px;
    text-align: center;
    background: #eaf4f8 url('<?php echo THEME_IMAGES ?>/popup-body_bg.png') bottom no-repeat;
}
.popup-body .image {
    display: inline-block;
    height: auto;
    width: 235px;

}
@media only screen and (min-width:768px) {
    .popup-body .image {
        float: left;
    }
    .popup-body--inner {
        float: right;
        width: 350px;
    }
}
.popup-body--inner {

}
.popup-body--inner .title {
    margin-bottom: 20px;
    font: 700 16px/22px 'Open Sans', sans-serif;
    color: #28476c;
}
.popup-body--inner .list li {
    position: relative;
    margin-bottom: 25px;
    padding-left: 35px !important;
    font: 400 16px/20px 'Open Sans', sans-serif;
    color: #000;
}
.popup-body--inner .list li:before {
    display: inline-block;
    content: '';
    position: absolute;
    top: 5px;
    left: 0;
    width: 23px;
    height: 23px;
    background: url('<?php echo THEME_IMAGES ?>/check.png') 0 0 no-repeat;
}
.popup-body--inner .list li:first-child:before {top: 0;}
/* popup footer */
.popup-footer {
    position: relative;
    padding: 30px;
    background-color: #203b5b;
}
.popup-footer--title {
    margin-bottom: 0;
    text-align: center;
    color: #FFF;
}
.popup-footer--title small {
    display: block;
    margin-top: 0;
    margin-bottom: 20px;
    font: 500 16px/27px 'Open Sans', sans-serif;
}
.popup-footer .moonray-form-p2c26856f1 .moonray-form {background: none !important;}
.popup-footer .moonray-form-p2c26856f1 .moonray-form {
    width: auto;
    background: none;
    border: 0;
}
.popup-footer .moonray-form-p2c26856f1 .moonray-form-element-wrapper {
    display: block;
    float: none;
    width: auto;
    padding: 0;
}
.popup-footer label {display: none;}
.popup-footer input[type="text"],
.popup-footer input[type="email"] {
    margin-bottom: 20px !important;
    width: 100% !important;
    height: 35px !important;
    padding: 5px 10px !important;
    font: 400 15px/25px 'Source Sans Pro', sans-serif !important;
    color: #bac7dd !important;
    border: 1px solid #657B9E !important;
    border-radius: 4px !important;
    background: #1e2c41 !important;
}
.popup-footer input[type="text"].moonray-form-state-error,
.popup-footer input[type="email"].moonray-form-state-error {
    color: #F00 !important;
    border-color: #F00 !important;
}
.popup-footer input[type="text"]::-webkit-input-placeholder,
.popup-footer input[type="email"]::-webkit-input-placeholder {
    padding-top: 0;
    padding-bottom: 5px;
    font: 400 15px/25px 'Source Sans Pro', sans-serif;
    color: #b9c7dd;
}
.popup-footer input[type="text"]:-moz-placeholder,
.popup-footer input[type="email"]:-moz-placeholder {
    padding-top: 0;
    padding-bottom: 5px;
    font: 400 15px/25px 'Source Sans Pro', sans-serif;
    color: #b9c7dd;
}
.popup-footer input[type="text"]::-moz-placeholder,
.popup-footer input[type="email"]::-moz-placeholder {
    padding-top: 0;
    padding-bottom: 5px;
    font: 400 15px/25px 'Source Sans Pro', sans-serif;
    color: #b9c7dd;
}
.popup-footer input[type="text"]:-ms-input-placeholder,
.popup-footer input[type="email"]:-ms-input-placeholder {
    padding-top: 0;
    padding-bottom: 5px;
    font: 400 15px/25px 'Source Sans Pro', sans-serif;
    color: #b9c7dd;
}
.popup-footer input[type="text"]:focus,
.popup-footer input[type="email"]:focus {border: 1px solid #FFF !important;}
.popup-footer--privacy {
    display: inline-block;
    margin-top: 10px;
    padding-left: 20px;
    font: 400 15px/15px 'Source Sans Pro', sans-serif;
    color: #FFF;
    background: url(<?php echo THEME_IMAGES ?>/lock.png) left center no-repeat;
}
.popup-footer .moonray-form-p2c26856f1 .moonray-form-input-type-submit .moonray-form-input {
    padding: 10px 20px;
    font: 700 19px/23px 'Open Sans', sans-serif;
    color: #303030;
    text-shadow: 0px 1px 0px rgba(255, 255, 255, 0.69);
    border-radius: 3px;
    background: -webkit-linear-gradient(90deg, #ffa800 0%, #ffb600 47%, #ffc600 50%);
    background: -moz-linear-gradient(90deg, #ffa800 0%, #ffb600 47%, #ffc600 50%);
    background: -o-linear-gradient(90deg, #ffa800 0%, #ffb600 47%, #ffc600 50%);
    background: -ms-linear-gradient(90deg, #ffa800 0%, #ffb600 47%, #ffc600 50%);
    background: linear-gradient(0deg, #ffa800 0%, #ffb600 47%, #ffc600 50%);
    border: 0;
    white-space: normal;
}

@media only screen and (max-width:767px) {
    .moonray-form-clearfix .g-66, .moonray-form-clearfix .g-50, .moonray-form-clearfix .g-33 {
        display: block !important;
        width:100% !important;
    }
    .popup-footer .moonray-form form {display: block !important;}
}
</style>

<div id="popup" class="pop-call mfp-hide animated animated-Delay-1s fadeInDown ">
    <div class="popup-header">
        <h3 class="popup-header--title">Need To Improve You Trading?</h3>
    </div>
    <div class="popup-body clearfix">
        <img class="image" src="<?php echo THEME_IMAGES ?>/book-ipad.png" alt="Book-iPad" width="267" height="261" />
        <div class="popup-body--inner l-align-left">
        	<p class="title">Sign up now to receive a free ebook on How to Get an Edge trading the Forex markets</p>
            <ul class="list l-stacked">
            	<li><i></i> Learn what it Means to Have an Edge</li>
                <li><i></i> Learn what are the Key Moves in the Market</li>
                <li><i></i> Discover an Intra-day Trading Tool For Precise Entries & Exits</li>
            </ul>
        </div>
    </div>
    <div class="popup-footer l-align-center">
     	<p class="popup-footer--title">
            <small>As a bonus for signing up, you will also get exclusive access to our monthly newsletter, which contains insights not published on the website</small>
        </p>
		<div class="moonray-form-p2c26856f1 ussr">
            <div class="moonray-form moonray-form-label-pos-stacked">
                <form class="moonray-form-clearfix" action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
                    <div class="g-50">
                        <div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-text"><label for="mr-field-element-153568200068" class="moonray-form-label"></label><input name="firstname" type="text" class="moonray-form-input" id="mr-field-element-153568200068" required value="" placeholder=" Your Name"/></div>
                    </div>
					<div class="g-50">
                        <div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-email"><label for="mr-field-element-558496658457" class="moonray-form-label"></label><input name="email" type="email" class="moonray-form-input" id="mr-field-element-558496658457" required value="" placeholder="Email Address"/></div>
					</div>
                    <div class="g-100">
                        <div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-submit"><input type="submit" name="submit-button" value="Yes, I Need To Improve My Trading" class="moonray-form-input" id="mr-field-element-520094126695" src/></div>
                    </div>
                    <div class="clear"></div>
                    <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="afft_" type="hidden" value=""/></div>
					<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="aff_" type="hidden" value=""/></div>
					<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="sess_" type="hidden" value=""/></div>
					<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="ref_" type="hidden" value=""/></div>
					<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="own_" type="hidden" value=""/></div>
					<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="oprid" type="hidden" value=""/></div>
					<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="contact_id" type="hidden" value=""/></div>
					<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_source" type="hidden" value=""/></div>
					<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_medium" type="hidden" value=""/></div>
					<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_term" type="hidden" value=""/></div>
					<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_content" type="hidden" value=""/></div>
					<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_campaign" type="hidden" value=""/></div>
					<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="referral_page" type="hidden" value=""/></div>
					<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="uid" type="hidden" value="p2c26856f1"/></div>
                </form>
			</div>
		</div>
        <a class="popup-footer--privacy" href="/privacy-policy/">We respect your privacy</a>
    </div>
</div>