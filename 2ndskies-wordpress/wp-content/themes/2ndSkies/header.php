<!DOCTYPE HTML>
<html lang="en-US">
<head>
<script src="https://cdn.optimizely.com/js/5767624972.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1 " />
	<meta http-equiv="Content-type" content="text/html;charset=utf-8">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
		<link rel="profile" href="//gmpg.org/xfn/11" />
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?t=98765" />
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_url' ); ?>/stylenew.css" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo( 'template_url' ); ?>/favicon.ico">
    <link rel="stylesheet" href="//app.ontraport.com/js/formeditor/moonrayform/paymentplandisplay/production.css" type="text/css" />
    <link rel="stylesheet" href="//forms.ontraport.com/formeditor/formeditor/css/form.default.css" type="text/css" />
    <link rel="stylesheet" href="//forms.ontraport.com/formeditor/formeditor/css/form.publish.css" type="text/css" />
    <link rel="stylesheet" href="//forms.ontraport.com/v2.4/include/minify/?g=moonrayCSS" type="text/css" />
    <link rel="stylesheet" href="//forms.ontraport.com/v2.4/include/formEditor/gencss.php?uid=p2c26856f1" type="text/css" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>


    <?php wp_head(); ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="//forms.ontraport.com/v2.4/include/formEditor/genjs-v3.php?html=false&uid=p2c26856f1"></script>
    <!-- Start Visual Website Optimizer Asynchronous Code -->
    <script type='text/javascript'>
    var _vwo_code=(function(){
    var account_id=61633,
    settings_tolerance=2000,
    library_tolerance=2500,
    use_existing_jquery=false,
    // DO NOT EDIT BELOW THIS LINE
    f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
    </script>
    <!-- End Visual Website Optimizer Asynchronous Code -->
</head>
<body <?php body_class("customize-support"); ?>>
<!-- START #wrapper -->
<div id="wrapper" class="customize-support">
    <div id="header">
        <div class="center-container header-container">
            <a class="member-login mobile-hide" href="http://courses.2nd-skies-forex.com/user/login">Member Login</a>

            <?php //dynamic_sidebar('social-networks-sidebar'); ?>

            <!-- social-networks-sidebar -->
            <?php get_search_form(); ?>
            <!-- search form -->
            <div class="subscribe-form">
                <div class="moonray-form-p2c26856f1 ussr"><div class="moonray-form moonray-form-label-pos-stacked">
                        <a href="#" class="btn-subscribe">sign up for newsletter</a>
                        <form class="moonray-form-clearfix" action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8" novalidate>
                            <div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-text"><label for="mr-field-element-153568200068" class="moonray-form-label"></label><input name="firstname" type="text" class="moonray-form-input" id="mr-field-element-153568200068" required value="" placeholder=" Your Name"/></div>
                            <div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-email"><label for="mr-field-element-558496658457" class="moonray-form-label"></label><input name="email" type="email" class="moonray-form-input" id="mr-field-element-558496658457" required value="" placeholder="Email Address"/></div>
                            <div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-submit"><input type="submit" name="submit-button" value="Sign Up for Newsletter" class="moonray-form-input" id="mr-field-element-520094126695" src/></div>
                            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="afft_" type="hidden" value=""/></div>
                            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="aff_" type="hidden" value=""/></div>
                            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="sess_" type="hidden" value=""/></div>
                            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="ref_" type="hidden" value=""/></div>
                            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="own_" type="hidden" value=""/></div>
                            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="oprid" type="hidden" value=""/></div>
                            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="contact_id" type="hidden" value=""/></div>
                            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_source" type="hidden" value=""/></div>
                            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_medium" type="hidden" value=""/></div>
                            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_term" type="hidden" value=""/></div>
                            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_content" type="hidden" value=""/></div>
                            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_campaign" type="hidden" value=""/></div>
                            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="referral_page" type="hidden" value=""/></div>
                            <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="uid" type="hidden" value="p2c26856f1"/></div>
                        </form>
                    </div>
                </div>
            </div><!-- .subscribe-form -->
            <a class="member-login-mob mobile-show" href="http://courses.2nd-skies-forex.com/user/login">Member Login</a>
        </div><!-- .center-container -->
    </div><!-- #header-->

    <div class="nav-holder">
        <div class="center-container">
            <strong class="logo"><a href="<?php echo home_url( '/' ); ?>"><img src="<?php echo THEME_IMAGES ?>/2ndSkies-logo.png" width="90" height="78" alt="2ndskies" /></a></strong><!-- .logo -->
            <div id="nav" class="main-navigation">
                <a href="#" class="opener">menu</a>
                <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => '', 'items_wrap' => '<ul>%3$s</ul>')); ?>
            </div> <!-- #nav .main-navigation -->
            <?php //get_search_form(); ?>

            <?php get_template_part('searchform','mobile'); ?>
            <!-- search form -->
        </div><!-- .center-container -->
    </div><!-- .nav-holder -->
