<?php 
/* Template name: Alternate Footer Template */
?>

<?php get_header(); ?>

<div id="main" class="clearfix">

    <div id="content">

        <div class="breadcrumbs-container clearfix">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="<?php echo home_url(); ?>">Home</a></li>
                    <li><?php the_title(); ?></li>
                </ul>
            </div><!-- .breadcrumbs -->
        </div><!-- .breadcrumbs-container -->

        <?php if (have_posts()) : ?>

        <?php while (have_posts()) : the_post(); ?>

        <div class="default-template">
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
        </div><!-- .default-template -->

        <?php endwhile; ?>

        <?php else : ?>

        <div class="default-template">
            <h1>Not Found</h1>
            <p>Sorry, but you are looking for something that isn't here.</p>
        </div><!-- .default-template -->

        <?php endif; ?>
		
		 <?php get_template_part('newsletter', 'box'); ?>
		 
		 <?php get_template_part('inc', 'share-print'); ?>
		 
		 <div class="comment-form-holder">
                <?php
                $args = array(
                  //  'fields' => apply_filters( 'comment_form_default_fields', $fields )
                'comment_field' => '<div class="row clearfix">
					<div class="photo">'.get_avatar('', 55).'</div>
					<div class="textfield">
					<textarea name="comment" id="comment" cols="30" rows="10" placeholder="Leave a comment...."></textarea></div></div>'
                ,'must_log_in' => '<p class="must-log-in">' .  sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>'
                ,'logged_in_as' => ''
                ,'comment_notes_before' => '<p class="comment-notes">' . __( 'Your email address will not be published.' ) . ( $req ? $required_text : '' ) . '</p>'
                ,'comment_notes_after' => ''
                ,'id_form' => 'commentform'
                ,'id_submit' => 'submit'
                ,'title_reply' => __( 'Comments on the article' )
                ,'title_reply_to' => __( 'Leave a Reply to %s' )
                ,'cancel_reply_link' => __( 'Cancel reply' )
                ,'label_submit' => __( 'Post Comment' )
                ); 
                comment_form( $args, $commentPostId );
                ?>
            </div>
        <div class="post-comments" id="comments">
            <h2 class="comments-title">Discussion</h2>
            <!-- comment form -->
            
            <?php
            
            echo '<h3>'.count($wp_query->comments).' Comments on the article</h3>';
            echo '<ul class="comment-list">';
            wp_list_comments('callback=skies_comment');
            echo '</ul>';
            ?>
        </div>

    </div><!-- #content -->
    
	
	<?php get_sidebar('custom'); ?><!-- sidebar -->
    
</div><!-- #main -->

<?php get_footer(); ?>