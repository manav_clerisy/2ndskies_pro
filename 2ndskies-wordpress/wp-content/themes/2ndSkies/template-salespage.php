<?php
/*
Template Name: Sales Page
*/
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<script src="https://cdn.optimizely.com/js/5767624972.js"></script>
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo( 'template_url' ); ?>/favicon.ico">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<meta name="format-detection" content="telephone=no" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
    <!--[if (lt IE 9) & (!IEMobile)]>
        <script src="<?php echo get_template_directory_uri(); ?>/js/fallbacks/html5shiv-3.7.2.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/fallbacks/selectivizr-1.0.2.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/sales-templ-new.min.css" />
    <!--[if (lt IE 9) & (!IEMobile)]>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css" />
        <script src="<?php echo get_template_directory_uri(); ?>/js/fallbacks/respond-1.4.2.min.js"></script>
    <![endif]-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/helpers/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/library/js/sales-new.min.js"></script>
<?php wp_head(); ?>
<!-- Start Visual Website Optimizer Asynchronous Code -->
    <script type='text/javascript'>
    var _vwo_code=(function(){
    var account_id=61633,
    settings_tolerance=2000,
    library_tolerance=2500,
    use_existing_jquery=false,
    // DO NOT EDIT BELOW THIS LINE
    f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
    </script>
    <!-- End Visual Website Optimizer Asynchronous Code -->
</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WLRNLV"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WLRNLV');</script>
<!-- End Google Tag Manager -->
    <header class="header">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img class="image" src="<?php echo get_template_directory_uri(); ?>/images/sales-new/logo.png" alt="" /></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav header--menu">
                        <li><a href="#whatyoulearn">What you learn</a></li>
                        <li><a href="#tradesetups">Trade setups</a></li>
                        <li><a href="#testimonials">Testimonials</a></li>
                        <li class="visible-lg"><a href="#" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/images/sales-new/logo.png" alt=""></a></li>
                        <li><a href="#faq">F.A.Q.</a></li>
                        <li><a href="#aboutchris">About Chris</a></li>
                        <li><a href="#buycourse">Buy course</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container -->
        </nav>
    </header>
    <main class="main">
        <section id="" class="section_hero" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/sales-new/background-promo-2.jpg')">
            <div class="l-align-center">
                <h1 class="section_hero--title">Chris Capre's Advanced Price Action Course</h1>
                <h2 class="section_hero--subtitle">Because You're Ready to be A <strong>Successful Trader</strong> - <i>Yesterday</i></h2>
                <ul class="section_hero--list l-floated clearfix">
                    <li class="section_hero--list__image col-xs-12 col-md-6 custom_padding l-align-right">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/sales-new/banner-hero-2.png" alt="" class="image" />
                    </li>
                    <li class="col-xs-12 col-md-6 custom_padding l-align-left">
                        <ul class="section_hero--inner_custom_list"><br><br>
                            <li>Sharpen Your Trading Edge</li>
                            <li>Improve Your Day-To-Day Performance</li>
                            <li>Increase Your Confidence, Discipline & Consistency</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </section><!-- /.section_hero -->
        <section class="section_promo">
            <div class="container no_padding">
                <div class="col-xs-12 col-md-6">
                    <p class="quote">
                        <q><strong>Elite performers</strong> put more into training than the actual event. After this course, <strong>you will never see price action the same. Become</strong> the next elite trader.</q>
                    </p>
                    <h5 class="author">Chris Capre</h5>
                    <h6 class="occupation">Founder of 2ndSkiesForex</h6>
                </div>
                <div class="col-xs-12 col-md-6 l-align-right">
                    <div class="video-container">
                        <iframe width="560" height="300" src="https://www.youtube.com/embed/9IwfmELGFe8" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="testimonial" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/sales-new/background-testimonials-2.png');">
                    <p>Before you I couldn't make successful trades after trying for months. Now I am so consistent trading the EURUSD it has totally changed my trading.</p>
                    <div class="author">Andrew D</div>
                </div><!-- /.testimonial -->
                <h3 id="" class="section-promo--title">You want to make money trading. You have a passion for it and becoming successful. Recognize the opportunity and see the challenge in this.</h3>
                <p class="section-promo--description">
                    Likely you've been working hard to trade profitably, but find yourself struggling to produce consistent results. You understand something is missing.
                </p>
                <div class="row">
                    <div class="section_promo--columns clearfix">
                        <div class="col-xs-12 col-md-6">
                            <ul class="section_hero--inner_custom_list">
                                <li>Maybe you can't seem to find consistency in your trading</li>
                                <li>Perhaps you are in a draw down and not sure what to do</li>
                                <li>Many times the stress of being a trader has caused you to make mistakes </li>
                            </ul>
                        </div>
                        <div class="section_promo--columns__image col-xs-12 col-md-6">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/sales-new/banner-testimonials-2.png" alt="" class="image" />
                        </div>
                    </div><!-- /.section_promo--columns -->
                    <div class="section_promo--columns clearfix">
                        <div class="section_promo--columns__image col-xs-12 col-md-5" style="padding-top: 20px;">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/sales-new/banner-testimonials-2-2.png" alt="" class="image" />
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <p class="section-promo--description" style="margin-top:-5px;">
                                Most courses just provide 'information'. If you want to survive the learning curve most traders fail at, you have to go beyond an 'informational course'.<br /><br />
                                <strong>Our course is completely different.</strong> We focus on <strong>developing skills</strong>, have students engage in <strong>structured practice</strong>, then tackle real-world situations.<br /><br />
                                We are a <strong>'skill-based course'</strong> that is solution focused, which is why we differ from all the other 'courses' out there.<br /><br />
                                Through this training, hard work and practice, <strong>you can achieve your trading goals.</strong><br /><br />
                                Those who take this training on in full make continual progress, see changes in their mindset, and start to trade profitably.
                            </p>
                        </div>
                    </div><!-- /.section_promo--columns -->
                </div>
                <div class="testimonial" style="margin-top:30px; background-image: url('<?php echo get_template_directory_uri(); ?>/images/sales-new/background-testimonials-2.png');">
                    <p>I thought I knew how to trade price action. After watching your course videos, I can now say with confidence I am really learning how to trade with price action.</p>
                    <div class="author">Ali N</div>
                </div><!-- /.testimonial -->
                <h3 id="whatyoulearn" class="section-promo--title">Register today & you get immediate access to:</h3>
                <div class="row">
                    <div class="section_promo--columns clearfix" style="margin-bottom: 0;">
                        <div class="section_promo--columns__image col-xs-12 col-md-5">
                            <img class="image" src="<?php echo get_template_directory_uri(); ?>/images/sales-new/banner-testimonials-2-3.png" alt="" />
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <h4 class="section-promo--subtitle">45+ Hours of Price Action Course Videos</h4>
                            <p class="section-promo--description">You get access to over 40+ videos packed with strategies, examples and advanced price action techniques. Each one is clear and specific. You'll get to watch and see exactly how I trade my own money using these exact same strategies.</p>
                        </div>
                    </div><!-- /.section_promo--columns -->
                    <div id="tradesetups" class="section_promo--columns clearfix">
                        <div class="section_promo--columns__image col-xs-12 col-md-5">
                            <img class="image" src="<?php echo get_template_directory_uri(); ?>/images/sales-new/banner-testimonials-2-4.png" alt="" />
                        </div>
                        <div class="col-xs-12 col-md-7" style="padding-top: 20px;">
                            <h4 class="section-promo--subtitle">Trade Setups Commentary + Trader Quizzes</h4>
                            <p class="section-promo--description">3x per week you'll get access to my <strong>trade setups ideas and price action analysis</strong> on the market. I cover the <strong>price action context</strong>, what <strong>key levels</strong> I'm watching, my trend bias (bullish or bearish), and entry locations.
<br>
<br>
Most members use this not only to profit from the trade ideas, but to compare what they're doing vs. what I'm seeing in the market.
<br>
<br>
In addition to this, I do two <strong>weekly trader quizzes</strong> to test your knowledge and price action skills in the current market.</p>
                        </div>
                    </div><!-- /.section_promo--columns -->
                </div>
                <div class="testimonial" style="margin-top: 40px; background-image: url('<?php echo get_template_directory_uri(); ?>/images/sales-new/background-testimonials-2.png');">
                    <p>It's absolutely amazing to see how close the the market behaved today when compared with your Daily Commentary. Your ability to read the price action is inspirational!</p>
                    <div class="author">Zoran V</div>
                </div><!-- /.testimonial -->
                <div class="row">
                    <div class="section_promo--columns clearfix">
                        <div class="section_promo--columns__image col-xs-12 col-md-5">
                            <img class="image" src="<?php echo get_template_directory_uri(); ?>/images/sales-new/banner-testimonials-2-5.png" alt="" />
                        </div>
                        <div class="col-xs-12 col-md-7" style="padding-top: 45px;">
                            <h4 class="section-promo--subtitle">Live Trade Setups Forum</h4>
                            <p class="section-promo--description">Myself, senior traders and forum members post trade setups in the members area, so you always have lots of eyes on the market.<br/>We trade forex, global indices and commodities. Look for specific trade setups based on specific strategies or instruments.</p>
                        </div>
                    </div><!-- /.section_promo--columns -->
                    <div class="section_promo--columns clearfix">
                        <div class="section_promo--columns__image col-xs-12 col-md-5" style="padding-top: 40px;">
                            <img class="image" src="<?php echo get_template_directory_uri(); ?>/images/sales-new/banner-testimonials-2-6.png" alt="" />
                        </div>
                        <div class="col-xs-12 col-md-7">
                            <h4 class="section-promo--subtitle">Develop a Winning Trading Psychology</h4>
                            <p class="section-promo--description">I've been studying Neuroscience for the past two decades, meditating every day for 15 of those years, and completed a 1 year meditation retreat.<br /><br />

<strong>No other trading mentor has this unique combination of training and experience</strong>, which I share with you on how to develop a <strong>winning trading psychology</strong>.<br /><br />

Just think - <strong>a 1% difference in your mindset</strong> could <strong>drastically increase your profits</strong>.
                            </p>
                        </div>
                    </div><!-- /.section_promo--columns -->
                </div>
<section id="" class="section_coursebenefits">
            <div class="container">
                <div class="section_coursebenefits__bordered">
                    <h3 class="section_coursebenefits--title l-align-center l-text-capitalize" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/sales-new/background-benefits.jpg');"><span>What you'll get in this course</span></h3>
                    <div class="section_coursebenefits--list--wrap">
                        <ul class="section_coursebenefits--list l-floated clearfix">
                            <li><span>Learn to Trade With the Trend</span></li>
                            <li><span>Building Successful Trading Psychology</span></li>
                            <li><span>Identify Order Flow Behind the Price Action</span></li>
                            <li><span>How to Prepare for Your Trading Day</span></li>
                            <li><span>Identify Critical Support & Resistance Levels</span></li>
                            <li><span>Building Expert Trading Skills</span></li>
                            <li><span>Identify the Most Important moves in Market</span></li>
                            <li><span>Learn How to Gauge Your Growth</span></li>
                            <li><span>Know When High-Probability Breakouts are Forming</span></li>
                            <li><span>3-Dimensional Risk Management</span></li>
                        </ul>
                    </div>
                    <div class="more l-align-center">and 30+ other lessons</div>
                </div>
            </div>
        </section><!-- /.section_coursebenefits -->

                <div class="testimonial" style="margin-top: 10px; background-image: url('<?php echo get_template_directory_uri(); ?>/images/sales-new/background-testimonials-2.png');">
                    <p>This training has far surpassed my expectations. I don't beat myself up anymore when I make a mistake. My negative self talk & fear/doubt/worry has pretty much stopped. I never thought I'd get past these mental hurdles.</p>
                    <div class="author">Dom D</div>
                </div><!-- /.testimonial -->
            </div>
        </section><!-- /.section_promo -->
        <section id="testimonials" class="section_membership">
            <div class="container">
                <h3 class="section-membership--title l-align-center">Your Membership Includes:</h3>
                <ul class="section-membership--list l-unstyled clearfix">
                    <li class="col-xs-12 col-md-6 custom_padding-left">
                        <div class="section-membership--package clearfix">
                            <div class="column col-xs-6 no_padding">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/sales-new/banner-membership-2.png" alt="">
                            </div>
                            <div class="column col-xs-6 no_padding l-align-center">
                                <h4 class="title">Private Member Webinars</h4>
                            </div>
                        </div>
                        <div class="testimonial" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/sales-new/background-testimonials-2.png');">
                            <p>This webinar has been the biggest mind blowing moment in my quest to become a successful trader. I was always waiting for certain price patterns & signals that never formed, missing the boat for hundreds and hundreds of pips when the market runs. I am so glad I joined your course.</p>
                            <div class="author">Hugh D</div>
                        </div><!-- /.testimonial -->
                    </li>
                    <li class="col-xs-12 col-md-6 custom_padding-right">
                        <div class="section-membership--package clearfix">
                            <div class="column col-xs-6 no_padding">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/sales-new/banner-membership-2-1.png" alt="">
                            </div>
                            <div class="column col-xs-6 no_padding l-align-center">
                                <h4 class="title">Private Skype <br>Follow Up Session</h4>
                            </div>
                        </div>
                        <div class="testimonial" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/sales-new/background-testimonials-2.png');">
                            <p>It is clearly evident you are genuinely interested in helping people. I am amazed at how much time/energy you take to engage with your students.</p>
                            <div class="author">Eric S</div>
                        </div><!-- /.testimonial -->
                    </li>
                </ul>
            </div>
        </section><!-- /.section_membership -->
        <section class="section_future_webinars">
            <div class="container">
                <h3 class="section_future_webinars--title">Future webinars include:</h3>
                <ul class="section_future_webinars--list l-floated clearfix">
                    <li><span>How to Improve Your +R Per Trade</span></li>
                    <li><span>How to Eliminate Your Trading Weaknesses & Increase Your Strengths</span></li>
                    <li><span>Career Paths to You Becoming a Professional Trader</span></li>
                    <li><span>What is the 90/10 Rule & How Can This Help My Trading?</span></li>
                    <li><span>Advanced Price Action Context</span></li>
                    <li><span>Models to Managing Your Trades to Maximize Your +R</span></li>
                </ul>
                <div class="more l-align-center">and more...</div>
            </div>
        </section><!-- /.section_future_webinars -->
        <section id="aboutchris" class="section_passion">
            <div class="container no_padding clearfix">
                <div class="section_passion--image col-xs-12 col-md-6">
                    <img class="image" src="<?php echo get_template_directory_uri(); ?>/images/sales-new/banner-passion-2.png" alt="" />
                </div>
                <div class="col-xs-12 col-md-6">
                    <h3 class="section_passion--title">Why I'm Passionate About Your Trading Success</h3>
                    <p class="section_passion--description">
                        My goal with this course is simple: To <strong>change the way you think, trade and perform</strong>.
                        <br /><br />
                        I do this through cutting edge neuroscience, my <strong>15 year daily meditation practice</strong>, and over a decade of trading.
                        <br /><br />
                        This gives me a unique combination of trading experience and mindset training nobody else has.
                        <br /><br />
                        I'm passionate about helping you become <strong>a better trader and building a successful trading mindset</strong>.
                    </p>
                </div>
            </div>
        </section><!-- /.section_passion -->
        <div class="container">
            <div class="testimonial" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/sales-new/background-testimonials-2.png');">
                <p>That last lesson is a real life changer. I have watched it twice and I am beginning to realize why I have had limited success in trading while only moderate success in other areas of my life. This last lesson is worth the course fee in itself.</p>
                <div class="author">Martin</div>
            </div><!-- /.testimonial -->
        </div>
        <section id="faq" class="section_faq">
            <div class="container">
                <div class="section_faq__bordered">
                    <h3 class="section_faq--title">Frequently asked questions</h3>
                    <ul class="section_faq--list l-stacked">
                        <li>
                            <h4 class="section_faq--question">Is this a one time fee and do I have unlimited access to the videos?</h4>
                            <div class="l-no-overflow">
                                <p class="section_faq--answer">Yes, it's a one-time fee and you have lifetime access to the course and videos.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="section_faq--question">What kind of support do I get and how is the follow up session conducted?</h4>
                            <div class="l-no-overflow">
                                <p class="section_faq--answer">You get full email support from me, a free follow up session from me, and access to the traders forums. The follow up session is usually done via Skype after a student logs at least 20 trades in their trading journal and presents it to me beforehand.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="section_faq--question">I am a complete newbie in trading. How will this course benefit me?</h4>
                            <div class="l-no-overflow">
                                <p class="section_faq--answer">Of all the trading skills you can have, price action would be the most important base you can have. In this course I start with the most basic models of how to read and trade price action. From here there is a progression of lessons which start from the beginning and work their way up. I have every level of trader from beginners to fund traders, so the course is suitable for all levels.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="section_faq--question">I have a full-time job. Can I still use your systems and are there certain times of the day they only work on?</h4>
                            <div class="l-no-overflow">
                                <p class="section_faq--answer">A large majority of my students have full time jobs and still trade successfully using these systems. If they can, so can you. I have strategies that work throughout the day as there is a huge range of strategies from the 5m, 15m, 30m, 1hr, 4hr, daily and weekly charts, so regardless of your time zone and availability, there are strategies for you to trade.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="section_faq--question">What are the hit rates of your systems and are they subjective or rule-based and can be programmed?</h4>
                            <div class="l-no-overflow">
                                <p class="section_faq--answer">System accuracy is a meaningless figure without reward to risk ratios. I have over 11+ systems across 8+ instruments, across 6+ time frames, and accuracy varies per pair, time frame and system, so to list them all would be ridiculous.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="section_faq--question">I trade index futures, currency futures, and commodities. Do your systems work in these markets?</h4>
                            <div class="l-no-overflow">
                                <p class="section_faq--answer">Our systems work with any markets that have sufficient liquidity. I personally use them on forex, index futures and commodities.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="section_faq--question">If you are such a successful trader, then why do you teach?</h4>
                            <div class="l-no-overflow">
                                <p class="section_faq--answer">Good question - please read my article <a href="/trade-signals/forex-strategies/why-i-do-this/" target="_blank">Why I Do This</a>.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="section_faq--question">Do you post trade alerts in real time?</h4>
                            <div class="l-no-overflow">
                                <p class="section_faq--answer">No, as students would just hitch a free ride. My daily trade setups commentary is done after the NY Close Sun-Thurs.<br />My students and top traders however post their trades before, during and after daily.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="section_faq--question">Are there any independent reviews of your course?</h4>
                            <div class="l-no-overflow">
                                <p class="section_faq--answer">Yes, you can find them here on the following link (<a href="http://www.forexpeacearmy.com/public/review/2ndskiesforex.com" target="_blank">2ndSkiesForex Reviews</a>).</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section><!-- /.section_faq -->
        
        <div class="container">
            <div class="notice clearfix">
                <span class="left">You get:</span>
                <p>45+ Hours of Inductional Videos, Trade Setups 3x per Week, Trader Quizzes 2x per Week, Live Trade Setups Forum,
                 Members Only Webinars, Private Follow Up Session, Full Email Support & More</p>
            </div>
        </div>
        <section id="buycourse" class="section_order">
            <div class="container">
                <h3 class="section_order--title">Order the Advanced Price Action Course</h3>
                <div class="section_order--column-left col-xs-12 col-md-6 no_padding">
                    <div class="section_order--column--inner">
                        <img class="image" src="<?php echo get_template_directory_uri(); ?>/images/sales-new/banner-order-2.png" alt="" />
                    </div>
                </div>
                <div class="section_order--column-right col-xs-12 col-md-6 no_padding">
                    <div class="section_order--column--inner">
                        <h4 class="section_order--subtitle">Get Lifetime Access - No Ongoing Fees, Ever!</h4>
                        <p class="section_order--description">Join by June 30th, for a single one-time payment of <del>$499</del> now <strong>only $299.</strong></p>
                        <a href="https://courses.2ndskiesforex.com/courses/startPayment?id=1" class="button">Yes - I want to Make More Profits!</a>
                        <ul class="section_order--list l-block l-align-center">
                            <li><img src="<?php echo get_template_directory_uri(); ?>/images/sales-new/visa.png" alt="" /></li>
                            <li><img src="<?php echo get_template_directory_uri(); ?>/images/sales-new/mastercard.png" alt="" /></li>
                            <li><img src="<?php echo get_template_directory_uri(); ?>/images/sales-new/discover.png" alt="" /></li>
                            <li><img src="<?php echo get_template_directory_uri(); ?>/images/sales-new/americanexpress.png" alt="" /></li>
                            <li><img src="<?php echo get_template_directory_uri(); ?>/images/sales-new/paypal.png" alt="" /></li>
                        </ul>
                        <div class="l-align-center">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/sales-new/comodosecure.png" alt="" />
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </section><!-- /.section_order -->
    </main>
    <footer class="footer l-align-center">
        Copyright &copy; 2007 - <?php echo date('Y'); ?> 2ndSkies Forex. All rights reserved.
    </footer>
<?php wp_footer(); ?>
</body>
</html>
