<?php
/*
Template Name: Course - Pro Forex Trading
*/
?>
<?php get_header(); ?>
	<!-- main content -->
	<div id="content">
     
		<!-- promo video -->
		<div class="promo-video-block">
			<?php the_post(); ?>
			<?php the_content(); ?>
            
            <?php if (get_post_meta($post->ID, "embed-video", true) != '')   { 
				?>
                <div class="video-holder_ video-holder-new">
                    <?php echo get_post_meta($post->ID, "embed-video", true); ?>
                </div>
            	<?php } 
			?>
            
		</div>
		<!-- promo text -->
		<div class="promo-text clearfix">
			<h2 class="center">The Pro Forex Course</h2>
            <p>The systems and strategies in this course help you find and trade momentum based moves. These systems are indicator based, with some of the models dating back to 2004, so they are quite robust.</p>

<p>The core of the models you'll learn here are centered around our "Shadow Models", which help to shadow turns/momentum changes in the market. </p>

<p>They are generally "two target" systems, designed to capture a quick profit, neutralize the risk, then stay in the market if it goes for a runner.</p>

		</div><!-- promo text END -->
        
        
        
		<h2 class="center">What You Will Learn in the Pro Forex Course</h2>
		<!-- table list -->
		<div class="table-list">
			<ul>
                <li>Learn the Primary "Shadow" Model</li>
                <li>How to Trade the 3min, 1hr, & 4hr Shadow Models</li>
                <li>The 6NT Pullback System</li>
                <li>The Volatility Striker System</li>
                <li>Pivot Point Strategies & Statistics</li>
                <li>Advanced Pin Bar Trading</li>
                <li>BR Trading & Pattern Finding</li>
                <li>3 Dimensional Risk Models</li>
                <li>Trading Like a Business</li>
                <li>How to Prepare for Your Trading Day</li>
                <li>When to Let Trades Run</li>
                <li>Learn the Base Model for Price Action</li>
			</ul>
		</div>
        
        
        
        
		<!-- testimonials -->
		<div class="testimonials-block adv-bg">
			<h2>Here's What Students Are Saying About The Course...</h2>
			<div class="testimonials-list">
				<div class="list-item">
					<blockquote>
						<q>I have had my head in the Shadow System and your webinars.  I guess I am getting my PhD from Capre University.  Anyway its going great...been trading live and watching price action with the Shadow - just really nailing it down.  The results are great so far.  The impulsive/corrective video is like a bible of understanding price action.</q>
						<cite>Chris H, US &ndash; Course Member</cite>
					</blockquote>
				</div>
				<div class="list-item">
					<blockquote>
						<q>I made 3% in 3 days since obtaining the courses, they are excellent...I appreciate what you have done for me, even in this little time, I feel like with hard work, I can actually make this work consistently :)</q>
						<cite>Ken T, Australia &ndash; Course Member</cite>
					</blockquote>
				</div>
				<div class="list-item">
					<blockquote>
						<q>I am so happy that I found you and happy to learn a lot from you, now and next years. You were born to trade, teach and educate. Keep going your great mission.</q>
						<cite>Florin, Canada &ndash; Course Member</cite>
					</blockquote>
				</div>
			</div>
		</div><!-- testimonials-block END -->
        


		<h2 class="center">Frequently Asked Questions</h2>
		<div class="shadow-box clearfix">
			<ul class="accordion clearfix">
				<?php query_posts( 'cat=1167&posts_per_page=-1' );?>
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
					<li>
						<a href="#" class="opener"><?php the_title();?></a>
						<div class="slide">
							<?php echo get_the_content();?>
						</div>
					</li>
					<?php endwhile; ?>
				<?php endif; ?>
			</ul>
		</div>
        <br>
        
        
	<div class="subtle-box subtle-box-content">
<div class="new-headline-sub">
<p><u>YOU GET:</u> 15+ Instructional Videos & Tutorials, Live Trade Setups Forum, Private Follow Up Session, Full Email Support</p>
</div>
</div>
        

		<!-- order box -->
		<div class="order-box">
			<div class="container">
				<h2>Order the Pro Forex Course Here</h2>
				<div class="clearfix">
					<div class="img"><img src="<?php echo THEME_IMAGES ?>/img11c.png" width="459" height="270" alt="image description" /></div>
					<div class="text-holder">
				
						<a href="https://courses.2nd-skies-forex.com/courses/startPayment?id=3<?php if(isset($_GET['orid'])) echo '&orid='.$_GET['orid']; ?>" class="btn-add">Add To Cart</a>
						<span class="price"><br>
*Normally $499, <strong>NOW</strong> all this<br><span class="newpriceline">for a One Time Fee of <span class="newprice">$315</span><br><strong> - Ends August 31st</strong></span></span>
						<ul class="payment">
							<li><a class="visa">VISA</a></li>
							<li><a class="mastercard">MasterCard</a></li>
							<li><a class="american-express">American Express</a></li>
							<li><a class="discover">DISCOVER</a></li>
							<li><a class="paypal">PayPal</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="desc">
				<p><span>NOTE:</span> 10+% of all net revenue from course sales goes towards various charities and non-profit organizations around the world. To us, making money is one thing,
but having a positive impact on the world is another thing entirely. We work to uplift the world.</p>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
