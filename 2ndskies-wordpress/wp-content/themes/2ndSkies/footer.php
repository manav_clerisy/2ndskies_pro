
<div id="footer">

    <div class="center-container">

        <div class="cols-holder">

            <div class="col">
                <h2>Navigate</h2>
                <!-- navigation -->
                <div class="nav">
                    <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container' => '', 'items_wrap' => '<ul>%3$s</ul>' ) ); ?>
                </div><!-- .nav-->
            </div><!-- .col -->

            <div class="col">
                <h2>Forex Strategies</h2>
                <!-- blog -->
                <div class="blog slideshow">
                    <div class="pager">
                        <span class="title">Featured</span>
                        <div class="buttons">
                            <a href="#" class="btn-prev">Previous</a>
                            <a href="#" class="btn-next">Next</a>
                        </div>
                    </div>
                    <div class="mask">
                        <div class="blog-list slideset">
                            <?php dynamic_sidebar('footer-sidebar'); ?>
                        </div>
                    </div>
                    <a href="/forex-trading-strategies/forex-strategies/" class="more">Read more &gt;</a>
                </div><!--blog slideshow-->
            </div><!-- .col -->

            <div class="col follow">
                <h2>Recent Comments</h2>

                <ul class="rc">
                    <?php
                    //get_template_part('inc','featured-video');
                   // echo str_replace('http:', 'https:', bwp_get_recent_comments([], false));
                    ?>
                </ul>



            </div><!-- .col.follow -->

        </div><!-- .cols-holder -->

    </div><!-- .center-container -->

    <div class="add-info">
        <div class="center-container">

            <div class="logo-small"><img src="<?php echo THEME_IMAGES ?>/logo-small.png" width="59" height="53" alt="2ndSkies logo"  /></div>
            <!-- copyright and address -->
            <?php dynamic_sidebar('social-networks-sidebar'); ?>
            <div class="clearfix"></div>
            <?php dynamic_sidebar('copyright-sidebar'); ?>
        </div>
    </div><!-- .add-info -->

</div><!-- #footer -->

</div><!-- END #wrapper -->

<!--[if lt IE 9]>
<script src="<?php echo trailingslashit( THEME_JS ) . 'html5.js'; ?>"></script>
<![endif]-->
<!--[if (lte IE 8)]>
<script type="text/javascript" src="<?php echo trailingslashit( THEME_JS ) . 'selectivizr-min.js'; ?>"></script>
<![endif]-->






<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WLRNLV"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WLRNLV');</script>
<!-- End Google Tag Manager -->


<?php wp_footer(); ?>

<?php get_template_part('inc','popup'); ?>
<?php if (is_page('home')) { ?>
    <?php //if(!isset($_COOKIE['pop-up-cookie'])) { # because of CloudFlare this popup should always be rendered ?>
    <?php //} ?>
    <script>
        jQuery(document).ready(function($){
            console.log("!!!");
           // console.log($.cookie('pop-up-cookie'));
            console.log("!!!-end");
            if ($.cookie('pop-up-cookie') != '1') {
                setTimeout(function (){
                    $.magnificPopup.open({
                        items: {
                            src: '.pop-call', // can be a HTML string, jQuery object, or CSS selector
                            type: 'inline'

                        },
                        mainClass: 'my-mfp-zoom-in',
                        removalDelay: 500
                    });
                }, 1500); // how long do you want the delay to be?
                $.cookie('pop-up-cookie', '1', { expires: 120 }); }
        });
    </script>
<?php } ?>
<?php if (!is_page('home')) { ?>
    <?php
        /*
        print_r('<script>console.log('not frontpage');</script>');

        function url_origin($s, $use_forwarded_host=false) {
            $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true:false;
            $sp = strtolower($s['SERVER_PROTOCOL']);
            $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
            $port = $s['SERVER_PORT'];
            $port = ((!$ssl && $port=='80') || ($ssl && $port=='443')) ? '' : ':'.$port;
            $host = ($use_forwarded_host && isset($s['HTTP_X_FORWARDED_HOST'])) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
            $host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
            return $protocol . '://' . $host;
        }
        function full_url($s, $use_forwarded_host=false) {
            return url_origin($s, $use_forwarded_host) . $s['REQUEST_URI'];
        }
        $absolute_url = full_url($_SERVER);

        if (strpos($absolute_url, 'homepage') !== false) {
            print_r('<script>console.log(\'is/ homepage\');</script>');
            get_template_part('inc','popup');
        }
        */
    ?>


    <script>
        jQuery(document).ready(function($){
            if (window.location.href.indexOf("homepage") > -1) {
                console.log('NEW cookie /homepage: ' + $.cookie('pop-up-cookie-2'));
                if ($.cookie('pop-up-cookie-2') != '2') {
                    setTimeout(function (){
                        $.magnificPopup.open({
                            items: {
                                src: '.pop-call', // can be a HTML string, jQuery object, or CSS selector
                                type: 'inline'

                            },
                            mainClass: 'my-mfp-zoom-in',
                            removalDelay: 500
                        });
                    }, 1500); // how long do you want the delay to be?
                    $.cookie('pop-up-cookie-2', '2', { path: '/homepage' }, { expires: 120 });
                }
            }
        });
    </script>
<?php } ?>

<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">
    twttr.conversion.trackPid('l5in3');</script>
<noscript>
    <img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=l5in3&p_id=Twitter" />
    <img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=l5in3&p_id=Twitter" /></noscript>

</body>
</html>
