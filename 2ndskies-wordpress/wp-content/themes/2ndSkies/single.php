<?php
global $post;
if ($post && $post->ID == 14743) {
   include TEMPLATEPATH . '/single-14743.php';
   exit();
}
?>
<?php get_header(); ?>
<!-- main -->
<!--<div id="main" class="clearfix">-->

<div id="main" class="clearfix" data-developer="true">
	<!-- content -->
	<div id="content">
		<div class="breadcrumbs-container clearfix">
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo home_url(); ?>">Home</a></li>
					<li><?php the_category(', ') ?></li>
					<li><?php the_title() ?></li>
				</ul>
			</div>
		</div>
		<div class="posts">
			<div class="main-post">
				<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
				<h1><?php the_title(); ?></h1>
				<!-- meta info -->
				<?php
					global $post, $wp_query;

					$args = array(
						'post_id'       => $post->ID,
						'status' => 'approve',
						'order'   => 'ASC',
					);
					$wp_query->comments = get_comments( $args );
				?>
				<div class="meta">
					<ul>
						<?php if ( hide_the_date($post->ID)) : ?>
							<!-- nothing -->
						<?php else: ?>
							<li><strong class="date"><?php //the_time('F jS, Y') ?></strong></li>
						<?php endif; ?>
						<li>
						<?php if ( hide_the_date($post->ID)) : ?>
							<!-- nothing -->
						<?php else: ?>
							in&nbsp;
						<?php endif; ?><?php the_category(', ') ?></li>
						<li>| <a href="#comments"><?php if(count($wp_query->comments) == 1) echo '1 comment'; else echo count($wp_query->comments).' comments'; ?></a></li>
					</ul>
				</div>
				<!-- fb-box-holder -->
				<div class="fb-box-holder">
					<div id="fb-root"></div>
					<script>(function(d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) return;
						js = d.createElement(s); js.id = id;
						js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=454996944561399";
						fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));
					</script>
					<div class="fb-like-box" data-href="https://www.facebook.com/2ndSkiesForex" data-colorscheme="light" data-show-faces="false" data-header="true" data-stream="false" data-show-border="true"></div>
                </div>


				<?php if (get_post_meta($post->ID, "embed-video", true) != '')   {
                    ?>
                    <div class="post-video-holder imhere">
                    <!-- post video holder -->
					<?php
						echo $embed = get_post_meta($post->ID, "embed-video", true);
						setPostViews($post->ID);

						$commentPostId = $post->ID;
					?>
                    </div>
                    <?php }
                ?>
				<div class="content">
					<?php the_content(); ?>

                                    <?php if(in_category(15) || in_category(17)){ ?>
                                            <p><strong>Want More?</strong> My private members get all <strong>my trade ideas &amp; market commentary up to 3x per week.</strong> <a target="_blank" href="<?= get_site_url(); ?>/advanced-price-action-course/">Click here</a> to become a member.</p>
                                    <?php } ?>
				</div><!-- content -->

				<?php get_template_part('inc', 'share-print'); ?>

			</div><!-- .main-post -->

            <?php get_template_part('author', 'box'); ?>

			<?php
				$tags = wp_get_post_tags($post->ID);

				if ($tags) {

				$tagsArr = array();

				foreach($tags as $tag)
					$tagsArr[] = $tag->term_id;

				$args=array(
					'tag__in' => $tagsArr,
					'post__not_in' => array($post->ID),
					'posts_per_page'=>3,
					'caller_get_posts'=>1
				);
				$my_query = new WP_Query($args);
				if( $my_query->have_posts() ) {

			?>
			<div class="post-list row-list">
				<div class="main-title">
					<h2>Related Article</h2>
				</div>
				<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
				<div class="list-item">
					<div class="photo">
						<a href="<?php the_permalink() ?>"><?php the_post_thumbnail(array(286,137)); ?></a>
					</div>
					<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
				</div>
				<?php endwhile; ?>
           		</div>

                    <input type="hidden" name="1qazpageloaded" value="<?php echo $_SERVER["REMOTE_ADDR"]; ?>" />
                     	<div id="fixed-related-box" class="1111!">
			    <div class="holder">
				<h2>You Might Also Enjoy</h2>
				<ul class="list-item">
				     <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
				    <li>
					<div class="photo">
					    <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('archive-thumb'); ?></a>
					</div>
					<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
				    </li>
				    <?php endwhile; ?>
				</ul>
				<div id="related-close">x</div>
			    </div>
			</div>
			<?php
					//endwhile;
				}
			} wp_reset_query();
?>

			<?php endwhile; ?>
			<?php else : ?>
				<h1>Not Found</h1>
				<p>Sorry, but you are looking for something that isn't here.</p>
			<?php endif; ?>

		</div>
        <!-- post comments -->
<div class="comment-form-holder">
                <?php
                $args = array(
                  //  'fields' => apply_filters( 'comment_form_default_fields', $fields )
                'comment_field' => '<div class="row clearfix">
					<div class="photo">'.get_avatar('', 55).'</div>
					<div class="textfield">
					<textarea name="comment" id="comment" cols="30" rows="10" placeholder="Leave a comment...."></textarea></div></div>'
                ,'must_log_in' => '<p class="must-log-in">' .  sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>'
                ,'logged_in_as' => ''
                ,'comment_notes_before' => '<p class="comment-notes">' . __( 'Your email address will not be published.' ) . ( $req ? $required_text : '' ) . '</p>'
                ,'comment_notes_after' => ''
                ,'id_form' => 'commentform'
                ,'id_submit' => 'submit'
                ,'title_reply' => __( 'Comments on the article' )
                ,'title_reply_to' => __( 'Leave a Reply to %s' )
                ,'cancel_reply_link' => __( 'Cancel reply' )
                ,'label_submit' => __( 'Post Comment' )
                );
                comment_form( $args, $commentPostId );
                ?>
            </div>
        <div class="post-comments" id="comments">
            <h2 class="comments-title">Discussion</h2>
            <!-- comment form -->

            <?php

            echo '<h3>'.count($wp_query->comments).' Comments on the article</h3>';
            echo '<ul class="comment-list">';
            wp_list_comments('callback=skies_comment');
            echo '</ul>';
            ?>
        </div>
	</div>
	<!-- sidebar -->
	<?php get_sidebar('recent-popular'); ?>
</div>
	<script type="text/javascript">
		jQuery(document).ready(function ($){

			$(window).scroll(function() {
			var scroll_rp = (jQuery('.comment-form-holder #respond').offset().top - $(window).height());
			if ( $(document).width() > 999 && $('#fixed-related-box').attr('class') != 'close_related_box' ) {
				if ($(this).scrollTop() >= scroll_rp) {
					$('#fixed-related-box').fadeIn('slow');
				}else {
					$('#fixed-related-box').fadeOut('slow');
				};
			}
			});
			$(document).on('click', '#related-close', function(){
				$('#fixed-related-box').addClass('close_related_box').fadeOut('slow');
			});
		});
	</script>
<?php get_footer(); ?>
