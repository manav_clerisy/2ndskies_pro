<div class="share-block clearfix">

<?php wp_enqueue_script('sharrre');
    

    ?>

<?php
    
/*$matches=array();
$input = '<span>Watch as I execute a live price action trade on the USD/CHF. Currently up +143 pips, I explain my entry, stop loss placement and why I took the trade.</span><iframe width="560" height="315" src="http://www.youtube.com/embed/2RR-tzGOyi0" frameborder="0" allowfullscreen=""></iframe>';
preg_match_all('/https?\:\/\/(?:www\.)?(?:youtube\.com\/[^\s]*v\=|youtu\.be\/)([a-z0-9]+)/is', $input, $matches);
    
    
    print_r($matches);
    
    
    
    
    
     $subject = "http://www.youtube.com/watch?v=z_AbfPXTKms&NR=1";

 preg_match("(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=[0-9]/)[^&\n]+|(?<=v=)[^&\n]+", $input, $matches);

 print "<pre>";
 print_r($matches);
 print "</pre>";
    */
    
    
    function short_url($url) {
        $ch = curl_init();
        $timeout = 4;
        curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
	$url='https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $short = short_url($url);


    $totalCount = 0;
    $fieldValue = simple_fields_value("fb_post_id");

    if (!is_null($fieldValue) && $fieldValue != '') {
        $likes = parse("https://graph.facebook.com/v1.0/$fieldValue/likes?limit=0&summary=1");
        $result = json_decode($likes);
        if (isset($result->summary->total_count))
        {
            $totalCount += $result->summary->total_count;
        }

        $commets = parse("https://graph.facebook.com/v1.0/$fieldValue/comments?limit=0&summary=1");
        $result = json_decode($commets);
        if (isset($result->summary->total_count))
        {
            $totalCount += $result->summary->total_count;
        }
        
        $shares=0;
        query_posts('category_name=forex-videos&posts_per_page=-1'); 
        if ( have_posts() ) : while ( have_posts() ) : the_post();
        
           $youtubeLink=get_post_meta( get_the_ID(), 'youtube_links_post',true);
      
            $sharres_Link="https://graph.facebook.com/?id=".$youtubeLink;
            $sharres = parse($sharres_Link);
            $result = json_decode($sharres);
        

            if (isset($result->shares))
             {
                $shares+=$result->shares;
                
             }
        endwhile;
        endif; 
        wp_reset_query();
       
        
        
        
    }

    function parse($encUrl){
        $options = array(
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // don't return headers
            CURLOPT_FOLLOWLOCATION => true, // follow redirects
            CURLOPT_ENCODING => "", // handle all encodings
            CURLOPT_AUTOREFERER => true, // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 5, // timeout on connect
            CURLOPT_TIMEOUT => 10, // timeout on response
            CURLOPT_MAXREDIRS => 3, // stop after 10 redirects
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => false,
        );
        $ch = curl_init();

        $options[CURLOPT_URL] = $encUrl;
        curl_setopt_array($ch, $options);

        $content = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);

        curl_close($ch);

        if ($errmsg != '' || $err != '') {
            /*print_r($errmsg);
            print_r($errmsg);*/
        }
        return $content;
    }
	
	
	
?>

    <div class="sharecount-box">
        <div id="sharecount"><?php echo $shares; ?></div>
        <span>Shares</span>
    </div>

    <div id="sharrre-icons">
        <div id="facebook" data-url="<?php echo $short; ?>"></div>
        <div id="twitter" data-url="<?php echo $short; ?>" >
		
		<a class="box" href="#"><div class="share">
		<div class="dashicons dashicons-twitter-alt"></div>
		<div class="sh-wrap"><span class="shareon">Share on</span>
		<span class="shareto">Twitter</span></div></div></a>

		
		</div>
        <div id="googleplus" data-url="<?php echo $short; ?>" >
		<a class="box" href="#"><div class="share">
		<div class="dashicons dashicons-googleplus-alt"></div>
		<div class="sh-wrap"><span class="shareon">Share on</span>
		<span class="shareto">Google+</span></div></div></a></div>
    </div>

<?php 

exit();

if(is_single()) { ?>
    <div class="icon-share-wrap">
        <a class="icon-share" href='javascript:window.print();'><div class="dashicons dashicons-pressthis"></div></a>
        <a class="icon-share" id="emailLink" href="mailto:"><div class="dashicons dashicons-email-alt"></div></a>
    </div>

    <script>
        jQuery(document).ready(function ($){
            $('#emailLink').click();
        });
    </script>
<?php } ?>
    <script>

        var SherrreTotals   = <?= $totalCount ?>;
        var SherrreURL      = '<?php echo get_permalink($post->ID); ?>';
        var SherrreURLCurl  = '<?php echo get_site_url(); ?>/wp-content/themes/2ndSkies/library/sharrre/sharrre.php';

        
        jQuery(document).ready(function($){

            if($("#sharecount .count").text().trim() == ""){
                $('#sharecount').sharrre({
                    share: {
                        googlePlus: true,
                        facebook: true,
                        twitter: true
                    },
                    total: SherrreTotals,
                    url: SherrreURL,
                    urlCurl: SherrreURLCurl,
                    enableHover: false
                });
            }
        })
    </script>
</div><!-- share block -->
<?php
    $augmented_nr = get_field('augmented_share_number');
    if (!isset($augmented_nr) || (!$augmented_nr > 0)) {
        $augmented_nr = 0;
    }
?>
<!--<script>
//augmented_nr is send to jquery.sharrre.min.js
    var augmented_nr = '<?php echo $augmented_nr; ?>';
    jQuery(document).ready(function ($){
        $('#sharecount').sharrre({
          share: {
            googlePlus: true,
            facebook: true,
            twitter: true
          },
          url: "<?php echo get_permalink( $post->ID ); ?>",
          urlCurl: 'http://<?php echo $_SERVER['HTTP_HOST']?>/wp-content/themes/2ndSkies/library/sharrre/sharrre.php',
          enableHover: false
        });
    });
</script>-->
