<?php /*
  Template Name: Home A
 */ ?>
<?php get_header(); ?>
<script type="text/javascript">
    /*jQuery(document).ready(function ($){
     $(document).on('click', '.gotop', function(event){
     event.preventDefault();
     $('body,html').animate({
     scrollTop: 0
     }, 400);
     return false;
     });
     });*/
</script>
<div class="banner-homepage">
    <div class="holder homeb">
        <h3 class="title">Chris Capre's Price Action Trading Course</h3>
        <h5 class="sub-title">Take Your Forex Trading To The Next Level</h5>
        <div class="img-text">
            <div class="img-holder">
                <img src="<?php bloginfo( 'template_url' ); ?>/library/images/homepage/devices.png" alt="Check Out Our Daily Trade Signals and Setup">
            </div>
            <div class="text-box">
                <ul class="list">
                    <li>Lifetime Membership</li>
                    <li>Private Member Webinar</li>
                    <li>Access to Daily Trade Setups Commentary</li>
                    <li>Increase Your Confidence, Discipline &amp;<br/> Consistency</li>
                </ul>
                <a href="<?php echo site_url("advanced-price-action-course"); ?>" class="link gotop">Learn More</a>
            </div>
        </div>
    </div>
</div>
<div class="holder homepage holder-block">
    <ul class="list">
        <li>
            <a href="/trade-signals/" class="gotop"><img src="<?php bloginfo('template_url'); ?>/library/images/img-4-1.jpg" alt="price action strategy, ichimoku trading system, forex trading strategies that work">
                <span>Trade Signals</span></a>
        </li>
        <li>
            <a href="/trading-articles"> <img src="<?php bloginfo('template_url'); ?>/library/images/img-5-1.jpg" alt="price action strategy, ichimoku trading system, forex trading strategies that work">
                <span>Forex Strategies</span></a>
        </li>
        <li>
            <a href="/advanced-price-action-course"> <img src="<?php bloginfo('template_url'); ?>/library/images/img-6-1.jpg" alt="price action strategy, ichimoku trading system, forex trading strategies that work">
                <span>Price Action Course</span></a>
        </li>
    </ul>
</div>

<div class="holder homepage section-block">
    <div class="col-box">
        <div class="box">
            <h4>Price Action Skills</h4>
            <p>Learn the most essential<br/> price action tools.</p>
            <a href="/price-action-skills/" class="gotop">Learn Now</a>
        </div>
        <div class="box">
            <h4>Pull the Trigger</h4>
            <p>Cut down the noise and get past<br/> analysis paralysis</p>
            <a href="/pull-the-trigger/" class="gotop">Improve Your Trading</a>
        </div>
    </div>
    <div class="categories-list">
        <?php dynamic_sidebar('home-sidebar'); ?>
    </div>
</div>

<?php get_footer(); ?>
