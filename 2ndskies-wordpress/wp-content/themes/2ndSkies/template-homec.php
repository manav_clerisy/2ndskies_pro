<?php /*
  Template Name: Home C
 */ ?>
<?php get_header(); ?>
<script type="text/javascript">
    /*jQuery(document).ready(function ($){
     $(document).on('click', '.gotop', function(event){
     event.preventDefault();
     $('body,html').animate({
     scrollTop: 0
     }, 400);
     return false;
     });
     });*/
</script>
<?php /*
<div class="holder">
    <div class="text-block">
        <h2>Trade Forex Successfully</h2>
        <ul class="list">
            <li>Price Action</li>
            <li>Ichimoku</li>
            <li>Online Training</li>
        </ul>
    </div>
    <div class="video-block">
        <div class="video-holder">
            <div class="lightbox-holder">
                <div id="home-popup01" class="home-popup">
                    <div class="lightbox-video">
                        <?php echo $embed = get_post_meta($post->ID, "embed-video", true); ?>
                    </div><!-- .lightbox-video -->
                </div><!-- #home-popup01 -->
            </div><!-- .lightbox-holder -->
            <?php echo $embed = get_post_meta($post->ID, "embed-video", true); ?>
            <a href="#home-popup01" class="lightbox-opener-link popup-video"></a>
        </div><!-- .video-holder -->
        <p><span>"Change the Way You Trade, Think & Perform."</span>Chris Capre, Founder, 2ndSkiesForex</p>
    </div>
</div>
 * 
 */ ?>
<div class="banner-homepage lightbulb">
    <div class="holder homec">
        <h3 class="title">Learn Why A '<strong>Confirmation Price Action Signal</strong>'<br/>is <strong>Hurting Your Profits</strong></h3>
        <div class="img-text">
            <div class="img-holder">
            </div>
            <div class="img-text">
            <div class="text-box">
                <ul class="list">
                    <li>Learn Why The Typical Pin Bar Entry is a Retail Entry</li>
                    <li>Discover One Small Adjustment To Increase Your<br/> Edge &amp; Accuracy</li>
                    <li>Learn A Professional Trader's Mentality to Price Action</li>
                </ul>
                <a href="<?php echo get_permalink(14167); ?>" class="link gotop">Learn More</a>
            </div>
        </div>
        </div>
    </div>
</div>
<div class="holder homepage holder-block">
    <ul class="list">
        <li>
            <a href="/trade-signals/" class="gotop"><img src="<?php bloginfo('template_url'); ?>/library/images/img-4-1.jpg" alt="price action strategy, ichimoku trading system, forex trading strategies that work">
                <span>Trade Signals</span></a>
        </li>
        <li>
            <a href="/trading-articles"> <img src="<?php bloginfo('template_url'); ?>/library/images/img-5-1.jpg" alt="price action strategy, ichimoku trading system, forex trading strategies that work">
                <span>Forex Strategies</span></a>
        </li>
        <li>
            <a href="/advanced-price-action-course"> <img src="<?php bloginfo('template_url'); ?>/library/images/img-6-1.jpg" alt="price action strategy, ichimoku trading system, forex trading strategies that work">
                <span>Price Action Course</span></a>
        </li>
    </ul>
</div>

<div class="holder homepage section-block">
    <div class="col-box">
        <div class="box">
            <h4>Price Action Skills</h4>
            <p>Learn the most essential<br/> price action tools.</p>
            <a href="/price-action-skills/" class="gotop">Learn Now</a>
        </div>
        <div class="box">
            <h4>Pull the Trigger</h4>
            <p>Cut down the noise and get past<br/> analysis paralysis</p>
            <a href="/pull-the-trigger/" class="gotop">Improve Your Trading</a>
        </div>
    </div>
    <div class="categories-list">
        <?php dynamic_sidebar('home-sidebar'); ?>
    </div>
</div>

<?php get_footer(); ?>
