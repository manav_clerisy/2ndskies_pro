<?php 
/* Template name: Homepage Page */

?>
<?php get_header();
 ?>
<script type="text/javascript">
    /*jQuery(document).ready(function ($){
     $(document).on('click', '.gotop', function(event){
     event.preventDefault();
     $('body,html').animate({
     scrollTop: 0
     }, 400);
     return false;
     });
     });*/
</script>
<?php /*
<div class="holder">
    <div class="text-block">
        <h2>Trade Forex Successfully</h2>
        <ul class="list">
            <li>Price Action</li>
            <li>Ichimoku</li>
            <li>Online Training</li>
        </ul>
    </div>
    <div class="video-block">
        <div class="video-holder">
            <div class="lightbox-holder">
                <div id="home-popup01" class="home-popup">
                    <div class="lightbox-video">
                        <?php echo $embed = get_post_meta($post->ID, "embed-video", true); ?>
                    </div><!-- .lightbox-video -->
                </div><!-- #home-popup01 -->
            </div><!-- .lightbox-holder -->
            <?php echo $embed = get_post_meta($post->ID, "embed-video", true); ?>
            <a href="#home-popup01" class="lightbox-opener-link popup-video"></a>
        </div><!-- .video-holder -->
        <p><span>"Change the Way You Trade, Think & Perform."</span>Chris Capre, Founder, 2ndSkiesForex</p>
    </div>
</div>
<div class="header-box top-banner-new" style="background-image:url(<?php bloginfo( 'template_url' ); ?>/library/images/bannerbg.jpg) !important;">
    <div class="holder">
 		<h2>The Advanced Traders Mindset Course</h2>
        <p>Think Successfully &amp; Trade Profitably</p>
        <div class="box-holder">
            <div class="img-holder">
                <img src="<?php get_template_directory_uri (); ?>/library/images/advanced_traders.png" alt="Advanced Traders">
            </div>
            <ul class="list">
                <li>Build a Mindset To Take On Any Obstacle</li>
                <li>Develop Real Confidence, Make Money Trading</li>
                <li>Cultivate Success in Your Trading &amp; Life</li>
                
                	<a class="popup-opener" href="https://2ndskiesforex.com/advanced-traders-mindset-course/">Yes, I Want To Wire My Brain For Success</a>
            </ul>
        </div>
    </div>
</div>
 *
 */ ?>

<?php
/*
echo '<!-- country code ' . $country_code . ' -->';
if (in_array($country_code, array( "IS", "FO", "NO", "SE", "AX", "FI", "RU", "EE", "LV", "LT", "BY", "UA", "PL", "DK", "DE", "NL", "IM", "GB", "IE", "GG", "BE", "LU", "JE", "FR", "PT", "ES", "GI", "AD", "MC", "CH", "LI", "SM", "VA", "IT", "CZ", "AT", "SK", "HU", "HR", "BA", "ME", "AL", "GR", "BG", "RO", "MD", "TR", "RS" ))):
?>

<section class="section_hero" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/banners/background-hero-london.jpg);">
    <div class="container">
        <div class="section_hero--promo l-align-center">
            <h1 class="title">London Trading Seminar<br />with Chris Capre</h1>
            <h3 class="subtitle">Change the Way You Think, Trade & Perform</h3>
            <ul class="section_hero--list l-block l-align-left">
                <li>
                    <div class="bubble">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon1.png" class="img icon1" />
                        <h4 class="bubble--name">Date</h4>
                        <div class="bubble--date">November 14 / 15th 2015</div>
                    </div>
                </li><li>
                    <div class="bubble">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon2.png" class="img icon2" />
                        <h4 class="bubble--name">Times</h4>
                        <div class="bubble--date">9am - 5pm</div>
                    </div>
                </li><li>
                    <div class="bubble bubble__double">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon3.png" class="img icon3" />
                        <h4 class="bubble--name">Location</h4>
                        <div class="bubble--date">London, UK</div>
                        <div class="inner">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon4.png" class="img icon4" />
                            <h4 class="inner-name">The Montcalm<br />London Marble Arch</h4>
                        </div>
                    </div>
                </li>
                <li id="sponsored-by-fxstreet">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/icons/iconfx.png" class="iconfx" />
                </li>
            </ul>
     		<section id="reserve-your-seat" class="section_register">
				<ul class="section_registernow--list l-stacked">
                    <li>
                    	<a href="https://2ndskiesforex.com/london-seminar/"><span class="hover_hax">Reserve Your Seat!</span></a>
                    </li>
                </ul>
			</section>
        </div>
    </div>
</section>

<?php
else: 
*/
?>

<div class="gallery-js-ready2 slider-container slideshow-testimonials">
<div class="mask"><div class="slideset list-item">


    
    
    
    
  

  
  
  <?php /*?><div class="slider-slide off-stage">
    <div class="banner-homepage">
      <div class="holder homea">
        <h3 class="title">Want to Make <strong>More Profits in Trading</strong>?<br/>Build Up <strong>These Three Things</strong></h3>
         /* <h5 class="sub-title">Learn to Read Price Action &amp; Spot Key Support & Resistance Levels</h5> */ /*
        <div class="img-text">
            <div class="img-holder">
                <?php /* <img src="<?php bloginfo( 'template_url' ); ?>/library/images/homepage/graphs.png" alt="Check Out Our Daily Trade Signals and Setup"> */ /*?>
            </div>
            <div class="text-box">
                <ul class="list">
                    <li>Do you know how your brain is working against you (and how to change it)?</li>
                    <li>Wonder why you keep making the same mistakes?</li>
                    <li>Can't get past breaking even or small profits without big losses?</li>
                </ul>
                <a href="<?php echo site_url("trading-strategies/self-image-determines-successful-forex-trading-psychology/"); ?>" class="link gotop">Learn How to Make Greater Profits</a>
            </div>
        </div>
      </div>
    </div>
  </div><?php */?>
    
    
  
<?php /*?><div class="slider-slide slide list-item active">
    <div class="header-box top-banner-new" style="background-image:url(<?php bloginfo( 'template_url' ); ?>/library/images/banners/bannerbg.jpg) !important;">
      <div class="holder">
 	<h2>The Advanced Traders Mindset Course</h2>
        <p>Think Successfully &amp; Trade Profitably</p>
     
          
             <img src="<?php bloginfo( 'template_url' ); ?>/library/images/banners/brain.png" alt="Advanced Traders">
        <div style="display:inline-block; text-align:center;max-width:355px;">
          <ul class="list">
            <li>Build a Mindset To Take On Any Obstacle</li>
            <li>Develop Real Confidence, Make Money Trading</li>
            <li>Cultivate Success in Your Trading &amp; Life</li>
            
             
          </ul>
		   <a class="popup-opener" style="color:#000000;text-decoration:none;" href="https://2ndskiesforex.com/advanced-traders-mindset-course/">Yes, I Want To Wire My Brain For Success</a>
        </div>
      </div>
    </div>
  </div><?php */?>
    
    
    
      
    <!-- Slide Brain -->
<div class="slider-slide slide list-item">
    <div class="header-box top-banner-new" style="background-image:url(<?php bloginfo( 'template_url' ); ?>/library/images/banners/bannerbg.jpg) !important;">
      <div class="holder">
	  
			<h2 style="font-size:50px">Building A Better Brain:</h2>
			<p>Discover One Technique to Improve Your Brain's Performance</p>
			<img src="<?php bloginfo( 'template_url' ); ?>/library/images/banners/head1.png" alt="" style="width: 363px; margin-top: -127px; margin-left: 108px; margin-right: 0px;">
			<div style="display: inline-block; text-align: center; max-width: 42%;">
			<ul class="list">
			<li>Learn why you make mental trading errors</li>
			<li>Wire your brain to trade profitably</li>
			<li>Get a simple practice to improve your brain's performance</li>
			</ul>
			<a style="color:#000000;text-decoration:none;" href="<?php echo site_url("trading-strategies/building-a-better-brain-a-holiday-message-from-chris-capre-the-2ndskiesforex-team/"); ?>">Learn more</a>
	       </div>
    </div>
  </div>
  
    </div> 
    
    <div class="slider-slide slide list-item">
    <div class="header-box top-banner-new" style="background:#0d2a4c !important;">
      <div class="holder">
	  
			<h2>Mental Toughness : The Only Way Out Is Through</h2>
			<p>Why your success in trading depends upon this</p>
			<img src="<?php bloginfo( 'template_url' ); ?>/library/images/banners/hands.png" alt="" style="width: 363px; margin-top: -127px; margin-left: 108px; margin-right: 0px;">
			<div style="display: inline-block; text-align: center; max-width: 42%;">
			<ul class="list"   style="margin-top: 37px;">
			<li>Discover how to thrive in adversity</li>
			<li>Learn why coping is not the endgame</li>
			<li>Build a no-obstacle mindset to succeed in<br>trading and life</li>
			</ul>
			<a style="color:#000000;text-decoration:none;" href="<?php echo site_url("trading-strategies/mental-toughness-the-only-way-out-is-through/"); ?>">Learn more</a>
	       </div>
    </div>
  </div>
  
    </div> 
    
    
    <div class="slider-slide slide list-item">
    <div class="header-box top-banner-new" style="background-image:url(<?php bloginfo( 'template_url' ); ?>/library/images/banners/bannerbg3.jpg) !important;">
      <div class="holder">
	  
			<h2 style="font-size:50px">Meditation For Trading</h2>
			<p>A Practice to Improve Your Trading Mindset</p>
			<img src="<?php bloginfo( 'template_url' ); ?>/library/images/banners/brain2.png" alt="" style="width: 363px; margin-top: -127px; margin-left: 108px; margin-right: 0px;">
			<div style="display: inline-block; text-align: center; max-width: 42%;">
			<ul class="list">
			<li>Improve pattern recognition for trading</li>
			<li>Sharpen your mind with this simple practice</li>
			<li>Discover how meditation improves brain<br>performance</li>
			</ul>
			<a style="color:#000000;text-decoration:none;" href="<?php echo site_url("trading-strategies/meditation-trading-15-years-practice/"); ?>">Learn more</a>
	       </div>
    </div>
  </div>
  
    </div>
    
    <div class="slider-slide slide list-item">
    <div class="header-box top-banner-new white-bg" style="background-image:url(<?php bloginfo( 'template_url' ); ?>/library/images/banners/bannerbg4.jpg) !important;">
      <div class="holder">
	  
			<h2>Chris Capre's Advanced Price Action Course</h2>
          <p>Because You're Ready to be A <strong>Successful Trader</strong><i> - <span style="font-style:italic;">Yesterday</span></i></p>
			<img src="<?php bloginfo( 'template_url' ); ?>/library/images/banners/banner4.png" alt="" style="width: 363px; margin-top: -127px; margin-left: 108px; margin-right: 0px;">
			<div style="display: inline-block; text-align: center; max-width: 42%;">
			<ul class="list"  style="margin-top: 37px;">
			<li>Sharpen Your Trading Edge</li>
			<li>Improve Your Day-To-Day Performance</li>
			<li>Increase Your Confidence, Discipline &<br>Consistency</li>
			</ul>
			<a style="color:#000000;text-decoration:none;" href="<?php echo site_url("trading-strategies/advanced-price-action-course/"); ?>">Learn more</a>
	       </div>
    </div>
  </div>
  
    </div>
    
    <!-- Slide Brain -->
	
	
  <!--
  
  <div class="slider-slide slide list-item">
    <div class="header-box top-banner-new" style="">
      <div class="holder">
	  
			<h2>Chris Capre's Advanced Price Action Course</h2>
			<p>Because You're Ready to be A Successful Trader - Yesterday</p>
			<img src="<?php bloginfo( 'template_url' ); ?>/library/images/banners/banner4.png" alt="" style="width:250px">
			<div style="display:inline-block; text-align:center;max-width:55%;">
			<ul class="list">
			<li>Sharpen Your Trading Edge</li>
			<li>Improve Your Day-To-Day Performance</li>
			<li>Increase Your Confidence, Discipline & Consistency</li>
			</ul>
			 <a style="color:#000000;text-decoration:none;" href="<?php echo site_url("trading-strategies/advanced-price-action-course/"); ?>" class="link gotop">Learn More </a>
	       </div>
    </div>
  </div>
  
    </div> 
    
    
-->
    
    
    <!--- End fo dynamic slider -->
      <?php /*
  
  <div class="slider-slide off-stage">
    <div class="header-box top-banner-new" style="background-image:url(<?php bloginfo( 'template_url' ); ?>/library/images/banners/bannerbg3.jpg) !important;">
      <div class="holder">
 	<h2>The Advanced Traders Mindset Course</h2>
        <p>Think Successfully &amp; Trade Profitably</p>
     
          
             <img src="<?php bloginfo( 'template_url' ); ?>/library/images/banners/brain2.png" alt="Advanced Traders">
        <div style="display:inline-block; text-align:center;max-width:355px;">
          <ul class="list">
            <li>Build a Mindset To Take On Any Obstacle</li>
            <li>Develop Real Confidence, Make Money Trading</li>
            <li>Cultivate Success in Your Trading &amp; Life</li>
            
             
          </ul>
		   <a class="popup-opener" href="https://2ndskiesforex.com/advanced-traders-mindset-course/">Yes, I Want To Wire My Brain For Success</a>
        </div>
      </div>
    </div>
  </div>
  <div class="slider-slide off-stage">
    <div class="header-box top-banner-new" style="">
      <div class="holder">
 	<h2>The Advanced Traders Mindset Course</h2>
        <p>Think Successfully &amp; Trade Profitably</p>
     
          
             <img src="<?php bloginfo( 'template_url' ); ?>/library/images/banners/hands.png" alt="Advanced Traders">
        <div style="display:inline-block; text-align:center;max-width:355px;">
          <ul class="list">
            <li>Build a Mindset To Take On Any Obstacle</li>
            <li>Develop Real Confidence, Make Money Trading</li>
            <li>Cultivate Success in Your Trading &amp; Life</li>
            
             
          </ul>
		   <a class="popup-opener" href="https://2ndskiesforex.com/advanced-traders-mindset-course/">Yes, I Want To Wire My Brain For Success</a>
        </div>
      </div>
    </div>
  </div>
  
  <div class="slider-slide off-stage">
    <div class="header-box top-banner-new"> <!--style="background-image:url(<?php bloginfo( 'template_url' ); ?>/library/images/banners/bannerbg4.jpg) !important;"-->
      <div class="holder">
 	<h2>The Advanced Traders Mindset Course</h2>
        <p>Think Successfully &amp; Trade Profitably</p>
     
          
             <img src="<?php bloginfo( 'template_url' ); ?>/library/images/banners/banner4.png" alt="Advanced Traders">
        <div style="display:inline-block; text-align:center;max-width:355px;">
          <ul class="list">
            <li>Build a Mindset To Take On Any Obstacle</li>
            <li>Develop Real Confidence, Make Money Trading</li>
            <li>Cultivate Success in Your Trading &amp; Life</li>
            
             
          </ul>
		   <a class="popup-opener" href="https://2ndskiesforex.com/advanced-traders-mindset-course/">Yes, I Want To Wire My Brain For Success</a>
        </div>
      </div>
    </div>
  </div>
   * */
  ?>
   

</div>
  <div class="left-scroll-hotspot">
    <img src="<?php echo get_template_directory_uri(); ?>/images/icons/arrow-left.png">
  </div>
  <div class="right-scroll-hotspot">
    <img src="<?php echo get_template_directory_uri(); ?>/images/icons/arrow-right.png">
  </div></div></div>

<?php
/* GEOIP
endif;
*/
?>

<div class="clear"></div>

<div class="holder homepage holder-block">
    <ul class="list">
        <li>
            <a href="<?php echo site_url(); ?>/trade-signals/" class="gotop"><img src="<?php bloginfo('template_url'); ?>/library/images/img-4-1.jpg" alt="price action strategy, ichimoku trading system, forex trading strategies that work">
                <span>Trade Signals</span></a>
        </li>
        <li>
            <a href="<?php echo site_url(); ?>/trading-articles"> <img src="<?php bloginfo('template_url'); ?>/library/images/img-5-1.jpg" alt="price action strategy, ichimoku trading system, forex trading strategies that work">
                <span>Forex Strategies</span></a>
        </li>
        <li>
            <a href="<?php echo site_url(); ?>/advanced-price-action-course"> <img src="<?php bloginfo('template_url'); ?>/library/images/img-6-1.jpg" alt="price action strategy, ichimoku trading system, forex trading strategies that work">
                <span>Price Action Course</span></a>
        </li>
    </ul>
</div>

<div class="holder homepage section-block">
    <div class="col-box">
        <div class="box">
            <h4>Price Action Skills</h4>
            <p>Learn the most essential<br/> price action tools.</p>
            <a href="<?php echo site_url(); ?>/price-action-skills/" class="gotop">Learn Now</a>
        </div>
        <div class="box">
            <h4>Pull the Trigger</h4>
            <p>Cut down the noise and get past<br/> analysis paralysis</p>
            <a href="<?php echo site_url(); ?>/pull-the-trigger/" class="gotop">Improve Your Trading</a>
        </div>
    </div>
    <div class="categories-list">
        <?php dynamic_sidebar('home-sidebar'); ?>
    </div>
</div>

<?php get_footer(); ?>
<style>
a.link.gotop {
    text-decoration: none;
	color:#000000;
	
}
	.container {
	  margin-left: auto;
	  margin-right: auto;
	  padding-left: 15px;
	  padding-right: 15px;
	}
       .banner-homepage .homea .text-box .list {
          margin-bottom: 20px;
        }
       .banner-homepage .text-box a {
          float: left;
          margin-left: 58px;
        }
	@media (max-width: 767px) {
		.container {width: 100%;}
	}
	@media (min-width: 768px) {
		.container {width: 750px;}
	}
	@media (min-width: 992px) {
		.container {width: 970px;}
	}
	@media (min-width: 1200px) {
		.container {width: 1170px;}
	}

	.container {
	  *zoom: 1;
	}
	.container:before,
	.container:after {
	  content: "";
	  display: table;
	}
	.container:after {
	  clear: both;
	}
	.no_padding {
	  padding-left: 0 !important;
	  padding-right: 0 !important;
	}
	[class*="icon-"] {
	  display: inline-block;
	  background-image: url(<?php echo get_template_directory_uri(); ?>/images/sprite.png);
	}
	.left {
	  float: left;
	}
	.right {
	  float: right;
	}
	.clear {
	  display: block;
	  height: 0;
	  width: 0;
	  clear: both;
	}
	.clearfix {
	  *zoom: 1;
	}
	.clearfix:before,
	.clearfix:after {
	  content: "";
	  display: table;
	}
	.clearfix:after {
	  clear: both;
	}
	.l-full {
	  width: 100%;
	}
	.l-align-left {
	  text-align: left;
	}
	.l-align-center {
	  text-align: center;
	}
	.l-align-right {
	  text-align: right;
	}
	.l-stacked {
	  list-style-type: none;
	  margin: 0;
	  padding: 0;
	}
	.l-stacked > li {
	  display: block;
	}
	.l-block {
	  list-style-type: none;
	  margin: 0;
	  padding: 0;
	}
	.l-block > li {
	  display: inline-block;
	  vertical-align: top;
	  *display: inline;
	  *zoom: 1;
	}
	.l-floated {
	  list-style-type: none;
	  margin: 0;
	  padding: 0;
	  *zoom: 1;
	}
	.l-floated:before,
	.l-floated:after {
	  content: "";
	  display: table;
	}
	.l-floated:after {
	  clear: both;
	}
	.l-floated > li {
	  float: left;
	  display: block;
	}
	.section_hero {
	  min-height: 550px;
	  margin-bottom: 50px;
	  background-position: center bottom;
	  background-repeat: no-repeat;
	  background-color: #102773;
	}
	@media only screen and (max-width: 767px) {
	  .section_hero {
	  	padding-top: 50px;
	  }
	}
	@media only screen and (max-width: 991px) {
	  .section_hero {
	    background-image: url('<?php echo get_template_directory_uri(); ?>/images/banners/background-hero.jpg') !important;
	  }
	}
	@media only screen and (min-width: 768px) {
	  .section_hero {
	    padding-top: 100px;
	  }
	}
	.section_hero .title {
	  margin-top: 0;
	  margin-bottom: 0;
	  color: #FFF;
	}
	@media only screen and (max-width: 767px) {
	  .section_hero .title {
	    font: 600 34px/50px 'Open Sans', sans-serif;
	  }
	}
	@media only screen and (min-width: 768px) {
	  .section_hero .title {
	    font: 600 42px/46px 'Open Sans', sans-serif;
	  }
	}
	.section_hero .subtitle {
	  margin-top: 20px;
	  margin-bottom: 0;
	  font: 400 22px/30px 'Open Sans', sans-serif;
	  color: #ffcb04;
	}
	@media only screen and (min-width: 768px) {
	  .section_hero--promo {
	    max-width: 710px;
	  }
	}
	@media only screen and (min-width: 992px) {
		.section_hero--promo {float: right;}
	}
	.section_hero--list {
		display: inline-block;
	  	margin-top: 40px;
	  	max-width: 450px;
	}
	.section_hero--list .bubble__double {
	  position: relative;
	}
	@media only screen and (min-width: 768px) {
	  .section_hero--list .bubble__double {
	    padding-right: 185px !important;
	  }
	}
	.section_hero--list .bubble {
	  position: relative;
	  margin-bottom: 10px;
	  margin-left: 10px;
	  margin-right: 10px;
	  padding: 10px 30px 10px 50px;
	  text-align: left;
	  background: rgba(10, 34, 99, 0.8);
	  -webkit-border-radius: 35px;
	  -moz-border-radius: 35px;
	  border-radius: 35px;
	}
	.section_hero--list .bubble .img {
	  position: absolute;
	  top: 0;
	  left: 20px;
	  bottom: 0;
	  margin: auto;
	}
	.section_hero--list .iconfx {
		margin: 12px 10px;
	}
	.section_hero--list .bubble .inner {
	  position: absolute;
	  top: 5px;
	  right: 5px;
	  padding: 7px 20px 7px 40px;
	  border: 1px solid #1C3578;
	  -webkit-border-radius: 29px;
	  -moz-border-radius: 29px;
	  border-radius: 29px;
	}
	.section_hero--list .bubble .inner .icon4 {
		margin-top: 12px;
		margin-bottom: 12px;
	}
	@media only screen and (max-width: 767px) {
	  .section_hero--list .bubble .inner {
	    display: none;
	  }
	}
	.section_hero--list .bubble .inner-name {
	  margin-top: 0;
	  margin-bottom: 0;
	  font: 600 10px/12px 'Open Sans', sans-serif;
	  color: #FFF;
	}
	.section_hero--list .bubble--name {
	  margin-top: 0;
	  margin-bottom: 0;
	  font: 700 9px/9px 'Open Sans', sans-serif;
	  color: #7fa0ea;
	}
	.section_hero--list .bubble--date {
	  margin-top: 5px;
	  margin-bottom: 3px;
	  font: 700 14px/14px 'Open Sans', sans-serif;
	  color: #FFF;
	}

	.section_register {
	    margin: 30px 40px;
	}
	.section_registernow--list a {
	  position: relative;
	  display: block;
	  width: 100%;
	  padding: 0 5px;
	  text-decoration: none;
	  text-align: center;
	  font: 700 18px/20px 'Open Sans', sans-serif;
	  color: #303030;
	  border-top: 2px solid #ffb500;
	  border-bottom: 2px solid #ffb500;
	}
	.section_registernow--list a:before,
	.section_registernow--list a:after {
	  content: '';
	  display: block;
	  position: absolute;
	  top: 0;
	  height: 100%;
	  width: 5px;
	  background-image: url('<?php echo get_template_directory_uri(); ?>/images/banners/shape2.png');
	  background-repeat: repeat-y;
	}
	.section_registernow--list a:before {
	  left: 0;
	  background-position: center left;
	}
	.section_registernow--list a:after {
	  right: 0;
	  background-position: center right;
	}
	.section_registernow--list a .hover_hax {
	  display: block;
	  padding: 25px 0;
	  -webkit-transition: all 0.2s ease-in-out;
	  -moz-transition: all 0.2s ease-in-out;
	  -o-transition: all 0.2s ease-in-out;
	  transition: all 0.2s ease-in-out;
	  background-color: #ffb500;
	}
	.section_registernow--list a:hover {
	  color: #fff;
	  border-color: #325B8D;
	}
	.section_registernow--list a:hover .hover_hax {
		background-color: transparent;
	}
	.section_registernow--list a:hover:before,
	.section_registernow--list a:hover:after {
	  background-image: url('<?php echo get_template_directory_uri(); ?>/images/banners/shape1.png');
	}
	/*sandeep code*/

	.header-box {
		margin-top: 0;
	}

	.enroll a {
		display: block;
    	line-height: 52px !important;
    	height: 50px;
    	color: #303030;
    	text-align: center;
    	float: right;
    	margin-top: 18px;
    	margin-right: 23%;
    	background: url(/wp-content/themes/2ndSkies/library/images/btn.png) no-repeat;
    	width: 345px;
    	list-style: none;
    	font-family: 'Open Sans', sans-serif;
    	font-size: 16px;
    	font-weight: 600;
	}

	@media screen and (max-width: 979px) {
		.enroll a {
			float: none;
		}
	}
	
</style>
