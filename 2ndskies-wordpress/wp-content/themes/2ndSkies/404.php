<?php get_header(); ?>


	<div id="content">
    


		<div class="default-template">
			<h1>Sorry, we couldn't find that page</h1>
			<p>The page you were trying to reach doesn't seem to exist. We apologize for the inconvenience.</p>

<p><strong> Here’s what you can do now:</strong></p>   
<div class="list">

<ul>

    <li>Use the search function above to find what you are looking for</li>
    <li>Use the navigation bar to browse the site</li>
    <li>Check our <a href="/" title="2ndSkies: Learn to trade forex successfully using price action & ichimoku trading systems.">Home page</a>, it's a good starting point.</li>
    <li>Check our <a href="/forex-courses" title="Interested in trading more profitably and professionally? We’re pleased to offer three informative new online trading courses that will help enhance your knowledge of sophisticated trading strategies and increase your profitable trades.">Trading Courses</a>. We’re pleased to offer three online trading courses that will help enhance your knowledge of sophisticated trading strategies and increase your profitable trades.</li>
    <li>Browse our <a href="/forex-trading-strategies/forex-strategies/" title="Latest Forex Strategy articles">Forex Strategy articles</a>.</li>
    <li>Think that this is an error on our side? Please use our <a href="/contact/">contact form</a> to let us know!</li>

</ul>

</div>




            
            
		</div><!-- .default-template -->

	</div><!-- #content -->

<?php get_footer(); ?>
