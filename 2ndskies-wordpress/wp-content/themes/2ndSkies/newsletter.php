<?php 
/* Template name: Newsletter Page */
?>

<?php get_header(); ?>

<div id="main" class="clearfix">

    <div id="content">

        <div class="breadcrumbs-container clearfix">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="<?php echo home_url(); ?>">Home</a></li>
                    <li><?php the_title(); ?></li>
                </ul>
            </div><!-- .breadcrumbs -->
        </div><!-- .breadcrumbs-container -->

        <?php if (have_posts()) : ?>

        <?php while (have_posts()) : the_post(); ?>

        <div class="default-template">
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
        </div><!-- .default-template -->

        <?php endwhile; ?>

        <?php else : ?>

        <div class="default-template">
            <h1>Not Found</h1>
            <p>Sorry, but you are looking for something that isn't here.</p>
        </div><!-- .default-template -->

        <?php endif; ?>
		
		<div class="newsletter-widget">
		
	<div class="top-content">	
		<img src="<?php bloginfo( 'template_url' ); ?>/library/images/chris_small.jpg" alt="Chris Capre">
		<h2>Chris Capre<span>Buddhist, Trader and Philanthropist</span></h2>
		
		<p><a href="#">Chris Capre</a>, Founder of 2ndSkiesForex, is a professional trader who specializes in trading price action. His blog attracts 150,000+ monthly readers across 110+ countries, including: Australia, USA, Singapore, UK, Canada & Germany. To discover more about Chris's Price Action Strategies, visit his <a href="#">Price Action Course</a> page.</p>
	</div>
		<div class="newsletter">
		<p>Want to Learn Price Action Strategies for Trading Forex?</p>
		
		<div class="subscribe-form-bottom">
			Sign Up for our Monthly Newsletter and GET our <strong>FREE E-Book</strong>
			
			<form action="https://app.getresponse.com/add_subscriber.html" accept-charset="utf-8" method="post">
                    <fieldset>
                        <input type="text" id="name" name="name" placeholder="Your Name" />
                        <input type="text" name="email" placeholder="Email Address" />
                        <input type="submit" class="btn btn-red" value="sign up" name="subscribe">
                        
                        <input type="hidden" name="campaign_token" value="BsQw"/>
                     <!--   <input type="hidden" name="thankyou_url" value="http://2ndskiesforex/newsletter-thank-you/"/>-->
  
                    </fieldset>
            </form>
			<img src="<?php bloginfo( 'template_url' ); ?>/library/images/book-ipad-small.png" alt="Ebook">
		</div><!-- .subscribe-form -->
		
		</div><!-- .newsletter -->

	</div><!-- .newsletter-widget -->

    </div><!-- #content -->
    
	
	<?php get_sidebar('pages'); ?><!-- sidebar -->
    
</div><!-- #main -->

<?php get_footer(); ?>