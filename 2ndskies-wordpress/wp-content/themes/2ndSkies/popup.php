<?php
/*
Template Name: popup
*/
?>

<?php get_header();?>

<div class="grid-container cl-wrapper">
    <!-- main content -->
    <div id="content" class="cl-content">
    	<div class="default-template">
	    <?php
	    if( isset($_COOKIE['testApp']) ){
		showResulst();
	    }

	    if( !isset($_GET["answers"]) && !isset($_COOKIE['testApp']) && (!isset($_GET['start'])))
	    {
	    ?>
		<div class="cl-heading ribbon">
		    <div class="ribbon-content">
			<h1 class="ribbon-heading">Wanting a Tailored Learning Experience?</h1>
			<p><!--Pass the quick test to get recommendations--></p>
		    </div>
		</div>
	    <?php
		echo '<div class="grid-25 tablet-grid-30 mobile-grid-100">';
		echo '<img class="head-cogs aligncenter" src="'. trailingslashit( THEME_IMAGES ) .'il-head-cogs.png">';
		echo '</div>';
		
		echo '<div class="grid-65 tablet-grid-70 mobile-grid-100">';
		echo 'Are you unsure where to begin, and what material to study? To streamline your learning process, we have built a customized learning app that will give suggested content for you, based on your skill level and experience.
		    <br><br>
		    Just answer a few simple questions, and at the end you will be given a recommended list of videos and articles to watch.';
		echo '</div>';
		echo '<div class="clearfix"></div>';
		echo '<a href="/customized-learning?start=1" class="testAppButton testAppButtonStart">Start Now</a>';
	    }
	    if((isset($_GET['start'])))
		showTestQuestions();

	    if (isset($_GET["answers"]) && !empty($_GET["answers"]) && (!isset($_COOKIE['testApp'])))
		getRecommendedSet();
	    ?>
      </div>
    </div>
</div><!-- .grid-container .cl-wrapper-->
<?php get_footer();

function showResulst($setIds = null){
    $cookie_name = 'testApp';
    $cookie_value = $_COOKIE[$cookie_name][setIds];
    
    $class_oncookie = 'testapp-'.$cookie_value;
    
    $class_beginner = 'testapp-beginner';
    
    if ($_COOKIE[$cookie_name][setIds]!='1') { 
	$class_grid = 'grid-50';
    } 

    if ($setIds !== null)
	showLinks($setIds);

    if (isset($_COOKIE['testApp']['items'])){
	showLinks($_COOKIE['testApp']['setIds']);

        $progressBarArr = $_COOKIE['testApp']['items'];
        $totalCount = $_COOKIE['testApp']['totalCount'];
        $ready = 0;
        $left = 0;

        foreach($progressBarArr as $pb)
            if($pb == 1)
                $ready++;
		
        $left = count($progressBarArr)-$ready;
        $percent = intval($ready/$totalCount * 100)."%";// Calculate the percentation
	if ($left == 0 || $ready == $totalCount){
	    // Javascript for updating the progress bar and information
	    echo '<script language="javascript">
		jQuery("#progress").html("<div class=\"left-'.$left.'\" style=\"width:'.$percent.';background-color:#ddd;\">&nbsp;</div>");
		jQuery("#progress").addClass("left-'.$left.'");
		jQuery("#information").html("<span>Finished!</span>");
		</script>';
	}else{	
	    // Javascript for updating the progress bar and information
	    echo '<script language="javascript">
		jQuery("#progress").html("<div class=\"left-'.$left.'\" style=\"width:'.$percent.';background-color:#ddd;\">&nbsp;</div>");
		jQuery("#progress").addClass("left-'.$left.'");
		jQuery("#information").html("<span>'.$left.'/'.$totalCount.'</span> materials left.");
		</script>'; 
	}
    }

    if(($ready == count($progressBarArr)) && ($progressBarArr && $ready))
    {
        $recommendedCourse = '';

	$recommended_message = $_COOKIE['testApp']['setIds'];
//echo '<br/>'.$recommended_message;
//var_dump(strpos($recommended_message,'6'));
//var_dump(!strpos($recommended_message,'7'));
	$recommended_prefix = '<div>Your Recommended Course is the</div>';

	if (strpos($recommended_message,'6') && !strpos($recommended_message,'7')) {
	    $recommendedCourse = $recommended_prefix . ' <a class="button" href="/advanced-price-action-course/">Advanced Price Action Course</a>';
	}elseif(strpos($recommended_message,'7') == true) {
	    $recommendedCourse = $recommended_prefix . ' <a class="button" href="/advanced-ichimoku-course/">Advanced Ichimoku Course</a>';
	}elseif($recommended_message == '20') {
	    $recommendedCourse = $recommended_prefix . ' <a class="button" href="/advanced-price-action-course/">Advanced Price Action Course</a>';
	}elseif($recommended_message == '21') {
	    $recommendedCourse = $recommended_prefix . ' <a class="button" href="/advanced-ichimoku-course/">Advanced Ichimoku Course</a>';
	}else{
	    $recommendedCourse = "";
	}

        switch($_COOKIE['testApp']['setIds']){
           // case '20': $recommendedCourse = 'Your Recommended Course is <a href="/advanced-price-action-course/">Advanced Price Action Course</a>'; break;
           // case '21': $recommendedCourse = 'Your Recommended Course is <a href="/advanced-ichimoku-course/">Advanced Ichimoku Course</a>'; break;
        }
//echo $recommendedCourse;
        if(!isset($_COOKIE['testApp']['code'])){
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function ($){
				var url = "https://courses.2nd-skies-forex.com/courses/callback";
				$.ajax({
					type: 'GET',
					url: url,
					dataType: 'jsonp',
					success: function(json){
						$('#code').html(json);
						var date = new Date;
						date.setDate( date.getDate() + 365 );
						document.cookie = "testApp[code]="+json+"; path=/; expires="+date.toUTCString();
					}
				});
				
			});
        </script>
        <?php } ?>
        <div class="congratulations"><h3>Congratulations!</h3> You've completed our recommended content based on your skill level and experience.<br>Would you like to take your learning process to the next level?</div>
        <div class="congratulations-course"><?php echo $recommendedCourse ?></div>
        <!--There is your Voucher Code for the Recommended Course:<br>
        <div id="code"><?php echo ($_COOKIE['testApp']['code']) ? $_COOKIE['testApp']['code'] : '' ?></div><br>
        <a href="https://www.courses.2ndskies.lh" target="_blank">Buy the Course!</a>-->
    <?php
    }

    if($_COOKIE['testApp']['setIds'] == 1)
    {
        global $wpdb;
        $sql = "SELECT * FROM `wp_recommended_sets` WHERE `id`=1;";
        $items = $wpdb->get_results($sql);
        foreach($items as $item) echo $item->text;
    }
}




function showLinks($setIds){

    function readyCount() {
	$progressBarArr = $_COOKIE['testApp']['items'];
	foreach($progressBarArr as $pb) {
	    if($pb == 1)
		$ready++;
		//$ready = 'yes';
	}
	return $ready;
    }

    global $wpdb;
    $sql = "SELECT * FROM `wp_recommended_sets` WHERE `question_id` IN (".$setIds.");";
    $items = $wpdb->get_results($sql);
    ?>
    <div class="ribbon-2-wrapper">
    <div class="ribbon-2"><div class="ribbon-stitches-top"></div><div class="ribbon-content"><h1 class="center recommended-heading">Recommended Reading and Viewing</h1></div><div class="ribbon-stitches-bottom"></div></div> 
    </div>
    
    <div class="recommended-info">Clicking on any of the links below will automatically open the link in a new tab. You can come back to this list at any time, and it'll keep track of which articles and videos you've read and watched. You can also close this browser at any time and resume whenever you want, by simply visiting the "Customized Learning" option under the tools menu. Clearing your browser's cookies will reset your answers.</div>

    <?php
    echo '<div class="recommended-list grid-50 tablet-grid-50">';

    foreach($items as $item) {
        $links = explode(',',$item->links);
        $names = explode(';',$item->text);
		$images = explode(',',$item->images);
        $totalItemCount = count($links);

        for($i = 0; $i< $totalItemCount; $i++)

            echo '<p class="cl-item ' . readyCount() . ' "><img class="cl-thumb" src="' . THEME_IMAGES . '/cl-thumbs/' . $images[$i] . '"><a id="clitem-'.$links[$i].'" class="cl-url" target="_blank" href="'.$links[$i].'" oncontextmenu="return false;"><span>' . $names[$i] .'</span></a></p>';
    }
	
	echo '</div>'; 
    
        // Javascript for updating the progress bar and information
        /*echo '<script language="javascript">
            document.getElementById("progress").innerHTML="<div class=\"left-'.$left.'\" style=\"width:'.$percent.';background-color:#ddd;\">&nbsp;</div>";
            document.getElementById("information").innerHTML="<span>Finished!</span>";
            </script>';*/
    ?>
	<script>
	jQuery(document).ready(function ($){
		$(function() {  
			var count = $(".cl-item").length;
			$( "#information" ).addClass( 'count-' + count );
			<?php if($_COOKIE['testApp']['items'] == null){?>
			$("#information").html("<span>"+count+"/"+count+"</span> materials left.");
			<?php }?>
		});
	});
	</script>
	<div class="clearfix"></div>
        <div class="cl-footer">
            <div class="progress-wrap">
                <div id="progress"></div>
                <div id="information"><span></span></div>
            </div>
        </div>

    <?php
}

function getRecommendedSet()
{
    $id = $_GET['answers'];
    $usersAnswers = $_GET["usersAnswers"];

    global $wpdb;
    $sql = "SELECT * FROM `wp_recommended_sets` WHERE `question_id`=".$id.";";
    $sets = $wpdb->get_results($sql);

    foreach($sets as $set)
        if($set->is_last)
        {
            $results = $set->text;
            if($set->links)
            {
                $linksArr = explode(',',$set->links);
                $setIds = $set->question_id;
                writeUsersCookie($linksArr,$setIds);
                showResulst($set->question_id);
            }else echo $results;

            if($set->id == 1) writeUsersCookie(0,1);
        }

    if(!isset($sets) || empty($sets) || empty($results))
    {
        (empty($usersAnswers)) ? $usersAnswers = $id : $usersAnswers .= ','.$id;

        $questionNumber = $_GET["questionNumber"] +1;
        showTestQuestions($questionNumber, $usersAnswers);
    }

}

function getRecommendedItems()
{
    global $wpdb;
    $answers = $_GET['usersAnswers'];

    $answers .= ','.$_GET['answers'];
    
    $sql = "SELECT * FROM `wp_recommended_sets` WHERE `question_id` IN (".$answers.");";
    $items = $wpdb->get_results($sql);

    $linksArr = array();

    foreach($items as $item)
        $linksArr = array_merge($linksArr,explode(',',$item->links));

    writeUsersCookie($linksArr,$answers);
    showResulst($answers);
}

function getLastRecommendedItems(){
    global $wpdb;

    $answers = $_GET['usersAnswers'].','.$_GET['answers'];
 
    $sql = "SELECT * FROM `wp_recommended_sets` WHERE `question_id` IN (".$answers.");";
    $items = $wpdb->get_results($sql);

    $linksArr = array();

    foreach($items as $item)
        $linksArr = array_merge($linksArr,explode(',',$item->links));

    writeUsersCookie($linksArr,$_GET['usersAnswers']);
}

function showTestQuestions($questionNumber = 0, $usersAnswers = null)
{
    global $wpdb;

    $sql = "SELECT * FROM `wp_test_questions` WHERE `question_number`=".$questionNumber.";";
    $questions = $wpdb->get_results($sql);
    $num_objects = $wpdb->num_rows;

    if($num_objects > 0){
    ?>
	<form name="form" method="get" action="/customized-learning/" >
    
	<?php
	$value = 0;
	    
	echo '<div id="question1" class="question-form clearfix">';
	    
	    echo '<div class="qnum-wrapper">';
	    echo '<div class="qnum">';
	    echo  $questionNumber +1;
	    echo '</div>';
	    echo '</div>';
    
	foreach($questions as $question)
	{
	    if(!$question->parent_id)
		echo '<h1 class="question-heading">'.$question->question_text.'</h1>';
	    else
			    echo '<div class="row"><label><input type="radio" name="answers" value="'.$question->id.'" checked>  '.$question->question_text.'</label></div>';
    
	    $value ++;
	}

	    echo '<div class="btn-holder">
		    <input type="hidden" name="questionNumber" value="'.$questionNumber.'">
		    <input type="hidden" name="usersAnswers" value="'.$usersAnswers.'">
		    <input type="submit" class="testAppButton" value="Next >>">
		    </div></div>';	
		    
	    
	    echo '</form>';
    }else
        getRecommendedItems();
}


function writeUsersCookie($recommendedItems, $setIds = null)
{
    if(!isset($_COOKIE['testApp']))
    {
        $totalCount = count($recommendedItems);
        $expires = strtotime('next year');

        echo '<script language="javascript">
                    document.cookie = "testApp[totalCount]='.$totalCount.'; path=/; expires='.$expires.'";
                    </script>';

        if(isset($recommendedItems) && !empty($recommendedItems))
        {
            foreach($recommendedItems as $ri)
                echo '<script language="javascript">
                    document.cookie = "testApp[items]['.$ri.']=0; path=/; expires='.$expires.'";
                    </script>';
        }

        if(isset($setIds) && !empty($setIds))
            echo '<script language="javascript">
                    document.cookie = "testApp[setIds]='.$setIds.'; path=/; expires='.$expires.'";
                    </script>';
    }

}

?>



<script>
jQuery(document).ready(function ($){

//	$('.testAppButtonNext').click(function () {
//		var lid = this.id;
//			
//		Reload()
//
//		function Reload(){
//		  setTimeout(function () { PageReload(); }, 4000);
//		}
//	
//		function PageReload(){
//		  //$('.cl-url').fadeOut();
//		  top.location.href=top.location.href
//		} 
//
//	});
	
	
//$('#deleteCookie').click(function () {	
//	$.removeCookie('testApp', null, { path: '/' });
//});
		
});
</script>

<script>
    /*initFinishedTest(){
	var count = $(".cl-item").length;
	    console.log(count);
	    if( count == 0 ){
	    $( "#progress" ).addClass( 'finished' );
	    }else{
		$( "#progress" ).removeClass( 'finished' );
	    }  
    }*/
jQuery(document).ready(function ($){
	
	$('.recommended-list').easyListSplitter({ 
			colNumber: 2,
			direction: 'horizontal'
	});
	
	$(function(){
	    var count = $(".cl-item").length;

	    if( count == 0 ){
	    $( "#progress" ).addClass( 'finished' );
	    }else{
		$( "#progress" ).removeClass( 'finished' );
	    }    
	});
 
	$(function() {
      
		$('.cl-url').click(function () {
		  var lid = this.id;
		  $.cookie('clicked_' + lid, 1, {expires: 30});

		  Reload();
		 //window.opener.location.href = window.opener.location;
		  $(this).addClass('lesson-learned');
		  
		});
		  
		var cookieName = 'clicked_';
		
		$('.cl-url').each(function() {
			var id = $(this).attr('id'), cookie = cookieName + id;
			if ($.cookie(cookie) == 'true') {
				$(this).addClass('lesson-learned');
			}
		}).live('click', function(e) {
			$.cookie(cookieName + $(this).attr('id'), true);
		});
		
	    function Reload(){
		    setTimeout(function () { PageReload(); }, 4000);
	    }
		  
	    function PageReload(){
		  //$('.cl-url').fadeOut();
		  top.location.href=top.location.href
	    } 

		
		
	});
	


	
});
</script>

<?php 
