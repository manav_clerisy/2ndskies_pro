<?php
/*
Template Name: Course - Advanced Price Action
*/
?>
<?php get_header(); ?>
	<!-- main content -->
	<div id="content">
       
		<!-- promo video -->
		<div class="promo-video-block">
			<?php the_post(); ?>
			<?php the_content(); ?>
            
            <?php if (get_post_meta($post->ID, "embed-video", true) != '')   { 
				?>
                <div class="video-holder_ video-holder-new">
                    <?php echo get_post_meta($post->ID, "embed-video", true); ?>
                </div>
            	<?php } 
			?>
            
		</div>
		<!-- promo text -->
		<div class="promo-text clearfix">
			<h2>You want to be a good trader. You have a passion for trading. Recognize the opportunity and see the challenge in this.</h2>
            <p>
            Likely, you have been working hard to trade profitably, but find yourself struggling to produce consistent results. You understand something is missing:
            </p>
			<div class="clearfix space-10">
				<div class="alignright">
					<a href="#"><img src="<?php echo THEME_IMAGES ?>/img12.png" width="382" height="182" alt="image description" /></a>
				</div>
				<div class="list">
					<ul>
						<li>Maybe you've lost more than you've won</li>
						<li>Maybe you just can't find consistency in your trading
</li>
<!--						<li>Several Free Webinar trainings per year</li>
						<li>Ongoing Lessons</li>-->
						<li>Maybe the stress and anxiety of being a trader has overwhelmed you
</li>
						<li>Maybe you've been on a losing streak of late and not sure what to do
</li>
					</ul>
				</div>
			</div>

            
                    <p><strong>If you want to survive the learning curve most traders fail at, you have to go beyond an 'informational course'. Most 'courses' or 'training programs' provide information.

Although information is necessary, it is not sufficient to trade successfully. </strong></p>

<p><strong>We focus on developing skills, have students engage in structured practice, and then tackle real-world trading situations. 

This is why we differ from all other 'courses' out there.
</strong></p>

<p>Through hard work, practice and proper training, you can achieve your trading goals. Those who do make continual progress, see changes in their mindset, and start to trade profitably.</p>
		</div><!-- promo text END -->
        
        <div class="shadow-box blue">
        <blockquote><p class="lead">"If you want something you've never had, you'll have to do things you've never done."</p></blockquote>
        </div>
        
        
        
		<h2 class="center">What You Can Expect To Learn from My Course</h2>
		<!-- table list -->
		<div class="table-list">
			<ul>

				<li>Learn to Trade With the Trend</li>

				<li>Building Successful Trading Psychology </li>

				<li>Identify Order Flow Behind the Price Action</li>

				<li>How to Prepare for Your Trading Day</li>

				<li>Identify Critical Support & Resistance Levels</li>

				<li>Building Expert Trading Skills</li>

				<li>Identify the Most Important Moves in the Market</li>

				<li>Learn How to Gauge Your Growth</li>

				<li>Know When High-Probability Breakouts are Forming</li>

				<li>3-Dimensional Risk Management</li>

				<li>Advanced Pin Bar Trading</li>

				<li>Find Your Natural Trading Style</li>

				<li>Our Intra-day 3P HLR Setup</li>

				<li>Get Over Fears, Doubts, & Build Confidence</li>

				<li>Five 1 & 2 Bar Price Action Patterns</li>

				<li>How to Advance Your Pattern Recognition Skills</li>

				<li>Quantitative Data on 5 Price Action Patterns</li>

				<li>Build A Professional Trading Plan</li>

				<li>Finding Intra-day 6NT Pullbacks</li>

				<li>How to Trade Like a Business</li>

				<li>Understand When Tops & Bottoms are Forming</li>

				<li>Know When To Trade, and When Not To</li>

				<li>When to Trade Counter-Trend</li>

				<li>Build A Mindset of Abundance</li>

			</ul>
		</div>
        

		<!-- testimonials -->
		<div class="testimonials-block adv-bg">
			<h2>Here's What Students Are Saying About The Course...</h2>
			<div class="testimonials-list">
				<div class="list-item">
					<blockquote>
						<q>I really love the PA course and have learned so much about price behavior, order flow and what is really moving the markets. Slowly but surely I'm 
getting profitable with my trades, started in December with a small account and am already up over 20%!.</q>
						<cite>Arthur, Germany &ndash; Course Member</cite>
					</blockquote>
				</div>
				<div class="list-item">
					<blockquote>
						<q>I've just finished the PA course and learned your strategies after watching the ton of valuable material. In Nov/13 when I started this course, I had to cut off some wrong doings from my previous courses I had. However, Now on Dec/13 I've already banked +620pips and I'm feeling like I'm finally getting it as I'm seeing the charts in a whole new way aside from typical price patterns like pin bars. Very happy about that :)</q>
						<cite>Wandy, Singapore &ndash; Course Member</cite>
					</blockquote>
				</div>
				<div class="list-item">
					<blockquote>
						<q>Loving the systems from the Price Action Course as they are all rule-based which completely takes out the guesswork. I've already had several winners Not many people if any can back up their strategies with quantified data.</q>
						<cite>Grant D, UK &ndash; Course Member</cite>
					</blockquote>
				</div>
			</div>
		</div><!-- testimonials-block END -->
        
        
		<!-- text content -->
		<h2>Advanced Pivot Point Trading</h2>
        
		<p>Along with the price action course and training, you also get access to the Advanced Pivot Point Trading Course, specifically focused on trading intra-day with price action.</p>
        
        <div class="alignright"><a href="#"><img src="<?php echo THEME_IMAGES ?>/img13.png" width="449" height="320" alt="image description" /></a></div>
        <p>We've tested more than 10 unique metrics on pivot points over the past 10 years regarding:</p>
		<div class="list">
			<ul>
                <li>The percentage change of any single pivot point being broken on any given day</li>
                <li>The percentage chance that if one pivot is broken, the next one will be touched</li>
                <li>How close price has to get to a pivot before it is likely to touch it</li>
                <li>How to trade reversals with pivot points (reversion to the mean)</li>
                <li>How to find optimum breakouts using pivot points (RS3 breakouts)</li>
			</ul>
		</div>

		<p>You <u>will not find</u> this information anywhere, which would take you years to compile  on your own (as it did for me).</p>

        


        
        
        
		<h2 class="center">Frequently Asked Questions</h2>
		<div class="shadow-box clearfix">
			<ul class="accordion clearfix">
				<?php query_posts( 'cat=1165&posts_per_page=-1' );?>
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
					<li>
						<a href="#" class="opener"><?php the_title();?></a>
						<div class="slide">
							<?php echo get_the_content();?>
						</div>
					</li>
					<?php endwhile; ?>
				<?php endif; ?>
			</ul>
		</div>
        <br>
<!--        <span class="content-border clearfix"></span>-->


<div class="subtle-box subtle-box-content">
<div class="new-headline-sub">
<p><u>YOU GET:</u> 45+ Hours of Instructional Videos, Trade Setups 3x per Week, Trader Quizzes 2x per Week, Live Trade Setups Forum, Members Only Webinars, Private Follow Up Session, Full Email Support & More</p>
</div>
</div>
        

		<!-- order box -->
		<div class="order-box">
			<div class="container">
				<h2>Order the Advanced Price Action Course Here</h2>
				<div class="clearfix">
					<div class="img"><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=1<?php if(isset($_GET['orid'])) echo '&orid='.$_GET['orid']; ?>"><img src="<?php echo THEME_IMAGES ?>/img11a.png" width="459" height="270" alt="image description" /></a></div>
					<div class="text-holder">
				
						<a href="http://courses.2ndskiesforex.com/courses/startPayment?id=1<?php if(isset($_GET['orid'])) echo '&orid='.$_GET['orid']; ?>" class="btn-add">Add To Cart</a>
						<span class="price"><br>
*Normally $499, <strong>NOW</strong> all this<br><span class="newpriceline">for a One Time Fee of <span class="newprice">$315</span><br><strong> - Ends August 31st</strong></span></span>
						<ul class="payment">
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=1<?php if(isset($_GET['orid'])) echo '&orid='.$_GET['orid']; ?>" class="visa">VISA</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=1<?php if(isset($_GET['orid'])) echo '&orid='.$_GET['orid']; ?>" class="mastercard">MasterCard</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=1<?php if(isset($_GET['orid'])) echo '&orid='.$_GET['orid']; ?>" class="american-express">American Express</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=1<?php if(isset($_GET['orid'])) echo '&orid='.$_GET['orid']; ?>" class="discover">DISCOVER</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=1<?php if(isset($_GET['orid'])) echo '&orid='.$_GET['orid']; ?>" class="paypal">PayPal</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="desc">
				<p><span>NOTE:</span> 10+% of all net revenue from course sales goes towards various charities and non-profit organizations around the world. To us, making money is one thing,
but having a positive impact on the world is another thing entirely. We work to uplift the world.</p>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
