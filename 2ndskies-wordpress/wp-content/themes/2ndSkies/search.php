<?php get_header(); ?>
<div id="content">
	<!-- breadcrumbs container -->
	<div class="breadcrumbs-container clearfix">
		<div class="breadcrumbs">
			<ul>
				<li><a href="<?php echo home_url(); ?>">Home</a></li>
				<li>Search Results</li>
			</ul>
		</div>
	</div>
	<div class="posts">
		<?php if (have_posts()) : ?>
		<h1>Search Results <br /><br /></h1>
		<?php while (have_posts()) : the_post(); ?>
		<?php //if(get_post_type() != 'page') { ?>
		<div class="main-post" id="post-<?php the_ID(); ?>">
			<div class="title">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<div class="meta">
					<ul>
						<li><strong class="date"><?php the_time('F jS, Y') ?></strong></li>
						<li>in <?php if(get_post_type() == 'videos') echo '<a href="/videos">videos</a>'; else the_category(', ') ?></li>
						<li>| <?php comments_popup_link('0 comments', '1 comment', '% comments'); ?></li>
					</ul>
				</div>
			</div>
			<div class="contente">
				<?php the_excerpt('Read the rest of this entry &raquo;'); ?>
			</div>
		</div>
		<?php //} ?>
		<?php endwhile; ?>
		<?php wp_pagenavi();?>
		<?php else : ?>
		<h1>Nothing found</h1>
		<p>Try a different search?</p>
		<?php get_search_form(); ?>
		<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>
