<?php
/*
Template Name: Advanced Singapore Seminar FXStreet
*/
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<script src="https://cdn.optimizely.com/js/5767624972.js"></script>
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo( 'template_url' ); ?>/favicon.ico">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<meta name="format-detection" content="telephone=no" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
    <!--[if (lt IE 9) & (!IEMobile)]>
        <script src="<?php echo get_template_directory_uri(); ?>/js/fallbacks/html5shiv-3.7.2.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/fallbacks/selectivizr-1.0.2.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.min.css" />
    <!--[if (lt IE 9) & (!IEMobile)]>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css" />
        <script src="<?php echo get_template_directory_uri(); ?>/js/fallbacks/respond-1.4.2.min.js"></script>
    <![endif]-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/helpers/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/app.min.js"></script>
<?php wp_head(); ?>
    <!-- Start Visual Website Optimizer Asynchronous Code -->
    <script type='text/javascript'>
    var _vwo_code=(function(){
    var account_id=61633,
    settings_tolerance=2000,
    library_tolerance=2500,
    use_existing_jquery=false,
    // DO NOT EDIT BELOW THIS LINE
    f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
    </script>
    <!-- End Visual Website Optimizer Asynchronous Code -->
</head>
<body>
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WLRNLV"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WLRNLV');</script>
    <!-- End Google Tag Manager -->
    <header class="header">
        <div class="container">
            <a href="" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/library/images/logo-traders.png" alt=""></a>
            <ul class="header--menu">
                <li><a class="scroll-to" href="javascript:void(0);" data-scrollto="about-chris">About Chris</a></li>
                <li><a class="scroll-to" href="javascript:void(0);" data-scrollto="seminar-topics">Seminar Topics</a></li>
                <li><a class="scroll-to" href="javascript:void(0);" data-scrollto="reserve-your-seat">Reserve Your Seat</a></li>
            </ul>
        </div>
    </header>
    <main class="main">
        <section class="section_hero" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/banners/background-hero2.jpg);">
            <div class="container">
                <div class="section_hero--promo l-align-center">
                    <h1 class="title">Singapore Trading Seminar<br />with Chris Capre</h1>
                    <h3 class="subtitle">Change the Way You Think, Trade & Perform</h3>
                    <ul class="section_hero--list l-block l-align-center">
                        <li>
                            <div class="bubble">
                                <i class="icon-hero-calendar"></i>
                                <h4 class="bubble--name">Date</h4>
                                <div class="bubble--date">July 11 /12th 2015</div>
                            </div>
                        </li><li>
                            <div class="bubble">
                                <i class="icon-hero-clock"></i>
                                <h4 class="bubble--name">Times</h4>
                                <div class="bubble--date">9am - 5pm</div>
                            </div>
                        </li><li>
                            <div class="bubble bubble__double">
                                <i class="icon-hero-pin"></i>
                                <h4 class="bubble--name">Location</h4>
                                <div class="bubble--date">Singapore</div>
                                <div class="inner">
                                    <i class="icon-hero-statue"></i>
                                    <h4 class="inner-name">Ritz<br />Carlton</h4>
                                </div>
                            </div>
                        </li>
                        <li id="sponsored-by-fxstreet">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/fxstreet.png" />
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="section_registernow" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/banners/background-register.jpg);">
            <div class="container l-align-center">
                <h3 class="title">Register now! Only <span class="line-through">&nbsp;50&nbsp;</span> 21 seats are available.</h3>
                <ul class="section_registernow--list l-floated clearfix">
                    <li class="col-xs-12 col-md-6 section_registernow--list__left">
                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                            <input type="hidden" name="cmd" value="_s-xclick">
                            <input type="hidden" name="hosted_button_id" value="5Q4H4KE73RX7C">
                            <a href="javascript:void(0)" onclick="jQuery(this).parent().submit();"><span class="hover_hax">$625 Special Discount Price for FXstreet Clients</span></a>
                        </form>
                    </li>
                </ul>
                <a class="scroll-to scroll-down" href="javascript:void(0);" data-scrollto="about-chris"><i class="icon-arrow-down"></i></a>
            </div>
        </section><!-- /.section_registernow -->
        <section id="about-chris" class="section_about" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/banners/background-chris.jpg);">
            <div class="custom_container clearfix">
                <img src="<?php echo get_template_directory_uri(); ?>/images/banners/guy-small.png" alt="" class="image" />
                <div class="floating_fluid">
                    <h2 class="title"><span class="i--wrapper"><i class="icon-microphone"></i></span><span><strong>About Chris Capre</strong></span></h2>
                    <p class="quote">
                        "Hi - I'm Chris Capre, and I'm a <strong>Buddhist</strong>, <strong>Trader</strong> & <strong>Philanthropist</strong>.<br/>I <strong>trade</strong>, <strong>meditate</strong> and <strong>donate</strong>.<br/>I've spent the last 20 years studying <strong>Neuroscience</strong> and <strong>Peak Performance</strong>.<br/>For 15 of those years, I've been <strong>Trading</strong> and <Strong>Meditating</strong> every day.<br/>This is what I'll be sharing with you at this <strong>must-attend seminar</strong>."
                    </p>
                    <div class="author">Chris Capre</div>
                    <div class="occupation">Founder of 2ndSkiesForex</div>
                </div>
            </div>
        </section><!-- /section_about -->
        <section class="section_introduction" id="section_introduction">
            <div class="l-align-center">
                <h3 class="title" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/banners/background-seminar-title.png');"><i class="icon-book"></i><span>Trading seminar</span></h3>
            </div>
            <div class="custom_container clearfix">
                <img src="<?php echo get_template_directory_uri(); ?>/images/banners/image-set.png" alt="" class="image">
                <div class="floating_fluid">
                    <p class="quote">
                        "<strong>Elite performers</strong> put more into the training process than the actual event.<br/><br/>After this trading seminar, you will never see <strong>price action</strong> the same, and have a completely different <strong>mindset</strong> about <strong>success</strong>.<br/><br/><strong>Become</strong> the next <strong>elite trader</strong>."
                    </p>
                    <div class="author">Chris Capre</div>
                    <div class="occupation">Founder of 2ndSkiesForex</div>
                </div>
            </div>
        </section><!-- /.section_introduction -->
        <section id="seminar-topics" class="section_seminar" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/banners/background-speakers.jpg);">
            <div class="custom_container">
                <h3 class="title"><strong>Seminar Topics</strong></h3>
                <ul class="section_seminar--list_topics l-stacked">
                    <li>The 90/10 Rule for Trading</li>
                    <li>Building Your 4 Pillars for Success</li>
                    <li>Advanced Models for Understanding Impulsive & Corrective Price Action</li>
                    <li>Improving Your +R Per Trade</li>
                    <li>Advanced Tactics for Intra-day trading</li>
                    <li>Optimizing Your Training Routine</li>
                    <li>Re-wire Your Mindset for Success</li>
                    <li>Visualization Techniques for Success & the Mindset of Abundance</li>
                    <li>Mindset Tactics to Transform & Overpower Your Emotions</li>
                </ul>
                <ul class="section_seminar--list_speakers">
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/banners/speaker1.png" alt=""></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/banners/speaker2.png" alt=""></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/banners/speaker3.png" alt=""></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/banners/speaker4.png" alt=""></li>
                </ul>
                <div class="clear"></div>
            </div>
        </section><!-- /.section_seminar -->
        <section id="reserve-your-seat" class="section_register">
            <div class="custom_container l-align-center">
                <h3 class="title">Register right now!</h3>
                <h4 class="subtitle">Do not miss your chance. Only <span class="line-through">&nbsp;50&nbsp;</span> 40 seats available!</h4>
                <ul class="section_registernow--list l-floated clearfix">
                    <li class="col-xs-12 col-md-6 section_registernow--list__left">
                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                            <input type="hidden" name="cmd" value="_s-xclick">
                            <input type="hidden" name="hosted_button_id" value="5Q4H4KE73RX7C">
                            <a href="javascript:void(0)" onclick="jQuery(this).parent().submit();"><span class="hover_hax">$625 Special Discount Price for FXstreet Clients</span></a>
                        </form>
                    </li>
                </ul>
                <div class="wrap-envelope">
                    <span class="i-wrap">
                        <i class="icon-envelope"></i>
                    </span>
                </div>
                <p class="details">
                        Or you can reserve your seat for a $250 USD non-refundable deposit -<br /><a class="scroll-to" href="javascript:void(0);" data-scrollto="section-contact">email me using the form below</a>
                </p>
            </div>
        </section><!-- /.section_register -->
        <section class="section_maps" id="map_canvas"></section><!-- /.section_maps -->
        <section id="section-contact" class="section_contact clearfix" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/banners/background-form.png);">
            <div class="custom_container">
                <h3 class="title">Have questions?</h3>
                    <?php echo do_shortcode( '[contact-form-7 id="14507" title="CONTACT FORM - SINGAPORE"]' ); ?>
                <div class="clear"></div>
            </div>
        </section><!-- /.section_contact -->
    </main>
    <footer class="footer l-align-center">
        Copyright &copy; 2015 Singapore Trading Seminar with Chris Capre. All rights reserved.
    </footer>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>

        function initialize() {

            var mapCanvas = document.getElementById('map_canvas');
            var mapOptions = {
              center: new google.maps.LatLng(1.291867, 103.860364),
              zoom: 17,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              scrollwheel: false,
            }
            var map = new google.maps.Map(mapCanvas, mapOptions)
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(1.290867, 103.860364),
                map: map,
                icon : "<?php echo get_template_directory_uri(); ?>/images/banners/custom-pin.png"
            })

            var name = 'Singapore Trading Seminar<br />with Chris Capre';
            var address = 'The Ritz-Carlton, Millenia Singapore<br />7 Raffles Ave, Singapore 039799';
            var email = 'chris@2nd-skies-forex.com';
            var content = '';
            content = content + '<div class="section_maps--popup">';
            content = content + '<h5  class="section_maps--popup--title">' + name + '</h5>';
            content = content + '<div class="section_maps--popup--item">';
            content = content + '<h6 class="section_maps--popup--name">Address</h6>';
            content = content + '<p class="section_maps--popup--description">' + address + '</p>';
            content = content + '</div>'

            var infowindow = new google.maps.InfoWindow({
                disableAutoPan: false,
                maxWidth: 450,
                //pixelOffset: new google.maps.Size(225, 140),
                zIndex: null,
                boxStyle: {
                    background: "url('<?php echo get_template_directory_uri(); ?>/images/banners/tipbox.png') no-repeat",
                    opacity: 0.75,
                    width: "45px"
                },
                closeBoxMargin: "12px 4px 2px 2px",
                closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                infoBoxClearance: new google.maps.Size(1, 1)
            });
            google.maps.event.addListener(marker,'click', (
                function(marker,content,infowindow){
                    return function() {
                       infowindow.setContent(content);
                       infowindow.open(map,marker);
                    };
                })(marker,content,infowindow)
            );
            infowindow.setContent(content);
            infowindow.open(map,marker);
        }
        google.maps.event.addDomListener(window, 'load', initialize);

        jQuery(".scroll-to").on("click", function(e){

            jQuery("html, body").animate({ scrollTop: jQuery("#"+ jQuery(this).attr("data-scrollto")).offset().top}, 800);

            e.preventDefault();
            return false;
        });
    </script>
<?php wp_footer(); ?>
</body>
</html>
