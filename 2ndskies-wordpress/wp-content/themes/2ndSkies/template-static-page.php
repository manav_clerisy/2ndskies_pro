<?php
/*
Template Name: empty template
*/
?>
<?php get_header(); ?>

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
	<?php the_content(); ?>
<?php endwhile; ?>
<?php endif; ?>
<Style type="text/css">
.page-template-template-static-page div#content {
    padding-top: 69px;
}
</Style>

<?php get_footer(); ?>