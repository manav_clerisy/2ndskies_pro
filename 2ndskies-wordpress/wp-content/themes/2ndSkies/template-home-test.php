<?php 
/*
Template Name: HOME TEST
*/

get_header(); 
?>
		
		<div class="promo">
			<div class="center-container">
				<img src="<?php echo THEME_IMAGES ?>/img01.png" width="517" height="297" alt="" class="bg-img" />
				<?php the_post(); ?>
				<?php the_content(); ?>
				<div class="video-box">
					<div class="video-holder">
						<div class="lightbox-holder">
							<div id="home-popup01" class="home-popup">
								<div class="lightbox-video">
									<?php echo $embed = get_post_meta($post->ID, "embed-video", true); ?>
								</div><!-- .lightbox-video -->
							</div><!-- #home-popup01 -->
						</div><!-- .lightbox-holder -->
						<?php echo $embed = get_post_meta($post->ID, "embed-video", true); ?>
						<a href="#home-popup01" class="lightbox-opener-link popup-video"></a>
					</div><!-- .video-holder -->
					<span class="desc">Watch our 2 minute introductory video</span>
				</div><!-- .video-box -->
			</div><!-- .center-container -->
		</div><!-- .promo -->

		<div class="section-images">
			<div class="center-container">
				
				<div class="featured-article">
					<?php
						$posts = get_posts('category_name=featured-article&orderby=post_date&order=DESC&posts_per_page=1');
						foreach ($posts as $post) { ?>
							<div class="photo">
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail(array(344, 331)); ?>
								</a>
							</div><!-- .photo -->
							<div class="text-holder">
								<div class="meta-info"><a href="/category/featured">Featured</a><?php //the_category(', ') ?> | <?php echo mysql2date('j M Y', $post->post_date); ?> at <?php echo mysql2date('H:m', $post->post_date); ?></div><!-- .meta-info -->
								<span class="front-page-title"><a href="<?php the_permalink(); ?>">Featured Post</a></span>
								<div class="content">
									<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
									<div class="footer">
										<em>By</em> Chris Capre<?php //the_author(); ?> in <a href="/category/featured">Featured</a><?php //the_category(', ') ?>
									</div><!-- .footer -->
								</div><!-- .content -->
							</div><!-- .text-holder -->
						<?php
						}
						wp_reset_query();
					?>
				</div><!-- .featured article -->

				<div class="trading-lab">
					<a class="trading-lab-uri" href="/trading-library"><h4>Trading Library</h4><img src="<?php echo THEME_IMAGES ?>/trading-library.png" width="250" height="127" alt="" /></a>
                    
                    
                        <figure class="trading-tools">
                        	<a href="#"><h4>Our Trading Tools</h4></a>
                            <a href="#" class="btn btn-red" role="button">Try It Now</a> 
                        	<a href="#"><img src="<?php echo THEME_IMAGES ?>/trading-tools.jpg" width="246" height="155" alt="Our Trading Tools" /></a>
                        </figure>
                    
                    
				</div><!-- .trading-lab -->
                
				<div class="daily-signals">
					<?php
						$posts = get_posts('category_name=daily-signals&orderby=post_date&order=DESC&posts_per_page=1');
						add_image_size( 'mycustomsize', 284, 185, true );
						foreach ($posts as $post) { ?>
							<div class="header">
								<h4><em class="date"><?php echo mysql2date('j M Y', $post->post_date); ?></em> Daily Trade Setups</h4>
							</div>
                            <div class="daily-wrapper">
								<div class="photo"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('medium'); ?></a></div>
                            	<div class="daily-content"><p><strong><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></strong></p><p class="dc-content"><?php echo excerpt(10); ?></p></div>
                            </div>
						<?php
						}
						wp_reset_query();
					?>
				</div><!-- .daily signals -->
                
                
			</div><!-- .center-container -->
		</div><!-- .section-images -->
		
		<div class="section-add-categories">
			<div class="center-container">
				<div class="categories-list">
					<?php dynamic_sidebar('home-sidebar'); ?>
				</div><!-- .categories-list -->
			</div><!-- .center-container -->
		</div><!-- .add categories -->

<?php get_template_part('inc','popup'); ?>
   
<?php get_footer(); ?>