<?php
/*
Template Name: Advanced Singapore Page (NEW)
*/
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo( 'template_url' ); ?>/favicon.ico">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<meta name="format-detection" content="telephone=no" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
    <!--[if (lt IE 9) & (!IEMobile)]>
        <script src="<?php echo get_template_directory_uri(); ?>/js/fallbacks/html5shiv-3.7.2.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/fallbacks/selectivizr-1.0.2.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.min.css" />
    <!--[if (lt IE 9) & (!IEMobile)]>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css" />
        <script src="<?php echo get_template_directory_uri(); ?>/js/fallbacks/respond-1.4.2.min.js"></script>
    <![endif]-->
    <script src="<?php echo get_template_directory_uri(); ?>/js/helpers/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/app.min.js"></script>
<?php wp_head(); ?>
    <!-- Start Visual Website Optimizer Asynchronous Code -->
    <script type='text/javascript'>
    var _vwo_code=(function(){
    var account_id=61633,
    settings_tolerance=2000,
    library_tolerance=2500,
    use_existing_jquery=false,
    // DO NOT EDIT BELOW THIS LINE
    f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
    </script>
    <!-- End Visual Website Optimizer Asynchronous Code -->
</head>
<body>
    <header class="header">
        <div class="container">
            <a href="" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/images/banners/logo.png" alt=""></a>
            <ul class="header--menu">
                <li><a href="#">About Chris</a></li>
                <li><a href="#">Seminar Topics</a></li>
                <li><a href="#">Reserve Your Seat</a></li>
            </ul>
        </div>
    </header>
    <main class="main">
        <section class="section_hero" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/banners/background-hero2.jpg);">
            <div class="container">
                <div class="section_hero--promo l-align-center">
                    <h1 class="title">Singapore Trading Seminar<br />with Chris Capre</h1>
                    <h3 class="subtitle">Change the Way You Think, Trade & Perform</h3>
                    <ul class="section_hero--list l-block l-align-center">
                        <li>
                            <div class="bubble">
                                <i class="icon-hero-calendar"></i>
                                <h4 class="bubble--name">Date</h4>
                                <div class="bubble--date">July 11 /12th 2015</div>
                            </div>
                        </li><li>
                            <div class="bubble">
                                <i class="icon-hero-clock"></i>
                                <h4 class="bubble--name">Times</h4>
                                <div class="bubble--date">9am - 5pm</div>
                            </div>
                        </li><li>
                            <div class="bubble bubble__double">
                                <i class="icon-hero-pin"></i>
                                <h4 class="bubble--name">Location</h4>
                                <div class="bubble--date">Singapore</div>
                                <div class="inner">
                                    <i class="icon-hero-statue"></i>
                                    <h4 class="inner-name">Marina<br />Bay Sands</h4>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="section_registernow" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/banners/background-register.jpg);">
            <div class="container l-align-center">
                <h3 class="title">Register now! Only 50 seats are available.</h3>
                <ul class="section_registernow--list l-floated clearfix">
                    <li class="col-xs-12 col-md-6 section_registernow--list__left">
                        <a href=""><span class="hover_hax">$625 for current course member</span></a>
                    </li>
                    <li class="col-xs-12 col-md-6 section_registernow--list__right">
                        <a href=""><span class="hover_hax">$849 for non-course members</span></a>
                    </li>
                </ul>
                <a href="" class="scroll-down"><i class="icon-arrow-down"></i></a>
            </div>
        </section><!-- /.section_registernow -->
        <section class="section_about" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/banners/background-chris.jpg);">
            <div class="custom_container clearfix">
                <img src="<?php echo get_template_directory_uri(); ?>/images/banners/guy-small.png" alt="" class="image" />
                <div class="floating_fluid">
                    <h2 class="title"><span class="i--wrapper"><i class="icon-microphone"></i></span><span>About Chris</span></h2>
                    <p class="quote">
                        "Elite performers put more into training than the actual event. After this course, you will never see price action the same. Become the next elite trader."
                    </p>
                    <div class="author">Chris Capre</div>
                    <div class="occupation">Founder of 2ndSkiesForex</div>
                </div>
            </div>
        </section><!-- /section_about -->
        <section class="section_introduction">
            <div class="l-align-center">
                <h3 class="title" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/banners/background-seminar-title.png');"><i class="icon-book"></i><span>Trading seminar</span></h3>
            </div>
            <div class="custom_container clearfix">
                <img src="<?php echo get_template_directory_uri(); ?>/images/banners/image-set.png" alt="" class="image">
                <div class="floating_fluid">
                    <h4 class="subtitle">Introduction of seminar</h4>
                    <p class="description">
                        Most courses just provide 'information'. If you want to survive the learning curve most traders fail at, you have to go beyond an 'informational course'.
                        <br /><br />
                        Our course is completely different. We focus on <strong>developing skills</strong>, have students engage in <strong>structured practice</strong>, then tackle real-world situations. We are a <strong>'skill-based course'</strong> that is solution focused, which is why we differ from all the other 'courses' out there.
                    </p>
                </div>
            </div>
        </section><!-- /.section_introduction -->
        <section class="section_seminar" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/banners/background-speakers.jpg);">
            <div class="custom_container">
                <h3 class="title">Seminar Topics</h3>
                <ul class="section_seminar--list_topics l-stacked">
                    <li>#</li>
                    <li>The 3 Stages of Your Development & What They Mean For You</li>
                    <li>Advanced models of understanding impulsive and corrective moves</li>
                    <li>Advanced tactics for trading intra-day</li>
                    <li>The 90/10 Rule for Trading</li>
                    <li>Improving Your +R per trade</li>
                    <li>Optimizing Your Training Routine</li>
                    <li>Visualization Techniques for Success & The Mindset of Abundance</li>
                    <li>Re-wiring your mindset & bias for success</li>
                    <li>Mindset techniques to transform and overpower your emotions</li>
                    <li>Building Your 4 Pillars for Success</li>
                </ul>
                <ul class="section_seminar--list_speakers">
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/banners/speaker1.png" alt=""></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/banners/speaker2.png" alt=""></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/banners/speaker3.png" alt=""></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/images/banners/speaker4.png" alt=""></li>
                </ul>
                <div class="clear"></div>
            </div>
        </section><!-- /.section_seminar -->
        <section class="section_register">
            <div class="custom_container l-align-center">
                <h3 class="title">Register right now!</h3>
                <h4 class="subtitle">Do not miss your chance. Only 50 seat is available!</h4>
                <ul class="section_registernow--list l-floated clearfix">
                    <li class="col-xs-12 col-md-6 section_registernow--list__left">
                        <a href="#"><span class="hover_hax">$625 for current course members</span></a>
                    </li>
                    <li class="col-xs-12 col-md-6 section_registernow--list__right">
                        <a href="#"><span class="hover_hax">$849 for non-course members</span></a>
                    </li>
                </ul>
                <div class="wrap-envelope">
                    <span class="i-wrap">
                        <i class="icon-envelope"></i>
                    </span>
                </div>
                <p class="details">
                    Or you can reserve seat fpr $250 non-refundable deposit -<br /><a href="mailto:chris@2nd-skies-forex.com">email mailto:chris@2ndskiesforex.com for details</a>
                </p>
            </div>
        </section><!-- /.section_register -->
        <section class="section_maps" id="map_canvas"></section><!-- /.section_maps -->
        <section class="section_contact clearfix" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/banners/background-form.png);">
            <div class="custom_container">
                <h3 class="title">Have questions?</h3>
                <div class="col-xs-12 col-md-6 custom_padding">
                    <div class="form-item">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="input" id="name" />
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 custom_padding">
                    <div class="form-item">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="input" id="email" />
                    </div>
                </div>
                <div class="col-xs-12 custom_padding">
                    <div class="form-item">
                        <label for="question">Question</label>
                        <textarea name="question" id="question" class="textarea" cols="30" rows="10"></textarea>
                    </div>
                </div>
                <div class="col-xs-12 custom_padding">
                    <div class="form-controls">
                        <button type="submit" class="button">Send</button>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </section><!-- /.section_contact -->
    </main>
    <footer class="footer l-align-center">
        Copyright &copy; 2015 Singapore Trading Seminar with Chris Capre. All rights reserved.
    </footer>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
        function initialize() {
            var mapCanvas = document.getElementById('map_canvas');
            var mapOptions = {
              center: new google.maps.LatLng(1.290867, 103.860364),
              zoom: 17,
              mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var map = new google.maps.Map(mapCanvas, mapOptions)
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(1.290867, 103.860364),
                map: map,
                icon : "<?php echo get_template_directory_uri(); ?>/images/banners/custom-pin.png"
            })

            var name = 'Singapore Trading Seminar<br />with Chris Capre';
            var address = 'The Ritz-Carlton, Millenia Singapore<br />7 Raffles Ave, Singapore 039799';
            var email = 'chris@2nd-skies-forex.com';
            var content = '';
            content = content + '<div class="section_maps--popup">';
            content = content + '<h5  class="section_maps--popup--title">' + name + '</h5>';
            content = content + '<div class="section_maps--popup--item">';
            content = content + '<h6 class="section_maps--popup--name">Address</h6>';
            content = content + '<p class="section_maps--popup--description">' + address + '</p>';
            content = content + '<div class="section_maps--popup--item">';
            content = content + '<h6 class="section_maps--popup--name">E-mail</h6>';
            content = content + '<p class="section_maps--popup--description">'  + email + '</p>';
            content = content + '</div>'

            var infowindow = new google.maps.InfoWindow({
                disableAutoPan: false,
                maxWidth: 450,
                //pixelOffset: new google.maps.Size(225, 140),
                zIndex: null,
                boxStyle: {
                    background: "url('<?php echo get_template_directory_uri(); ?>/images/banners/tipbox.png') no-repeat",
                    opacity: 0.75,
                    width: "45px"
                },
                closeBoxMargin: "12px 4px 2px 2px",
                closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                infoBoxClearance: new google.maps.Size(1, 1)
            });
            google.maps.event.addListener(marker,'click', (
                function(marker,content,infowindow){
                    return function() {
                       infowindow.setContent(content);
                       infowindow.open(map,marker);
                    };
                })(marker,content,infowindow)
            );
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
<?php wp_footer(); ?>
</body>
</html>
