// accordion menu init
jQuery(document).ready(function ($){

// slideAccordion
	$('ul.accordion').slideAccordion({
		opener: 'a.opener',
		slider: '.slide',
		animSpeed: 1000
	});

// transparent iframe
	$('iframe').each(function(){
		var url = $(this).attr("src")
		$(this).attr("src",url+"?autohide=1&amp;rel=0&amp;showinfo=0&amp;wmode=transparent")
	});

// Target your .container, .wrapper, .post, etc.
	$(".content").fitVids();
	$(".promo-video-block").fitVids(); 
	$(".v-pop").fitVids(); 
	$(".default-template").fitVids(); 
	$(".home-popup").fitVids(); 

// content tabs init
	$('ul.tabset').contentTabs({
		addToParent: true,
		animSpeed: 1000,
		effect: 'fade',
		tabLinks: 'a'
	});

// remove scroll on click
	$('#sidebar .tabs .post-list+.btn-more').click(function(e) {
		e.preventDefault();
		$(this).hide();
		$('#sidebar .post-list').css({'max-height': '9999px'});
	});

// magnific popup
	$('.popup-video').magnificPopup({
		type:'inline',
		closeBtnInside: true
	});
	
// cycle scroll gallery init
	$('.slideshow').scrollAbsoluteGallery({
		mask: '.mask',
		slider: '.slideset',
		slides: '.slide',
		btnPrev: 'a.btn-prev',
		btnNext: 'a.btn-next',
		stretchSlideToMask: true,
		maskAutoSize: true,
		autoRotation: false,
		switchTime: 3000,
		animSpeed: 1000
	});

// slideshow-testimonials
	$('.slideshow-testimonials').scrollAbsoluteGallery({
		mask: '.mask',
		slider: '.slideset',
		slides: '.slide',
		btnPrev: 'a.btn-prev, .left-scroll-hotspot',
		btnNext: 'a.btn-next, .right-scroll-hotspot',
		stretchSlideToMask: true,
		maskAutoSize: true,
		autoRotation: true,
		switchTime: 6000,
		animSpeed: 1000
	});
	

// full clickable area
	$('.chapter-list .list-item').click(function(){
		window.location=$(this).find("a").attr("href"); return false;
	});

	lib.each(lib.queryElementsBySelector('#nav ul'), function(){
		new TouchNav({
			navBlock: this
		});
	});

// open subscribe form
	$('.btn-subscribe').on('click', function(){
		$(this).hide();
		$('.subscribe-form form').slideDown();
	})

// initPositionDrop
	function PositionDrop() {
		var navList = $('.center-container');
		var lis = navList.find('#nav ul> li');
		var navW = navList.width();
		
		lis.each(function() {
			var li = $(this);
			var drop = li.find('> .sub-menu');
			var dropW = drop.width();
			
			if(li.offset().left + dropW > navW) {
				li.addClass('right-side');
			}
			else{
				li.removeClass('right-side');
			}
		});
	}
	PositionDrop();
	$(window).resize(PositionDrop);

//placeholder
	$('input, textarea').placeholder();

//navmenu
	$('#nav .opener').click(function(e) {
		e.preventDefault();
		$('#nav').toggleClass('active');
	});
});