jQuery(document).ready(function ($){
	$(function(){
		var test_app_url = window.location.pathname;
		var arr_cookie_val = false;
		var arr_cookie_newval = false;
		var doc_cookie = document.cookie;
		var arr_cookie = doc_cookie.split(';');
		$.each(arr_cookie, function( index, value ) {
			arr_cookie_val = value.split('=');
			$.each(arr_cookie_val, function( index, value ) {
				arr_cookie_newval = value.split('/');
				$.each(arr_cookie_newval, function( index, value ) {
					if (arr_cookie_val[1] != 1) {
					if(substr_count( test_app_url , arr_cookie_newval[arr_cookie_newval.length-2] )){
						var part1 = '';
						if (arr_cookie_newval[arr_cookie_newval.length-3] != 'forex-videos'){
							part1 = "/"+arr_cookie_newval[arr_cookie_newval.length-4];
						}
						var part2 = "/"+arr_cookie_newval[arr_cookie_newval.length-3];
						var part3 = "/"+arr_cookie_newval[arr_cookie_newval.length-2]+"/";
						document.cookie = "testApp[items]["+part1+part2+part3+"]=1; path=/;";
					}
					}
				});
			});
		});
	});
	
	function substr_count( haystack, needle, offset, length ) {
	    var pos = 0, cnt = 0;
	 
	    if(isNaN(offset)) offset = 0;
	    if(isNaN(length)) length = 0;
	    offset--;
	 
	    while( (offset = haystack.indexOf(needle, offset+1)) != -1 ){
	        if(length > 0 && (offset+needle.length) > length){
	            return false;
	        } else{
	            cnt++;
	        }
	    }
	 
	    return cnt;
	}
});