<?php

	if ( function_exists('register_sidebar') ) {
		register_sidebar(array(
			'id' => 'default-sidebar',
			'name' => 'Default Sidebar',
			'before_widget' => '<div class="widget %2$s" id="%1$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3>',
			'after_title' => '</h3>'
		));
		register_sidebar(array(
			'id' => 'home-sidebar',
			'name' => 'Home Sidebar',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '<h3>',
			'after_title' => '</h3>'
		));
		register_sidebar(array(
			'id' => 'footer-sidebar',
			'name' => 'Footer Sidebar',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '<h2>',
			'after_title' => '</h2>'
		));
		register_sidebar(array(
			'id' => 'social-networks-sidebar',
			'name' => 'Social Sidebar',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '<h2>',
			'after_title' => '</h2>'
		));
		register_sidebar(array(
			'id' => 'copyright-sidebar',
			'name' => 'Copyright Sidebar',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '<h2>',
			'after_title' => '</h2>'
		));
		register_sidebar(array(
			'id' => 'testimonials-sidebar',
			'name' => 'Testimonials Sidebar',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '<h2>',
			'after_title' => '</h2>'
		));
	}