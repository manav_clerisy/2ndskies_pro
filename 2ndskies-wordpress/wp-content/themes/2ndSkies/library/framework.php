<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * Framework - core of WP Theme which defines the main class
 * class ThemeSSFRX Main class loads all includes, functions, adds/removes filters, sets variables.
 *
 */

if ( !class_exists( 'ThemeSSFRX' ) ) {

	class ThemeSSFRX {

		/**
		 * Constructor method for the Main class.  This method adds other methods of the class to
		 * specific hooks within WordPress.  It controls the load order of the required files for running
		 * the framework.
		 *
		 */
		function __construct() {
			global $themeliafw;

			/* Set up an empty class for the global $themeliafw object. */
			$themeliafw = new stdClass;

			/* Define constants. */
			add_action( 'after_setup_theme', array( $this, 'enviroment' ), 1 );
			
			/* Loads all the core functions for Theme. */
			add_action( 'after_setup_theme', array( $this, 'framework' ), 2 );
			
			/* Load the core functions/classes required by the rest of the framework. */
			add_action( 'after_setup_theme', array( $this, 'includes' ), 4 );
		}

		/**
		 * enviroment() defines themes directory constants
		 *
		 * @since 1.0
		 */
		function enviroment() {

			/* Sets the path to the parent theme directory. */
			define( 'THEME_DIR', get_template_directory() );

			/* Sets the path to the parent theme directory URI. */
			define( 'THEME_URI', get_template_directory_uri() );

			/* Sets the path to the core framework directory. */
			define( 'LIBRARY_DIR', trailingslashit( THEME_DIR ) . basename( dirname( __FILE__ ) ) );

			/* Sets the path to the core framework directory URI. */
			define( 'LIBRARY_URI', trailingslashit( THEME_URI ) . basename( dirname( __FILE__ ) ) );

			define( 'THEME_FUNCTIONS', trailingslashit( LIBRARY_DIR ) . 'functions' );

			define('THEME_CSS', trailingslashit( LIBRARY_URI ) . 'css');

			define('THEME_JS', trailingslashit( LIBRARY_URI ) . 'js');
			
			define('THEME_IMAGES', trailingslashit( LIBRARY_URI ) . 'images');
			
			// Video Categories
			define('TESTIMONIALS', 6);
			define('PriceActionTradingVideos', 9);
			define('IchimokuCloudTradingVideos', 10);
			define('ForexTradingTradingPsychologyVideos', 11);
			define('FAQ', 13);
			
		}

		/**
		 * framework() includes widgets and sidebars
		 *
		 */
		function framework() {
			
			require_once( trailingslashit( THEME_FUNCTIONS ) . 'sidebars.php' );
			
			require_once( trailingslashit( THEME_FUNCTIONS ) . 'widgets.php' );
			
			require_once( trailingslashit( THEME_FUNCTIONS ) . 'hybrid-media-grabber.php' );
			
		}

		/**
		 * includes() includes user's custom-functions.php if it exists,
		 *
		 */
		function includes() {

			if ( file_exists( get_template_directory() . '/custom-functions.php' ) ) {
				include_once( get_template_directory() . '/custom-functions.php' ); // include custom-functions.php if that file exist
			}
		}

	}

}
// end of ThemeSSFRX;