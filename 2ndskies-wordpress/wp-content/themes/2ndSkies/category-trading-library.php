<?php
/*
Template for Trading Library cat
*/
$type = @$_GET['type'];

$tax_query = array(
					array(
						'taxonomy' => 'category',
                        'terms' => 'forex-strategies',
						'field' => 'slug',
					)
				);

switch ($type) {
    case 'price-action-trading':
        $tax_query = array(
					array(
						'taxonomy' => 'category',
						'terms' => 'price-action-trading-library',
						'field' => 'slug',
					)
				);
        break;
    case 'ichimoku-cloud-trading':
        $tax_query = array(
					array(
						'taxonomy' => 'category',
						'terms' => 'ichimoku-cloud-trading',
						'field' => 'slug',
					)
				);
        break;
    case 'trading-mindset-risk-management':
        $tax_query = array(
					array(
						'taxonomy' => 'category',
						'terms' => 'trading-mindset-risk-management',
						'field' => 'slug',
					)
				);
        break;
}

?>
<?php get_header(); ?>
		<!-- main content -->
		<div id="content" class="video-template">
			<!-- breadcrumbs container -->
			<div class="breadcrumbs-container clearfix">
				<div class="breadcrumbs">
					<ul>
						<li><a href="<?php echo home_url(); ?>">Home</a></li>
						<li><?php //the_title() ?>Trading Library</li>
					</ul>
				</div>
			</div>

			<div class="promo-text clearfix">
            <h1><?php single_cat_title('',true); ?></h1>
            
        <div class="grid-container grid-parent nopad">
            <div class="grid-95 tablet-grid-95">
				<?php echo category_description(); ?>
            </div>
            <div class="grid-5 tablet-grid-5">
                <?php
					
                ?>
                &nbsp;
            </div>
        </div>
            
            

			<?php 
            get_template_part('inc', 'share-print'); 
            ?>
			</div><!-- .promo-text -->

			<div class="videos-block">
				<!-- nav -->
				<div class="nav">
					<ul>
						<li class="<?php if(!$type) echo 'active';?>"><a href="<?php echo home_url( 'trading-library' );?>?"><span>All</span></a></li>
						<li class="<?php if($type=='price-action-trading') echo 'active';?>"><a href="<?php echo home_url( 'trading-library' );?>?type=price-action-trading"><span>Price Action Trading</span></a></li>
						<li class="<?php if($type=='ichimoku-cloud-trading') echo 'active';?>"><a href="<?php echo home_url( 'trading-library' );?>?type=ichimoku-cloud-trading"><span>Ichimoku Cloud Trading</span></a></li>
						<li class="<?php if($type=='trading-mindset-risk-management') echo 'active';?>"><a href="<?php echo home_url( 'trading-library' );?>?type=trading-mindset-risk-management"><span>Trading Mindset &amp; Risk Management</span></a></li>
					</ul>
				</div>
				<!-- video list -->
				<?php
                
				//$paged = $_GET['paged_custom'];
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				query_posts(array( 
					'post_type' => 'post',
					'posts_per_page' => 9,
					'tax_query' => $tax_query,
					'order' => 'DESC',
					'paged' => $paged
				));
				?>
				<?php if (have_posts()) : ?>
				<div class="video-list trending-list clearfix">
					<?php while (have_posts()) : the_post(); ?>
					<div class="list-item">
						<div class="video-holder">
							
                            <?php // get_the_ID() instead $post_id ?>	
                            
							<a href="<?php the_permalink();?>" class="trend-post-opener">
								<?php echo get_the_post_thumbnail(get_the_ID(), array( 288,166 ) );?>
                            </a>

						</div>
						<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
					</div>
					<?php endwhile; ?>
                    <p><!--offering price action trading strategies, ichimoku trading strategies.--></p>
				</div>
				<!-- pager -->
				<?php wp_pagenavi();?>
				<?php endif; ?>
			</div>
		</div>
		<!-- main banner -->
		<div class="main-banner">
			<a href="/forex-courses/"><img src="<?php echo THEME_IMAGES ?>/2ndSkies-Courses-Ad-Footer.png" width="960" height="126" alt="2ndskies Forex Courses" /></a>
		</div>
<?php get_footer(); ?>