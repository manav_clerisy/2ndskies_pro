<?php get_header(); ?>
<!-- main -->
<div id="main" class="clearfix">
	<!-- content -->
	<div id="content">
		<div class="breadcrumbs-container clearfix">
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo home_url(); ?>">Home</a></li>
					<li><a href="<?php echo get_post_type_archive_link( 'videos' ); ?>"><?php echo get_post_type() ?></a></li>
					<li><?php the_title() ?></li>
				</ul>
			</div>
		</div>
		<div class="posts">
			<!-- main post -->
			<div class="main-post">
				<?php if (have_posts()) : ?>
				<?php while (have_posts()) : the_post(); ?>
                
                <?php $tags = wp_get_post_tags($post->ID); ?>
				<!-- <span class="featured clearfix">featured</span> -->
				<h1><?php the_title(); ?></h1>
                <?php setPostViews(get_the_ID()); ?>
				<!-- meta info -->
				<?php
					global $post, $wp_query;
                    
                    $args = array(
                        'post_id'       => $post->ID,
                        'status' => 'approve',
                       // 'order'   => 'ASC',                        
                    );
                    $wp_query->comments = get_comments( $args );
				?>
				<div class="meta">
					<ul>
						<li><strong class="date"><?php the_time('F jS, Y') ?></strong></li>
						<li>in  <a href="<?php echo get_post_type_archive_link( 'videos' ); ?>"><?php echo get_post_type() ?></a></li>
						<li>| <a href="#comments"><?php if(count($wp_query->comments) == 1) echo '1 comment'; elseif(count($wp_query->comments) > 1) echo count($wp_query->comments).' comments'; ?></a></li>
					</ul>
				</div>
				<!-- fb-box-holder -->
				<div class="fb-box-holder">
					<div id="fb-root"></div>
					<script>(function(d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) return;
						js = d.createElement(s); js.id = id;
						js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=454996944561399";
						fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));
					</script>
					<div class="fb-like-box" data-href="https://www.facebook.com/2ndSkiesForex" data-colorscheme="light" data-show-faces="false" data-header="true" data-stream="false" data-show-border="true"></div>
                </div>
	
				<?php if (get_post_meta($post->ID, "embed-video", true) != '')   { 
                    ?>
                    <div class="post-video-holder">
                    <!-- post video holder -->
					<?php
						echo $embed = get_post_meta($post->ID, "embed-video", true);
						setPostViews($post->ID);
	
						$commentPostId = $post->ID;
					?>
                    </div>
                    <?php } 
                ?>
                
				<!-- content of the post -->
				<div class="content">
					<?php the_content(); ?>
				</div>
				<!-- share block -->
				<div class="share-block clearfix">
					<!-- AddThis Button BEGIN -->
                <a href="http://api.addthis.com/oexchange/0.8/forward/facebook/offer?pco=tbx32nj-1.0&amp;url=http%3A%2F%2F&amp;pubid=ra-52f4b153538fc7fd" target="_blank" >
                    <img src="http://cache.addthiscdn.com/icons/v1/thumbs/32x32/facebook.png" height="20" width="20" border="0" alt="Facebook" />
                </a>
                <a href="http://api.addthis.com/oexchange/0.8/forward/twitter/offer?pco=tbx32nj-1.0&amp;url=http%3A%2F%2F&amp;pubid=ra-52f4b153538fc7fd" target="_blank" >
                    <img src="http://cache.addthiscdn.com/icons/v1/thumbs/32x32/twitter.png" height="20" width="20" border="0" alt="Twitter" />
                </a>
                <a href="http://www.addthis.com/bookmark.php?source=tbx32nj-1.0&amp;=300&amp;pubid=ra-52f4b153538fc7fd&amp;url=http%3A%2F%2F " target="_blank"  >
                    <img src="http://cache.addthiscdn.com/icons/v1/thumbs/32x32/more.png" height="20" width="20" border="0" alt="More..." /></a>
                <!-- AddThis Button END -->
				</div>
			</div>
			<!-- other posts -->
			<?php
				$tags = wp_get_post_tags($post->ID);
				
				if ($tags) {
				
				$tagsArr = array();
				
				foreach($tags as $tag)
					$tagsArr[] = $tag->term_id;
					
				$args=array(
					'tag__in' => $tagsArr,
					'post__not_in' => array($post->ID),
					'posts_per_page'=>3,
					'caller_get_posts'=>1
				);
				$my_query = new WP_Query($args);
				if( $my_query->have_posts() ) {
					
			?>
			<div class="post-list row-list">
				<div class="main-title">
					<h2>Related Article</h2>
				</div>
				<?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
				<div class="list-item">
					<div class="photo">
						<a href="<?php the_permalink() ?>"><?php the_post_thumbnail(array(286,137)); ?></a>
					</div>
					<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
				</div>
				<?php endwhile; ?>
			</div>
			<?php
				}
			} ?>
			<!-- post comments -->
			<div class="post-comments">
				<h2 class="comments-title">Discussion</h2>
				<!-- comment form -->
				<div class="comment-form-holder">
					<?php
					$args = array(
						//'fields' => apply_filters( 'comment_form_default_fields', $fields )
						'comment_field' => '<div class="row clearfix">
								<div class="photo">'.get_avatar('', 55).'</div>
								<div class="textfield">
								<textarea name="comment" id="comment" cols="30" rows="10" placeholder="Leave a comment...."></textarea></div></div>'
						,'must_log_in' => '<p class="must-log-in">' .  sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>'
						,'logged_in_as' => ''
						,'comment_notes_before' => '<p class="comment-notes">' . __( 'Your email address will not be published.' ) . ( $req ? $required_text : '' ) . '</p>'
						,'comment_notes_after' => ''
						,'id_form' => 'commentform'
						,'id_submit' => 'submit'
						,'title_reply' => __( 'Comments on the article' )
						,'title_reply_to' => __( 'Leave a Reply to %s' )
						,'cancel_reply_link' => __( 'Cancel reply' )
						,'label_submit' => __( 'Post Comment' )
					);
					comment_form( $args );
					?>
				</div>
                <?php
                    echo '<h3>'.count($wp_query->comments).' Comments</h3>';
                    echo '<ul class="comment-list">';
                    wp_list_comments('callback=skies_comment');
                    echo '</ul>';
                ?>
			</div>
			<?php endwhile; ?>
			<?php else : ?>
				<h1>Not Found</h1>
				<p>Sorry, but you are looking for something that isn't here.</p>
			<?php endif; ?>
		</div>
	</div>
	<!-- sidebar -->
    <?php get_sidebar('recent-popular'); ?>  	
</div>
<?php get_footer();


?>