<div class="share-block clearfix">

<?php wp_enqueue_script('sharrre'); ?>

<?php
    function short_url($url) {
        $ch = curl_init();
        $timeout = 4;
        curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
    $short = short_url('http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);

    $totalCount = 0;
    $fieldValue = simple_fields_value("fb_post_id");

    if (!is_null($fieldValue) && $fieldValue != '') {
        $likes = parse("https://graph.facebook.com/v1.0/$fieldValue/likes?limit=0&summary=1");
        $result = json_decode($likes);
        if (isset($result->summary->total_count))
        {
            $totalCount += $result->summary->total_count;
        }

        $commets = parse("https://graph.facebook.com/v1.0/$fieldValue/comments?limit=0&summary=1");
        $result = json_decode($commets);
        if (isset($result->summary->total_count))
        {
            $totalCount += $result->summary->total_count;
        }
    }

    function parse($encUrl){
        $options = array(
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // don't return headers
            CURLOPT_FOLLOWLOCATION => true, // follow redirects
            CURLOPT_ENCODING => "", // handle all encodings
            CURLOPT_AUTOREFERER => true, // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 5, // timeout on connect
            CURLOPT_TIMEOUT => 10, // timeout on response
            CURLOPT_MAXREDIRS => 3, // stop after 10 redirects
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => false,
        );
        $ch = curl_init();

        $options[CURLOPT_URL] = $encUrl;
        curl_setopt_array($ch, $options);

        $content = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);

        curl_close($ch);

        if ($errmsg != '' || $err != '') {
            /*print_r($errmsg);
            print_r($errmsg);*/
        }
        return $content;
    }
?>

    <div class="sharecount-box">
        <div id="sharecount"></div>
        <span>Shares</span>
    </div>

    <?php /*?><div id="sharrre-icons">
        <div id="facebook" data-url="<?php echo $short; ?>"></div>
        <div id="twitter" data-url="<?php echo $short; ?>" ></div>
        <div id="googleplus" data-url="<?php echo $short; ?>" ></div>
    </div><?php */?>
	
	

<?php if(is_single()) { ?>
    <div class="icon-share-wrap">
        <a class="icon-share" href='javascript:window.print();'><div class="dashicons dashicons-pressthis"></div></a>
        <a class="icon-share" id="emailLink" href="mailto:"><div class="dashicons dashicons-email-alt"></div></a>
    </div>

    <script>
        jQuery(document).ready(function ($){
            $('#emailLink').click();
        });
    </script>
<?php } ?>
    <script>

        var SherrreTotals   = <?= $totalCount ?>;
        var SherrreURL      = '<?php echo get_permalink($post->ID); ?>';
        var SherrreURLCurl  = '<?php echo get_site_url(); ?>/wp-content/themes/2ndSkies/library/sharrre/sharrre.php';

        <?php /* better without it
        jQuery(document).ready(function($){

            if($("#sharecount .count").text().trim() == ""){
                $('#sharecount').sharrre({
                    share: {
                        googlePlus: true,
                        facebook: true,
                        twitter: true
                    },
                    total: SherrreTotals,
                    url: SherrreURL,
                    urlCurl: SherrreURLCurl,
                    enableHover: false
                });
            }
        })
        */ ?>
    </script>
</div><!-- share block -->
<?php
    $augmented_nr = get_field('augmented_share_number');
    if (!isset($augmented_nr) || (!$augmented_nr > 0)) {
        $augmented_nr = 0;
    }
?>
<script>
//augmented_nr is send to jquery.sharrre.min.js
    var augmented_nr = '<?php echo $augmented_nr; ?>';
    jQuery(document).ready(function ($){
        $('#sharecount').sharrre({
          share: {
            googlePlus: true,
            facebook: true,
            twitter: true
          },
          url: "<?php echo get_permalink( $post->ID ); ?>",
          urlCurl: 'http://<?php echo $_SERVER['HTTP_HOST']?>/wp-content/themes/2ndSkies/library/sharrre/sharrre.php',
          enableHover: false
        });
    });
</script>